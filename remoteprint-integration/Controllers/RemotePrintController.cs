﻿using System;
using Microsoft.AspNetCore.Mvc;
using sofd_fjernprint_integration.Controllers.Dto;
using sofd_fjernprint_integration.Services;

namespace sofd_fjernprint_integration.Controllers
{
    [Route("api/[controller]")]
    public class RemotePrintController : Controller
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private RemotePrintService _remotePrintService;

        public RemotePrintController(RemotePrintService remotePrintService)
        {
            _remotePrintService = remotePrintService;
        }

        [HttpPost("SendLetterToCpr")]
        public IActionResult SendLetterToCpr([FromBody] SenderLetterToCprRequest request)
        {
            try
            {
                string requestGuid = _remotePrintService.SendLetterToCpr(request.cpr, request.cvr, request.senderId, request.contentTypeId, request.subject, Convert.FromBase64String(request.pdfFileBase64));
                return Json(requestGuid);
            }
            catch (Exception e)
            {
                log.Error("Error occurred while trying to send digital post.", e);
                return BadRequest();
            }
        }
    }
}
