﻿using System;

namespace sofd_fjernprint_integration.Controllers.Dto
{
    public class SenderLetterToCprRequest
    {
        public string cpr { get; set; }
        public string cvr { get; set; }
        public string contentTypeId { get; set; }
        public string senderId { get; set; }
        public string subject { get; set; }
        public string pdfFileBase64 { get; set; }
    }
}
