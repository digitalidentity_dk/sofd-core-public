﻿using Microsoft.Extensions.Configuration;
using PrintService;
using sofd_fjernprint_integration.Certificates;
using sofd_fjernprint_integration.Util;
using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;

namespace sofd_fjernprint_integration.Services
{
    public class RemotePrintService
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IConfiguration _configuration;
        private int counter = 1;

        private PrintPortTypeClient printClient;

        public RemotePrintService(IConfiguration configuration)
        {
            _configuration = configuration;
            ConfigureClient();

            // a good random starting place - as we cannot send the same ID twice (and when restarting on the same day, that will make new ones fail)
            counter = new Random().Next(10000, 50000);
        }
        private void ConfigureClient()
        {
            var binding = new BasicHttpsBinding();
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            var remoteAddress = new EndpointAddress(_configuration["serviceUrl"]);

            printClient = new PrintPortTypeClient(binding, remoteAddress);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                printClient.ClientCredentials.ClientCertificate.SetCertificate(StoreLocation.LocalMachine, StoreName.My, X509FindType.FindByThumbprint, _configuration["certThumbprint"]);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                printClient.ClientCredentials.ClientCertificate.Certificate = CertificateLoader.LoadCertificateAndPrivateKeyFromFile(_configuration["certPath"], SecureStringUtil.ConvertToSecureString(_configuration["certPassword"]));
            }
            // Disable revocation checking
            printClient.ClientCredentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
        }

        public string SendLetterToCpr(string cpr, string cvr, string senderId, string contentTypeId, string subject, byte[] pdfFile)
        {
            var request = new PrintAfsendBrevRequestType();
            request.AuthorityContext = new AuthorityContextType() { MunicipalityCVR = cvr };
            request.BrevSPBody = new BrevSPBodyType();
            request.BrevSPBody.Kanalvalg = KanalvalgType.A; // F=Fysisk print, A=Automatic distribution (snail-mail if recipeint can't receive digital mail)
            request.BrevSPBody.Prioritet = PrioritetType.D; // M=Batch, D=Direct
            request.BrevSPBody.ForsendelseI = new ForsendelseIType();
            string afsendelseIdentifikator = senderId + "SFD" + getSerialNumber();
            request.BrevSPBody.ForsendelseI.AfsendelseIdentifikator = afsendelseIdentifikator;
            request.BrevSPBody.ForsendelseI.ForsendelseModtager = new ForsendelseModtagerType()
            {
                AfsendelseModtager = new SlutbrugerIdentitetType()
                {
                    ItemElementName = ItemChoiceType.CPRnummerIdentifikator,
                    Item = cpr
                }
            };
            request.BrevSPBody.ForsendelseI.FilformatNavn = "pdf";
            request.BrevSPBody.ForsendelseI.MeddelelseIndholdData = pdfFile;
            request.BrevSPBody.ForsendelseI.DokumentParametre = new DokumentParametreType() {
                TitelTekst = subject
            };
            request.BrevSPBody.ForsendelseI.DigitalPostParametre = new DigitalPostParametreType() {
                MeddelelseIndholdstypeIdentifikator = contentTypeId
            };

            afsendBrevResponse result = null;
            try
            {
                result = printClient.afsendBrevAsync(request).Result;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to send letter. Cpr: {0}. Municipality: {1}. Guid: {2} ", pseudonomiseCpr(cpr), cvr, afsendelseIdentifikator), ex);
            }

            if (result.PrintAfsendBrevResponse.resultat == false)
            {
                throw new Exception(String.Format("Failed to send letter. Cpr: {0}. Municipality: {1}. Guid: {2} ", pseudonomiseCpr(cpr), cvr, afsendelseIdentifikator));
            }

            log.Info(String.Format("Sent letter. Cpr: {0}. Municipality: {1}. Guid: {2} ", pseudonomiseCpr(cpr), cvr, afsendelseIdentifikator));

            return afsendelseIdentifikator;
        }

        private string pseudonomiseCpr(string cpr)
        {
            return cpr.Substring(0, 6) + "-xxxx";
        }

        // must return 21 characters
        private string getSerialNumber()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(DateTime.Now.ToString("MMddy"));
            string counterStr = counter.ToString();
            counter++;

            for (int i = 0; i < (15 - counterStr.Length); i++)
            {
                builder.Append("0");
            }
            builder.Append(counterStr);

            return builder.ToString();
        }
    }
}
