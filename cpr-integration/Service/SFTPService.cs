using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Renci.SshNet;

namespace SofdCprIntegration
{

    public class SFTPService
    {
        private readonly IConfiguration _configuration;
        private const string HostName = "sftp.serviceplatformen.dk";
        private const string remoteDirectory = "IN";

        public SFTPService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public FileDTO GetNewestFile(DateTime previousDate)
        {
            var pk = new PrivateKeyFile(_configuration["SFTPService:keyfile"], _configuration["SFTPService:keyPass"]);
            var keyFiles = new[] { pk };

            var methods = new List<AuthenticationMethod>();
            methods.Add(new PrivateKeyAuthenticationMethod(_configuration["SFTPService:username"], keyFiles));

            var connectionInfo = new ConnectionInfo(HostName, 22, _configuration["SFTPService:username"], methods.ToArray());

            using (var client = new SftpClient(connectionInfo))
            {
                client.Connect();

                var files = client.ListDirectory(remoteDirectory);

                var newestFile = files.Where(f => ValidFile(f, previousDate)).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
                if (newestFile != null)
                {
                    var result = new FileDTO();
                    result.Content = client.ReadAllText(newestFile.FullName);
                    result.LastWriteTime = newestFile.LastWriteTime;
                    return result;
                }
            }
            return null;
        }

        private bool ValidFile(Renci.SshNet.Sftp.SftpFile f, DateTime previousDate)
        {
            return (
                !f.Name.Equals(".")
                && !f.Name.Equals("..")
                && !f.Name.EndsWith(".metadata")
                && f.LastWriteTime.CompareTo(previousDate) > 0
            );
        }
    }
}