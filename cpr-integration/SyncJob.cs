using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using SofdCprIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

[DisallowConcurrentExecution]
public class SyncJob : IJob
{
    // Inject the DI provider
    private readonly IServiceProvider _provider;
    private readonly ILogger<SyncJob> _logger;
    private readonly IConfiguration _configuration;
    private PersonContext personContext;

    public SyncJob(IConfiguration configuration, ILogger<SyncJob> logger, IServiceProvider provider)
    {
        _configuration = configuration;
        _logger = logger;
        _provider = provider;
    }

    public Task Execute(IJobExecutionContext context)
    {
        // Create a new scope
        using (var scope = _provider.CreateScope())
        {
            personContext = scope.ServiceProvider.GetService<PersonContext>();
            _logger.LogInformation("Running Sync Job");

            SFTPService sftp = new SFTPService(_configuration);
            DateTime previousDate = default;

            LastSync lastSync = personContext.LastSync.OrderBy(x => x.Id).FirstOrDefaultAsync().Result;
            if (lastSync != null)
            {
                previousDate = lastSync.LastSyncDate;
            }
            else
            {
                lastSync = new LastSync();
                previousDate = DateTime.MinValue;
            }

            var fileData = sftp.GetNewestFile(previousDate);
            if (fileData != null)
            {
                // parse the file
                List<string> updatedPersons = ExtractCPR(fileData.Content);

                // update timestamp after reading from sftp
                lastSync.LastSyncDate = fileData.LastWriteTime;
                personContext.Update(lastSync);

                // remove updated persons from cache-db
                personContext.Person.RemoveRange(personContext.Person.Where(p => updatedPersons.Contains(p.Cpr)));

                personContext.SaveChanges();
            }
        }

        return Task.CompletedTask;
    }

    private List<string> ExtractCPR(string content)
    {
        return content.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None) //split string into lines
                        .Where(l => !l.StartsWith("0000") && !l.StartsWith("9999") && !string.IsNullOrWhiteSpace(l)) //skip empty lines and start & end line
                        .Select(l => l.Substring(3, 10)) // cut only the cpr
                        .Distinct() // remove duplicates
                        .ToList(); // return as list
    }
}