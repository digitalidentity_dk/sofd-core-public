package dk.sofd.organization.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Municipality {
	private String name;
	private String sofdApiKey;
	private String sofdUrl;
	private String rcApiKey;
	private String rcUrl;
	private boolean titlesEnabled;
}
