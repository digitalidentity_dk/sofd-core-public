package dk.sofd.organization.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "integration.settings")
public class MunicipalityConfiguration {
	private List<Municipality> municipalities;
}
