package dk.sofd.organization.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import dk.sofd.organization.config.Municipality;
import dk.sofd.organization.config.MunicipalityConfiguration;
import dk.sofd.organization.rc.RoleCatalogueService;

@EnableScheduling
@Component
public class SyncTask {

	@Autowired
	private RoleCatalogueService roleCatalogueService;
	
	@Autowired
	private MunicipalityConfiguration configuration;

	@Scheduled(cron = "0 0/15 7-21 * * *")
	public void run() {
		for (Municipality municipality : configuration.getMunicipalities()) {
			roleCatalogueService.performSync(municipality);
		}
	}
}
