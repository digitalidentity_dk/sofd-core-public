INSERT INTO it_systems (name, identifier, system_type) values ('SOFD Core', 'sofdcore', 'SAML');
SELECT id INTO @systemId FROM it_systems WHERE identifier = 'sofdcore';

INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('Administrator', 'admin', 'Fuld adgang, inkl administration', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('Skriveadgang', 'edit', 'Adgang til at oprette data', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('Læseadgang', 'read', 'Læseadgang til data', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('Kontaktinfo redaktør', 'edit_contact_info', 'Læseadgang, samt skriveadgang til åbningstider og søgeord på enheder', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('LOS Administrator', 'losadmin', 'Redigering af enheder i SOFD Core', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('Telefoni Administrator', 'telephonyadmin', 'Redigering af telefoni oplysninger i SOFD Core', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('SMS Anvender', 'sms', 'Adgang til SMS modulet i SOFD Core', @systemId, 'BOTH');
INSERT INTO system_roles (name, identifier, description, it_system_id, role_type) VALUES ('CPR Adgang', 'cpr_access', 'Kan se fulde CPR numre i SOFD Core brugergrænsefladen', @systemId, 'BOTH');


SELECT id INTO @srAdmin FROM system_roles WHERE identifier = 'admin' AND it_system_id = @systemId;
SELECT id INTO @srEdit FROM system_roles WHERE identifier = 'edit' AND it_system_id = @systemId;
SELECT id INTO @srRead FROM system_roles WHERE identifier = 'read' AND it_system_id = @systemId;
SELECT id INTO @srEditContactInfo FROM system_roles WHERE identifier = 'edit_contact_info' AND it_system_id = @systemId;
SELECT id INTO @srLosAdmin FROM system_roles WHERE identifier = 'losadmin' AND it_system_id = @systemId;
SELECT id INTO @srTelephonyAdmin FROM system_roles WHERE identifier = 'telephonyadmin' AND it_system_id = @systemId;
SELECT id INTO @srSms FROM system_roles WHERE identifier = 'sms' AND it_system_id = @systemId;
SELECT id INTO @srCpr FROM system_roles WHERE identifier = 'cpr_access' AND it_system_id = @systemId;


INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('Administrator', 'sofd-core-admin', 'Fuld adgang, inkl administration', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('Skriveadgang', 'sofd-core-edit', 'Adgang til at oprette data', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('Læseadgang', 'sofd-core-read', 'Læseadgang til data', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('Kontaktinfo redaktør', 'sofd-core-edit-contact-info', 'Læseadgang, samt skriveadgang til åbningstider og søgeord på enheder', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('LOS Administrator', 'sofd-core-losadmin', 'Redigering af enheder i SOFD Core', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('Telefoni Administrator', 'sofd-core-telephonyadmin', 'Redigering af telefoni oplysninger i SOFD Core', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('SMS Anvender', 'sofd-core-sms', 'Adgang til SMS modulet i SOFD Core', @systemId);
INSERT INTO user_roles (name, identifier, description, it_system_id) VALUES ('CPR Adgang', 'sofd-core-cpr-access', 'Kan se fulde CPR numre i SOFD Core brugergrænsefladen', @systemId);


SELECT id INTO @urAdmin FROM user_roles WHERE identifier = 'sofd-core-admin' AND it_system_id = @systemId;
SELECT id INTO @urEdit FROM user_roles WHERE identifier = 'sofd-core-edit' AND it_system_id = @systemId;
SELECT id INTO @urRead FROM user_roles WHERE identifier = 'sofd-core-read' AND it_system_id = @systemId;
SELECT id INTO @urEditContactInfo FROM user_roles WHERE identifier = 'sofd-core-edit-contact-info' AND it_system_id = @systemId;
SELECT id INTO @urLosAdmin FROM user_roles WHERE identifier = 'sofd-core-losadmin' AND it_system_id = @systemId;
SELECT id INTO @urTelephonyAdmin FROM user_roles WHERE identifier = 'sofd-core-telephonyadmin' AND it_system_id = @systemId;
SELECT id INTO @urSms FROM user_roles WHERE identifier = 'sofd-core-sms' AND it_system_id = @systemId;
SELECT id INTO @urCpr FROM user_roles WHERE identifier = 'sofd-core-cpr-access' AND it_system_id = @systemId;


INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urAdmin, @srAdmin);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urEdit, @srEdit);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urRead, @srRead);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urEditContactInfo, @srEditContactInfo);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urLosAdmin, @srLosAdmin);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urTelephonyAdmin, @srTelephonyAdmin);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urSms, @srSms);
INSERT INTO system_role_assignments (user_role_id, system_role_id) VALUES (@urCpr, @srCpr);
