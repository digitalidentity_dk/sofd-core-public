package dk.digitalidentity.sofd.dao;

import java.util.List;

import dk.digitalidentity.sofd.dao.model.Organisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.rest.core.annotation.RestResource;

import com.querydsl.core.types.dsl.StringPath;

import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.QOrgUnit;
import dk.digitalidentity.sofd.security.RequireDaoWriteAccess;

public interface OrgUnitDao extends JpaRepository<OrgUnit, String>, QueryDslPredicateExecutor<OrgUnit>, QuerydslBinderCustomizer<QOrgUnit> {

	@RestResource(exported = false)
	void delete(Iterable<? extends OrgUnit> entities);

	@RestResource(exported = false)
	void delete(OrgUnit entity);

	@RestResource(exported = false)
	void delete(String id);

	@RestResource(exported = false)
	void deleteAll();
	
	@RestResource(exported = false)
	List<OrgUnit> findByDeletedFalseAndBelongsTo(Organisation organisation);

	@RestResource(exported = false)
	List<OrgUnit> findByBelongsTo(Organisation organisation);

	@RestResource(exported = false)
	OrgUnit findByMasterId(String masterId);

	@Query(nativeQuery = true, value =
			"SELECT o.* FROM orgunits o " +
			" INNER JOIN affiliations a ON a.orgunit_uuid = o.uuid " + 
			" WHERE o.deleted = 0 AND belongs_to = ?1 " +
			" GROUP BY o.uuid")
	@RestResource(exported = false)
	List<OrgUnit> findAllActiveWithAffiliations(long admOrgId);

	@RestResource(exported = false)
	List<OrgUnit> findByUuidIn(List<String> uuids);
	
	@RestResource(exported = false)
	OrgUnit findByUuid(String uuid);
	
	@RestResource(exported = false)
	List<OrgUnit> findByName(String name);
	
	@RequireDaoWriteAccess
	<S extends OrgUnit> List<S> save(Iterable<S> entities);

	@RequireDaoWriteAccess
	<S extends OrgUnit> S save(S entity);

	@RequireDaoWriteAccess
	<S extends OrgUnit> S saveAndFlush(S entity);

	@Override
	default public void customize(QuerydslBindings bindings, QOrgUnit orgunit) {

		// enable case-insensitive searching
		bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));

		// prefix search on specific fields
		bindings.bind(orgunit.name).first((path, value) -> path.startsWith(value));
		bindings.bind(orgunit.postAddresses.any().street).first((path, value) -> path.startsWith(value));
		bindings.bind(orgunit.localExtensions).first((path, value) -> path.contains(value));
	}
}
