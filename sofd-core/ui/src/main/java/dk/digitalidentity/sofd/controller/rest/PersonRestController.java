package dk.digitalidentity.sofd.controller.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.controller.mvc.datatables.dao.GridPersonDatatableDao;
import dk.digitalidentity.sofd.controller.mvc.datatables.dao.GridPersonDeletedDatatableDao;
import dk.digitalidentity.sofd.controller.mvc.datatables.dao.model.GridPerson;
import dk.digitalidentity.sofd.controller.mvc.dto.CprLookupDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.PrimeUserDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.ProfileDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.SimpleAffiliationDTO;
import dk.digitalidentity.sofd.controller.rest.model.ContactDTO;
import dk.digitalidentity.sofd.controller.rest.model.LeaveDTO;
import dk.digitalidentity.sofd.controller.rest.model.PhoneDTO;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.FunctionType;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.PersonLeave;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderType;
import dk.digitalidentity.sofd.dao.model.enums.EndDate;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.security.RequireReadAccess;
import dk.digitalidentity.sofd.security.RequireWriteContactInfoAccess;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.CprService;
import dk.digitalidentity.sofd.service.FunctionTypeService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import dk.digitalidentity.sofd.service.UserService;
import lombok.extern.log4j.Log4j;

@Log4j
@RequireReadAccess
@RestController
public class PersonRestController {

	@Autowired
	private PersonService personService;

	@Autowired
	private GridPersonDatatableDao personDatatableDao;

	@Autowired
	private GridPersonDeletedDatatableDao personDeletedDao;

	@Autowired
	private CprService cprService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountOrderService accountOrderService;

	@Autowired
	private FunctionTypeService functionTypeService;
		
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/{uuid}/leave")
	@ResponseBody
	public ResponseEntity<String> leave(@PathVariable("uuid") String uuid, @RequestBody LeaveDTO leaveDTO) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Date startDate = leaveDTO.getLeaveStartDate();
		if (startDate == null) {
			startDate = new Date();
		}

		Date stopDate = leaveDTO.getLeaveStopDate();
		if (stopDate != null && stopDate.before(startDate)) {
			stopDate = startDate;
		}

		// ensure only valid data in accounts to deactivate
		leaveDTO.setAccountsToDeactivate(personService.sanatizeAccountsToDeactivate(leaveDTO.getAccountsToDeactivate()));

		List<AccountOrder> newOrders = new ArrayList<AccountOrder>();

		if (leaveDTO.isLeave()) {
			if (person.getLeave() == null) {
				person.setLeave(new PersonLeave());

				// can only be modified on creation of leave
				person.getLeave().setDeactivateAccounts(leaveDTO.isDeactivateAccounts());
				person.getLeave().setDisableAccountOrders(leaveDTO.isDisableAccountOrders());
				person.getLeave().setAccountsToDeactivate(leaveDTO.getAccountsToDeactivate());
				person.getLeave().setStartDate(startDate);

				if (leaveDTO.isDeactivateAccounts()) {
					// this is left commented out on purpose - this is NOT the same as setting the force-stop flag,
					// as this is just for a single-use deactivation of a SET of user accounts (not always all of them)
					// person.setForceStop(true);

					String[] accountsToDeactivate = leaveDTO.getAccountsToDeactivate().split(",");
					
					List<AccountOrder> orders = personService.generateDeactivateOrders(person, accountsToDeactivate, startDate);
					newOrders.addAll(orders);

					// the UI enforces this, but lets make sure
					leaveDTO.setDisableAccountOrders(true);
				}
				
				// the leaveForm can order the setting of this flag (it can be removed from the usual dialogue though
				// so this is just an easy way to do two things in one go)
				if (leaveDTO.isDisableAccountOrders()) {
					person.setDisableAccountOrders(true);
				}
			}
			
			// these are modifiable on existing leave data
			person.getLeave().setStopDate(stopDate);
			person.getLeave().setReason(leaveDTO.getReason());
			person.getLeave().setReasonText(leaveDTO.getReasonText());
		}
		else {
			List<AccountOrder> orders = personService.removeLeave(person);
			newOrders.addAll(orders);
		}

		// we have to loop (there won't be many) as the saveAll method has some nasty side-effects
		for (AccountOrder order : newOrders) {
			accountOrderService.save(order);
		}
		
		personService.save(person);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/{uuid}/forceStop")
	@ResponseBody
	public ResponseEntity<String> flipForceStopFlag(@PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		person.setForceStop(!person.isForceStop());

		// follows forceStop setting
		person.setDisableAccountOrders(person.isForceStop());
		
		String[] userTypes = supportedUserTypeService.findAll().stream()
				.filter(u -> u.isCanOrder())
				.map(u -> u.getKey())
				.toArray(String[]::new);

		List<AccountOrder> orders;		
		if (person.isForceStop()) {
			orders = personService.generateDeactivateOrders(person, userTypes, new Date());
		}
		else {
			orders = personService.generateReactivateOrders(person, userTypes);
		}

		for (AccountOrder order : orders) {
			accountOrderService.save(order);
		}

		personService.save(person);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping("/rest/person/{uuid}/disableAccountOrders")
	@ResponseBody
	public ResponseEntity<String> flipDisableAccountOrders(@PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		person.setDisableAccountOrders(!person.isDisableAccountOrders());
		personService.save(person);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/updatePrimaryAffiliations")
	@ResponseBody
	public ResponseEntity<String> updatePrimaryAffiliations(@RequestHeader("uuid") String uuid, @RequestBody List<SimpleAffiliationDTO> affiliationDTOs) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		// filter out non-prime, and ensure 1 affiliation is selected as prime
		affiliationDTOs = affiliationDTOs.stream().filter(a -> a.isPrime()).collect(Collectors.toList());
		if (affiliationDTOs.size() != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		SimpleAffiliationDTO affiliationDTO = affiliationDTOs.get(0);

		if (person.getAffiliations() != null) {
			for (Affiliation affiliation : person.getAffiliations()) {
				if (affiliation.getId() == affiliationDTO.getId()) {
					affiliation.setSelectedPrime(true);					
				}
				else {
					affiliation.setSelectedPrime(false);
				}	
			}
			
			personService.save(person);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping("/rest/person/sofdUpdatePrimaryAffiliations")
	@ResponseBody
	public ResponseEntity<String> sofdUpdatePrimaryAffiliations(@RequestHeader("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		if (person.getAffiliations() != null) {
			for (Affiliation affiliation : person.getAffiliations()) {
				affiliation.setSelectedPrime(false);
			}

			// calling save() will ensure a prime is selected according to SOFD internal logic
			personService.save(person);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/deleteUser/{userType}/{userId:.+}")
	@ResponseBody
	public ResponseEntity<String> deleteUser(@PathVariable("userType") String userType, @PathVariable("userId") String userId) {
		if (supportedUserTypeService.findByKey(userType) == null) {
			log.warn("Unknown usertype: " + userType);
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		User user = userService.findByUserIdAndUserType(userId, userType);
		if (user == null) {
			log.warn("Could not find user: " + userId + " / " + userType);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (user.isDisabled() == true) {
			log.warn("User not active: " + userId + " / " + userType);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Person person = personService.findByUser(user);
		if (person == null) {
			log.warn("User account did not have any Person associated: " + userId + " / " + userType);

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		deactivateAndDeleteAccount(person, user);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping("/rest/person/reactivateUser/{userType}/{userId:.+}")
	@ResponseBody
	public ResponseEntity<String> reactivateUser(@PathVariable("userType") String userType, @PathVariable("userId") String userId) {
		if (supportedUserTypeService.findByKey(userType) == null) {
			log.warn("Unknown usertype: " + userType);
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		User user = userService.findByUserIdAndUserType(userId, userType);
		if (user == null) {
			log.warn("Could not find user: " + userId + " / " + userType);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (user.isDisabled() == false) {
			log.warn("User not deaktivated: " + userId + " / " + userType);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Person person = personService.findByUser(user);
		if (person == null) {
			log.warn("User account did not have any Person associated: " + userId + " / " + userType);

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		reactivateAndDeleteAccount(person, user);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void deactivateAndDeleteAccount(Person person, User user) {
		AccountOrder order = accountOrderService.deactivateOrDeleteAccountOrder(AccountOrderType.DEACTIVATE, person, user.getEmployeeId(), user.getUserType(), user.getUserId());
		accountOrderService.save(order);

		SupportedUserType supportedUserType = supportedUserTypeService.findByKey(user.getUserType());
		if (supportedUserType.getDaysToDelete() > 0) {
			Date date = LocalDate.now().plusDays((int) supportedUserType.getDaysToDelete()).toDate();

			order = accountOrderService.deactivateOrDeleteAccountOrder(AccountOrderType.DELETE, person, user.getEmployeeId(), user.getUserType(), user.getUserId(), date);
			accountOrderService.save(order);
		}
	}
	
	private void reactivateAndDeleteAccount(Person person, User user) {
		AccountOrder order = accountOrderService.createAccountOrder(person, supportedUserTypeService.findByKey(user.getUserType()), user.getUserId(), user.getEmployeeId(), null, EndDate.NO);
		accountOrderService.save(order);
	}

	@RequireControllerWriteAccess
	@GetMapping(value = "/rest/person/getByCPR", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CprLookupDTO> getByCPR(@RequestHeader("cpr") String cpr) {
		Person person = personService.findByCpr(cpr);
		if (person != null) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		CprLookupDTO cprLookupDTO = cprService.getByCpr(cpr);

		if (cprLookupDTO != null) {
			return new ResponseEntity<>(cprLookupDTO, HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/updateFromCPR")
	@ResponseBody
	public ResponseEntity<HttpStatus> updateFromCPR(@RequestHeader("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		CprLookupDTO cprLookupDTO = cprService.getByCpr(person.getCpr());

		if (cprLookupDTO != null) {
			person.setFirstname(cprLookupDTO.getFirstname());
			person.setSurname(cprLookupDTO.getLastname());
			person.setRegisteredPostAddress(Post.builder()
					.street(cprLookupDTO.getStreet())
					.postalCode(cprLookupDTO.getPostalCode())
					.city(cprLookupDTO.getCity())
					.localname(cprLookupDTO.getLocalname())
					.country(cprLookupDTO.getCountry())
					.addressProtected(cprLookupDTO.isAddressProtected())
					.master("SOFD")
					.masterId("SOFD")
					.build());

			personService.save(person);

			return new ResponseEntity<>(HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/taxedPhone/{uuid}")
	@ResponseBody
	public ResponseEntity<HttpStatus> updatePhoneTax(@PathVariable("uuid") String uuid, @RequestHeader("checked") boolean checked) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		person.setTaxedPhone(checked);
		personService.save(person);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping("/rest/person/profile/update")
	public HttpEntity<?> updateProfile(@RequestBody @Valid ProfileDTO profileDTO, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
		}

		String uuid = profileDTO.getPersonUuid();
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("Could not find person with uuid " + uuid + " while updating profile.");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (!Objects.equals(person.getChosenName(), profileDTO.getChosenName())) {
			person.setChosenName(profileDTO.getChosenName());
			person = personService.save(person);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/editPhone")
	@ResponseBody
	public ResponseEntity<HttpStatus> editPhone(@RequestHeader("uuid") String uuid, @RequestBody PhoneDTO phoneDTO) {
		if (StringUtils.isEmpty(phoneDTO.getPhoneNumber())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		FunctionType functionType = functionTypeService.findById(phoneDTO.getFunctionType());

		if (phoneDTO.getId() == 0) {
			Phone phone = new Phone();
			phone.setMaster("SOFD");
			phone.setMasterId(UUID.randomUUID().toString());
			phone.setPhoneNumber(phoneDTO.getPhoneNumber());
			phone.setPhoneType(phoneDTO.getPhoneType());
			phone.setNotes(phoneDTO.getNotes());
			phone.setVisibility(phoneDTO.getVisibility());
			phone.setPrime(false);
			phone.setTypePrime(false);
			phone.setFunctionType(functionType);

			person.getPhones().add(phone);
		}
		else {
			List<Phone> phones = person.getPhones();
			Optional<Phone> existingPhone = phones.stream().filter(p -> p.getId() == phoneDTO.getId()).findFirst();
			if (existingPhone.isPresent()) {
				Phone modifiedPhone = existingPhone.get();

				modifiedPhone.setPhoneNumber(phoneDTO.getPhoneNumber());
				modifiedPhone.setPhoneType(phoneDTO.getPhoneType());
				modifiedPhone.setNotes(phoneDTO.getNotes());
				modifiedPhone.setVisibility(phoneDTO.getVisibility());
				modifiedPhone.setFunctionType(functionType);
			}
			else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		personService.save(person);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/deletePhone")
	@ResponseBody
	public ResponseEntity<HttpStatus> deletePhone(@RequestHeader("uuid") String uuid, @RequestHeader("id") long id) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean removedAny = person.getPhones().removeIf(phone -> phone.getId() == id);

		personService.save(person);

		return removedAny ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	// read access is fine here, used by datatables
	@SuppressWarnings("unchecked")
	@PostMapping("/rest/person/list")
	public DataTablesOutput<GridPerson> list(@Valid @RequestBody DataTablesInput input, BindingResult bindingResult, @RequestHeader("show-inactive") boolean showInactive) {
		if (bindingResult.hasErrors()) {
			DataTablesOutput<GridPerson> error = new DataTablesOutput<>();
			error.setError(bindingResult.toString());

			return error;
		}

		DataTablesOutput<?> result = new DataTablesOutput<>();
		if (showInactive) {
			result = personDeletedDao.findAll(input);
		}
		else {
			result = personDatatableDao.findAll(input);
		}

		DataTablesOutput<GridPerson> output = new DataTablesOutput<>();
		output.setDraw(result.getDraw());
		output.setRecordsFiltered(result.getRecordsFiltered());
		output.setRecordsTotal(result.getRecordsTotal());
		output.setError(result.getError());
		output.setData((List<GridPerson>) result.getData());

		return output;
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/updatePrimaryUserAccounts")
	@ResponseBody
	public ResponseEntity<String> updatePrimaryUserAccounts(@RequestHeader("uuid") String uuid, @RequestBody List<PrimeUserDTO> primeUserDTOs ) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Map<String, List<PrimeUserDTO>> users = primeUserDTOs.stream()
				.collect(Collectors.groupingBy(PrimeUserDTO::getUserType));

		// Check if data is correct
		for (String type : users.keySet()) {
			if (users.get(type).stream().filter(u -> u.isPrime()).count() != 1) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		for (User user : person.getUsers()) {
			Optional<PrimeUserDTO> userDTO = primeUserDTOs.stream().filter(dto -> dto.getUuid().equals(user.getUuid())).findAny();

			if (userDTO.isPresent()) {
				user.setPrime(userDTO.get().isPrime());
			}
			else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		personService.save(person);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/person/updatePrimaryPhones")
	@ResponseBody
	public ResponseEntity<String> updatePrimaryPhones(@RequestHeader("uuid") String uuid, @RequestBody List<PhoneDTO> phoneDTOs ) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		for (PhoneDTO phoneDTO : phoneDTOs) {
			Optional<Phone> first = person.getPhones().stream().filter(p -> p.getId() == phoneDTO.getId()).findFirst();
			if (first.isPresent()) {
				Phone phone = first.get();

				phone.setPrime(phoneDTO.isPrime());
				phone.setTypePrime(phoneDTO.isTypePrime());
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		personService.save(person);
		return new ResponseEntity<>(HttpStatus.OK);
	}


	@RequireWriteContactInfoAccess
	@PostMapping("/rest/person/update/contacts")
	@ResponseBody
	public HttpEntity<ContactDTO> updateContactInfo(@RequestHeader("uuid") String uuid, @RequestBody ContactDTO contactInfo) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		person.setKeyWords(contactInfo.getKeywords());
		person.setNotes(contactInfo.getNotes());
		personService.save(person);

		return new ResponseEntity<>(contactInfo, HttpStatus.OK);
	}
}
