package dk.digitalidentity.sofd.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.data.annotation.ReadOnlyProperty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.enums.AffiliationFunction;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationType;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Audited
@Entity(name = "affiliations")
@Getter
@Setter
@EqualsAndHashCode(exclude = "person", callSuper = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Affiliation extends MasteredEntity implements Serializable {
	private static final long serialVersionUID = -1443788795789576943L;

	@Id
	@GeneratedValue
	private long id;
	
	@Column
	@NotNull
	private String uuid;
	
	@Column
	@NotNull
	private String master;
	
	@Column
	@NotNull
	private String masterId;

	@Column
	private Date startDate;

	@Column
	private Date stopDate;

	// TODO: this should be removed from the datamodel at some point (need to update agents to support this change)
	@Column
	private boolean deleted;

	// used for internal rules with regards to account creation
	@JsonIgnore
	@Column
	private boolean inheritPrivileges = true; // defaults to true when constructed
	
	// maintained by batch job and write intercepter
	@Column
	@ReadOnlyProperty
	private boolean prime;

	// only maintained by gui - used to override automatic prime logic
	@Column
	@JsonIgnore
	@ReadOnlyProperty
	private boolean selectedPrime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orgunit_uuid")
	@NotNull
	private OrgUnit orgUnit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	private Person person;

	@Column
	@Size(max = 255)
	private String employeeId;

	@Column
	@Size(max = 255)
	private String employmentTerms;

	@Column
	@Size(max = 255)
	private String employmentTermsText;

	@Column
	@Size(max = 255)
	private String payGrade;

	@Column
	private Double workingHoursDenominator;

	@Column
	private Double workingHoursNumerator;

	@Column
	@NotNull
	@Enumerated(EnumType.STRING)
	private AffiliationType affiliationType;

	@Column
	@Size(max = 255)
	private String positionId;

	@Column
	@NotNull
	@Size(max = 255)
	private String positionName;

	@Column
	@Size(max = 255)
	private String positionTypeId;

	@Column
	@Size(max = 255)
	private String positionTypeName;

	@ElementCollection
	@CollectionTable(name = "affiliations_kle_primary", joinColumns = @JoinColumn(name = "affiliation_id"))
	@Column(name = "kle_value")
	private List<String> klePrimary;

	@ElementCollection
	@CollectionTable(name = "affiliations_kle_secondary", joinColumns = @JoinColumn(name = "affiliation_id"))
	@Column(name = "kle_value")
	private List<String> kleSecondary;

	@Column
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	private String localExtensions;

	@ElementCollection(targetClass = AffiliationFunction.class)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "affiliations_function", joinColumns = @JoinColumn(name = "affiliation_id"))
	@Column(name = "function")
	private List<AffiliationFunction> functions;

	// this needs to be a Set, otherwise Hibernate will throw a hissy fit, because it cannot make multiple
	// joins on Lists, and in PersonService.findAllManagers(), we make a double join through a predicate
	@OneToMany
	@JoinTable(name = "affiliations_manager", joinColumns = @JoinColumn(name = "affiliation_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "orgunit_uuid"))
	private Set<OrgUnit> managerFor;

}