package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressSync {
    private boolean enabled;
    private boolean runOnStartup;
}
