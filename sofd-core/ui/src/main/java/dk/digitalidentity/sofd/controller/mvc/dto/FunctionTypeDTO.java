package dk.digitalidentity.sofd.controller.mvc.dto;

import java.util.List;

import dk.digitalidentity.sofd.dao.model.enums.PhoneType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FunctionTypeDTO {
    private long id;
    private String name;
    private List<PhoneType> phoneTypes;
}
