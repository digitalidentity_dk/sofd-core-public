package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.ClientConfig;

@RepositoryRestResource(exported = false)
public interface ClientConfigDao extends CrudRepository<ClientConfig, Long> {
	List<ClientConfig> findAll();
	ClientConfig findByClientId(long clientId);
}