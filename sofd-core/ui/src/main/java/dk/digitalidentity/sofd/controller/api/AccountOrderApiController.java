package dk.digitalidentity.sofd.controller.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.controller.api.dto.AccountOrderDTO;
import dk.digitalidentity.sofd.controller.api.dto.AccountOrderResponseDTO;
import dk.digitalidentity.sofd.controller.api.dto.SetOrderStatusDTO;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderStatus;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderType;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;
import dk.digitalidentity.sofd.security.RequireDaoWriteAccess;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.NotificationService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import lombok.extern.log4j.Log4j;

@Log4j
@RestController
@RequireDaoWriteAccess
public class AccountOrderApiController {

	@Autowired
	private AccountOrderService accountOrderService;

	@Autowired
	private PersonService personService;
		
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;
	
	/**
	 * Returns all pending orders (CREATE, DEACTIVATE and DELETE) of a specific UserType,
	 * filtered by ActivationTimestamp, so only those that are in need of being processed
	 * now will be returned
	 */
	@GetMapping("/api/account/{type}/pending")
	public ResponseEntity<AccountOrderResponseDTO> getPendingOrders(@PathVariable("type") String userType, @RequestParam("type") AccountOrderType type) {
		SupportedUserType supportedUserType = supportedUserTypeService.findByKey(userType);
		if (supportedUserType == null) {
			log.error("Unknown userType on getPendingOrders: " + userType);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		AccountOrderResponseDTO responseDTO = new AccountOrderResponseDTO();
		responseDTO.setSingleAccount(supportedUserType.isSingleUserMode());
		long userIdLength = 0;

		List<AccountOrder> pendingOrders = accountOrderService.getPendingOrders(userType, type);
		
		pendingOrders = accountOrderService.identifyAndDeleteDuplicates(pendingOrders);
		
		responseDTO.setPendingOrders(new ArrayList<AccountOrderDTO>());
		for (AccountOrder order : pendingOrders) {
			Person person = personService.getByUuid(order.getPersonUuid());
			if (person != null) {
				AccountOrderDTO dto = new AccountOrderDTO(order, person, userIdLength);

				responseDTO.getPendingOrders().add(dto);
			}
		}
		
		return new ResponseEntity<AccountOrderResponseDTO>(responseDTO, HttpStatus.OK);
	}

	/**
	 * update status on orders processed by external agents
	 */
	@PostMapping("/api/account/{type}/setStatus")
	public ResponseEntity<String> setOrderStatus(@PathVariable("type") String userType, @RequestBody List<SetOrderStatusDTO> request) {
		
		for (SetOrderStatusDTO dto : request) {
			AccountOrder accountOrder = accountOrderService.findById(dto.getId());
			if (accountOrder == null) {
				log.error("Unknown AccountOrder: " + dto.getId());
				continue;
			}
			
			if (!accountOrder.getUserType().equals(userType)) {
				log.error("UserType mismatch: " + accountOrder.getUserType() + " != " + userType + " for: " + accountOrder.getId());
				continue;
			}
			
			if (!accountOrder.getStatus().equals(AccountOrderStatus.PENDING)) {
				log.error("AccountOrder status mismatch, expected PENDING, was " + accountOrder.getStatus() + " for: " + accountOrder.getId());
				continue;
			}
			
			accountOrder.setStatus(dto.getStatus());
			accountOrder.setMessage(dto.getMessage());

			if (dto.getStatus().equals(AccountOrderStatus.FAILED)) {
				Person person = personService.getByUuid(accountOrder.getPersonUuid());
				if (person != null) {
					String prettyName = supportedUserTypeService.getPrettyName(accountOrder.getUserType());
					
					Notification notification = new Notification();
					notification.setActive(true);
					notification.setAffectedEntityName(PersonService.getName(person));
					notification.setAffectedEntityType(EntityType.PERSON);
					notification.setAffectedEntityUuid(accountOrder.getPersonUuid());
					notification.setMessage(prettyName + ": " + dto.getMessage());
					notification.setNotificationType(NotificationType.ACCOUNT_ORDER_FAILURE);
					notification.setCreated(new Date());

					notificationService.save(notification);
				}
			}
			else {
				accountOrder.setActualUserId(dto.getAffectedUserId());
			}

			try {
				accountOrderService.notify(accountOrder);
			}
			catch (Exception ex) {
				log.error("Failed to notify on order: " + accountOrder.getId(), ex);
			}

			accountOrderService.save(accountOrder);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
