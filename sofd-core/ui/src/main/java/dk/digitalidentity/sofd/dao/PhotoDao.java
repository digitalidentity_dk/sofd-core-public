package dk.digitalidentity.sofd.dao;

import dk.digitalidentity.sofd.dao.model.Photo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface PhotoDao extends CrudRepository<Photo, Long> {
    Photo findByPersonUuid(String personUuid);

    void deleteByPersonUuid(String personUuid);
}