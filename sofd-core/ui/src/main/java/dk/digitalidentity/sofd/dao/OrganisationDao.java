package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.rest.core.annotation.RestResource;

import dk.digitalidentity.sofd.dao.model.Organisation;
import dk.digitalidentity.sofd.dao.model.QOrganisation;

public interface OrganisationDao extends JpaRepository<Organisation, Long>, QueryDslPredicateExecutor<Organisation>, QuerydslBinderCustomizer<QOrganisation> {

	@RestResource(exported = false)
	void delete(Iterable<? extends Organisation> entities);

	@RestResource(exported = false)
	void delete(Organisation entity);

	@RestResource(exported = false)
	void delete(long id);

	@RestResource(exported = false)
	void deleteAll();

	@RestResource(exported = false)
	<S extends Organisation> List<S> save(Iterable<S> entities);

	@RestResource(exported = false)
	<S extends Organisation> S save(S entity);

	@RestResource(exported = false)
	<S extends Organisation> S saveAndFlush(S entity);

    List<Organisation> findAll();
    Organisation findById(long id);
    Organisation findByShortName(String shortName);
    
	@Override
	default public void customize(QuerydslBindings bindings, QOrganisation organisation) {

	}
}