package dk.digitalidentity.sofd.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "future_emails")
public class FutureEmail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private boolean allOrNone;
	
	@Column
	private Date deliveryTts;
	
	@Column
	private boolean eboks;
	
	@Column
	private String title;
	
	@Column
	private String message;
	
	@ManyToMany
	@JoinTable(name = "future_email_persons", joinColumns = @JoinColumn(name = "person_uuid"), inverseJoinColumns = @JoinColumn(name = "future_email_id"))
	private List<Person> persons;
	
	public FutureEmail() {
		this.persons = new ArrayList<>();
	}	
}
