package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.Tag;

@RepositoryRestResource(exported = false)
public interface TagsDao extends CrudRepository<Tag, Long> {
	List<Tag> findAll();

	Tag findByValue(String tag);

	boolean existsByValue(String value);
}
