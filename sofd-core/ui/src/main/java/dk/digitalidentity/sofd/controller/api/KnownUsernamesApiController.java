package dk.digitalidentity.sofd.controller.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.security.RequireReadAccess;
import dk.digitalidentity.sofd.service.KnownUsernamesService;

@RestController
@RequireReadAccess
public class KnownUsernamesApiController {

	@Autowired
	private KnownUsernamesService knownUsernamesService;

	@GetMapping("/api/knownusernames")
	public ResponseEntity<List<String>> list(@RequestParam(value = "userType") String userType) {
		return new ResponseEntity<>(knownUsernamesService.findByUserType(userType)
				.stream()
				.map(a -> a.getUsername())
				.collect(Collectors.toList()),
				HttpStatus.OK);
	}
}
