package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Los {
	private boolean enabled = false;
	private boolean altOrgsEnabled = false;
	private boolean futureOrgsEnabled = false;
	private String primeAffiliationMaster = "OPUS";
}
