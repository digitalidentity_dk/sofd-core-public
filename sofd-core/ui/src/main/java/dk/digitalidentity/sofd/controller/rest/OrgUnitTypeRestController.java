package dk.digitalidentity.sofd.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.dao.model.OrgUnitType;
import dk.digitalidentity.sofd.service.OrgUnitService;
import lombok.extern.log4j.Log4j;

@RestController
@Log4j
public class OrgUnitTypeRestController {

	@Autowired
	private OrgUnitService orgUnitService;

	@PostMapping("/rest/orgunittype/edit")
	@ResponseBody
	public ResponseEntity<String> editCategory(@RequestBody OrgUnitType orgUnitType) {		
		if (orgUnitType.getValue().length() < 1) {
			log.warn("Requested OrgUnitType with value: " + orgUnitType.getValue() + " has invalid value");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		OrgUnitType type = new OrgUnitType();
		if (orgUnitType.getId() > 0) {
			type = orgUnitService.findTypeById(orgUnitType.getId());

			if (type == null) {
				log.warn("Requested OrgUnitType with ID:" + orgUnitType.getId() + " not found.");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		else {
			String key = orgUnitType.getKey().toUpperCase().replace(" ", "_");

			if (key.length() < 0) {
				log.warn("Requested OrgUnitType with key: " + orgUnitType.getKey() + " has invalid key");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
			if (orgUnitService.findTypeByKey(key) != null) {
				log.warn("Requested OrgUnitType with key: " + orgUnitType.getKey() + " already exists");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}

			type.setKey(key);
		}
		
		type.setValue(orgUnitType.getValue());
		type.setActive(true);
		type.setExtId(orgUnitType.getExtId());

		orgUnitService.saveType(type);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
