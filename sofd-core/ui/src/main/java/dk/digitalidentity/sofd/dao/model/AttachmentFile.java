package dk.digitalidentity.sofd.dao.model;

import javax.jdo.annotations.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "email_templates_attachment_file")
@Getter
@Setter
public class AttachmentFile {

	@Id
	@GeneratedValue
	private long id;

	@Column
	private byte[] content;

}
