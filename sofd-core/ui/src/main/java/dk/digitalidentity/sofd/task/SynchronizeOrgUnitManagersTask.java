package dk.digitalidentity.sofd.task;

import java.util.List;

import dk.digitalidentity.sofd.service.OrgUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitManager;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.security.SecurityUtil;
import dk.digitalidentity.sofd.service.PersonService;
import lombok.extern.log4j.Log4j;

@Component
@EnableScheduling
@Log4j
public class SynchronizeOrgUnitManagersTask {

	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	private OrgUnitService orgUnitService;

	@Autowired
	private PersonService personService;

	@Scheduled(cron = "${cron.managersync:0 0 1 * * ?}")
	@Transactional(rollbackFor = Exception.class)
	public void processChanges() throws Exception {
		// TODO: if the API call hits the instance where synchronization is disabled, it bypasses this... we should have a boolean
		//       argument that allows bypass - and a wrapper method, as scheduled tasks cannot take arguments ;)
		if (!configuration.getScheduled().isEnabled()) {
			log.debug("Scheduled jobs are disabled on this instance");
			return;
		}
		if (!configuration.getScheduled().getManagerSync().isEnabled()) {
			log.debug("ManagerSync is disabled on this instance");
			return;
		}

		SecurityUtil.fakeLoginSession();

		// Remove all current manager links
		List<OrgUnit> allOrgUnits = orgUnitService.getAll();
		List<Person> allManagers = personService.findAllManagers();

		if (allManagers != null) {
			for (Person person : allManagers) {

				if (person.getAffiliations() != null) {
					for (Affiliation aff : person.getAffiliations()) {

						if (aff.getManagerFor() != null) {
							for (OrgUnit orgUnit : aff.getManagerFor()) {
								OrgUnitManager manager = new OrgUnitManager();
								manager.setInherited(false);
								manager.setManager(person);
								manager.setOrgUnit(orgUnit);
								manager.setName(person.getChosenName() != null ? person.getChosenName() : person.getFirstname() + " " + person.getSurname());
								orgUnit.setNewManager(manager);

								setManager(orgUnit, person);
							}
						}
					}
				}
			}
		}

		for (OrgUnit orgUnit : allOrgUnits) {
			boolean changes = false;

			// readability over efficiency :)
			if (orgUnit.getManager() == null && orgUnit.getNewManager() == null) {
				changes = false;
			}
			else if (orgUnit.getManager() != null && orgUnit.getNewManager() == null) {
				changes = true;
			}
			else if (orgUnit.getManager() == null && orgUnit.getNewManager() != null) {
				changes = true;
			}
			else if (!orgUnit.getManager().getManager().getUuid().equals(orgUnit.getNewManager().getManager().getUuid())) {
				changes = true;
			}

			if (changes) {
				orgUnit.setManager(orgUnit.getNewManager());

				orgUnitService.save(orgUnit);
			}
		}
	}

	private void setManager(OrgUnit orgUnit, Person person) {
		for (OrgUnit child : orgUnit.getChildren()) {

			if (child.getNewManager() != null && !child.getNewManager().isInherited()) {
				// already has a real manager. we skip.
			}
			else {
				// set inherited manager
				OrgUnitManager manager = new OrgUnitManager();
				manager.setInherited(true);
				manager.setManager(person);
				manager.setOrgUnit(child);
				manager.setName(person.getChosenName() != null ? person.getChosenName() : person.getFirstname() + " " + person.getSurname());
				child.setNewManager(manager);

				setManager(child, person);
			}
		}
	}
}