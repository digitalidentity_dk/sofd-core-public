package dk.digitalidentity.sofd.dao.model.enums;

public enum EventType {
	SAVE, DELETE, LOGIN, FAILED_LOGIN, VIEW_PERSON, DONWLOAD_REPORT, VIEW_REPORT;
}
