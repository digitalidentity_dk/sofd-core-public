package dk.digitalidentity.sofd.listener;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.NotificationService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.EntityChangeQueueDetail;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;
import dk.digitalidentity.sofd.listener.EntityListenerService.ChangeType;

// will trigger the creation of a notification when an existing employee changes "work-place"

@Component
public class AffiliationLocationChangeListener implements ListenerAdapter {

	@Autowired
	private PersonService personService;
	
	@Autowired
	private AffiliationService affiliationService;
	
	@Autowired
	private NotificationService notificationService;

	@Override
	public void personUpdated(String uuid, List<EntityChangeQueueDetail> changes) {
		List<EntityChangeQueueDetail> affiliationChanges = changes.stream().filter(c -> c.getChangeType().equals(ChangeType.CHANGED_AFFILIATION_LOCATION)).collect(Collectors.toList());
		if (affiliationChanges.isEmpty()) {
			return;
		}

		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return;
		}
		
		for (EntityChangeQueueDetail affiliationChange : affiliationChanges) {
			Affiliation affiliation = affiliationService.findByUuid(affiliationChange.getChangeTypeDetails());
			if (affiliation == null) {
				continue;
			}
			
			Notification notification = new Notification();
			notification.setActive(true);
			notification.setAffectedEntityUuid(affiliation.getPerson().getUuid());
			notification.setAffectedEntityType(EntityType.PERSON);
			notification.setAffectedEntityName(PersonService.getName(affiliation.getPerson()));
			notification.setNotificationType(NotificationType.NEW_AFFILIATION_LOCATION);
			notification.setCreated(new Date());
			notification.setMessage("Nyt tilhørsforhold i " + affiliation.getOrgUnit().getName() + " (" + affiliation.getOrgUnit().getShortname() + ")");
			
			notificationService.save(notification);
		}
	}
}
