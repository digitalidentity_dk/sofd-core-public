package dk.digitalidentity.sofd.controller.mvc.datatables.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.controller.mvc.datatables.dao.model.GridTelephonyPhone;

@RepositoryRestResource(exported = false)
public interface GridTelephonyPhoneDatatableDao extends DataTablesRepository<GridTelephonyPhone, Long> {

}