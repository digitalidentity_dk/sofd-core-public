package dk.digitalidentity.sofd.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import dk.digitalidentity.sofd.controller.mvc.dto.InlineImageDTO;
import dk.digitalidentity.sofd.dao.EmailQueueDao;
import dk.digitalidentity.sofd.dao.model.Attachment;
import dk.digitalidentity.sofd.dao.model.EmailQueue;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class EmailQueueService {
	
	@Autowired
	private EmailQueueDao emailQueueDao;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private EboksService eBoksService;

	public void queueEmail(String email, String title, String message, long delay, EmailTemplate template) {
		EmailQueue mail = new EmailQueue();
		mail.setEmail(email);
		mail.setMessage(message);
		mail.setTitle(title);
		mail.setDeliveryTts(getDeliveryTts(delay));
		mail.setEmailTemplate(template);

		emailQueueDao.save(mail);
	}
	
	public void queueEboks(String cpr, String title, String message, long delay, EmailTemplate template) {
		EmailQueue mail = new EmailQueue();
		mail.setCpr(cpr);
		mail.setMessage(message);
		mail.setTitle(title);
		mail.setDeliveryTts(getDeliveryTts(delay));
		mail.setEmailTemplate(template);
		
		emailQueueDao.save(mail);		
	}
	
	private Date getDeliveryTts(long delay) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, (int) delay);

		return cal.getTime();
	}

	@Transactional
	public void sendPending() {
		List<EmailQueue> emails = findPending();
		
		for (EmailQueue email : emails) {
			EmailTemplate template = email.getEmailTemplate();
			if (template != null) {
				template.forceLoadAttachments();
			}
			List<Attachment> attachments = (template != null && template.getAttachments() != null && template.getAttachments().size() > 0) ? template.getAttachments() : null;
			
			boolean success = false;
			if (!StringUtils.isEmpty(email.getCpr())) {
				if (attachments != null) {
					success = eBoksService.sendMessageWithAttachments(email.getCpr(), email.getTitle(), email.getMessage(), attachments);
				}
				else {
					success = eBoksService.sendMessage(email.getCpr(), email.getTitle(), email.getMessage());
				}
			}
			else if (!StringUtils.isEmpty(email.getEmail())) {
				if (template != null) {
					List<InlineImageDTO> inlineImages = transformImages(email);
					if (attachments != null) {
						success = emailService.sendMessageWithAttachments(email.getEmail(), email.getTitle(), email.getMessage(), attachments, inlineImages);
					}
					else {
						success = emailService.sendMessage(email.getEmail(), email.getTitle(), email.getMessage(), inlineImages);
					}
				} else {
					if (attachments != null) {
						success = emailService.sendMessageWithAttachments(email.getEmail(), email.getTitle(), email.getMessage(), attachments);
					}
					else {
						success = emailService.sendMessage(email.getEmail(), email.getTitle(), email.getMessage());
					}
				}
			}
			else {
				log.error("Cannot send message with title '" + email.getTitle() + "' due to no cpr/email");
			}
			
			if (success) {
				emailQueueDao.delete(email);				
			}
		}
	}
	
	private List<EmailQueue> findPending() {
		Date tts = new Date();
		
		return emailQueueDao.findTop10ByDeliveryTtsBefore(tts);
	}
	
	private List<InlineImageDTO> transformImages(EmailQueue email) {
		List<InlineImageDTO> inlineImages = new ArrayList<>();
		String message = email.getMessage();
		Document doc = Jsoup.parse(message);
		int counter = 1;
		
		for (Element img : doc.select("img")) {
			String src = img.attr("src");
			if (src == null || src == "") {
				continue;
			}
			
			String filename = img.attr("data-filename");
			
			if (filename == null || filename == "") {
				filename = "file" + counter;
				counter ++;
			}
			
			InlineImageDTO inlineImageDto = new InlineImageDTO();
			inlineImageDto.setBase64(src.contains("base64"));
			
			if (inlineImageDto.isBase64()) {
				inlineImageDto.setUrl(false);
			}
			else {
				inlineImageDto.setUrl(true);
			}
			
			inlineImageDto.setCid(filename);
			inlineImageDto.setSrc(src);
			inlineImages.add(inlineImageDto);
			img.attr("src", "cid:" + filename);
		}
		
		email.setMessage(doc.html());
		
		return inlineImages;		
	}
}
