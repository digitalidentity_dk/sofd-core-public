package dk.digitalidentity.sofd.dao;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.AuditLog;

@RepositoryRestResource(exported=false)
public interface AuditLogDao extends CrudRepository<AuditLog, Long> {
	void deleteByTimestampBefore(Date before);
}