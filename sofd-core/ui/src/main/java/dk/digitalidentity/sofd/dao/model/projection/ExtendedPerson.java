package dk.digitalidentity.sofd.dao.model.projection;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;

@Projection(name = "extended", types = { Person.class })
public interface ExtendedPerson {
	
	// all existing fields
	
	String getUuid();
	String getMaster();
	Date  getCreated();
	Date getLastChanged();
	boolean isDeleted();
	String getNotes();
	String getKeyWords();
	String getCpr();
	String getFirstname();
	String getSurname();
	String getChosenName();
	Date getFirstEmploymentDate();
	Date getAnniversaryDate();
	Post getRegisteredPostAddress();
	Post getResidencePostAddress();
	List<Phone> getPhones();
	List<User> getUsers();
	List<Affiliation> getAffiliations();
	
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	String getLocalExtensions();
	
	// additional virtual fields
	
	@Value("#{target.getResidencePostAddress() != null ? target.getResidencePostAddress() : target.getRegisteredPostAddress()}")
	Post getPostAddress();
	
	@Value("#{target.getChosenName() != null ? target.getChosenName() : target.getFirstname() + \" \" + target.getSurname()}")
	String getName();
	
	boolean isTaxedPhone();
}
