package dk.digitalidentity.sofd.controller.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Objects;

import dk.digitalidentity.sofd.controller.mvc.admin.dto.TagDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.PostDTO;
import dk.digitalidentity.sofd.controller.rest.model.ContactDTO;
import dk.digitalidentity.sofd.controller.rest.model.OrgUnitCoreInfo;
import dk.digitalidentity.sofd.controller.rest.model.PhoneDTO;
import dk.digitalidentity.sofd.controller.rest.model.TryAccountOrderRulesResult;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.FunctionType;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitAccountOrder;
import dk.digitalidentity.sofd.dao.model.OrgUnitTag;
import dk.digitalidentity.sofd.dao.model.Organisation;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.Tag;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderRule;
import dk.digitalidentity.sofd.security.RequireAdminAccess;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.security.RequireLosAdminAccess;
import dk.digitalidentity.sofd.security.RequireReadAccess;
import dk.digitalidentity.sofd.security.RequireWriteContactInfoAccess;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.FunctionTypeService;
import dk.digitalidentity.sofd.service.OrgUnitService;
import dk.digitalidentity.sofd.service.OrganisationService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import dk.digitalidentity.sofd.service.TagsService;
import dk.digitalidentity.sofd.service.model.OUTreeForm;

@RestController
public class OrgUnitRestController {

	@Autowired
	private OrgUnitService orgUnitService;

	@Autowired
	private FunctionTypeService functionTypeService;

	@Autowired
	private AccountOrderService accountOrderService;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;

	@Autowired
	private OrganisationService organisationService;

	@Autowired
	private TagsService tagsService;
	
	@Autowired
	private PersonService personService;

	@RequireAdminAccess
	@PostMapping(value = "/rest/orgunit/{uuid}/copyRulesTo")
	@ResponseBody
	public HttpEntity<List<String>> copyRulesTo(@PathVariable("uuid") String uuid, @RequestBody List<String> uuids) {
		OrgUnit ou = orgUnitService.getByUuid(uuid);
		if (ou == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		OrgUnitAccountOrder accountOrder = accountOrderService.getAccountOrderSettings(ou, true);

		List<String> failedOrgUnits = new ArrayList<String>();

		List<OrgUnit> orgUnits = orgUnitService.getByUuid(uuids);
		for (OrgUnit orgUnit : orgUnits) {
			// copy-to-self should not trigger any kind of update ;)
			if (orgUnit.getUuid().equals(ou.getUuid())) {
				continue;
			}
			
			OrgUnitAccountOrder existingOrder = accountOrderService.getAccountOrderSettings(orgUnit, true);
			boolean byPositionRulesExist = existingOrder.getTypes().stream().anyMatch(t -> t.getRule().equals(AccountOrderRule.BY_POSITION_NAME));
			
			// do not overwrite rules on OrgUnits that has existing rules based on positions
			if (!byPositionRulesExist) {
				accountOrderService.setAccountOrderSettings(orgUnit, accountOrder);
			}
			else {
				failedOrgUnits.add(orgUnit.getName());
			}
		}
		
		return new ResponseEntity<>(failedOrgUnits, HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping(value = "/rest/orgunit/update/kle")
	@ResponseBody
	public HttpEntity<String> updateKle(@RequestHeader("uuid") String uuid, @RequestHeader("type") String type, @RequestBody List<String> codes) throws Exception {
		OrgUnit ou = orgUnitService.getByUuid(uuid);
		if (ou == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		switch (type) {
			case "KlePrimary":
				ou.setKlePrimary(codes);
				orgUnitService.save(ou);
				break;
			case "KleSecondary":
				ou.setKleSecondary(codes);
				orgUnitService.save(ou);
				break;
			case "KleTertiary":
				ou.setKleTertiary(codes);
				orgUnitService.save(ou);
				break;
			default:
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireControllerWriteAccess
	@PostMapping(value = "/rest/orgunit/update/kle/inherit")
	@ResponseBody
	public HttpEntity<String> updateKleInherit(@RequestHeader("uuid") String uuid, @RequestHeader("inherit") boolean inherit) throws Exception {
		OrgUnit ou = orgUnitService.getByUuid(uuid);
		if (ou == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		ou.setInheritKle(inherit);
		orgUnitService.save(ou);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireAdminAccess
	@PostMapping(value = "/rest/orgunit/{uuid}/update/accountOrderRules/try")
	@ResponseBody
	public HttpEntity<TryAccountOrderRulesResult> tryUpdateAccountOrderRules(@PathVariable("uuid") String uuid, @RequestBody OrgUnitAccountOrder accountOrders) {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		TryAccountOrderRulesResult result = new TryAccountOrderRulesResult();
		result.setResult(new HashMap<>());
		
		List<AccountOrder> accountsToCreate = accountOrderService.getAccountsToCreate(orgUnit, accountOrders);
		for (AccountOrder order : accountsToCreate) {
			String userType = order.getUserType();
			
			// don't count orders for persons with disabled ordering
			Person person = personService.getByUuid(order.getPersonUuid());
			if (person != null && person.isDisableAccountOrders()) {
				continue;
			}
			
			if (result.getResult().containsKey(userType)) {
				result.getResult().put(userType, result.getResult().get(userType) + 1);
			}
			else {
				result.getResult().put(userType, 1L);
			}
		}

		Map<String, Long> prettyMap = new HashMap<>();

		// translate to prettier messages
		for (String key : result.getResult().keySet()) {
			prettyMap.put(supportedUserTypeService.getPrettyName(key), result.getResult().get(key));
		}
		result.setResult(prettyMap);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequireAdminAccess
	@PostMapping(value = "/rest/orgunit/{uuid}/update/accountOrderRules")
	@ResponseBody
	public HttpEntity<OrgUnitAccountOrder> updateAccountOrderRules(@PathVariable("uuid") String uuid, @RequestBody OrgUnitAccountOrder accountOrders) {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		accountOrders = accountOrderService.setAccountOrderSettings(orgUnit, accountOrders);
		
		return new ResponseEntity<>(accountOrders, HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/orgunit/{uuid}/update/coreInfo")
	public HttpEntity<?> updateCoreInformation(@PathVariable("uuid") String uuid, @RequestBody @Valid OrgUnitCoreInfo coreInfoDTO, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
		}

		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		orgUnitService.updateCoreInformation(orgUnit, coreInfoDTO);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireReadAccess
	@GetMapping(value = "/rest/orgunit/{uuid}/getPositionNames")
	@ResponseBody
	public HttpEntity<Set<String>> getPositionNames(@PathVariable("uuid") String uuid) {
		Set<String> result = new HashSet<>();
		result.add("Ukendt");

		OrgUnit ou = orgUnitService.getByUuid(uuid);
		if (ou == null) {
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		result = orgUnitService.getPositionNames(ou, false);
		
		if (!result.contains("Ekstern")) {
			result.add("Ekstern");			
		}

		if (!result.contains("Ansat")) {
			result.add("Ansat");
		}
		
		if (!result.contains("Medlem")) {
			result.add("Medlem");
		}

		if (!result.contains("Praktikant")) {
			result.add("Praktikant");
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequireWriteContactInfoAccess
	@PostMapping(value = "/rest/orgunit/update/contactinfo")
	@ResponseBody
	public HttpEntity<ContactDTO> updateContactInfo(@RequestHeader("uuid") String uuid, @RequestBody ContactDTO contactInfo) throws Exception {
		OrgUnit ou = orgUnitService.getByUuid(uuid);
		if (ou == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		ou.setOpeningHours(contactInfo.getOpeningHours());
		ou.setKeyWords(contactInfo.getKeywords());
		ou.setNotes(contactInfo.getNotes());
		orgUnitService.save(ou);
		
		return new ResponseEntity<>(contactInfo, HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping(value = "/rest/orgunit/editPhone")
	@ResponseBody
	public ResponseEntity<HttpStatus> editPhone(@RequestHeader("uuid") String uuid, @RequestBody PhoneDTO phoneDTO) throws Exception {
		if (StringUtils.isEmpty(phoneDTO.getPhoneNumber())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		FunctionType functionType = functionTypeService.findById(phoneDTO.getFunctionType());

		if (phoneDTO.getId() == 0) {
			Phone phone = new Phone();
			phone.setMaster("SOFD");
			phone.setMasterId(UUID.randomUUID().toString());
			phone.setPhoneNumber(phoneDTO.getPhoneNumber());
			phone.setPhoneType(phoneDTO.getPhoneType());
			phone.setNotes(phoneDTO.getNotes());
			phone.setVisibility(phoneDTO.getVisibility());
			phone.setPrime(false);
			phone.setTypePrime(false);
			phone.setFunctionType(functionType);

			orgUnit.getPhones().add(phone);
		}
		else {
			List<Phone> phones = orgUnit.getPhones();
			Optional<Phone> existingPhone = phones.stream().filter(p -> p.getId() == phoneDTO.getId()).findFirst();
			if (existingPhone.isPresent()) {
				Phone modifiedPhone = existingPhone.get();

				modifiedPhone.setPhoneNumber(phoneDTO.getPhoneNumber());
				modifiedPhone.setPhoneType(phoneDTO.getPhoneType());
				modifiedPhone.setNotes(phoneDTO.getNotes());
				modifiedPhone.setVisibility(phoneDTO.getVisibility());
				modifiedPhone.setFunctionType(functionType);
			}
			else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		orgUnitService.save(orgUnit);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping(path = { "/rest/orgunit/updatePrimaryPhones" })
	@ResponseBody
	public ResponseEntity<String> updatePrimaryPhones(@RequestHeader("uuid") String uuid, @RequestBody List<PhoneDTO> phoneDTOs) throws Exception {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		for (PhoneDTO phoneDTO : phoneDTOs) {
			Optional<Phone> first = orgUnit.getPhones().stream().filter(p -> p.getId() == phoneDTO.getId()).findFirst();
			if (first.isPresent()) {
				Phone phone = first.get();

				phone.setPrime(phoneDTO.isPrime());
				phone.setTypePrime(phoneDTO.isTypePrime());
			}
			else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		orgUnitService.save(orgUnit);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/orgunit/deletePhone")
	@ResponseBody
	public ResponseEntity<HttpStatus> deletePhone(@RequestHeader("uuid") String uuid, @RequestHeader("id") long id) throws Exception {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean removedAny = orgUnit.getPhones().removeIf(phone -> phone.getId() == id);

		if (removedAny) {
			orgUnitService.save(orgUnit);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireLosAdminAccess
	@PostMapping("/rest/orgunit/deletePost")
	@ResponseBody
	public ResponseEntity<HttpStatus> deletePost(@RequestHeader("uuid") String uuid, @RequestHeader("id") long id) throws Exception {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean removedAny = orgUnit.getPostAddresses().removeIf(post -> post.getId() == id);

		if (removedAny) {
			orgUnitService.save(orgUnit);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireLosAdminAccess
	@PostMapping(value = "/rest/orgunit/editOrCreatePost")
	@ResponseBody
	public ResponseEntity<?> editOrCreatePost(@RequestHeader("uuid") String uuid, @RequestBody @Valid PostDTO postDTO, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
		}
		
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean changes = false;
		if (postDTO.getId() == 0) {
			Post post = new Post();
			post.setMaster("SOFD");
			post.setMasterId(UUID.randomUUID().toString());
			post.setCity(postDTO.getCity());
			post.setCountry(postDTO.getCountry());
			post.setLocalname(postDTO.getLocalname());
			post.setPostalCode(postDTO.getPostalCode());
			post.setStreet(postDTO.getStreet());

			orgUnit.getPostAddresses().add(post);
			changes = true;
		}
		else {
			List<Post> posts = orgUnit.getPostAddresses();
			Optional<Post> existingPost = posts.stream().filter(p -> p.getId() == postDTO.getId()).findFirst();
			if (existingPost.isPresent()) {
				Post modifiedPost = existingPost.get();

				if (!Objects.equal(modifiedPost.getCity(), postDTO.getCity())) {
					modifiedPost.setCity(postDTO.getCity());
					changes = true;
				}
				
				if (!Objects.equal(modifiedPost.getCountry(), postDTO.getCountry())) {
					modifiedPost.setCountry(postDTO.getCountry());
					changes = true;
				}
				
				if (!Objects.equal(modifiedPost.getLocalname(), postDTO.getLocalname())) {
					modifiedPost.setLocalname(postDTO.getLocalname());
					changes = true;
				}
				
				if (!Objects.equal(modifiedPost.getPostalCode(), postDTO.getPostalCode())) {
					modifiedPost.setPostalCode(postDTO.getPostalCode());
					changes = true;
				}
				
				if (!Objects.equal(modifiedPost.getStreet(), postDTO.getStreet())) {
					modifiedPost.setStreet(postDTO.getStreet());
					changes = true;
				}
			}
			else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		if (changes) {
			orgUnitService.save(orgUnit);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping(value = "/rest/orgunit/{uuid}/saveTag")
	@ResponseBody
	public ResponseEntity<?> saveTag(@PathVariable("uuid") String uuid, @RequestBody TagDTO tagDTO) throws Exception {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if(orgUnit == null) {
			return ResponseEntity.badRequest().build();
		}

		Tag tag = tagsService.findById(tagDTO.getId());
		if(tag == null) {
			return ResponseEntity.badRequest().build();
		}

		Optional<OrgUnitTag> exisingTag = orgUnit.getTags().stream().filter(t -> t.getTag().getId().equals(tag.getId())).findFirst();
		OrgUnitTag orgUnitTag;
		if( exisingTag.isPresent() )
		{
			orgUnitTag = exisingTag.get();
		}
		else
		{
			orgUnitTag = new OrgUnitTag();
			orgUnitTag.setOrgUnit(orgUnit);
			orgUnitTag.setTag(tag);
			orgUnit.getTags().add(orgUnitTag);
		}
		if( tag.isCustomValueEnabled() )
		{
			// verify regex tag syntax
			if( tag.getCustomValueRegex() != null && tag.getCustomValueRegex() != "" )
			{
				if( tagDTO.getCustomValue() == null || !Pattern.matches(tag.getCustomValueRegex(), tagDTO.getCustomValue()))
				{
					return new ResponseEntity<>("Ugyldig værdi", HttpStatus.BAD_REQUEST);
				}
			}
			// check for uniqueness if enabled for this tag
			if( tag.isCustomValueUnique() )
			{
				Optional<OrgUnitTag> exising = tag.getOrgUnitTags().stream().filter(t -> t.getId() != orgUnitTag.getId() && Objects.equal(t.getCustomValue(), tagDTO.getCustomValue())).findFirst();
				if( exising.isPresent() )
				{
					String errorMessage = String.format("Enheden \"%s\" er allerede opmærket med %s - %s: %s"
							,exising.get().getOrgUnit().getName()
							,exising.get().getTag().getValue()
							,exising.get().getTag().getCustomValueName()
							, tagDTO.getCustomValue());
					return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
				}
			}
			orgUnitTag.setCustomValue(tagDTO.getCustomValue());
		}
		else
		{
			orgUnitTag.setCustomValue(null);
		}
		orgUnitService.save(orgUnit);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireControllerWriteAccess
	@PostMapping("/rest/orgunit/deleteTag")
	@ResponseBody
	public ResponseEntity<HttpStatus> deleteTag(@RequestHeader("uuid") String uuid, @RequestHeader("id") long id) throws Exception {
		OrgUnit orgUnit = orgUnitService.getByUuid(uuid);
		if (orgUnit == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean removedAny = orgUnit.getTags().removeIf(tag -> tag.getTag().getId() == id);

		if (removedAny) {
			orgUnitService.save(orgUnit);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequireLosAdminAccess
	@PostMapping(value = "/rest/orgunit/new")
	@ResponseBody
	public ResponseEntity<?> createOrgUnit(@RequestBody @Valid OrgUnitCoreInfo orgUnitDTO, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
		}

		Organisation organisation = organisationService.getById(orgUnitDTO.getBelongsTo());
		if (organisation == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		OrgUnit parent = orgUnitService.getByUuid(orgUnitDTO.getParent());
		if (parent == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		OrgUnit orgUnit = new OrgUnit();
		orgUnit.setName(orgUnitDTO.getName());
		orgUnit.setShortname(orgUnitDTO.getShortname());
		orgUnit.setEan(orgUnitDTO.getEan());
		orgUnit.setCvr(orgUnitDTO.getCvr());
		orgUnit.setSenr(orgUnitDTO.getSenr());
		orgUnit.setPnr(orgUnitDTO.getPnr());
		orgUnit.setMaster("SOFD");
		orgUnit.setMasterId("SOFD-" + UUID.randomUUID());
		orgUnit.setParent(parent);
		orgUnit.setType(orgUnitService.getDepartmentType());
		orgUnit.setBelongsTo(organisation);

		OrgUnit newOrgUnit = orgUnitService.save(orgUnit);

		return new ResponseEntity<>(newOrgUnit.getUuid(), HttpStatus.OK);
	}

	@RequireReadAccess
	@GetMapping(value = "/rest/orgunit/get-by-org/{id}")
	@ResponseBody
	public ResponseEntity<List<OUTreeForm>> getOUsByOrg(@PathVariable("id") Long id) {
		Organisation organisation = organisationService.getById(id);
		if (organisation == null) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		List<OUTreeForm> orgUnits = orgUnitService.getAllTree(organisation);

		return new ResponseEntity<>(orgUnits, HttpStatus.OK);
	}
	
	@RequireLosAdminAccess
	@GetMapping(value = "/rest/orgunit/check")
	@ResponseBody
	public ResponseEntity<Boolean> checkName(@RequestParam String name) {
		if (name == null || name.equals("")) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		
		boolean found = !orgUnitService.getByName(name).isEmpty();

		return new ResponseEntity<>(found, HttpStatus.OK);
	}
	
	
}
