package dk.digitalidentity.sofd.interceptor;

import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;

@RepositoryEventHandler({ Person.class, OrgUnit.class })
public class BeforeSDRSaveInterceptor {
	private AbstractBeforeSaveInterceptor interceptor;
	
	public BeforeSDRSaveInterceptor(AbstractBeforeSaveInterceptor interceptor) {
		this.interceptor = interceptor;
	}

	@HandleBeforeCreate
	@HandleBeforeSave
	protected void handleBeforeSavePerson(Person person) {
		interceptor.handleSavePerson(person);
	}
	
	@HandleBeforeCreate
	@HandleBeforeSave
	protected void handleBeforeSaveOrgUnit(OrgUnit orgUnit) {
		interceptor.handleSaveOrgUnit(orgUnit);
	}
	
	@HandleAfterSave
	@HandleAfterCreate
	protected void handleAfterSavePerson(Person person) {
		interceptor.handleAccountOrders(person);
	}
}
