package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.SOFDAccount;

@RepositoryRestResource(exported = false)
public interface SOFDAccountDao extends CrudRepository<SOFDAccount, Long> {
	List<SOFDAccount> findAll();

	SOFDAccount findById(Long id);

	SOFDAccount findByUserId(String userId);
}