package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.SupportedUserType;

@RepositoryRestResource(exported = false)
public interface SupportedUserTypeDao extends JpaRepository<SupportedUserType, Long> {
	SupportedUserType findById(Long id);
	List<SupportedUserType> findAll();
	SupportedUserType findByKey(String userType);
}
