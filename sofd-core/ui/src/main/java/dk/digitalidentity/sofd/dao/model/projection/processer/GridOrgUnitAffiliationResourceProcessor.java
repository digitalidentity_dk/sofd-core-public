package dk.digitalidentity.sofd.dao.model.projection.processer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.projection.GridOrgUnitAffiliation;

@Component
public class GridOrgUnitAffiliationResourceProcessor implements ResourceProcessor<Resource<GridOrgUnitAffiliation>> {

	@Autowired
	private EntityLinks entityLinks;

	@Override
	public Resource<GridOrgUnitAffiliation> process(Resource<GridOrgUnitAffiliation> resource) {
		GridOrgUnitAffiliation content = resource.getContent();
		Person person = content.getPerson();

		resource.add(entityLinks.linkToSingleResource(Person.class, person.getUuid()));

		return resource;
	}
}
