package dk.digitalidentity.sofd.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import dk.digitalidentity.sofd.dao.impl.SofdRepositoryFactoryBean;

@Configuration
@EnableJpaRepositories(basePackages = { "dk.digitalidentity.sofd.dao", "dk.digitalidentity.sofd.telephony.dao" }, repositoryFactoryBeanClass = SofdRepositoryFactoryBean.class)
public class EnversConfiguration {

}