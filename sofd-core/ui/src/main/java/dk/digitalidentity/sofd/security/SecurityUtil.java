package dk.digitalidentity.sofd.security;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import dk.digitalidentity.sofd.config.RoleConstants;
import dk.digitalidentity.sofd.dao.model.Client;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.AccessRole;
import dk.digitalidentity.sofd.service.PersonService;

public class SecurityUtil {

	// certain scheduled tasks needs to update data that requires WRITE_ACCESS,
	// so a fakeLoginSession can be created for this purpose, and put onto the
	// running thread
	public static void fakeLoginSession() {
		ArrayList<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + AccessRole.WRITE_ACCESS.toString()));

		// the client is not persisted, and is only created to fulfill the expectations
		// of the rest of the code
		Client client = new Client();
		client.setName("SystemTask");
		client.setAccessRole(AccessRole.WRITE_ACCESS);
		client.setApiKey(UUID.randomUUID().toString());
		
		ClientToken token = new ClientToken(client.getName(), client.getApiKey(), authorities);
		token.setClient(client);

		SecurityContextHolder.getContext().setAuthentication(token);
	}
	
	public static Authentication getLoginSession() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public static void setLoginSession(Authentication authentication) {
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	public static void loginPerson(Person person) {
		ArrayList<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_SMS));
		authorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
		authorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_SMS_GATEWAY));

		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(PersonService.getName(person), null, authorities);

		SecurityContextHolder.getContext().setAuthentication(token);
	}

	public static String getUser() {
		String name = null;

		if (isUserLoggedIn()) {
			name = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}

		return name;
	}
	
	public static String getUsername() {
		String name = null;

		if (isUserLoggedIn()) {
			name = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			// if the principal name is in X.509 format, we just pull the CN part in this method (use getUser() for the full name)
			String nameCandidate = getNameIdValue("CN", name);
			if (nameCandidate.length() > 0) {
				name = nameCandidate;
			}
		}

		return name;
	}

	public static Client getClient() {
		Client client = null;

		if (isClientLoggedIn()) {
			client = ((ClientToken) SecurityContextHolder.getContext().getAuthentication()).getClient();
		}

		return client;
	}

	public static List<String> getUserRoles() {
		List<String> roles = new ArrayList<>();

		if (isUserLoggedIn()) {
			for (GrantedAuthority grantedAuthority : (SecurityContextHolder.getContext().getAuthentication()).getAuthorities()) {
				roles.add(grantedAuthority.getAuthority());
			}
		}

		return roles;
	}

	public static boolean isUserLoggedIn() {
		if (isLoggedIn() && SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isClientLoggedIn() {
		if (isLoggedIn() && SecurityContextHolder.getContext().getAuthentication() instanceof ClientToken) {
			return true;
		}
		
		return false;
	}
	
	private static boolean isLoggedIn() {
		if (SecurityContextHolder.getContext().getAuthentication() != null/* && SecurityContextHolder.getContext().getAuthentication() instanceof ClientToken*/) {
			return true;
		}

		return false;
	}
	
	private static String getNameIdValue(String field, String nameId) {
		StringBuilder builder = new StringBuilder();

		int idx = nameId.indexOf(field + "=");
		if (idx >= 0) {
			for (int i = idx + field.length() + 1; i < nameId.length(); i++) {
				if (nameId.charAt(i) == ',') {
					break;
				}

				builder.append(nameId.charAt(i));
			}
		}

		return builder.toString();
	}
}
