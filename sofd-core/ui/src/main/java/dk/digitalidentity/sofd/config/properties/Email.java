package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Email {
	private boolean enabled = false;
	private String from = "no-reply@sofd.io";
	private String fromName = "SOFD Core";

	@JsonIgnore
	private String username;
	
	@JsonIgnore
	private String password;

	@JsonIgnore
	private String host;
}
