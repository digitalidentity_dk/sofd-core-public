package dk.digitalidentity.sofd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.ReservedUsernameDao;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.ReservedUsername;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderStatus;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderType;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;
import dk.digitalidentity.sofd.service.opus.dto.Envelope;
import dk.digitalidentity.sofd.service.opus.dto.Kommunikation;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class OpusService {
	private static final String OPUS_SOAP_BEGIN = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:oio:medarbejder:1.0.0\" xmlns:urn1=\"urn:oio:sagdok:3.0.0\"><soapenv:Header/><soapenv:Body>";
	private static final String OPUS_SOAP_END = "</soapenv:Body></soapenv:Envelope>";

	private static final String OPUS_SOAP_RET_BEGIN = "<urn:RetInput>";
	private static final String OPUS_SOAP_RET_END = "</urn:RetInput>";

	private static final String OPUS_SOAP_LAES_BEGIN = "<urn:LaesInput>";
	private static final String OPUS_SOAP_LAES_END = "</urn:LaesInput>";

	private static final String OPUS_SOAP_DATAKATAGORI = "<urn:Datakategori>0105</urn:Datakategori>";
	
	// {0} : ID'et på kommunens organisation (tenant i KMD OPUS)
	private static final String OPUS_SOAP_MODTAGER_REF = "<urn1:ModtagerRef><urn1:UUIDIdentifikator>NotUsed</urn1:UUIDIdentifikator><urn1:URNIdentifikator>urn:oio:kmd:lpe:modtager:{0}</urn1:URNIdentifikator></urn1:ModtagerRef>";

	// {0} : ID'et på medarbejderen (medarbejdernummer fra ansættelsen)
	private static final String OPUS_SOAP_MEDARBEJDER_REF = "<urn1:MedarbejderRef><urn1:UUIDIdentifikator>NotUsed</urn1:UUIDIdentifikator><urn1:URNIdentifikator>urn:oio:kmd:lpe:medarbejdernummer:{0}</urn1:URNIdentifikator></urn1:MedarbejderRef>";

	private static final String OPUS_SOAP_ATTRIBUTLISTE_BEGIN = "<urn:AttributListe><urn:Egenskaber>";
	private static final String OPUS_SOAP_ATTRIBUTLISTE_END = "</urn:Egenskaber></urn:AttributListe>";
	
	// {0} = 2020-10-23
	// {1} = 9999-12-31
	// {2} = 0010 (email) / 0001 (brugerid) / (9905) IT Bruger / 0020 (ingen anelse) 
	// {3} = Værdi på ovenstående (fx email eller brugernavn)
	private static final String OPUS_SOAP_KOMMUNIKATION = "<urn:Kommunikation><urn:Gyldighedsstart>{0}</urn:Gyldighedsstart><urn:Gyldighedsstop>{1}</urn:Gyldighedsstop><urn:Sekvensnummer>000</urn:Sekvensnummer><urn:Kommunikationsart>{2}</urn:Kommunikationsart><urn:KommunikationsID>{3}</urn:KommunikationsID></urn:Kommunikation>";
	
	@Qualifier("opusRestTemplate")
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	private PersonService personService;

	@Autowired
	private AccountOrderService accountOrderService;
	
	@Autowired
	private ReservedUsernameDao reservedUsernameDao;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;

	@Transactional(rollbackFor = Exception.class)
	public void handleOrders() {
		handleCreate();
		handleDelete();
	}

	public void updateEmail(Person person) {
		List<User> opusUsers = person.getUsers().stream().filter(u -> u.getUserType().equals(SupportedUserTypeService.getOpusUserType())).collect(Collectors.toList());
		
		for (User user : opusUsers) {
			ExistingOpusValue existingOpusValue = callOpusAndReadUser(user.getEmployeeId());

			if (StringUtils.isEmpty(existingOpusValue.itBruger.value)) {
				log.warn("Unable to update email address for " + person.getUuid() + " / " + user.getEmployeeId() + " due to no OPUS account, but we have one in SOFD: " + user.getUserId());
				continue;
			}

			String email = getEmail(person, user.getEmployeeId());
			callOpusAndUpdateEmail(email, user.getEmployeeId());
		}
	}

	private void failAndNotify(Person person, AccountOrder accountOrder, String message) {
		if (person != null) {
			String prettyName = supportedUserTypeService.getPrettyName(accountOrder.getUserType());
			
			Notification notification = new Notification();
			notification.setActive(true);
			notification.setAffectedEntityName(PersonService.getName(person));
			notification.setAffectedEntityType(EntityType.PERSON);
			notification.setAffectedEntityUuid(accountOrder.getPersonUuid());
			notification.setMessage(prettyName + ": " + message);
			notification.setNotificationType(NotificationType.ACCOUNT_ORDER_FAILURE);
			notification.setCreated(new Date());
			notificationService.save(notification);
		}
		
		accountOrder.setStatus(AccountOrderStatus.FAILED);
		accountOrder.setMessage(message);
		accountOrderService.save(accountOrder);
	}

	private void handleCreate() {
		List<AccountOrder> pendingOrders = accountOrderService.getPendingOrders(SupportedUserTypeService.getOpusUserType(), AccountOrderType.CREATE);
		
		pendingOrders = accountOrderService.identifyAndDeleteDuplicates(pendingOrders);
		if (pendingOrders.size() == 0) {
			return;
		}
		
		log.info("Processing " + pendingOrders.size() + " create orders");

		for (AccountOrder order : pendingOrders) {
			String personUuid = order.getPersonUuid();
			Person person = personService.getByUuid(personUuid);
			if (person == null) {
				log.error("Failed to process OPUS order " + order.getId() + ", because person with UUID " + personUuid + " does not exist!");
				
				failAndNotify(null, order, "Person eksisterer ikke længere i SOFD");
				
				continue;
			}
			
			String employeeId = order.getEmployeeId();			
			String requestedUserId = order.getRequestedUserId();
			String email = getEmail(person, employeeId);

			// ensure an employeeId is present in the account order
			if (StringUtils.isEmpty(employeeId)) {
				List<Affiliation> affiliations = person.getAffiliations();
				if (affiliations == null || affiliations.size() == 0) {
					log.error("Failed to process OPUS order " + order.getId() + ", because person with UUID " + personUuid + " does not have any affiliations!");

					failAndNotify(person, order, "Personen har ikke nogen tilhørsforhold");

					continue;
				}
				
				Optional<Affiliation> oAffiliation = affiliations.stream().filter(a -> a.getMaster().equals("OPUS") && a.isPrime()).findFirst();
				if (!oAffiliation.isPresent()) {
					log.error("Failed to process OPUS order " + order.getId() + ", because person with UUID " + personUuid + " does not have any OPUS affiliations!");
					
					failAndNotify(person, order, "Personen har ikke nogen OPUS tilhørsforhold, eller det primære tilhørsforhold er ikke ejet af OPUS");
					
					continue;
				}
				
				employeeId = oAffiliation.get().getEmployeeId();
			}

			ExistingOpusValue existingOpusValue = callOpusAndReadUser(employeeId);
			if (!StringUtils.isEmpty(existingOpusValue.itBruger.value) && !StringUtils.isEmpty(existingOpusValue.userId.value)) {
				// person is null on purpose, as no notification is then generated
				failAndNotify(null, order, "Personen har allerede en OPUS konto for medarbejderID " + employeeId + ": " + existingOpusValue.userId.value);
				
				continue;
			}

			OpusStatusCodeWrapper status = callOpusAndOrderUser(employeeId, requestedUserId, email);

			if (status.status.equals(OpusStatusCode.OK)) {
				order.setActualUserId(requestedUserId);
				order.setStatus(AccountOrderStatus.CREATED);
				accountOrderService.save(order);

				// notify relevant parties about success
				accountOrderService.notify(order);
			}
			else if (status.status.equals(OpusStatusCode.NETWORK_ERROR)) {
				; // try again later
			}
			else {
				failAndNotify(person, order, status.message);
			}
		}
		
		log.info("Done processing create orders");
	}
	
	private void handleDelete() {
		List<AccountOrder> pendingDeactivateOrders = accountOrderService.getPendingOrders(SupportedUserTypeService.getOpusUserType(), AccountOrderType.DEACTIVATE);
		List<AccountOrder> pendingDeleteOrders = accountOrderService.getPendingOrders(SupportedUserTypeService.getOpusUserType(), AccountOrderType.DELETE);

		String prettyName = supportedUserTypeService.getPrettyName(SupportedUserTypeService.getOpusUserType());
		
		// change deactivate to delete (as we only handle delete orders for OPUS)
		pendingDeactivateOrders.stream().forEach(o -> o.setOrderType(AccountOrderType.DELETE));

		// filter duplicates
		List<AccountOrder> pendingOrders = new ArrayList<>();
		pendingOrders.addAll(pendingDeactivateOrders);
		pendingOrders.addAll(pendingDeleteOrders);
		pendingOrders = accountOrderService.identifyAndDeleteDuplicates(pendingOrders);

		if (pendingOrders.size() == 0) {
			return;
		}

		log.info("Processing " + pendingOrders.size() + " delete orders");
		
		for (AccountOrder order : pendingOrders) {
			String personUuid = order.getPersonUuid();
			Person person = personService.getByUuid(personUuid);
			if (person == null) {
				log.error("Failed to process OPUS order " + order.getId() + ", because person with UUID " + personUuid + " does not exist!");
				order.setStatus(AccountOrderStatus.FAILED);
				order.setMessage("Person does not exist!");
				accountOrderService.save(order);
				
				continue;
			}
			
			String employeeId = order.getEmployeeId();			
			String userId = order.getRequestedUserId();

			ExistingOpusValue existingOpusValue = callOpusAndReadUser(employeeId);
			OpusStatusCodeWrapper status = callOpusAndDeleteUser(employeeId, userId, existingOpusValue);

			if (status.status.equals(OpusStatusCode.OK)) {
				order.setActualUserId(userId);
				order.setStatus(AccountOrderStatus.DELETED);
				accountOrderService.save(order);

				// notify relevant parties about success
				accountOrderService.notify(order);
			}
			else if (status.status.equals(OpusStatusCode.NETWORK_ERROR)) {
				; // try again later
			}
			else {
				order.setStatus(AccountOrderStatus.FAILED);
				order.setMessage(status.message);
				accountOrderService.save(order);
				
				// notify
				Notification notification = new Notification();
				notification.setActive(true);
				notification.setAffectedEntityName(PersonService.getName(person));
				notification.setAffectedEntityType(EntityType.PERSON);
				notification.setAffectedEntityUuid(person.getUuid());
				
				notification.setMessage(prettyName + ": " + status.message);
				notification.setNotificationType(NotificationType.ACCOUNT_ORDER_FAILURE);
				notification.setCreated(new Date());

				notificationService.save(notification);
			}
		}
		
		log.info("Done processing delete orders");
	}
	
	private String getEmail(Person person, String employeeId) {
		SupportedUserType exchangeUserType = supportedUserTypeService.findByKey(SupportedUserTypeService.getExchangeUserType());

		String email = null;
		if (person != null) {
			email = PersonService.getEmail(person);
		}

		if (email == null) {
			String defaultEmail = configuration.getModules().getAccountCreation().getOpusHandler().getDefaultEmail();
			if (!StringUtils.isEmpty(defaultEmail)) {
				email = defaultEmail;
			}
			else if (person != null) {
				if (exchangeUserType.isSingleUserMode()) {
					ReservedUsername reservedUsername = reservedUsernameDao.findByPersonUuidAndUserType(person.getUuid(), SupportedUserTypeService.getExchangeUserType());
					if (reservedUsername != null) {
						email = reservedUsername.getUserId();
					}
				}
				else {
					ReservedUsername reservedUsername = reservedUsernameDao.findByPersonUuidAndEmployeeIdAndUserType(person.getUuid(), employeeId, SupportedUserTypeService.getExchangeUserType());
					if (reservedUsername != null) {
						email = reservedUsername.getUserId();
					}
				}
				
				// if we get a reserved username, it does not contain a domain name, so we need to add that
				if (email != null) {
					if (!StringUtils.isEmpty(configuration.getModules().getAccountCreation().getOpusHandler().getDefaultEmailDomain())) {
						email += configuration.getModules().getAccountCreation().getOpusHandler().getDefaultEmailDomain();
					}
					else {
						// sorry, we have to null it :(
						email = null;
					}
				}
			}
		}
		
		if (email == null) {
			email = "no-email@kommune.dk";
		}
		
		return email;
	}
	
	class OpusStatusCodeWrapper {
		OpusStatusCode status;
		String message;
	}

	private enum OpusStatusCode {
		OK,
		NETWORK_ERROR,
		FAILURE
	};

	static class ExistingField {
		String value;
		String startDate;
		String stopDate;
	}
	
	static class ExistingOpusValue {
		ExistingField email;
		ExistingField userId;
		ExistingField itBruger;
	}

	private ExistingOpusValue callOpusAndReadUser(String employeeId) {
		ExistingOpusValue opusResponse = new ExistingOpusValue();
		opusResponse.email = new ExistingField();
		opusResponse.userId = new ExistingField();
		opusResponse.itBruger = new ExistingField();

		StringBuilder builder = new StringBuilder();

		builder.append(OPUS_SOAP_BEGIN);
		builder.append(OPUS_SOAP_LAES_BEGIN);
		
		builder.append(OPUS_SOAP_MODTAGER_REF.replace("{0}", configuration.getModules().getAccountCreation().getOpusHandler().getMunicipalityNumber()));
		builder.append(OPUS_SOAP_MEDARBEJDER_REF.replace("{0}", employeeId));
		
		builder.append(OPUS_SOAP_DATAKATAGORI);

		builder.append(OPUS_SOAP_LAES_END);
		builder.append(OPUS_SOAP_END);
				
    	HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/xml; charset=utf-8");
        headers.add("SOAPAction", "http://sap.com/xi/WebService/soap1.1");

		String payload = builder.toString();
    	HttpEntity<String> request = new HttpEntity<String>(payload, headers);
		ResponseEntity<String> response;

    	// KMD has some issues, so we might have to try multiple times
    	int tries = 3;
    	do {
    		response = restTemplate.postForEntity(configuration.getModules().getAccountCreation().getOpusHandler().getConvertedUrl(), request, String.class);
			if (response.getStatusCodeValue() != 200) {
				if (--tries >= 0) {
					log.warn("Laes Request - Got responseCode " + response.getStatusCodeValue() + " from service: " + response.getBody());
					
					try {
						Thread.sleep(5000);
					}
					catch (InterruptedException ex) {
						;
					}
				}
				else {
					log.error("Laes Request - Got responseCode " + response.getStatusCodeValue() + " from service: " + response.getBody());
					
					return opusResponse;
				}
			}
			else {
				break;
			}
    	} while (true);

		String responseBody = response.getBody();

		try {
			XmlMapper xmlMapper = new XmlMapper();
			xmlMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

			Envelope envelope = xmlMapper.readValue(responseBody, Envelope.class);
			
			if (envelope.getBody() != null) {
				if (envelope.getBody().getLaesOutput() != null) {
					if (envelope.getBody().getLaesOutput().getStandardRetur() != null) {
						if (envelope.getBody().getLaesOutput().getStandardRetur().getStatusKode() != 0) {
							log.warn("Got error message from OPUS laes operation: " + envelope.getBody().getLaesOutput().getStandardRetur().getStatusKode() + " / " + envelope.getBody().getLaesOutput().getStandardRetur().getFejlbeskedTekst());
							
							return opusResponse;
						}
					}
					else {
						log.error("Got response without status on OPUS laes operation:\n" + responseBody);
						
						return opusResponse;
					}
				}
			}
			else {
				log.error("Got response without body on OPUS laes operation:\n" + responseBody);

				return opusResponse;
			}

			for (Kommunikation k : envelope.getBody().getLaesOutput().getLaesResultat().getRegistrering().getAttributListe().getEgenskaber().getKommunikation()) {
				if ("0001".equals(k.getKommunikationsart())) {
					opusResponse.userId.startDate = k.getGyldighedsstart();
					opusResponse.userId.stopDate = k.getGyldighedsstop();
					opusResponse.userId.value = k.getKommunikationsID();
				}
				else if ("0010".equals(k.getKommunikationsart())) {
					opusResponse.email.startDate = k.getGyldighedsstart();
					opusResponse.email.stopDate = k.getGyldighedsstop();
					opusResponse.email.value = k.getKommunikationsID();
				}
				else if ("9905".equals(k.getKommunikationsart())) {
					opusResponse.itBruger.startDate = k.getGyldighedsstart();
					opusResponse.itBruger.stopDate = k.getGyldighedsstop();
					opusResponse.itBruger.value = k.getKommunikationsID();
				}
			}
			
			return opusResponse;
		}
		catch (Exception ex) {
			log.error("Laes Request - Failed to decode response: " + responseBody, ex);
			
			return opusResponse;
		}
	}
	
	private OpusStatusCodeWrapper callOpusAndDeleteUser(String employeeId, String userId, ExistingOpusValue existingOpusValue) {
		StringBuilder builder = new StringBuilder();

		builder.append(OPUS_SOAP_BEGIN);
		builder.append(OPUS_SOAP_RET_BEGIN);
		
		builder.append(OPUS_SOAP_MODTAGER_REF.replace("{0}", configuration.getModules().getAccountCreation().getOpusHandler().getMunicipalityNumber()));
		builder.append(OPUS_SOAP_MEDARBEJDER_REF.replace("{0}", employeeId));
		
		builder.append(OPUS_SOAP_ATTRIBUTLISTE_BEGIN);

		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", (existingOpusValue.userId.startDate != null) ? existingOpusValue.userId.startDate : LocalDate.now().minusDays(1).toString())
				.replace("{1}", LocalDate.now().toString())
				.replace("{2}", "0001")
				.replace("{3}", (existingOpusValue.userId.value != null) ? existingOpusValue.userId.value : userId));
		
		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", (existingOpusValue.email.startDate != null) ? existingOpusValue.email.startDate : LocalDate.now().minusDays(1).toString())
				.replace("{1}", LocalDate.now().toString())
				.replace("{2}", "0010")
				.replace("{3}", (existingOpusValue.email.value != null) ? existingOpusValue.email.value : getEmail(null, null)));
		
		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", (existingOpusValue.itBruger.startDate != null) ? existingOpusValue.itBruger.startDate : LocalDate.now().minusDays(1).toString())
				.replace("{1}", LocalDate.now().toString())
				.replace("{2}", "9905")
				.replace("{3}", (existingOpusValue.itBruger.value != null) ? existingOpusValue.itBruger.value : "IT Bruger"));

		builder.append(OPUS_SOAP_ATTRIBUTLISTE_END);

		builder.append(OPUS_SOAP_RET_END);
		builder.append(OPUS_SOAP_END);
		String payload = builder.toString();

		return callOpusRet(payload);
	}
	
	private void callOpusAndUpdateEmail(String email, String employeeId) {
		StringBuilder builder = new StringBuilder();

		builder.append(OPUS_SOAP_BEGIN);
		builder.append(OPUS_SOAP_RET_BEGIN);
		
		builder.append(OPUS_SOAP_MODTAGER_REF.replace("{0}", configuration.getModules().getAccountCreation().getOpusHandler().getMunicipalityNumber()));
		builder.append(OPUS_SOAP_MEDARBEJDER_REF.replace("{0}", employeeId));
		
		builder.append(OPUS_SOAP_ATTRIBUTLISTE_BEGIN);

		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", LocalDate.now().toString())
				.replace("{1}", "9999-12-31")
				.replace("{2}", "0010")
				.replace("{3}", email));

		builder.append(OPUS_SOAP_ATTRIBUTLISTE_END);

		builder.append(OPUS_SOAP_RET_END);
		builder.append(OPUS_SOAP_END);
		String payload = builder.toString();

		log.info("Updating " + employeeId + " email to " + email);
		
		callOpusRet(payload);
	}

	private OpusStatusCodeWrapper callOpusAndOrderUser(String employeeId, String userId, String email) {
		StringBuilder builder = new StringBuilder();

		builder.append(OPUS_SOAP_BEGIN);
		builder.append(OPUS_SOAP_RET_BEGIN);
		
		builder.append(OPUS_SOAP_MODTAGER_REF.replace("{0}", configuration.getModules().getAccountCreation().getOpusHandler().getMunicipalityNumber()));
		builder.append(OPUS_SOAP_MEDARBEJDER_REF.replace("{0}", employeeId));
		
		builder.append(OPUS_SOAP_ATTRIBUTLISTE_BEGIN);

		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", LocalDate.now().toString())
				.replace("{1}", "9999-12-31")
				.replace("{2}", "0010")
				.replace("{3}", email));
		
		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", LocalDate.now().toString())
				.replace("{1}", "9999-12-31")
				.replace("{2}", "0001")
				.replace("{3}", userId));
		
		builder.append(OPUS_SOAP_KOMMUNIKATION
				.replace("{0}", LocalDate.now().toString())
				.replace("{1}", "9999-12-31")
				.replace("{2}", "9905")
				.replace("{3}", "IT Bruger"));
		
		builder.append(OPUS_SOAP_ATTRIBUTLISTE_END);

		builder.append(OPUS_SOAP_RET_END);
		builder.append(OPUS_SOAP_END);
		String payload = builder.toString();

		return callOpusRet(payload);
	}

	private OpusStatusCodeWrapper callOpusRet(String payload) {
    	HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/xml; charset=utf-8");
        headers.add("SOAPAction", "http://sap.com/xi/WebService/soap1.1");

    	HttpEntity<String> request = new HttpEntity<String>(payload, headers);
		ResponseEntity<String> response;
		
    	// KMD has some issues, so we might have to try multiple times
    	int tries = 3;
    	do {
    		response = restTemplate.postForEntity(configuration.getModules().getAccountCreation().getOpusHandler().getConvertedUrl(), request, String.class);
			if (response.getStatusCodeValue() != 200) {
				if (--tries >= 0) {
					log.warn("Ret Request - Got responseCode " + response.getStatusCodeValue() + " from service: " + response.getBody());
					
					try {
						Thread.sleep(5000);
					}
					catch (InterruptedException ex) {
						;
					}
				}
				else {
					log.error("Ret Request - Got responseCode " + response.getStatusCodeValue() + " from service: " + response.getBody());
					
					return createWrapper(OpusStatusCode.NETWORK_ERROR, null);
				}
			}
			else {
				break;
			}
    	} while (true);

		String responseBody = response.getBody();		

		try {
			XmlMapper xmlMapper = new XmlMapper();
			xmlMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

			Envelope envelope = xmlMapper.readValue(responseBody, Envelope.class);
			
			if (envelope.getBody() != null) {
				if (envelope.getBody().getRetOutput() != null) {
					if (envelope.getBody().getRetOutput().getStandardRetur() != null) {
						if (envelope.getBody().getRetOutput().getStandardRetur().getStatusKode() != 0) {
							
							// if we can get better error messages, we can do something else in the future, for now just log as warnings
							if (responseBody.contains("Opdatering af kommunikationsinfotype fejlet")) {
								log.warn("Got error message from OPUS ret operation: " + envelope.getBody().getRetOutput().getStandardRetur().getStatusKode() + "\n" + responseBody);
							}
							else {
								log.error("Got error message from OPUS ret operation: " + envelope.getBody().getRetOutput().getStandardRetur().getStatusKode() + "\n" + responseBody);
							}

							return createWrapper(OpusStatusCode.FAILURE, envelope.getBody().getRetOutput().getStandardRetur().getFejlbeskedTekst());
						}
					}
					else {
						log.error("Got response without status on OPUS ret operation:\n" + responseBody);

						return createWrapper(OpusStatusCode.FAILURE, "Ingen status i svar fra OPUS");
					}
				}
			}
			else {
				log.error("Got response without body on OPUS ret operation:\n" + responseBody);
				
				return createWrapper(OpusStatusCode.FAILURE, "Intet svar fra OPUS");
			}
		}
		catch (Exception ex) {
			log.error("Failed to parse OPUS rest operation response\n" + responseBody, ex);
			
			return createWrapper(OpusStatusCode.FAILURE, "Uventet svar fra OPUS");
		}

		return createWrapper(OpusStatusCode.OK, null);
	}

	private OpusStatusCodeWrapper createWrapper(OpusStatusCode status, String message) {
		OpusStatusCodeWrapper wrapper = new OpusStatusCodeWrapper();
		wrapper.status = status;
		wrapper.message = message;

		return wrapper;
	}
}
