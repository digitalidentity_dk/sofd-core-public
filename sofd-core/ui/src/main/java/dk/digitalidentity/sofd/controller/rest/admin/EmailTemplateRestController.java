package dk.digitalidentity.sofd.controller.rest.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.htmlcleaner.BrowserCompactXmlSerializer;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.controller.mvc.dto.InlineImageDTO;
import dk.digitalidentity.sofd.controller.rest.admin.model.AttachmentDTO;
import dk.digitalidentity.sofd.controller.rest.admin.model.EmailTemplateDTO;
import dk.digitalidentity.sofd.dao.model.Attachment;
import dk.digitalidentity.sofd.dao.model.AttachmentFile;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.security.RequireAdminAccess;
import dk.digitalidentity.sofd.security.SecurityUtil;
import dk.digitalidentity.sofd.service.EmailService;
import dk.digitalidentity.sofd.service.EmailTemplateService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import dk.digitalidentity.sofd.service.UserService;
import lombok.extern.log4j.Log4j;

@Log4j
@RequireAdminAccess
@RestController
public class EmailTemplateRestController {

	@Autowired
	private EmailTemplateService emailTemplateService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PersonService personService;
	
	@DeleteMapping("/rest/mailtemplates/attachment/{templateId}/{attachmentId}")
	public ResponseEntity<Long> uploadAttachment(@PathVariable("templateId") Long templateId, @PathVariable("attachmentId") Long attachmentId) {
		EmailTemplate template = emailTemplateService.findById(templateId);
		if (template == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		boolean changes = false;
		for (Iterator<Attachment> iterator = template.getAttachments().iterator(); iterator.hasNext();) {
			Attachment attachment = iterator.next();
			
			if (attachment.getId() == attachmentId) {
				iterator.remove();
				changes = true;
				break;
			}
		}
		
		if (changes) {
			emailTemplateService.save(template);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/rest/mailtemplates/attachment")
	public ResponseEntity<Long> uploadAttachment(@ModelAttribute AttachmentDTO attachment) {
		long id = 0;

		try {
			EmailTemplate template = emailTemplateService.findById(attachment.getTemplateId());
			if (template == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			String filename = attachment.getFile().getOriginalFilename();
			if (StringUtils.isEmpty(filename)) {
				log.warn("No file supplied!");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
			InputStream is = attachment.getFile().getInputStream();
			byte[] content = IOUtils.toByteArray(is);
			if (content == null || content.length == 0) {
				log.warn("No content supplied!");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
			Attachment a = new Attachment();
			a.setEmailTemplate(template);
			a.setFilename(filename);
			a.setFile(new AttachmentFile());
			a.getFile().setContent(content);
			
			template.getAttachments().add(a);
			template = emailTemplateService.save(template);

			// bit of a hack, using the filename... but it seems our only choice when cascade-saving
			for (Attachment att : template.getAttachments()) {
				if (att.getFilename().equals(a.getFilename())) {
					id = att.getId();
				}
			}
		}
		catch (Exception ex) {
			log.error("Bad file", ex);
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	@PostMapping(value = "/rest/mailtemplates")
	@ResponseBody
	public ResponseEntity<String> updateTemplate(@RequestBody EmailTemplateDTO emailTemplateDTO, @RequestParam("tryEmail") boolean tryEmail) {
		toXHTML(emailTemplateDTO);
		
		if (tryEmail) {
			User user = userService.findByUserIdAndUserType(SecurityUtil.getUser(), SupportedUserTypeService.getActiveDirectoryUserType());

			if (user != null) {
				Person person = personService.findByUser(user);
				
				Optional<User> oUser = person.getUsers().stream()
							.filter(u -> u.getUserType().equals(SupportedUserTypeService.getExchangeUserType()) && u.isPrime())
							.findFirst();
				
				String email = oUser.isPresent() ? oUser.get().getUserId() : null;
				if (email != null) {
					List<InlineImageDTO> inlineImages = transformImages(emailTemplateDTO);
					EmailTemplate template = emailTemplateService.findById(emailTemplateDTO.getId());
					if (template.getAttachments() != null && template.getAttachments().size() > 0) {
						template.forceLoadAttachments();
						emailService.sendMessageWithAttachments(email, emailTemplateDTO.getTitle(), emailTemplateDTO.getMessage(), template.getAttachments(), inlineImages);
					}
					else {
						emailService.sendMessage(email, emailTemplateDTO.getTitle(), emailTemplateDTO.getMessage(), inlineImages);
					}
					
					return new ResponseEntity<>("Test email sent til " + email, HttpStatus.OK);
				}
			}
			
			return new ResponseEntity<>("Du har ingen email adresse registreret!", HttpStatus.CONFLICT);
		}
		else {
			EmailTemplate template = emailTemplateService.findById(emailTemplateDTO.getId());
			if (template == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			template.setMinutesDelay(emailTemplateDTO.getMinutesDelay());
			template.setMessage(emailTemplateDTO.getMessage());
			template.setTitle(emailTemplateDTO.getTitle());
			template.setEnabled(emailTemplateDTO.isEnabled());
			template.setRecipients(emailTemplateDTO.getRecipients());
			emailTemplateService.save(template);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	private List<InlineImageDTO> transformImages(EmailTemplateDTO emailTemplateDTO) {
		List<InlineImageDTO> inlineImages = new ArrayList<>();
		String message = emailTemplateDTO.getMessage();
		Document doc = Jsoup.parse(message);
		int counter = 1;

		for (Element img : doc.select("img")) {
			String src = img.attr("src");
			if (src == null || src == "") {
				continue;
			}
			
			String filename = img.attr("data-filename");
			if (filename == null || filename == "") {
				filename = "file" + counter;
				counter ++;
			}

			InlineImageDTO inlineImageDto = new InlineImageDTO();
			inlineImageDto.setBase64(src.contains("base64"));
			
			if (inlineImageDto.isBase64()) {
				inlineImageDto.setUrl(false);
			}
			else {
				inlineImageDto.setUrl(true);
			}
			
			inlineImageDto.setCid(filename);
			inlineImageDto.setSrc(src);
			inlineImages.add(inlineImageDto);
			img.attr("src", "cid:" + filename);
		}

		emailTemplateDTO.setMessage(doc.html());
		
		return inlineImages;
	}

	/**
	 * summernote does not generate valid XHTML. At least the <br/> and <img/> tags are not closed,
	 * so we need to close them, otherwise our PDF processing will fail.
	 */
	private void toXHTML(EmailTemplateDTO emailTemplateDTO) {
		String message = emailTemplateDTO.getMessage();
		if (message != null) {
			try {
				CleanerProperties properties = new CleanerProperties();
				properties.setOmitXmlDeclaration(true);
				TagNode tagNode = new HtmlCleaner(properties).clean(message);
			
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				new BrowserCompactXmlSerializer(properties).writeToStream(tagNode, bos);
	
				emailTemplateDTO.setMessage(new String(bos.toByteArray(), Charset.forName("UTF-8")));
			}
			catch (IOException ex) {
				log.error("could not parse: " + emailTemplateDTO.getMessage());
			}
		}
	}
}
