package dk.digitalidentity.sofd.dao.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Person;

@Projection(name = "grid", types = { Affiliation.class })
public interface GridOrgUnitAffiliation {
	String getPositionName();
	
	@Value("#{target.getPerson().getChosenName() != null ? target.getPerson().getChosenName() : target.getPerson().getFirstname() + \" \" + target.getPerson().getSurname()}")
	String getName();

	@JsonIgnore
	Person getPerson();
}
