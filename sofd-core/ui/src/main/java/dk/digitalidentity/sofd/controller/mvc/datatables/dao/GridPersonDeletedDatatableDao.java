package dk.digitalidentity.sofd.controller.mvc.datatables.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.controller.mvc.datatables.dao.model.GridPersonDeleted;

@RepositoryRestResource(exported=false)
public interface GridPersonDeletedDatatableDao extends DataTablesRepository<GridPersonDeleted, String> {

}