package dk.digitalidentity.sofd.dao.model;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import dk.digitalidentity.sofd.dao.model.enums.PhoneType;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "function_types")
@Getter
@Setter
@Audited
public class FunctionType {

    @Id
    @GeneratedValue()
    private long id;

    @Column
    @NotNull
    private String name;

    @ElementCollection(targetClass = PhoneType.class)
    @CollectionTable(name = "function_type_constraints", joinColumns = @JoinColumn(name = "function_type_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "phone_type")
    private List<PhoneType> phoneTypes;
}
