package dk.digitalidentity.sofd.controller.mvc;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import dk.digitalidentity.sofd.config.RoleConstants;
import dk.digitalidentity.sofd.controller.mvc.dto.AffiliationDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.CommentDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.CreatePersonDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.FlaggedUserType;
import dk.digitalidentity.sofd.controller.mvc.dto.PersonDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.PostDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.PrimeUserDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.SimpleAffiliationDTO;
import dk.digitalidentity.sofd.controller.mvc.dto.SupportedUserTypeSlimDTO;
import dk.digitalidentity.sofd.controller.validation.AffiliationDTOValidator;
import dk.digitalidentity.sofd.controller.validation.CreatePersonDTOValidator;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.RevisionId;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationType;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.security.RequireReadAccess;
import dk.digitalidentity.sofd.security.SecurityUtil;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.CommentService;
import dk.digitalidentity.sofd.service.FunctionTypeService;
import dk.digitalidentity.sofd.service.OrgUnitService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import lombok.extern.log4j.Log4j;

@RequireReadAccess
@Controller
@Log4j
public class PersonController {
	
	@Autowired
	private PersonService personService;

	@Autowired
	private CreatePersonDTOValidator createPersonDTOValidator;
	
	@Autowired
	private AffiliationDTOValidator affiliationDTOValidator;

	@Autowired
	private OrgUnitService orgUnitService;

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private RepositoryRestMvcConfiguration rrmc;

	@Autowired
	private FunctionTypeService functionTypeService;

	@Autowired
	private SupportedUserTypeService userTypeService;
	
	@Autowired
	private AccountOrderService accountOrderService;

	@Autowired
	private SupportedUserTypeService supportedUserTypeService;
		
	@InitBinder("createPersonDTO")
	public void initClientBinder(WebDataBinder binder) {
		binder.setValidator(createPersonDTOValidator);
	}
	
	@InitBinder("affiliationDTO")
	public void initAffiliationBinder(WebDataBinder binder) {
		binder.setValidator(affiliationDTOValidator);
	}

	@RequireControllerWriteAccess
	@GetMapping("/ui/person/new")
	public String newPerson(Model model) {
		CreatePersonDTO person = CreatePersonDTO.builder()
				.affiliation(AffiliationDTO.builder().affiliationType(AffiliationType.EMPLOYEE).build())
				.build();

		model.addAttribute("createPersonDTO", person);
		model.addAttribute("ous", orgUnitService.getAllTree());

		return "person/new";
	}

	@RequireControllerWriteAccess
	@PostMapping("/ui/person/new")
	public String createPerson(Model model, @Valid @ModelAttribute("createPersonDTO") CreatePersonDTO createPersonDTO, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			model.addAttribute(bindingResult.getAllErrors());
			model.addAttribute("createPersonDTO", createPersonDTO);
			model.addAttribute("ous", orgUnitService.getAllTree());

			return "person/new";
		}

		Person person = new Person();
		person.setCpr(createPersonDTO.getCpr());
		person.setUuid(UUID.randomUUID().toString());
		person.setMaster("SOFD");
		person.setFirstname(createPersonDTO.getFirstname());
		person.setSurname(createPersonDTO.getSurname());
		person.setLastChanged(new Date());
		
		if (!StringUtils.isEmpty(createPersonDTO.getRegisteredPostAddress()) &&
			!StringUtils.isEmpty(createPersonDTO.getRegisteredPostAddress().getStreet()) &&
			!StringUtils.isEmpty(createPersonDTO.getRegisteredPostAddress().getPostalCode()) &&
			!StringUtils.isEmpty(createPersonDTO.getRegisteredPostAddress().getCity())) {

			Post post = new Post();
			post.setAddressProtected(createPersonDTO.getRegisteredPostAddress().isAddressProtected());
			post.setCity(createPersonDTO.getRegisteredPostAddress().getCity());
			post.setCountry(createPersonDTO.getRegisteredPostAddress().getCountry());
			post.setLocalname(createPersonDTO.getRegisteredPostAddress().getLocalname());
			post.setMaster("SOFD");
            post.setMasterId(UUID.randomUUID().toString());
			post.setPostalCode(createPersonDTO.getRegisteredPostAddress().getPostalCode());
			post.setPrime(true);
			post.setStreet(createPersonDTO.getRegisteredPostAddress().getStreet());
	
			person.setRegisteredPostAddress(post);
		}
		
		Affiliation aff = addAffiliationFromDTO(createPersonDTO.getAffiliation(), person);

		person = personService.save(person);
		
		long affiliationId = 0;
		for (Affiliation affiliation : person.getAffiliations()) {
			if (affiliation.getMasterId().equals(aff.getMasterId())) {
				affiliationId = affiliation.getId();
				break;
			}
		}

		return "redirect:/ui/person/new/" + person.getUuid() + "/" + affiliationId;
	}

	@GetMapping("/ui/person/new/{uuid}/{affiliationId}")
	public String newReceipt(Model model, @PathVariable("uuid") String uuid, @PathVariable("affiliationId") long affiliationId) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return "redirect:/ui/person/";
		}

		String orgUnitName = "";
		String positionName = "";
		Affiliation affiliation = null;
		if (person.getAffiliations() != null && person.getAffiliations().size() > 0) {
			for (Affiliation aff : person.getAffiliations()) {
				if (aff.getId() == affiliationId) {
					positionName = aff.getPositionName();
					orgUnitName = aff.getOrgUnit().getName();
					affiliation = aff;
				}
			}
		}
		
		List<FlaggedUserType> flaggedUserTypes = new ArrayList<>();
		for (SupportedUserType userType : userTypeService.findAll()) {
			if (!userType.isCanOrder()) {
				continue;
			}

			boolean flagged = accountOrderService.shouldOrderAccountOfType(userType.getKey(), affiliation, 0, false);

			FlaggedUserType flaggedUserType = new FlaggedUserType();
			flaggedUserType.setFlagged(flagged);
			flaggedUserType.setUserType(userType.getKey());
			flaggedUserType.setUserTypeName(userType.getName());

			flaggedUserTypes.add(flaggedUserType);
		}
		
		model.addAttribute("name", person.getFirstname() + " " + person.getSurname());
		model.addAttribute("orgUnitName", orgUnitName);
		model.addAttribute("positionName", positionName);
		model.addAttribute("userTypes", flaggedUserTypes);
		model.addAttribute("uuid", uuid);

		return "person/receipt";
	}

	@GetMapping("/ui/person")
	public String list(Model model) {
		return "person/list";
	}
	
	@GetMapping("/ui/person/core/{uuid}/{type}")
	public String getCoreFragment(Model model, @PathVariable("uuid") String uuid, @PathVariable("type") String type) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("No person with uuid: " + uuid);
			return "redirect:/ui/person";
		}

		model.addAttribute("person", person);

		if (type.equals("edit")) {			
			model.addAttribute("person", person);

			return "person/fragments/person_core_edit :: personCoreEdit";
		}

		return "person/fragments/person_core_view :: personCoreView";
	}

	@GetMapping("/ui/person/view/{uuid}")
	public String view(Model model, @PathVariable("uuid") String uuid, @RequestParam(required = false, value = "backRef") String backRef) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return "redirect:/ui/person/";
		}

		PersonDTO personDTO = toDTO(person);
		List<SimpleAffiliationDTO> affiliations = new ArrayList<SimpleAffiliationDTO>();

		for (Affiliation affiliation : AffiliationService.onlyActiveAffiliations(person.getAffiliations())) {
			SimpleAffiliationDTO dto = new SimpleAffiliationDTO(affiliation);
			affiliations.add(dto);
		}

		Set<String> personUserTypes = person.getUsers().stream().map(u -> u.getUserType()).collect(Collectors.toSet());
		List<SupportedUserTypeSlimDTO> supportedUserTypes = supportedUserTypeService.findAll().stream()
				.filter(u -> u.isCanOrder() && personUserTypes.contains(u.getKey()))
				.map(u -> new SupportedUserTypeSlimDTO(u))
				.collect(Collectors.toList());

		model.addAttribute("supportedUserTypes", supportedUserTypes);
		model.addAttribute("person", personDTO);
		model.addAttribute("affiliations", affiliations);
		model.addAttribute("phones", person.getPhones());
		model.addAttribute("users", person.getUsers().stream().map(u -> new PrimeUserDTO(u)).collect(Collectors.toList()));
		model.addAttribute("allUserTypes", userTypeService.findAll());
		model.addAttribute("functionTypes", functionTypeService.findAll());
		model.addAttribute("backRef", backRef);
		model.addAttribute("showChoosePrimeAD", personDTO.getUsers().stream().filter(user -> SupportedUserTypeService.isActiveDirectory(user.getUserType())).count() > 1);
		model.addAttribute("today", LocalDate.now().toString());

		if (SecurityUtil.getUserRoles().contains(RoleConstants.MODULE_ROLE_ACCOUNT_CREATION)) {
			List<SupportedUserType> userTypes = new ArrayList<>();
			long affiliationCount = person.getAffiliations().size();
			long opusAffiliationCount = person.getAffiliations().stream().filter(a -> "OPUS".equals(a.getMaster())).count();

			// only show userTypes that can be ordered, and where any affiliation requirements are fulfilled,
			// meaning that when singleUserMode is disabled, we need at least one affiliation (to link with), and
			// in the case of OPUS accounts, we need the OPUS account to link with
			userTypes = supportedUserTypeService.findAll().stream()
					.filter(u -> u.isCanOrder())
					.filter(u ->
								u.isSingleUserMode() ||
								(!SupportedUserTypeService.isOpus(u.getKey()) && affiliationCount > 0) ||
								(SupportedUserTypeService.isOpus(u.getKey()) && opusAffiliationCount > 0))
					.collect(Collectors.toList());

			model.addAttribute("userTypes", userTypes);
			model.addAttribute("stoppableAffiliations", person.getAffiliations().stream().filter(a -> a.isDeleted() == false && !AffiliationService.notActiveAnymore(a)).collect(Collectors.toList()));
		}
		else {
			model.addAttribute("userTypes", new ArrayList<SupportedUserType>());
			model.addAttribute("stoppableAffiliations", new ArrayList<Affiliation>());
		}
		
		return "person/view";
	}
	
	@GetMapping("/ui/person/phoneTab/{uuid}")
	public String getPhoneTab(Model model, @PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("No Person with uuid: " + uuid);
			return "fragments/phoneTab :: phoneTab";
		}

		model.addAttribute("phones", person.getPhones());
		return "fragments/phoneTab :: phoneTab";
	}
	

	@GetMapping("/ui/person/phonePrime/{uuid}")
	public String getPhonePrime(Model model, @PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("No Person with uuid: " + uuid);
			return "fragments/phoneTab :: phonePrimeModal";
		}

		model.addAttribute("phones", person.getPhones());
		return "fragments/phoneTab :: phonePrimeModal";
	}


	@RequireControllerWriteAccess
	@GetMapping(path = {"/ui/person/affiliation/{personUUID}"})
	public String orderNewUser(Model model, @PathVariable String personUUID) {
		AffiliationDTO affiliationDTO = new AffiliationDTO();
		affiliationDTO.setAffiliationType(AffiliationType.EMPLOYEE);

		model.addAttribute("affiliationDTO", affiliationDTO);
		model.addAttribute("personUUID", personUUID);
		model.addAttribute("ous", orgUnitService.getAllTree());

		return "person/new_affiliation";
	}

	@RequireControllerWriteAccess
	@PostMapping(path = {"/ui/person/affiliation"})
	public String createNewAffiliation(Model model, @ModelAttribute("personUUID") String personUUID, @Valid @ModelAttribute("affiliationDTO") AffiliationDTO affiliationDTO, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			model.addAttribute(bindingResult.getAllErrors());
			model.addAttribute("affiliationDTO", affiliationDTO);
			model.addAttribute("personUUID", personUUID);
			model.addAttribute("ous", orgUnitService.getAllTree());

			return "person/new_affiliation";			
		}
		
		Person person = personService.getByUuid(personUUID);
		if (person == null) {
			log.warn("Could not find person with uuid " + personUUID + " while assigning new affiliation");
			return "redirect:/ui/person/view/" + personUUID;
		}

		Affiliation aff = addAffiliationFromDTO(affiliationDTO, person);

		person = personService.save(person);

		long affiliationId = 0;
		for (Affiliation affiliation : person.getAffiliations()) {
			if (affiliation.getMasterId().equals(aff.getMasterId())) {
				affiliationId = affiliation.getId();
				break;
			}
		}

		return "redirect:/ui/person/new/" + person.getUuid() + "/" + affiliationId;
	}

	@GetMapping("/ui/person/contacts/{uuid}/{type}")
	public String getContactInfoTab(Model model, @PathVariable("uuid") String uuid, @PathVariable("type") String type) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("No Person with uuid: " + uuid);

			return "person/fragments/viewKeywordsTab :: keywordsTab";
		}

		model.addAttribute("keyWords", person.getKeyWords());
		model.addAttribute("notes", person.getNotes());

		if (type.equals("edit")) {
			return "person/fragments/editKeywordsTab :: keywordsTab";
		}

		return "person/fragments/viewKeywordsTab :: keywordsTab";
	}

	@GetMapping("/ui/person/revision/list/{uuid}")
	public String personHistory(Model model, @PathVariable("uuid") String uuid) {
		List<RevisionId> revs = personService.getRevisionIds(uuid);
		model.addAttribute("revisions", revs);
		model.addAttribute("uuid", uuid);

		return "person/revision_list";
	}
	
	private ObjectMapper getSpringDataRestObjectMapper() {
		return rrmc.objectMapper();
	}

	@GetMapping("/ui/person/revision/download/{uuid}/{revId}")
	public ResponseEntity<byte[]> downloadPersonHistory(@PathVariable("uuid") String uuid, @PathVariable("revId") Integer revId) {
		String json = "[]";

		Revision<Integer,Person> revision = personService.findRevision(uuid, revId);
		if (revision != null) {
			try {
				ObjectMapper mapper = getSpringDataRestObjectMapper();
				mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS); // ensures backwards compatibility with old data

				revision.getEntity().setCpr(PersonService.maskCpr(revision.getEntity().getCpr())); // mask CPR
				json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(revision.getEntity());
			}
			catch (JsonProcessingException ex) {
				log.error("Error occured while parsing Person to JSON string. " + ex.getMessage());
			}
		}

		byte[] output = json.getBytes(Charset.forName("UTF-8"));

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("charset", "utf-8");
		responseHeaders.setContentType(MediaType.valueOf("application/json"));
		responseHeaders.setContentLength(output.length);
		responseHeaders.set("Content-disposition", "attachment; filename=" + uuid + ".json");

		return new ResponseEntity<byte[]>(output, responseHeaders, HttpStatus.OK);
	}

	@GetMapping("/ui/person/comments/{uuid}")
	public String getCommentsTab(Model model, @PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			log.warn("No Person with uuid: " + uuid);

			return "person/fragments/viewCommentTab :: commentTab";
		}

		List<CommentDTO> comments = commentService.findByPersonUuid(uuid).stream()
				.map(c -> new CommentDTO(c))
				.sorted((o1, o2) -> (o1.getTimestamp().after(o2.getTimestamp())) ? -1 : 1)
				.collect(Collectors.toList());

		model.addAttribute("comments", comments);

		return "person/fragments/viewCommentTab :: commentTab";
	}

	private Affiliation addAffiliationFromDTO(AffiliationDTO affiliationDTO, Person person) {
		Affiliation affiliation = new Affiliation();
		affiliation.setUuid(UUID.randomUUID().toString());
		affiliation.setMaster("SOFD");
		affiliation.setMasterId(UUID.randomUUID().toString());
		affiliation.setOrgUnit(orgUnitService.getByUuid(affiliationDTO.getOrgUnitUuid()));
		affiliation.setPerson(person);
		affiliation.setStartDate(getToday());
		affiliation.setPositionName(affiliationDTO.getPositionName());
		affiliation.setAffiliationType(affiliationDTO.getAffiliationType());
		
		if (affiliationDTO.getAffiliationType().equals(AffiliationType.EXTERNAL)) {
			affiliation.setInheritPrivileges(affiliationDTO.isInheritPrivilegesFromOU());
		}

		if (!StringUtils.isEmpty(affiliationDTO.getStopDate())) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			try {
				Date stopDate = formatter.parse(affiliationDTO.getStopDate());
				affiliation.setStopDate(stopDate);
			}
			catch (ParseException ex) {
				log.warn("Failed to parse: " + affiliationDTO.getStopDate());
			}
		}
		
		if (person.getAffiliations() == null) {
			person.setAffiliations(new ArrayList<>());
		}
		person.getAffiliations().add(affiliation);
		
		return affiliation;
	}
	
	private static Date getToday() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		
		return cal.getTime();
	}

	private PersonDTO toDTO(Person person) {
		PersonDTO personDTO = PersonDTO.builder()
				.uuid(person.getUuid())
				.cpr(PersonService.maskCpr(person.getCpr()))
				.master(person.getMaster())
				.firstname(person.getFirstname())
				.surname(person.getSurname())
				.chosenName(person.getChosenName())
				.firstEmploymentDate(person.getFirstEmploymentDate())
				.anniversaryDate(person.getAnniversaryDate())
				.created(person.getCreated())
				.lastChanged(person.getLastChanged())
				.affiliations(person.getAffiliations())
				.users(person.getUsers())
				.keyWords(person.getKeyWords())
				.notes(person.getNotes())
				.taxedPhone(person.isTaxedPhone())
				.forceStop(person.isForceStop())
				.disableAccountOrders(person.isDisableAccountOrders())
				.phones(person.getPhones())
				.leave(person.getLeave() != null)
				.leaveReason((person.getLeave() != null) ? person.getLeave().getReason() : null)
				.leaveReasonText((person.getLeave() != null) ? person.getLeave().getReasonText() : null)
				.leaveStartDate((person.getLeave() != null) ? person.getLeave().getStartDate() : null)
				.leaveStopDate((person.getLeave() != null) ? person.getLeave().getStopDate() : null)
				.leaveDeactivateAccounts((person.getLeave() != null) ? person.getLeave().isDeactivateAccounts() : false)
				.leaveDisableAccountOrders((person.getLeave() != null) ? person.getLeave().isDisableAccountOrders() : false)
				.leaveAccountOrdersToDeactivate((person.getLeave() != null) ? person.getLeave().getAccountsToDeactivate() : null)
				.build();

		Post regAddr = person.getRegisteredPostAddress();
		if (regAddr != null) {
			personDTO.setRegisteredPostAddress(PostDTO.builder()
					.street(regAddr.getStreet())
					.localname(regAddr.getLocalname())
					.postalCode(regAddr.getPostalCode())
					.city(regAddr.getCity())
					.country(regAddr.getCountry())
					.addressProtected(regAddr.isAddressProtected())
					.master(regAddr.getMaster())
					.build());
		}

		Post resAddr = person.getResidencePostAddress();
		if (resAddr != null) {
			personDTO.setResidencePostAddress(PostDTO.builder()
					.street(resAddr.getStreet())
					.localname(resAddr.getLocalname())
					.postalCode(resAddr.getPostalCode())
					.city(resAddr.getCity())
					.country(resAddr.getCountry())
					.addressProtected(resAddr.isAddressProtected())
					.master(resAddr.getMaster())
					.build());
		}

		// add pending UserAccount orders to the list of users
		List<AccountOrder> accountOrders = accountOrderService.getPendingCreateOrdersForPerson(person.getUuid());
		for (AccountOrder accountOrder : accountOrders) {
			User user = new User();
			user.setUserId("(afventer oprettelse)");
			user.setUserType(accountOrder.getUserType());
			
			if (personDTO.getUsers() == null) {
				personDTO.setUsers(new ArrayList<>());
			}

			personDTO.getUsers().add(user);
		}
		
		return personDTO;
	}
}
