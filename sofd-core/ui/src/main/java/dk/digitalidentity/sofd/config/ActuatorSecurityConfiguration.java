package dk.digitalidentity.sofd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@Order(99) // the default security configuration has order 100
public class ActuatorSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private SofdConfiguration configuration;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User
				.withUsername(configuration.getActuator().getUsername())
				.password(configuration.getActuator().getPassword())
				.roles("ACTUATOR", "ADMIN")
				.build());

		http.authorizeRequests().antMatchers("/manage/health")
			.permitAll();

		http.antMatcher("/manage/**")
			.authorizeRequests()
			.anyRequest()
			.hasRole("ACTUATOR").and()
			.httpBasic().and().userDetailsService(manager);
	}
}