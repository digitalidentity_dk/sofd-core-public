package dk.digitalidentity.sofd.dao.model.enums;

public enum EntityType {
	ORGANISATION, ORGUNIT, PERSON, CLIENT, REPORT
}
