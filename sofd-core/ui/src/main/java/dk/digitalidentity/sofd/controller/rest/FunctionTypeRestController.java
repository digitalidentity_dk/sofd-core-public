package dk.digitalidentity.sofd.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.controller.mvc.dto.FunctionTypeDTO;
import dk.digitalidentity.sofd.dao.FunctionTypeDao;
import dk.digitalidentity.sofd.dao.model.FunctionType;
import lombok.extern.log4j.Log4j;

@RestController
@Log4j
public class FunctionTypeRestController {

	@Autowired
	private FunctionTypeDao functionTypeDao;

	@PostMapping("/rest/functiontype/edit")
	@ResponseBody
	public ResponseEntity<String> editCategory(@RequestBody FunctionTypeDTO functionTypeDTO) {
		FunctionType functionType = new FunctionType();

		if (functionTypeDTO.getId() > 0) {
			functionType = functionTypeDao.findById(functionTypeDTO.getId());

			if (functionType == null) {
				log.warn("Requested Function Type with ID:" + functionTypeDTO.getId() + " not found.");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		functionType.setName(functionTypeDTO.getName());
		functionType.setPhoneTypes(functionTypeDTO.getPhoneTypes());

		functionTypeDao.save(functionType);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/rest/functiontype/delete")
	@ResponseBody
	public ResponseEntity<String> deleteCategory(@RequestBody FunctionTypeDTO functionTypeDTO) {
		FunctionType functionType = functionTypeDao.findById(functionTypeDTO.getId());
		if (functionType == null) {
			log.warn("Requested Function Type with ID:" + functionTypeDTO.getId() + " not found.");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		functionTypeDao.delete(functionType);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
