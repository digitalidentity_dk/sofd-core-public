package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SmsGateway {
	private boolean enabled = false;
	private boolean pwdReminderEnabled = false;
	
	@JsonIgnore
	private String url = "";
}
