package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.Comment;

@RepositoryRestResource(exported = false)
public interface CommentDao extends CrudRepository<Comment, Long> {
	List<Comment> findByPersonUuid(String personUuid);
}