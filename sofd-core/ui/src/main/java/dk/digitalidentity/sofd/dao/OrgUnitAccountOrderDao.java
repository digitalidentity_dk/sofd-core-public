package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.OrgUnitAccountOrder;

@RepositoryRestResource(exported = false)
public interface OrgUnitAccountOrderDao extends CrudRepository<OrgUnitAccountOrder, Long> {
	List<OrgUnitAccountOrder> findAll();
	OrgUnitAccountOrder findByOrgunitUuid(String uuid);
}
