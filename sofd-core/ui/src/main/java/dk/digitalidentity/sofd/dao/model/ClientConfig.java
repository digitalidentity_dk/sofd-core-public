package dk.digitalidentity.sofd.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "client_config")
public class ClientConfig {

	@Id
	@GeneratedValue
	private long id;

	@Column
	private long clientId;

	@Column
	private String clientName;

	@Column
	private byte[] configuration;

	@Column
	private Date lastChanged;
}
