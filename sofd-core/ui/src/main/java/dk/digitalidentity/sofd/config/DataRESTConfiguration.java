package dk.digitalidentity.sofd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Organisation;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.validator.OrgUnitValidator;
import dk.digitalidentity.sofd.dao.model.validator.PersonValidator;
import dk.digitalidentity.sofd.interceptor.AbstractBeforeSaveInterceptor;
import dk.digitalidentity.sofd.interceptor.BeforeSDRSaveInterceptor;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;

@Configuration
public class DataRESTConfiguration extends RepositoryRestConfigurerAdapter {

	@Autowired
	private SupportedUserTypeService userTypeService;
	
	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Person.class, OrgUnit.class, Organisation.class);
	}

	@Bean
	public Validator validator() {
		return new LocalValidatorFactoryBean();
	}

	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		Validator validator = validator();

		// bean validation always before save and create
		validatingListener.addValidator("beforeCreate", validator);
		validatingListener.addValidator("beforeSave", validator);
		validatingListener.addValidator("BeforeLinkSave", validator);
		validatingListener.addValidator("BeforeDelete", validator);

		// special validation rules for Person
		PersonValidator personValidator = new PersonValidator(userTypeService);
		validatingListener.addValidator("beforeCreate", personValidator);
		validatingListener.addValidator("beforeSave", personValidator);
		validatingListener.addValidator("BeforeLinkSave", personValidator);
		
		// special validation rules for OrgUnit
		OrgUnitValidator orgUnitValidator = new OrgUnitValidator();
		validatingListener.addValidator("beforeCreate", orgUnitValidator);
		validatingListener.addValidator("beforeSave", orgUnitValidator);
		validatingListener.addValidator("BeforeLinkSave", orgUnitValidator);
	}

	// TODO: is this @Bean definition even needed anymore, as we do not reference it
	//       and it is annotated with @RepositoryEventHandler - do we actually
	//       get two instances like this (one which is dead I hope ;))
	@Bean
	public BeforeSDRSaveInterceptor uuidUpdateInterceptor(AbstractBeforeSaveInterceptor interceptor) {
		return new BeforeSDRSaveInterceptor(interceptor);
	}
}