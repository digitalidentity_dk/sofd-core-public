package dk.digitalidentity.sofd.dao.model.enums;

public enum AffiliationType {
	EMPLOYEE("html.enum.AffiliationType.employee"),
	EXTERNAL("html.enum.AffiliationType.external");

	private String message;

	private AffiliationType(String message) { this.message = message; }

	public String getMessage() {
		return message;
	}
}
