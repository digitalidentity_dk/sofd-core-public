package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.Client;

@RepositoryRestResource(exported=false)
public interface ClientDao extends CrudRepository<Client, Long> {
	List<Client> findAll();
	Client findByApiKey(String apiKey);
	Client findByName(String name);
	Client findById(long id);
}