package dk.digitalidentity.sofd.dao.model.projection.processer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.projection.GridPersonAffiliation;

@Component
public class GridPersonAffiliationResourceProcessor implements ResourceProcessor<Resource<GridPersonAffiliation>> {

	@Autowired
	private EntityLinks entityLinks;

	@Override
	public Resource<GridPersonAffiliation> process(Resource<GridPersonAffiliation> resource) {
		GridPersonAffiliation content = resource.getContent();
		OrgUnit orgUnit = content.getOrgUnit();

		resource.add(entityLinks.linkToSingleResource(OrgUnit.class, orgUnit.getUuid()));

		return resource;
	}
}
