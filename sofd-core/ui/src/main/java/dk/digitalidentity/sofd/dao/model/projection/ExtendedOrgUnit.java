package dk.digitalidentity.sofd.dao.model.projection;

import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.Email;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitManager;
import dk.digitalidentity.sofd.dao.model.OrgUnitType;
import dk.digitalidentity.sofd.dao.model.Organisation;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;

@Projection(name = "extended", types = { OrgUnit.class })
public interface ExtendedOrgUnit {
	
	// all existing fields (minus affiliations)
	
	String getUuid();
	String getMaster();
	String getMasterId();
	boolean isDeleted();
	Date getCreated();
	Date getLastChanged();
	String getShortname();
	String getName();
	Long getCvr();
	Long getEan();
	Long getSenr();
	Long getPnr();
	String getCostBearer();
	OrgUnitType getType();
	String getOrgType();
	Long getOrgTypeId();
	List<Post> getPostAddresses();
	List<Phone> getPhones();
	List<Email> getEmails();
	OrgUnitManager getManager();
	List<String> getKlePrimary();
	List<String> getKleSecondary();
	String getKeyWords();
	String getNotes();
	String getOpeningHours();
	
	
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	String getLocalExtensions();

	// just documentation to make our extension-projection test happy
	@JsonIgnore
	List<OrgUnit> getChildren();
	
	// just documentation to make our extension-projection test happy
	@JsonIgnore
	OrgUnit getParent();
	
	@JsonIgnore
	Organisation getBelongsTo();
	
	// just documentation to make our extension-projection test happy
	@JsonIgnore
	OrgUnitManager getNewManager();
	
	// affiliations are extended
	List<ExtendedAffiliation> getAffiliations();
}
