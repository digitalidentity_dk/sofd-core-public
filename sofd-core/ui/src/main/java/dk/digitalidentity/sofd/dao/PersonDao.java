package dk.digitalidentity.sofd.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.querydsl.core.types.dsl.StringPath;

import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.QPerson;
import dk.digitalidentity.sofd.dao.model.RevisionId;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.security.RequireDaoWriteAccess;

public interface PersonDao extends JpaRepository<Person, String>, QueryDslPredicateExecutor<Person>, QuerydslBinderCustomizer<QPerson>, JpaSpecificationExecutor<Person>, RevisionRepository<Person, String, Integer> {

	@RestResource(exported = false)
	void delete(Iterable<? extends Person> entities);

	@RestResource(exported = false)
	void delete(Person entity);

	@RestResource(exported = false)
	void delete(String id);

	@RestResource(exported = false)
	void deleteAll();

	@RestResource(exported = false)
	<S extends Person>S findByCpr(String cpr);

	@RestResource(exported = false)
	<S extends Person>S findByUsersContains(User user);

	@RequireDaoWriteAccess
	<S extends Person> List<S> save(Iterable<S> entities);

	@RequireDaoWriteAccess
	<S extends Person> S save(S entity);

	@RequireDaoWriteAccess
	<S extends Person> S saveAndFlush(S entity);
	
	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT MAX(last_changed) FROM persons")
	Date getLatestUpdate();

	@RestResource(exported = false)
	<S extends Person> List<S> findAllByUsersUserTypeAndDeletedFalse(String userType);
	
	@RestResource(exported = false)
	<S extends Person> List<S> findByDeletedTrue();

	@RestResource(exported = false)
	<S extends Person> List<S> findByTaxedPhoneTrue();
	
	@RestResource(exported = false)
	<S extends Person> List<S> findByLeaveNotNull();

	@RestResource(exported = false)
	Person findByUuid(String uuid);
	
	@Query(nativeQuery = true, value = "SELECT p.* " +
			"FROM persons p" +
			" WHERE p.deleted = 0" +
			"  AND (p.firstname LIKE CONCAT('%', ?1, '%')" +
			"   OR  p.surname LIKE CONCAT('%', ?1, '%')" +
			"   OR  p.chosen_name LIKE CONCAT('%',?1,'%'))" +
			" ORDER BY p.firstname LIMIT 10")
	List<Person> findTop10ByName(@Param("name") String input);

	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT p.* " + 
			"FROM   persons p " + 
			"       JOIN persons_users pu ON p.uuid = pu.person_uuid " +
			"       JOIN users u ON u.id = pu.user_id " +
			"WHERE  u.user_type = 'ACTIVE_DIRECTORY' " + 
			"       AND u.password_expire_date = ?1")
	List<Person> getPersonsWithADPwdChangePending(String date);
	
	// TODO: should these also respect force_stop_date ?
	
	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT p.* " + 
			"FROM   persons p " + 
			"       INNER JOIN affiliations a " + 
			"               ON a.person_uuid = p.uuid " + 
			"       INNER JOIN affiliations_manager am " + 
			"               ON am.affiliation_uuid = a.uuid " + 
			"WHERE  a.deleted = 0 " + 
			"       AND (a.start_date IS NULL OR CAST(a.start_date AS DATE) <= CAST(CURRENT_TIMESTAMP AS DATE))" +
			"       AND (a.stop_date IS NULL OR CAST(a.stop_date AS DATE) >= CAST(CURRENT_TIMESTAMP AS DATE))")
	<S extends Person> List<S> getManagers();

	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT p.* " + 
			"FROM   persons p " + 
			"       INNER JOIN affiliations a " + 
			"               ON a.person_uuid = p.uuid " + 
			"       INNER JOIN affiliations_function af " + 
			"               ON af.affiliation_id = a.id " + 
			"WHERE  a.deleted = 0 " + 
			"       AND (a.start_date IS NULL OR CAST(a.start_date AS DATE) <= CAST(CURRENT_TIMESTAMP AS DATE))" +
			"       AND (a.stop_date IS NULL OR CAST(a.stop_date AS DATE) >= CAST(CURRENT_TIMESTAMP AS DATE))" +
			"       AND (af.function = 'TR' OR af.function = 'TR_SUPPLEANT')")
	List<Person> getTRs();

	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT p.* " + 
			"FROM   persons p " + 
			"       INNER JOIN affiliations a " + 
			"               ON a.person_uuid = p.uuid " + 
			"       INNER JOIN affiliations_function af " + 
			"               ON af.affiliation_id = a.id " + 
			"WHERE  a.deleted = 0 " + 
			"       AND (a.start_date IS NULL OR CAST(a.start_date AS DATE) <= CAST(CURRENT_TIMESTAMP AS DATE))" +
			"       AND (a.stop_date IS NULL OR CAST(a.stop_date AS DATE) >= CAST(CURRENT_TIMESTAMP AS DATE))" +
			"       AND (af.function = 'SR')")
	List<Person> getSRs();

	@RestResource(exported = false)
	<S extends Person> List<S> findByDeletedFalse();
	
	@RestResource(exported = false)
	@Query(nativeQuery = true, value = "SELECT rev, last_changed FROM persons_aud WHERE uuid = ?1")
	List<RevisionId> getRevisionIds(String id);

	@RestResource(exported = false)
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM persons_aud WHERE uuid = ?1")
	void deletePersonLog(String uuid);
		
	@Override
	default public void customize(QuerydslBindings bindings, QPerson person) {

		// enable case-insensitive searching
		bindings.bind(String.class).first((StringPath path, String value) -> path.equalsIgnoreCase(value));

		// prefix search on specific fields
		bindings.bind(person.firstname).first((path, value) -> path.startsWith(value));
		bindings.bind(person.surname).first((path, value) -> path.startsWith(value));
		bindings.bind(person.chosenName).first((path, value) -> path.startsWith(value));
		bindings.bind(person.registeredPostAddress.street).first((path, value) -> path.startsWith(value));
		bindings.bind(person.residencePostAddress.street).first((path, value) -> path.startsWith(value));
		bindings.bind(person.localExtensions).first((path, value) -> path.contains(value));
		bindings.bind(person.users.any().localExtensions).first((path, value) -> path.contains(value));
	}
}
