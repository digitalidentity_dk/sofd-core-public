package dk.digitalidentity.sofd.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.service.AffiliationService;
import lombok.extern.log4j.Log4j;

@Log4j
@RequireControllerWriteAccess
@RestController
public class AffiliationRestController {

	@Autowired
	private AffiliationService affiliationService;

	@PostMapping(value = "/rest/affil/update/kle")
	@ResponseBody
	public HttpEntity<String> updateKle(@RequestHeader("uuid") String uuid, @RequestHeader("type") String type, @RequestBody List<String> codes) {
		Affiliation affiliation = affiliationService.findByUuid(uuid);
		if (affiliation == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		switch (type) {
			case "KlePrimary":
				affiliation.setKlePrimary(codes);
				affiliationService.save(affiliation);
				break;
			case "KleSecondary":
				affiliation.setKleSecondary(codes);
				affiliationService.save(affiliation);
				break;
			default:
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(value = "/rest/affil/delete/{uuid}")
	@ResponseBody
	public HttpEntity<String> delete(@PathVariable("uuid") String uuid) {
		Affiliation affiliation = affiliationService.findByUuid(uuid);
		if (affiliation == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (!affiliation.getMaster().equals("SOFD")) {
			log.warn("Affiliation with uuid " + uuid + " is not owned by SOFD!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		affiliationService.delete(affiliation);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
