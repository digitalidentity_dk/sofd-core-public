package dk.digitalidentity.sofd.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.log.Loggable;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "orgunits")
@Getter
@Setter
@Audited
@EqualsAndHashCode(exclude = { "children", "affiliations" })
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" }) // need this because we sometimes detach the object from Hibernate
public class OrgUnit implements Loggable {

	@Id
	private String uuid;
	
	@Column
	@NotNull
	private String master;
	
	@Column
	@NotNull
	private String masterId;

	@Column
	private boolean deleted;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date lastChanged;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_uuid")
	private OrgUnit parent;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "belongs_to")
	private Organisation belongsTo;

	@OneToMany(mappedBy = "parent")
	@Valid
	private List<OrgUnit> children;

	@Column
	@Size(max = 64)
	@NotNull
	private String shortname;

	@Column
	@NotNull
	@Size(max = 255)
	private String name;

	@Column
	private Long cvr;

	@Column
	private Long ean;

	@Column
	private Long senr;

	@Column
	private Long pnr;

	@Column
	@Size(max = 255)
	private String costBearer;

	@Column
	@Size(max = 255)
	private String orgType;

	@Column
	private Long orgTypeId;

	@Column
	private String keyWords;
	
	@Column
	private String notes;

	@Column
	private String openingHours;
	
	@Column
	private boolean inheritKle;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "orgunits_posts", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "post_id"))
	@Valid
	private List<Post> postAddresses;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "orgunits_phones", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
	@Valid
	private List<Phone> phones;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "orgunits_emails", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "email_id"))
	@Valid
	private List<Email> emails;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgUnit", orphanRemoval = true)
	@JsonIgnore
	private List<OrgUnitTag> tags;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "orgUnit", orphanRemoval = true)
	@Valid
	private OrgUnitManager manager;

	@OneToOne
	@JoinColumn(name = "orgunit_type_id")
	private OrgUnitType type;

	// used for update task, do not expose through API
	@Transient
	@JsonIgnore
	private OrgUnitManager newManager;
	
	@LazyCollection(LazyCollectionOption.FALSE) // we are using this Hibernate annotation instead of the JPA FetchType.EAGER, as it does not allow multiple EAGERs
	@OneToMany(mappedBy = "orgUnit")
	@Valid
	private List<Affiliation> affiliations;

	@ElementCollection
	@CollectionTable(name = "orgunits_kle_primary", joinColumns = @JoinColumn(name = "orgunit_uuid"))
	@Column(name = "kle_value")
	private List<String> klePrimary;

	@ElementCollection
	@CollectionTable(name = "orgunits_kle_secondary", joinColumns = @JoinColumn(name = "orgunit_uuid"))
	@Column(name = "kle_value")
	private List<String> kleSecondary;

	@ElementCollection
	@CollectionTable(name = "orgunits_kle_tertiary", joinColumns = @JoinColumn(name = "orgunit_uuid"))
	@Column(name = "kle_value")
	private List<String> kleTertiary;

	@Column
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	private String localExtensions;

	public OrgUnit() {
		// When calling PUT through Spring Data REST, and these values are not supplied, the setter is never called
		// and we get a cascade delete error if there was data present on the object before the PUT
		this.klePrimary = new ArrayList<>();
		this.kleSecondary = new ArrayList<>();
		this.kleTertiary = new ArrayList<>();
		this.emails = new ArrayList<>();
		this.phones = new ArrayList<>();
		this.postAddresses = new ArrayList<>();
	}

	// TODO: temporary method to use by the Better API until SDR is gone, and the above stuff is gone
	public void clearCollections() {
		this.emails = null;
		this.phones = null;
		this.postAddresses = null;
	}

	@JsonIgnore
	@Override
	public String getEntityId() {
		return uuid;
	}
	
	@JsonIgnore
	@Override
	public EntityType getEntityType() {
		return EntityType.ORGUNIT;
	}

	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setPhones(List<Phone> newPhones) {
		if (this.phones == null) {
			this.phones = new ArrayList<>();
		}

		if (newPhones == null) {
			this.phones.clear();
		}
		else {
			this.phones.clear();
			this.phones.addAll(newPhones);
		}
	}
	
	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setPostAddresses(List<Post> newPosts) {
		if (this.postAddresses == null) {
			this.postAddresses = new ArrayList<>();
		}

		if (newPosts == null) {
			this.postAddresses.clear();
		}
		else {
			this.postAddresses.clear();
			this.postAddresses.addAll(newPosts);
		}
	}

	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setEmails(List<Email> newMails) {
		if (this.emails == null) {
			this.emails = new ArrayList<>();
		}

		if (newMails == null) {
			this.emails.clear();
		}
		else {
			this.emails.clear();
			this.emails.addAll(newMails);
		}
	}
	
	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setKlePrimary(List<String> newKLE) {
		if (this.klePrimary == null) {
			this.klePrimary = new ArrayList<>();
		}

		if (newKLE == null) {
			this.klePrimary.clear();
		}
		else {
			this.klePrimary.clear();
			this.klePrimary.addAll(newKLE);
		}
	}
	
	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setKleSecondary(List<String> newKLE) {
		if (this.kleSecondary == null) {
			this.kleSecondary = new ArrayList<>();
		}

		if (newKLE == null) {
			this.kleSecondary.clear();
		}
		else {
			this.kleSecondary.clear();
			this.kleSecondary.addAll(newKLE);
		}
	}

	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setKleTertiary(List<String> newKLE) {
		if (this.kleTertiary == null) {
			this.kleTertiary = new ArrayList<>();
		}

		if (newKLE == null) {
			this.kleTertiary.clear();
		}
		else {
			this.kleTertiary.clear();
			this.kleTertiary.addAll(newKLE);
		}
	}
}
