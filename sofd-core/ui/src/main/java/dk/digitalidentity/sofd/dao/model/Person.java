package dk.digitalidentity.sofd.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.log.Loggable;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "persons")
@Getter
@Setter
@Audited
@EqualsAndHashCode
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" }) // need this because we sometimes detach the object from Hibernate
public class Person implements Loggable {

	@Id
	private String uuid;
	
	@Column
	@NotNull
	private String master;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date lastChanged;

	@Column
	private boolean deleted;

	@Column
	@Size(min = 10, max = 10)
	@NotNull
	private String cpr;

	@Column
	@NotNull
	@Size(max = 255)
	private String firstname;

	@Column
	@NotNull
	@Size(max = 255)
	private String surname;

	@Column
	@Size(max = 255)
	private String chosenName;

	@Column
	private String keyWords;
	
	@Column
	private String notes;

	@Temporal(TemporalType.DATE)
	@Column
	private Date firstEmploymentDate;

	@Temporal(TemporalType.DATE)
	@Column
	private Date anniversaryDate;

	@Column
	private boolean taxedPhone;
	
	@Column
	private boolean forceStop;
	
	@Column
	private boolean disableAccountOrders;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "leave_id")
	@Valid
	private PersonLeave leave;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "registered_post_address_id")
	@Valid
	private Post registeredPostAddress;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "residence_post_address_id")
	@Valid
	private Post residencePostAddress;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "persons_phones", joinColumns = @JoinColumn(name = "person_uuid"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
	@Valid
	private List<Phone> phones;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "persons_users", joinColumns = @JoinColumn(name = "person_uuid"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	@Valid
	private List<User> users;

	@LazyCollection(LazyCollectionOption.FALSE) // we are using this Hibernate annotation instead of the JPA FetchType.EAGER, as it does not allow multiple EAGERs 
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "person")
	@Valid
	private List<Affiliation> affiliations;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true)
	@Valid
	@JsonIgnore // TODO: jsonignore can be removed when SDR is removed
	private List<Child> children;

	@Column
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	private String localExtensions;

	public Person() {
		// When calling PUT through Spring Data REST, and these values are not supplied, the setter is never called
		// and we get a cascade delete error if there was data present on the object before the PUT
		this.affiliations = new ArrayList<>();
		this.phones = new ArrayList<>();
		this.users = new ArrayList<>();
	}
	
	// TODO: temporary method to use by the Better API until SDR is gone, and the above stuff is gone
	public void clearCollections() {
		this.affiliations = null;
		this.phones = null;
		this.users = null;
	}

	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setAffiliations(List<Affiliation> newAffiliations) {
		if (this.affiliations == null) {
			this.affiliations = new ArrayList<>();
		}

		if (newAffiliations == null) {
			this.affiliations.clear();
		}
		else {
			this.affiliations.clear();
			this.affiliations.addAll(newAffiliations);
		}
	}

	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setPhones(List<Phone> newPhones) {
		if (this.phones == null) {
			this.phones = new ArrayList<>();
		}

		if (newPhones == null) {
			this.phones.clear();
		}
		else {
			this.phones.clear();
			this.phones.addAll(newPhones);
		}
	}
	
	// Spring Data REST calls the setter instead of modifying the existing set, and we have
	// a Hibernate Proxy around this element, so we cannot just overwrite it
	public void setUsers(List<User> newUsers) {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}

		if (newUsers == null) {
			this.users.clear();
		}
		else {
			this.users.clear();
			this.users.addAll(newUsers);
		}
	}

	@JsonIgnore
	public boolean isOnActiveLeave() {
		if (this.leave == null) {
			return false;
		}
		
		if (this.leave.getStartDate() != null) {
			Date now = new Date();
			
			if (!now.after(this.leave.getStartDate())) {
				return false;
			}
		}
		
		return true;
	}

	@JsonIgnore
	public String getPrimeADAccount() {
		if (this.users != null) {
			Optional<User> primeAD = this.users.stream().filter(u -> u.isPrime() && SupportedUserTypeService.isActiveDirectory(u.getUserType())).findFirst();

			if (primeAD.isPresent()) {
				return primeAD.get().getUserId();
			}
		}
		
		return null;
	}
	
	@JsonIgnore
	public String getPrimeOPUSAccount() {
		if (this.users != null) {
			Optional<User> primeOPUS = this.users.stream().filter(u -> u.isPrime() && SupportedUserTypeService.isOpus(u.getUserType())).findFirst();

			if (primeOPUS.isPresent()) {
				return primeOPUS.get().getUserId();
			}
		}
		
		return null;
	}

	@JsonIgnore
	public Affiliation getPrimeAffiliation() {
		if (this.affiliations != null) {
			
		}
		
		for (Affiliation affiliation : affiliations) {
			if (affiliation.isPrime()) {
				return affiliation;
			}
		}
		
		return null;
	}

	@JsonIgnore
	@Override
	public String getEntityId() {
		return uuid;
	}
	
	@JsonIgnore
	@Override
	public EntityType getEntityType() {
		return EntityType.PERSON;
	}
	
	public List<User> onlyActiveUsers() {
		if (users == null) {
			return new ArrayList<>();
		}
		
		return users.stream().filter(u -> u.isDisabled() == false).collect(Collectors.toList());
	}
}
