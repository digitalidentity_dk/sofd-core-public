package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sofd.dao.model.KnownUsername;

public interface KnownUsernamesDao extends JpaRepository<KnownUsername, Long> {
	List<KnownUsername> findByUserType(String userType);

	KnownUsername findByUsernameAndUserType(String username, String userType);
}
