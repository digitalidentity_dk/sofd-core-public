package dk.digitalidentity.sofd.dao.model.enums;

import lombok.Getter;

@Getter
public enum EmailTemplateType {
	AD_CREATE_FAILED("html.enum.email.message.type.ad_create_failed", true),
	AD_CREATE_MANAGER("html.enum.email.message.type.ad_create_manager", true),
	AD_CREATE_EMPLOYEE("html.enum.email.message.type.ad_create_employee", true),
	AD_DISABLE_MANAGER("html.enum.email.message.type.ad_disable_manager", true),
	EXCHANGE_CREATE_EMPLOYEE("html.enum.email.message.type.exchange_create_employee", true),
	NEW_AFFILIATION("html.enum.email.message.type.new_affiliation", false),
	NEW_EMPLOYEE_WELCOME("html.enum.email.message.type.new_employee_welcome", false),
	NEW_EMPLOYEE_REMINDER("html.enum.email.message.type.new_employee_reminder", false),
	ORGUNIT_CHANGES("html.enum.email.message.type.orgunit_changes", false);
	
	private String message;
	private boolean requireAccountCreation;
	
	private EmailTemplateType(String message, boolean requireAccountCreation) {
		this.message = message;
		this.requireAccountCreation = requireAccountCreation;
	}
}
