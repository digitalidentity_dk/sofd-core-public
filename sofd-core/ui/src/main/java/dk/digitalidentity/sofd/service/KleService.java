package dk.digitalidentity.sofd.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.KleDao;
import dk.digitalidentity.sofd.dao.model.Kle;
import dk.digitalidentity.sofd.service.model.KleDtoWrapper;
import lombok.extern.log4j.Log4j;

@Log4j
@EnableCaching
@Service
public class KleService {

	@Autowired
	private KleDao kleDao;
	
	@Autowired
	private KleService self;
	
	@Autowired
	private SofdConfiguration configuration;

	@Qualifier("defaultRestTemplate")
	@Autowired
	private RestTemplate restTemplate;

	@Cacheable(value = "kleList")
	public List<Kle> findAll() {
		return kleDao.findAll();
	}
	
	// 4 hour cache should be enough to ensure solid performance
	@Scheduled(fixedRate = 4 * 60 * 60 * 1000)
	public void resetKleCacheTask() {
		self.resetKleCache();
	}

	@CacheEvict(value = "kleList", allEntries = true)
	public void resetKleCache() {
		;
	}

	public List<Kle> findAllByParent(String parent) {
		return kleDao.findAllByParent(parent);
	}

	public Kle getByCode(String code) {
		return kleDao.getByCode(code);
	}

	public long countByActiveTrue() {
		return kleDao.countByActiveTrue();
	}

	public Kle save(Kle kle) {
		return kleDao.save(kle);
	}
	
	public Collection<Kle> loadKleFromKOMBIT(String cvr) {
		Map<String, Kle> kleMap = new HashMap<>();

		HttpEntity<KleDtoWrapper> responseRootEntity = restTemplate.getForEntity(configuration.getIntegrations().getKle().getUrl() + "/odata/Klasser?$filter=KlasseTilhoerer eq '00000c7e-face-4001-8000-000000000000' and Cvr eq '" + cvr + "'&$select=UUID,BrugervendtNoegle,Titel,Tilstand", KleDtoWrapper.class);

		for (dk.digitalidentity.sofd.service.model.KleDto kleDto : responseRootEntity.getBody().getContent()) {
			Kle kle = new Kle();
			kle.setCode(kleDto.getCode());
			kle.setName(kleDto.getTitle());
			kle.setActive(kleDto.isActive());
			kle.setUuid(kleDto.getUuid());

			if (kleDto.getCode().length() == 2) {
				kle.setParent("0");
			}
			else if (kleDto.getCode().length() == 5) {
				kle.setParent(kleDto.getCode().substring(0, 2));
			}
			else {
				kle.setParent(kleDto.getCode().substring(0, 5));
			}

			if (kleMap.containsKey(kle.getCode())) {
				Kle otherKle = kleMap.get(kle.getCode());
				if (otherKle.isActive() == kle.isActive()) {
					log.warn("KLE with code " + kle.getCode() + " is in the set from KOMBIT twice with same status");
				}
				else if (!otherKle.isActive()) {
					// overwrite with active version (KOMBIT bug, they are keeping multple versions instead of updating the actual version)
					kleMap.put(kle.getCode(), kle);
				}
			}
			else {
				kleMap.put(kle.getCode(), kle);
			}
		}

		return kleMap.values();
	}
}
