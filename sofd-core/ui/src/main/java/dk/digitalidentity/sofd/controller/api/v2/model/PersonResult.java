package dk.digitalidentity.sofd.controller.api.v2.model;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonResult {
	private Set<PersonApiRecord> persons;
	private long page;
}
