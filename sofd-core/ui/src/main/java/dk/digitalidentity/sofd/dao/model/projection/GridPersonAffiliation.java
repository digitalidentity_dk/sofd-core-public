package dk.digitalidentity.sofd.dao.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;

@Projection(name = "grid", types = { Affiliation.class })
public interface GridPersonAffiliation {
	String getPositionName();
	
	@Value("#{target.getOrgUnit().getName()}")
	String getOrgUnitName();
	
	@JsonIgnore
	OrgUnit getOrgUnit();
}