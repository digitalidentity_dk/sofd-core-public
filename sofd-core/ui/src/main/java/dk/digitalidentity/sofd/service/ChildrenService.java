package dk.digitalidentity.sofd.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.controller.mvc.dto.ChildrenLookupDTO;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class ChildrenService {

	@Autowired
	private SofdConfiguration configuration;

	public List<ChildrenLookupDTO> getByCpr(String cpr) {
		RestTemplate restTemplate = new RestTemplate();

		// no reason to lookup invalid cpr numbers
		if (!validCpr(cpr)) {
			return new ArrayList<>();
		}

		String childrenResourceUrl = configuration.getIntegrations().getChildren().getUrl();
		if (!childrenResourceUrl.endsWith("/")) {
			childrenResourceUrl += "/";
		}
		childrenResourceUrl += "api/CPRLookup?cpr=" + cpr + "&cvr=" + configuration.getCustomer().getCvr();

		try {
			ResponseEntity<ChildrenLookupDTO[]> response = restTemplate.getForEntity(childrenResourceUrl,
					ChildrenLookupDTO[].class);

			List<ChildrenLookupDTO> children = Arrays.stream(response.getBody()).collect(Collectors.toList());
			children.removeIf(
					c -> getAgeFromCpr(c.getCpr()) > configuration.getIntegrations().getChildren().getMaxAge());
			return children;
		}
		catch (RestClientException ex) {
			log.warn("Failed to lookup children for: " + PersonService.maskCpr(cpr), ex);
			return null;
		}
	}

	private int getAgeFromCpr(String cpr) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyy");
			LocalDate birthDate = LocalDate.parse(cpr.substring(0, 6), formatter);
			int age = Period.between(birthDate, LocalDate.now()).getYears();
			return age;
		} catch (Exception e) {
			return 0;
		}
	}

	private boolean validCpr(String cpr) {
		if (cpr == null || cpr.length() != 10) {
			return false;
		}

		for (char c : cpr.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}

		int days = Integer.parseInt(cpr.substring(0, 2));
		int month = Integer.parseInt(cpr.substring(2, 4));

		if (days < 1 || days > 31) {
			return false;
		}

		if (month < 1 || month > 12) {
			return false;
		}

		return true;
	}
}
