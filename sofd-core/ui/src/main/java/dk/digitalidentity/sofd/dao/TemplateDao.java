package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.Template;

@RepositoryRestResource(exported = false)
public interface TemplateDao extends CrudRepository<Template, Long> {
	List<Template> findAll();
}
