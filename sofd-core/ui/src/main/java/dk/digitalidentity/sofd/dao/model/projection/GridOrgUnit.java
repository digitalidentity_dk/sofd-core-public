package dk.digitalidentity.sofd.dao.model.projection;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import dk.digitalidentity.sofd.dao.model.OrgUnit;

@Projection(name = "grid", types = { OrgUnit.class })
public interface GridOrgUnit {
	String getUuid();
	String getName();
	
	List<GridOrgUnitAffiliation> getAffiliations();
	
	@Value("#{target.getManager() != null ? target.getManager().getName() : \"\"}")
	String getManagerName();
}
