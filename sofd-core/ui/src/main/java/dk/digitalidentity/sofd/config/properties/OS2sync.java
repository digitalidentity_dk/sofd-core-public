package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OS2sync {
	private boolean enabled = false;
	private boolean cprEnabled = false;
	
	@JsonIgnore
	private String datasourceUrl;
	
	@JsonIgnore
	private String datasourceUsername;
	
	@JsonIgnore
	private String datasourcePassword;
}
