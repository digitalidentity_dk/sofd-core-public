package dk.digitalidentity.sofd.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.controller.mvc.dto.ChildrenLookupDTO;
import dk.digitalidentity.sofd.dao.model.Child;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.security.SecurityUtil;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
public class SynchronizePersonChildrenService {

    @Autowired
    private ChildrenService childrenService;

    @Autowired
    private PersonService personService;

	@Autowired
	private SofdConfiguration configuration;

	@Transactional
	public void updateChildrenOnAllWithLastCprDigit(String digit) {
        List<Person> activePersons = personService.getAll();
        for (Person person : activePersons) {
        	if (person.getCpr().endsWith(digit)) {
        		updatePersonChildren(person.getUuid());
        	}
        }
	}

	private void updatePersonChildren(String personUuid) {
    	Person person = personService.getByUuid(personUuid);

		Date yesterday = java.sql.Date.valueOf(LocalDate.now().plusDays(-1));
		Date tomorrow = java.sql.Date.valueOf(LocalDate.now().plusDays(1));
    	boolean hasValidAffiliation = !person.isDeleted() && person.getAffiliations().stream().anyMatch(a ->
				(a.getStartDate() == null || a.getStartDate().before(tomorrow))
				&& (a.getStopDate() == null || a.getStopDate().after(yesterday))
				&& configuration.getIntegrations().getChildren().getAffiliationMasters().contains(a.getMaster())
		);

    	List<ChildrenLookupDTO> children = hasValidAffiliation ? childrenService.getByCpr(person.getCpr()) : new ArrayList<>();

        if (children != null) {
			boolean shouldSave = false;

			// Handle inserts and updates
			for( ChildrenLookupDTO child : children)
			{
				String name = child.getName() == null || child.getName().isEmpty() ? "Ukendt" : child.getName();
				Optional<Child> existingChild = person.getChildren().stream().filter(c -> c.getCpr().equalsIgnoreCase(child.getCpr())).findFirst();
				if( !existingChild.isPresent() )
				{
					Child newChild = new Child();
					newChild.setCpr(child.getCpr());
					newChild.setName(name);
					newChild.setParent(person);
					person.getChildren().add(newChild);
					shouldSave = true;
				}
				else
				{
					Child sofdChild = existingChild.get();
					if( !sofdChild.getName().equalsIgnoreCase(name)){
						sofdChild.setName(name);
						shouldSave = true;
					}
				}
			}

			// Handle deletes
			List<Child> toBeDeleted = person.getChildren().stream().filter(sofdChild -> children.stream().noneMatch(serviceChild -> serviceChild.getCpr().equalsIgnoreCase(sofdChild.getCpr()))).collect(Collectors.toList());
			if( toBeDeleted.size() > 0 )
			{
				person.getChildren().removeAll(toBeDeleted);
				shouldSave = true;
			}

			if( shouldSave)
			{
				SecurityUtil.fakeLoginSession();
				log.debug("saving children for " + personUuid);
				personService.save(person);
			}
        }
    }
}
