package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChildrenSync {
    private boolean enabled = false;
    private boolean runOnStartup = false;
}