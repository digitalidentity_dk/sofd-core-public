package dk.digitalidentity.sofd.dao.model;

public abstract class MasteredEntity {
	public abstract String getMaster();
	public abstract String getMasterId();
	public abstract void setMaster(String master);
	public abstract void setMasterId(String masterId);
}
