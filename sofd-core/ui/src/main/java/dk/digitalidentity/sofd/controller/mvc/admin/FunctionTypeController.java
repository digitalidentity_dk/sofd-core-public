package dk.digitalidentity.sofd.controller.mvc.admin;

import dk.digitalidentity.sofd.controller.mvc.dto.FunctionTypeDTO;
import dk.digitalidentity.sofd.dao.FunctionTypeDao;
import dk.digitalidentity.sofd.dao.model.FunctionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class FunctionTypeController {

    @Autowired
    private FunctionTypeDao functionTypeDao;

    @GetMapping("/ui/functiontype")
    public String listFunctionTypes(Model model) {
        List<FunctionType> functionTypes = functionTypeDao.findAll();

        model.addAttribute("functionTypes", functionTypes);
        model.addAttribute("newFunctionType", new FunctionTypeDTO());

        return "admin/functionType/list";
    }
}
