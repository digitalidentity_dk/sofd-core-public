package dk.digitalidentity.sofd.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "orgunit_types")
@Audited
@Getter
@Setter
public class OrgUnitType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "type_key")
	@NotNull
	@Size(max = 64)
	private String key;

	@Column(name = "type_value")
	@NotNull
	@Size(max = 255)
	private String value;
	
	@Column
	private boolean active;
	
	@Column
	private String extId;
}
