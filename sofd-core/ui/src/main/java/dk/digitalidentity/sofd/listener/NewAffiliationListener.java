package dk.digitalidentity.sofd.listener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.EmailTemplateService;
import dk.digitalidentity.sofd.service.FutureEmailsService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import dk.digitalidentity.sofd.dao.model.EntityChangeQueueDetail;
import dk.digitalidentity.sofd.dao.model.FutureEmail;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.EmailTemplateType;
import dk.digitalidentity.sofd.listener.EntityListenerService.ChangeType;

// will trigger a welcome email being send the the manager AND the employee at the
// first workday, as well as a reminder on the 14th workday

@Component
public class NewAffiliationListener implements ListenerAdapter {

	@Autowired
	private PersonService personService;

	@Autowired
	private FutureEmailsService futureEmailsService;

	@Autowired
	private EmailTemplateService emailTemplateService;
	
	@Autowired
	private AffiliationService affiliationService;

	@Autowired
	private SofdConfiguration configuration;

	@Override
	public void personCreated(String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return;
		}
		
		// if a new Person object is created, then ALL affiliations (from the wages system) is a new affiliation
		if (person.getAffiliations() != null) {
			List<Affiliation> affiliations = person.getAffiliations().stream()
					.filter(a -> a.getMaster().equals(configuration.getModules().getLos().getPrimeAffiliationMaster()))
					.collect(Collectors.toList());

			if (affiliations.size() > 0) {
				handleNewAffiliations(person, affiliations);
			}
		}
	}

	@Override
	public void personUpdated(String uuid, List<EntityChangeQueueDetail> changes) {
		List<EntityChangeQueueDetail> newEmployeeChanges = changes.stream().filter(c -> c.getChangeType().equals(ChangeType.ADDED_AFFILIATION)).collect(Collectors.toList());
		if (newEmployeeChanges.isEmpty()) {
			return;
		}

		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return;
		}

		List<Affiliation> affiliations = new ArrayList<>();
		for (EntityChangeQueueDetail newEmployeeChange : newEmployeeChanges) {
			Affiliation affiliation = affiliationService.findByUuid(newEmployeeChange.getChangeTypeDetails());
			if (affiliation == null) {
				continue;
			}

			// only relevant for ACTUAL employments
			if (!affiliation.getMaster().equals(configuration.getModules().getLos().getPrimeAffiliationMaster())) {
				continue;
			}

			affiliations.add(affiliation);
		}
		
		if (affiliations.size() > 0) {
			handleNewAffiliations(person, affiliations);
		}
	}

	private void handleNewAffiliations(Person person, List<Affiliation> affiliations) {
		for (Affiliation affiliation : affiliations) {			
			Person manager = PersonService.getManager(person, affiliation.getEmployeeId());
			if (manager == null) {
				continue;
			}

			List<Person> persons = new ArrayList<>();
			persons.add(person);
			persons.add(manager);

			Calendar cal = Calendar.getInstance();
			cal.setTime(affiliation.getStartDate());
			cal.set(Calendar.HOUR_OF_DAY, 7);
			Date firstTts = cal.getTime();
			cal.add(Calendar.DATE, 14);
			Date secondTts = cal.getTime();
			
			EmailTemplate template = emailTemplateService.findByTemplateType(EmailTemplateType.NEW_EMPLOYEE_WELCOME);
			if (template.isEnabled()) {
				String message = template.getMessage();
				message = message.replace(EmailTemplateService.EMPLOYEE_PLACEHOLDER, PersonService.getName(person));

				FutureEmail futureEmail = new FutureEmail();
				futureEmail.setTitle(template.getTitle());
				futureEmail.setMessage(message);
				futureEmail.setAllOrNone(true);
				futureEmail.setEboks(false);
				futureEmail.setPersons(persons);
				futureEmail.setDeliveryTts(firstTts);

				futureEmailsService.save(futureEmail);
			}

			EmailTemplate templateReminder = emailTemplateService.findByTemplateType(EmailTemplateType.NEW_EMPLOYEE_REMINDER);
			if (templateReminder.isEnabled()) {
				String messageReminder = templateReminder.getMessage();
				messageReminder = messageReminder.replace(EmailTemplateService.EMPLOYEE_PLACEHOLDER, PersonService.getName(person));
				
				FutureEmail futureEmailReminder = new FutureEmail();
				futureEmailReminder.setAllOrNone(true);
				futureEmailReminder.setEboks(false);
				futureEmailReminder.setPersons(persons);
				futureEmailReminder.setDeliveryTts(secondTts);
				futureEmailReminder.setTitle(templateReminder.getTitle());
				futureEmailReminder.setMessage(messageReminder);

				futureEmailsService.save(futureEmailReminder);
			}
		}
	}
}
