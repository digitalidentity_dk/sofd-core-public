package dk.digitalidentity.sofd.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.service.AccountOrderService;
import lombok.extern.log4j.Log4j;

@Component
@EnableScheduling
@Log4j
public class AccountOrderCleanupTask {

	@Autowired
	private SofdConfiguration configuration;
	
	@Autowired
	private AccountOrderService accountOrderService;

	@Scheduled(cron = "0 15 2 * * ?")
	@Transactional(rollbackFor = Exception.class)
	public void processChanges() {
		if (!configuration.getScheduled().isEnabled()) {
			log.debug("Scheduled jobs are disabled on this instance");
			return;
		}

		accountOrderService.cleanupOld();
	}
}
