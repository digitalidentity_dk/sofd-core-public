package dk.digitalidentity.sofd.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.SecurityLog;

@RepositoryRestResource(exported=false)
public interface SecurityLogDao extends CrudRepository<SecurityLog, Long> {
	void deleteByTimestampBefore(Date before);
	
	@Query(nativeQuery = true, value = "SELECT timestamp FROM security_log sl WHERE sl.client_id = ?1 ORDER BY id DESC LIMIT 1")
	Date getLastTimestampByClientId(long id);
}