package dk.digitalidentity.sofd.dao.model.projection;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationFunction;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationType;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;

@Projection(name = "extended", types = { Affiliation.class })
public interface ExtendedAffiliation {
	
	// all existing fields

	long getId();
	String getUuid();
	String getMaster();
	String getMasterId();
	Date getStartDate();
	Date getStopDate();
	boolean isDeleted();
	boolean isStopped();
	boolean isPrime();
	String getEmployeeId();
	String getEmploymentTerms();
	String getEmploymentTermsText();
	String getPayGrade();
	Double getWorkingHoursDenominator();
	Double getWorkingHoursNumerator();
	AffiliationType getAffiliationType();
	String getPositionId();
	String getPositionName();
	String getPositionTypeId();
	String getPositionTypeName();
	List<AffiliationFunction> getFunctions();
	List<String> getKlePrimary();
	List<String> getKleSecondary();
	
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	String getLocalExtensions();

	// add virtual fields
	
	@Value("#{target.getPerson().getChosenName() != null ? target.getPerson().getChosenName() : target.getPerson().getFirstname() + \" \" + target.getPerson().getSurname()}")
	String getEmployeeName();

	// ignore these

	@JsonIgnore
	Person getPerson();
	
	@JsonIgnore
	boolean isInheritPrivileges();
	
	@JsonIgnore
	OrgUnit getOrgUnit();

	@JsonIgnore
	Set<OrgUnit> getManagerFor();

	@JsonIgnore
	boolean get_stopValue();

	@JsonIgnore
	Date getForceStopDate();
}
