package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.ReservedUsername;

@RepositoryRestResource(exported = false)
public interface ReservedUsernameDao extends JpaRepository<ReservedUsername, Long> {

	ReservedUsername findByPersonUuidAndEmployeeIdAndUserType(String uuid, String employeeId, String userType);

	ReservedUsername findByPersonUuidAndUserType(String uuid, String userType);

	List<ReservedUsername> findByPersonUuid(String uuid);

}