package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountCreation {
	private OpusAccountOrderHandler opusHandler = new OpusAccountOrderHandler();
	private boolean enabled = false;
	private String hourlyWageCode = "03";
	private boolean reuseExistingUsernames = false;
}
