package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Actuator {
	private String username = "admin";
	private String password = "admin";
}
