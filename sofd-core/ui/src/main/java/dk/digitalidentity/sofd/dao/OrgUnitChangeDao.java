package dk.digitalidentity.sofd.dao;

import dk.digitalidentity.sofd.dao.model.OrgUnitChange;
import dk.digitalidentity.sofd.dao.model.enums.OrgUnitChangeStatus;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface OrgUnitChangeDao extends CrudRepository<OrgUnitChange, Long> {
	List<OrgUnitChange> findByStatus(OrgUnitChangeStatus status);
}