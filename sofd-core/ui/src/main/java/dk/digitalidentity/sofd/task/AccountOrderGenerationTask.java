package dk.digitalidentity.sofd.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.service.AccountOrderService;

@Component
@EnableScheduling
public class AccountOrderGenerationTask {

	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	private AccountOrderService accountOrderService;

	// TODO: this takes roughly 5-10 seconds on an average municipality on a developer laptop
	//       so some fuzz to this cron would be nice, and perhaps some optimization
	@Scheduled(cron = "${cron.account.generation:0 45 3 * * ?}")
	@Transactional(rollbackFor = Exception.class)
	public void processChanges() {
		if (!configuration.getScheduled().isEnabled() || !configuration.getModules().getAccountCreation().isEnabled()) {
			return;
		}

		accountOrderService.nightlyJob();
	}
}
