package dk.digitalidentity.sofd.dao;

import dk.digitalidentity.sofd.dao.model.FunctionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface FunctionTypeDao extends CrudRepository<FunctionType, Long> {
    List<FunctionType> findAll();

    FunctionType findById(long id);
}