package dk.digitalidentity.sofd.dao.model.enums;

public enum NotificationType {
	PERSON_WITH_DISALLOWED_ACCOUNT("html.enum.notificationtype.person_with_disallowed_account"),
	PERSON_PERMANENTLY_DELETED("html.enum.notificationtype.person_permanently_deleted"),
	ORGUNIT_WITH_MISSING_RULES("html.enum.notificationtype.orgunit_with_missing_rules"),
	ORGUNIT_DELETED("html.enum.notificationtype.orgunit_deleted"),
	ACCOUNT_ORDER_FAILURE("html.enum.notificationtype.account_order_failure"),
	EBOKS_REJECTED("html.enum.notificationtype.eboks_rejected"),
	NEW_AFFILIATION_LOCATION("html.enum.notificationtype.new_affiliation_location");

	private String message;

	private NotificationType(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
