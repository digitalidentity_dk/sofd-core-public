package dk.digitalidentity.sofd.service.eboks.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EboksMessage {
	private String cpr;
	private String cvr;
	private String senderId;
	private String contentTypeId;
	private String subject;
	private String pdfFileBase64;
	private List<EboksAttachment> attachments;
}
