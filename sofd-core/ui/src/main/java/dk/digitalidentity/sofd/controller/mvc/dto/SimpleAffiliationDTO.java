package dk.digitalidentity.sofd.controller.mvc.dto;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SimpleAffiliationDTO {
	private long id;
	private boolean prime;
	private String positionName;
	private String orgUnitName;
	
	public SimpleAffiliationDTO(Affiliation affiliation) {
		this.id = affiliation.getId();
		this.prime = affiliation.isPrime();
		this.positionName = affiliation.getPositionName();
		this.orgUnitName = affiliation.getOrgUnit().getName();
	}
}
