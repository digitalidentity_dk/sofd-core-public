package dk.digitalidentity.sofd.task;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.ClientDao;
import dk.digitalidentity.sofd.dao.SecurityLogDao;
import dk.digitalidentity.sofd.dao.model.Client;
import lombok.extern.log4j.Log4j;

@Component
@EnableScheduling
@Log4j
public class UpdateClientActivityTask {

	@Autowired
	private SofdConfiguration configuration;

	// using DAO to avoid interceptors - as we don't actually want to audit this technical timestamp update
	@Autowired
	private ClientDao clientDao;

	@Autowired
	private SecurityLogDao securityLogDao;

	// run every hour
	@Scheduled(cron = "0 0 * * * ?")
	@Transactional(rollbackFor = Exception.class)
	public void updateTimestamps() {
		if (!configuration.getScheduled().isEnabled()) {
			log.debug("Scheduled jobs are disabled on this instance");
			return;
		}

		for (Client client : clientDao.findAll()) {
			if (!client.isMonitorForActivity()) {
				continue;
			}

			Date date = securityLogDao.getLastTimestampByClientId(client.getId());
			client.setLastActive(date);
			clientDao.save(client);
		}
	}
	
	// run at 8 every morning - good to have morning alarms :o)
	@Scheduled(cron = "0 0 8 * * ?")
	@Transactional(rollbackFor = Exception.class)
	public void checkForActivity() {
		if (!configuration.getScheduled().isEnabled()) {
			log.debug("Scheduled jobs are disabled on this instance");
			return;
		}

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date sevenDaysAgo = cal.getTime();

		for (Client client : clientDao.findAll()) {
			if (!client.isMonitorForActivity()) {
				continue;
			}

			if (client.getLastActive() == null || client.getLastActive().before(sevenDaysAgo)) {
				log.error("Client has not seen activity for 7 days: " + client.getName() + " / " + client.getId());
			}
		}
	}
}
