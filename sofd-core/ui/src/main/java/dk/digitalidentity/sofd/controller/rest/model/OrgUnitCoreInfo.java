package dk.digitalidentity.sofd.controller.rest.model;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrgUnitCoreInfo {

	@Size(min = 1, max = 64, message = "Skal være mellem 1 og 64 tegn")
	private String shortname;

	@Size(min = 1, max = 255, message = "Skal være mellem 1 og 255 tegn")
	private String name;

	private Long cvr;

	private Long ean;

	private Long senr;

	private Long pnr;

	@Size(max = 255, message = "Maksimalt 255 tegn")
	private String costBearer;

	private String orgUnitType;

	@Size(min = 1, message = "Skal være udfyldt")
	private String parent;

	private Long belongsTo;
}
