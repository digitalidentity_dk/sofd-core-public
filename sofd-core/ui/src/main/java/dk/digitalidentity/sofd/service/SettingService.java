package dk.digitalidentity.sofd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.dao.SettingDao;
import dk.digitalidentity.sofd.dao.model.Setting;
import dk.digitalidentity.sofd.dao.model.enums.CustomerSetting;
import dk.digitalidentity.sofd.service.model.PersonDeletePeriod;

@Service
public class SettingService {
	private static final String SETTING_PERSON_DELETE_PERIOD = "PersonDeletePeriod";

	@Autowired
	private SettingDao settingDao;

	public PersonDeletePeriod getPersonDeletePeriod() {
		String value = getKeyWithDefault(SETTING_PERSON_DELETE_PERIOD, PersonDeletePeriod.NEVER.toString());
		
		return PersonDeletePeriod.valueOf(value);
	}

	public void setPersonDeletePeriod(PersonDeletePeriod period) {
		Setting setting = settingDao.findByKey(SETTING_PERSON_DELETE_PERIOD);
		if (setting == null) {
			setting = new Setting();
			setting.setKey(SETTING_PERSON_DELETE_PERIOD);
		}
		
		setting.setValue(period.toString());
		settingDao.save(setting);
	}

	/// helper methods

	private String getKeyWithDefault(String key, String defaultValue) {
		Setting setting = settingDao.findByKey(key);
		if (setting != null) {
			return setting.getValue();
		}

		return defaultValue;
	}

	// Another way of managing settings

	public Long getLongValueByKey(CustomerSetting customerSetting) {
		Long value;

		Setting setting = getByKey(customerSetting);

		if (setting != null) {
			value = Long.parseLong(setting.getValue());
		}
		else {
			Setting newSetting = new Setting();
			newSetting.setKey(customerSetting.toString());
			newSetting.setValue(customerSetting.getDefaultValue());
			settingDao.save(newSetting);
			value = Long.parseLong(customerSetting.getDefaultValue());
		}

		return value;
	}

	public Setting getByKey(CustomerSetting key) {
		return settingDao.findByKey(key.toString());
	}

	public void save(Setting setting) {
		settingDao.save(setting);
	}

	public Setting getOpusAutoAffiliations() {
		Setting setting = settingDao.findByKey(CustomerSetting.OPUS_AUTO_AFF.toString());
		if (setting == null) {
			setting = new Setting();
			setting.setKey(CustomerSetting.OPUS_AUTO_AFF.toString());
			setting.setValue(CustomerSetting.OPUS_AUTO_AFF.getDefaultValue());
		}
		
		return setting;
	}
}
