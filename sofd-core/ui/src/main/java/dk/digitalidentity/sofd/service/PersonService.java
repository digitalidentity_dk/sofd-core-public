package dk.digitalidentity.sofd.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.history.Revision;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.RoleConstants;
import dk.digitalidentity.sofd.dao.PersonDao;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.RevisionId;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderType;
import dk.digitalidentity.sofd.dao.model.enums.EndDate;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;
import dk.digitalidentity.sofd.security.SecurityUtil;
import lombok.extern.log4j.Log4j;

@Log4j
@EnableCaching
@EnableScheduling
@Service
public class PersonService {

	@Autowired
	private PersonDao personDao;

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PersonService self;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;
	
	@Autowired
	private AccountOrderService accountOrderService;

	public List<Person> findAllByUserType(String userType) {
		return personDao.findAllByUsersUserTypeAndDeletedFalse(userType);
	}

	public Person getByUuid(String uuid) {
		return personDao.findByUuid(uuid);
	}
	
	public Person findByCpr(String cpr) {
		return personDao.findByCpr(cpr);
	}

	public Page<Person> getAll(Pageable pageable) {
		return personDao.findAll(pageable);
	}

	public List<Person> getAll() {
		return personDao.findAll();
	}

	public List<Person> getActive() {
		return personDao.findByDeletedFalse();
	}
	
	@Cacheable(value = "activePersons")
	public List<Person> getActiveCached() {
		List<Person> persons = personDao.findByDeletedFalse();

		// force-load all users and affiliations
		for (Person person : persons) {
			person.getUsers().size();
			
			if (person.getLeave() != null) {
				person.getLeave().getReason();
			}

			for (Affiliation affiliation : person.getAffiliations()) {
				affiliation.getOrgUnit().getName();
			}
		}
		
		return persons;
	}
	
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void resetActivePersonCacheTask() {
    	self.resetActivePersonCache();
    }
    
    @CacheEvict(value = "activePersons", allEntries = true)
    public void resetActivePersonCache() {
    	; // clears cache every hour - we want to protect
          // against force-refresh in the browser, as the lookup
          // can be a bit intensive
    }

    public List<Person> findAllWithADPwdChangePending(String date) {
    	return personDao.getPersonsWithADPwdChangePending(date);
    }

	public List<Person> findAllManagers() {
		return personDao.getManagers();
	}
	
	public List<Person> findAllTRs() {
		return personDao.getTRs();
	}

	public List<Person> findAllSRs() {
		return personDao.getSRs();
	}
	
	public Person save(Person person) {
		return personDao.save(person);
	}

	public static String getName(Person person) {
		if (person.getChosenName() != null && person.getChosenName().length() > 0) {
			return person.getChosenName();
		}

		return person.getFirstname() + " " + person.getSurname();
	}

	public Person findByUser(User user) {
		return personDao.findByUsersContains(user);
	}
	
	@Transactional
	public void setPostAndName(Person person, Post post, String firstname, String surname) {
        log.info("Updating name and address on person " + person.getUuid());

		if (!SecurityUtil.isUserLoggedIn() && !SecurityUtil.isClientLoggedIn()) {
			SecurityUtil.fakeLoginSession();
		}

		if (person.getRegisteredPostAddress() != null) {
			person.getRegisteredPostAddress().setAddressProtected(post.isAddressProtected());
			person.getRegisteredPostAddress().setCity(post.getCity());
			person.getRegisteredPostAddress().setCountry(post.getCountry());
			person.getRegisteredPostAddress().setLocalname(post.getLocalname());
			person.getRegisteredPostAddress().setMaster(post.getMaster());
			person.getRegisteredPostAddress().setMasterId(post.getMasterId());
			person.getRegisteredPostAddress().setPostalCode(post.getPostalCode());
			person.getRegisteredPostAddress().setStreet(post.getStreet());
			
		}
		else {
			person.setRegisteredPostAddress(post);
		}

		person.setFirstname(firstname);
		person.setSurname(surname);
		
		self.save(person);
	}

	public List<RevisionId> getRevisionIds(String uuid) {
		return personDao.getRevisionIds(uuid);
	}

	public Revision<Integer, Person> findRevision(String uuid, Integer revId) {
		return personDao.findRevision(uuid, revId);
	}

	public List<Person> findTop10ByName(String term) {
		return personDao.findTop10ByName(term);
	}

	public List<Person> findAllDeleted() {
		return personDao.findByDeletedTrue();
	}

	public List<Person> findAllTaxed() {
		return personDao.findByTaxedPhoneTrue();
	}

	public void delete(Person person) {
		personDao.delete(person);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
	public void hardDeletePersons(List<Person> entities) {
		personDao.delete(entities);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
	public void deletePersonsLogs(List<Person> entities) {
		for (Person person : entities) {
			personDao.deletePersonLog(person.getUuid());

			Notification notification = new Notification();
			notification.setActive(true);
			notification.setAffectedEntityName(getName(person));
			notification.setAffectedEntityType(EntityType.PERSON);
			notification.setAffectedEntityUuid("00000000-0000-0000-0000-000000000000");
			notification.setMessage("Personen blev fysisk slettet fra systemet, efter at have været inaktiv siden: " + person.getLastChanged());
			notification.setCreated(new Date());
			notification.setNotificationType(NotificationType.PERSON_PERMANENTLY_DELETED);
			notificationService.save(notification);
		}
	}

	public Person getLoggedInPerson() {
		String userId = SecurityUtil.getUser();
		if (userId != null) {
			User user = userService.findByUserIdAndUserType(userId, SupportedUserTypeService.getActiveDirectoryUserType());
			if (user != null) {
				return findByUser(user);
			}
		}

		return null;
	}

	public List<Person> getByAffiliationMasters(List<String> affiliationMasters) {
		return null;
	}
	
	public static Person getManager(Person person, String employeeId) {
		Affiliation affiliation = null;
		
		if (employeeId != null) {
			for (Affiliation aff : person.getAffiliations()) {
				if (aff.getEmployeeId() != null && aff.getEmployeeId().equals(employeeId)) {
					affiliation = aff;
					break;
				}
			}
		}
		else {
			// just pick the primary affiliation
			Optional<Affiliation> oAffiliation = person.getAffiliations().stream().filter(a -> a.isPrime()).findFirst();
			affiliation = (oAffiliation.isPresent()) ? oAffiliation.get() : null;

			// special case - we are not looking for an affiliation tied to a specific employment,
			// but the person has no prime affiliation (most likely cause, the person has no active
			// affiliations), so we find the affiliation that was stopped last, and use that.
			//
			// This is useful for deactivating account based on all-stopped affiliations, and the need
			// to notify the previous manager
			if (affiliation == null) {
				Affiliation latestStoppedAffiliation = null;

				for (Affiliation a : person.getAffiliations()) {
					if (!a.isDeleted() && a.getStopDate() != null) {
						if (latestStoppedAffiliation != null) {
							if (latestStoppedAffiliation.getStopDate().before(a.getStopDate())) {
								latestStoppedAffiliation = a;
							}
						}
						else {
							latestStoppedAffiliation = a;
						}
					}
				}
				
				affiliation = latestStoppedAffiliation;
			}
		}

		if (affiliation != null) {
			if (affiliation.getOrgUnit() != null &&
				affiliation.getOrgUnit().getManager() != null &&
				affiliation.getOrgUnit().getManager().getManager() != null) {
				
				return affiliation.getOrgUnit().getManager().getManager();
			}
		}
		
		return null;
	}

	public static String getEmail(Person person) {
		Optional<User> user = person.getUsers().stream().filter(u -> SupportedUserTypeService.isExchange(u.getUserType()) && u.isPrime()).findFirst();
		if (user.isPresent()) {
			return user.get().getUserId();
		}
		
		return null;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
	public void handlePersonsOnLeave() {
		Date now = new Date();
		List<Person> persons = personDao.findByLeaveNotNull();
		
		for (Person person : persons) {
			if (person.getLeave().getStopDate() == null) {
				continue;
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(person.getLeave().getStopDate());
			cal.add(Calendar.HOUR_OF_DAY, 23);
			cal.add(Calendar.MINUTE, 59);
			cal.add(Calendar.SECOND, 59);
			Date stopDate = cal.getTime();

			if (now.after(stopDate)) {
				List<AccountOrder> orders = removeLeave(person);
				for (AccountOrder order : orders) {
					accountOrderService.save(order);
				}

				personDao.save(person);
			}
		}
	}

	public static String maskCpr(String cpr) {
		boolean userHasCPRAccess = SecurityUtil.getUserRoles().contains(RoleConstants.USER_ROLE_CPR_ACCESS);
		if (userHasCPRAccess) {
			return cpr;
		}

		if (cpr != null && cpr.length() > 6) {
			return cpr.substring(0, 6) + "-XXXX";
		}

		return "";
	}
	
	public String sanatizeAccountsToDeactivate(String accountsToDeactivate) {
		if (accountsToDeactivate == null || accountsToDeactivate.length() == 0) {
			return "";
		}

		String[] userTypes = accountsToDeactivate.split(",");
		
		StringBuilder builder = new StringBuilder();
		for (String userType : userTypes) {
			SupportedUserType sup = supportedUserTypeService.findByKey(userType);
			if (sup != null && sup.isCanOrder()) {
				if (builder.length() > 0) {
					builder.append(",");
				}

				builder.append(userType);
			}
		}
		
		return builder.toString();
	}

	public List<AccountOrder> removeLeave(Person person) {
		List<AccountOrder> newOrders = new ArrayList<AccountOrder>();
		
		// safety valve
		if (person.getLeave() == null) {
			return newOrders;
		}

		if (person.getLeave().isDeactivateAccounts()) {
			if (person.getLeave().getAccountsToDeactivate() != null && person.getLeave().getAccountsToDeactivate().length() > 0) {
				String[] accountsToReactivate = sanatizeAccountsToDeactivate(person.getLeave().getAccountsToDeactivate()).split(",");
				
				newOrders = generateReactivateOrders(person, accountsToReactivate);
			}
		}
		
		// if the disable flag was set during leave, then remove it
		if (person.getLeave().isDisableAccountOrders()) {
			person.setDisableAccountOrders(false);
		}
		
		// bye bye leave flag
		person.setLeave(null);
		
		return newOrders;
	}

	public List<AccountOrder> generateReactivateOrders(Person person, String[] accountsToReactivate) {
		List<AccountOrder> newOrders = new ArrayList<AccountOrder>();

		List<AccountOrder> pendingOrders = accountOrderService.getPendingOrders(person);
		List<User> deactivatedUsers = person.getUsers().stream().filter(u -> u.isDisabled() == true).collect(Collectors.toList());
		
		for (String accountToReactivate : accountsToReactivate) {
			// if the original string is empty, we get a single empty entry, other than that we are assured valid data
			if (accountToReactivate.length() == 0) {
				continue;
			}

			// remove any pending deactivate and delete orders for this type
			for (AccountOrder order : pendingOrders) {
				if (!order.getUserType().equals(accountToReactivate)) {
					continue;
				}
				
				switch (order.getOrderType()) {
					case DEACTIVATE:
					case DELETE:
						accountOrderService.delete(order);
						break;
					case CREATE:
						// ignore
						break;
				}
			}
			
			// reactivate any deactivated accounts for this user of this type
			for (User user : deactivatedUsers) {
				if (!accountToReactivate.equals(user.getUserType())) {
					continue;
				}
				
				// if there already is a pending order to reactivate this account, skip
				boolean skip = false;
				for (AccountOrder order : pendingOrders) {
					if (!order.getOrderType().equals(AccountOrderType.CREATE)) {
						continue;
					}
					
					if (!order.getUserType().equals(user.getUserType())) {
						continue;
					}
					
					if (!Objects.equals(order.getRequestedUserId(), user.getUserId())) {
						continue;
					}
					
					// we have a CREATE (reactivate) order for this specific account, so skip
					skip = true;
				}
				
				if (skip) {
					continue;
				}
				
				SupportedUserType supportedUserType = supportedUserTypeService.findByKey(user.getUserType());
				AccountOrder order = accountOrderService.createAccountOrder(person, supportedUserType, null, user.getEmployeeId(), new Date(), EndDate.NO);
				order.setRequestedUserId(user.getUserId());
				newOrders.add(order);
			}
		}
		
		return newOrders;
	}

	public List<AccountOrder> generateDeactivateOrders(Person person, String[] accountsToDeactivate, Date startDate) {
		List<AccountOrder> newOrders = new ArrayList<AccountOrder>();

		List<User> activeUsers = person.getUsers().stream().filter(u -> u.isDisabled() == false).collect(Collectors.toList());
		
		for (String accountToDeactivate : accountsToDeactivate) {
			// split can have empty entries *sigh*
			if (accountToDeactivate.length() == 0) {
				continue;
			}
			
			for (User user : activeUsers) {
				if (!accountToDeactivate.equals(user.getUserType())) {
					continue;
				}

				// only generate deactive orders if deactivate is enabled
				SupportedUserType supportedUserType = supportedUserTypeService.findByKey(user.getUserType());
				if (supportedUserType == null || supportedUserType.isCanOrder() == false || supportedUserType.getDaysToDeactivate() == 0) {
					continue;
				}
				
				AccountOrder order = accountOrderService.deactivateOrDeleteAccountOrder(AccountOrderType.DEACTIVATE, person, user.getEmployeeId(), user.getUserType(), user.getUserId(), startDate);
				newOrders.add(order);
			}
		}
		
		return newOrders;
	}
}
