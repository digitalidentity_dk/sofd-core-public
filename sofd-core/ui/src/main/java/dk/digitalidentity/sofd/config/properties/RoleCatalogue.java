package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoleCatalogue {
	private String url = "";
	
	@JsonIgnore
	private String apiKey = "";
}
