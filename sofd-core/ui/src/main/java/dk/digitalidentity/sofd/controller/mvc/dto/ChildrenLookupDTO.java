package dk.digitalidentity.sofd.controller.mvc.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChildrenLookupDTO {
	private String cpr;
	private String name;
}
