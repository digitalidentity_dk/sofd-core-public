package dk.digitalidentity.sofd.controller.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import dk.digitalidentity.sofd.controller.mvc.dto.CreateAccountOrderDTO;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.enums.EndDate;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import lombok.extern.log4j.Log4j;

@Log4j
@RequireControllerWriteAccess
@Controller
public class AccountOrderController {

	@Autowired
	private AccountOrderService accountOrderService;

	@Autowired
	private PersonService personService;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;
	
	@GetMapping("/ui/account/order/{uuid}/error/{cause}")
	public String errorPage(Model model, @PathVariable("uuid") String uuid, @PathVariable("cause") String cause) {
		model.addAttribute("uuid", uuid);
		model.addAttribute("cause", cause);

		return "accountorder/error";
	}

	@GetMapping("/ui/account/order/{personUUID}/{userType}")
	public String orderNewAccount(Model model, @PathVariable("personUUID") String personUUID, @PathVariable("userType") String userType) {
		Person person = personService.getByUuid(personUUID);
		if (person == null) {
			log.warn("Person does not exist: " + personUUID);
			return "redirect:/ui/person";
		}

		List<Affiliation> affiliations = new ArrayList<>();
		Set<String> userIds = new HashSet<>();
		
		SupportedUserType supportedUserType = supportedUserTypeService.findByKey(userType);
		if (supportedUserType == null) {
			log.error("Unknown userType for orderNewAccount flow: " + userType);
			return "redirect:/ui/person/view/" + person.getUuid();			
		}
		
		if (SupportedUserTypeService.isExchange(userType)) {
			userIds = accountOrderService.getActiveDirectoryUsersForExchangeAccount(person.getAffiliations(), false);
			
			if (userIds.size() == 0) {
				log.warn("Person does not have any valid AD accounts for Exchange creation: " + personUUID);
				return "redirect:/ui/account/order/" + person.getUuid() + "/error/exchange";				
			}
		}
		else if (SupportedUserTypeService.isOpus(userType)) {
			// we always send out affiliations for OPUS accounts
			affiliations = AffiliationService.notStoppedAffiliations(person.getAffiliations());
			affiliations = affiliations.stream().filter(u -> "OPUS".equals(u.getMaster())).collect(Collectors.toList());

			if (affiliations.size() == 0) {
				log.warn("Person does not have any OPUS affiliations: " + personUUID);
				return "redirect:/ui/account/order/" + person.getUuid() + "/error/affiliation";
			}
		}
		else if (!supportedUserType.isSingleUserMode()) {
			// if the userType requires a link to an affiliation, we send all affiliations
			affiliations = AffiliationService.notStoppedAffiliations(person.getAffiliations());

			if (affiliations.size() == 0) {
				log.warn("Person does not have any relevant affiliations: " + personUUID);
				return "redirect:/ui/account/order/" + person.getUuid() + "/error/affiliation";
			}
		}
		
		CreateAccountOrderDTO accountOrder = CreateAccountOrderDTO.builder()
				.personUuid(personUUID)
				.personName(PersonService.getName(person))
				.userType(userType)
				.showEndDate(SupportedUserTypeService.getActiveDirectoryUserType().equals(userType))
				.affiliationUuid((affiliations.size() > 0) ? affiliations.get(0).getUuid() : "")
				.build();

		model.addAttribute("order", accountOrder);
		model.addAttribute("affiliations", affiliations);
		model.addAttribute("userIds", userIds);

		return "accountorder/order";
	}

	@PostMapping("/ui/account/order")
	public String createNewOrder(Model model, @ModelAttribute("order") CreateAccountOrderDTO order) {
		Person person = personService.getByUuid(order.getPersonUuid());
		if (person == null) {
			log.warn("Person does not exist: " + order.getPersonUuid());
			return "redirect:/ui/person";
		}
		
		SupportedUserType supportedUserType = supportedUserTypeService.findByKey(order.getUserType());
		if (supportedUserType == null) {
			log.warn("UserType does not exist: " + order.getUserType());
			return "redirect:/ui/person/view/" + person.getUuid();
		}

		if (order.getChosenUserId() != null) {
			order.setChosenUserId(order.getChosenUserId().trim());
		}
		else {
			order.setChosenUserId("");
		}
		
		// extra validation for exchange accounts
		if (SupportedUserTypeService.isExchange(order.getUserType())) {
			Set<String> userIds = accountOrderService.getActiveDirectoryUsersForExchangeAccount(person.getAffiliations(), false);

			// if they have added an actual mail domain, trim it
			if (order.getChosenUserId().contains("@")) {
				order.setChosenUserId(order.getChosenUserId().substring(0, order.getChosenUserId().indexOf("@")));
			}
			
			if (!userIds.contains(order.getUserId())) {
				log.warn("Chosen userId is not valid for ordering an Exchange Account: " + order.getUserId());
				return "redirect:/ui/account/order/" + person.getUuid() + "/error/exchange";
			}
		}

		// extra validation if we are not running in singleAccountMode (or if we are creating an OPUS account)
		String employeeId = null;
		if (!supportedUserType.isSingleUserMode() || SupportedUserTypeService.isOpus(supportedUserType.getKey())) {
			boolean valid = false;

			for (Affiliation affiliation : AffiliationService.onlyActiveAffiliations(person.getAffiliations())) {
				if (affiliation.getUuid().equals(order.getAffiliationUuid())) {
					valid = true;
					employeeId = affiliation.getEmployeeId();
					break;
				}
			}
			
			if (!valid) {
				log.warn("Chosen employeeId does not belong to chosen person: " + order.getAffiliationUuid());
				return "redirect:/ui/account/order/" + person.getUuid() + "/error/affiliation";
			}
		}
		
		AccountOrder accountOrder = accountOrderService.createAccountOrder(
				person,
				supportedUserType,
				order.getUserId(),
				employeeId,
				new Date(),
				(SupportedUserTypeService.getActiveDirectoryUserType().equals(supportedUserType.getKey()) ? order.getUserEndDate() : EndDate.NO));
		
		accountOrder.setRequestedUserId(order.getChosenUserId());
		
		accountOrderService.save(accountOrder);

		return "redirect:/ui/report/accountorders";
	}
}
