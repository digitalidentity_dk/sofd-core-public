package dk.digitalidentity.sofd.controller.api.v2.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j;

@Log4j
public abstract class BaseRecord {
	private static ObjectMapper mapper = new ObjectMapper();

	protected LocalDate toLocalDate(Date date) {
		if (date == null) {
			return null;
		}

	    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	protected LocalDateTime toLocalDateTime(Date date) {
	    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	protected Date toDate(LocalDate localDate) {
		if (localDate == null) {
			return null;
		}

	    return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	protected String mapToString(Map<String, Object> map) {
		if (map == null) {
			return null;
		}

		try {
			return mapper.writeValueAsString(map);
		}
		catch (Exception ex) {
			log.error("Failed to convert map to string", ex);
			
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> stringToMap(String localExtension) {
		if (StringUtils.isEmpty(localExtension)) {
			return null;
		}

		try {
			return (Map<String, Object>) mapper.readValue(localExtension, Map.class);
		}
		catch (Exception ex) {
			log.error("Failed to convert string to map", ex);

			return null;
		}
	}
}
