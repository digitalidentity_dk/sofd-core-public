package dk.digitalidentity.sofd.controller.mvc;

import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.opensaml.common.SAMLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import dk.digitalidentity.sofd.controller.mvc.dto.ClientActivityDTO;
import dk.digitalidentity.sofd.security.SecurityUtil;
import dk.digitalidentity.sofd.service.ClientService;

@Controller
public class DefaultController implements ErrorController {
	private ErrorAttributes errorAttributes = new DefaultErrorAttributes();

	@Value(value = "${error.showtrace:false}")
	private boolean showStackTrace;

	@Autowired
	private ClientService clientService;

	@GetMapping(value = { "/" })
	public String index(Model model) {
		if (SecurityUtil.getUser() != null) {
			model.addAttribute("clients", clientService.findAll().stream()
					.filter(c -> c.isShowOnFrontpage())
					.map(c -> new ClientActivityDTO(c))
					.collect(Collectors.toList()));
		}

		return "index";
	}

	@RequestMapping(value = "/error", produces = "text/html")
	public String errorPage(Model model, HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(request, showStackTrace);

		// deal with SAML errors first
		Object status = body.get("status");
		if (status != null && status instanceof Integer) {
			if ((Integer) status == 999) {
				Object authException = request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

				// handle the forward case
				if (authException == null && request.getSession() != null) {
					authException = request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				}
				
				if (authException != null && authException instanceof Throwable) {
					StringBuilder builder = new StringBuilder();
					Throwable t = (Throwable) authException;

					logThrowable(builder, t, false);
					model.addAttribute("exception", builder.toString());
					
					if (t.getCause() != null) {
						t = t.getCause();

						// deal with the known causes for this error
						if (t instanceof SAMLException) {
							if (t.getCause() != null && t.getCause() instanceof CredentialsExpiredException) {
								model.addAttribute("cause", "EXPIRED");
							}
							else if (t.getMessage() != null && t.getMessage().contains("Response issue time is either too old or with date in the future")) {
								model.addAttribute("cause", "SKEW");
							}
							else if (t.getMessage() != null && t.getMessage().contains("urn:oasis:names:tc:SAML:2.0:status:Responder")) {
								model.addAttribute("cause", "RESPONDER");
							}
							else {
								model.addAttribute("cause", "UNKNOWN");
							}
						}
						else {
							model.addAttribute("cause", "UNKNOWN");
						}
					}

					return "samlerror";
				}
			}
		}

		// default to ordinary error message in case error is not SAML related
		model.addAllAttributes(body);

		return "error";
	}
	
	private void logThrowable(StringBuilder builder, Throwable t, boolean append) {
		StackTraceElement[] stackTraceElements = t.getStackTrace();

		builder.append((append ? "Caused by: " : "") + t.getClass().getName() + ": " + t.getMessage() + "\n");
		for (int i = 0; i < 5 && i < stackTraceElements.length; i++) {
			builder.append("  ... " + stackTraceElements[i].toString() + "\n");
		}
		
		if (t.getCause() != null) {
			logThrowable(builder, t.getCause(), true);
		}
	}

	@RequestMapping(value = "/error", produces = "application/json")
	public ResponseEntity<Map<String, Object>> errorJSON(HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(request, showStackTrace);

		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		try {
			status = HttpStatus.valueOf((int) body.get("status"));
		}
		catch (Exception ex) {
			;
		}

		return new ResponseEntity<>(body, status);
	}

	@Override
	public String getErrorPath() {
		return "/_dummyErrorPage";
	}

	private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);

		return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
	}
}
