package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Modules {
	private AccountCreation accountCreation = new AccountCreation();
	private SmsGateway smsGateway = new SmsGateway();
	private PersonComments personComments = new PersonComments();
	private Los los = new Los();
	private Telephony telephony = new Telephony();
	private LocalLogin localLogin = new LocalLogin();
	private Profile profile = new Profile();
}
