package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.BadWord;

@RepositoryRestResource(exported = false)
public interface BadWordsDao extends CrudRepository<BadWord, Long> {
	List<BadWord> findAll();

	BadWord findByValue(String word);
}
