package dk.digitalidentity.sofd.dao.model.projection;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import dk.digitalidentity.sofd.dao.model.Person;

@Projection(name = "grid", types = { Person.class })
public interface GridPerson {
	String getUuid();
	List<GridUser> getUsers();
	List<GridPersonAffiliation> getAffiliations();

	@Value("#{target.getChosenName() != null ? target.getChosenName() : target.getFirstname() + \" \" + target.getSurname()}")
	String getName();
}
