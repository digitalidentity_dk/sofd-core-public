package dk.digitalidentity.sofd.dao.model.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;

@Projection(name = "withParentUuid", types = { Person.class })
public interface PersonWithParentUuid {
	
	// all existing fields (almost - overwrote affiliations)
	
	// TODO: could we move this into a "master" interface, so we do not need to update these everywhere?
	
	String getUuid();
	String getMaster();
	Date  getCreated();
	Date getLastChanged();
	boolean isDeleted();
	String getCpr();
	String getFirstname();
	String getSurname();
	String getChosenName();
	Date getFirstEmploymentDate();
	Date getAnniversaryDate();
	Post getRegisteredPostAddress();
	Post getResidencePostAddress();
	Set<Phone> getPhones();
	Set<User> getUsers();
		
	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	String getLocalExtensions();
	
	// affiliations should be projected
	
	Set<AffiliationWithParentUuid> getAffiliations();

}
