package dk.digitalidentity.sofd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.dao.NotificationDao;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitAccountOrder;
import dk.digitalidentity.sofd.dao.model.OrgUnitAccountOrderType;
import dk.digitalidentity.sofd.dao.model.OrgUnitAccountOrderTypePosition;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.SupportedUserType;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;

@Service
public class NotificationService {

	@Autowired
	private NotificationDao notificationDao;
	
	@Autowired
	private SupportedUserTypeService userTypeService;
	
	@Autowired
	private AccountOrderService accountOrderService;
	
	@Autowired
	private OrgUnitService orgUnitService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private SupportedUserTypeService supportedUserTypeService;

	public long countActive() {
		return notificationDao.countByActiveTrue();
	}
	
	public List<Notification> findAll() {
		return notificationDao.findAll();
	}

	public Notification findById(long id) {
		return notificationDao.findById(id);
	}

	public Notification save(Notification notification) {
		return notificationDao.save(notification);
	}
	
	public List<Notification> findAllByType(NotificationType type) {
		return notificationDao.findAllByNotificationType(type);
	}

	@Transactional
	public long generateUsersNotSupportedByRuleNotifications() {
		List<SupportedUserType> supportedUserTypes = userTypeService.findAll().stream()
				.filter(u -> u.isCanOrder())
				.collect(Collectors.toList());
		
		// if nothing can be ordered, no events can be generated
		if (supportedUserTypes.size() == 0) {
			return 0;
		}

		Map<String, Notification> existingNotificationsMap = findAllByType(NotificationType.PERSON_WITH_DISALLOWED_ACCOUNT).stream().collect(Collectors.toMap(Notification::getAffectedEntityUuid, Function.identity()));
		List<Notification> notifications = new ArrayList<>();
		
		List<Person> persons = personService.getActive();
		for (Person person : persons) {
			if (person.getUsers() == null || person.getUsers().size() == 0) {
				continue;
			}

			StringBuilder builder = null;
			for (SupportedUserType userType : supportedUserTypes) {

				// if the person does not have any of these accounts, skip to next userType
				if (!person.onlyActiveUsers().stream().anyMatch(u -> u.getUserType().equals(userType.getKey()))) {
					continue;
				}
				
				// the user has an account of this type - is it supported by a rule?
				boolean shouldOrder = false;
				for (Affiliation affiliation : person.getAffiliations()) {
					shouldOrder = accountOrderService.shouldOrderAccountOfType(userType.getKey(), affiliation, (int) userType.getDaysBeforeToCreate(), true);

					if (shouldOrder) {
						break;
					}
				}
				
				if (!shouldOrder) {
					Optional<User> oUser = person.getUsers().stream().filter(u -> u.getUserType().equals(userType.getKey())).findFirst();
					if (oUser.isPresent()) {
						if (builder == null) {
							builder = new StringBuilder("Følgende brugerkonti er ikke understøttet af bestillingsregler:\n\n");
						}
						
						builder.append("'" + oUser.get().getUserId() + "' af typen '" + supportedUserTypeService.getPrettyName(oUser.get().getUserType()) + "'\n");
					}
				}
			}
			
			if (builder != null && !existingNotificationsMap.containsKey(person.getUuid())) {
				addNotification(notifications, builder.toString(), PersonService.getName(person), person.getUuid(), EntityType.PERSON, NotificationType.PERSON_WITH_DISALLOWED_ACCOUNT);
			}

			if (builder != null) {
				builder = null;
			}
			else {
				// cleanup old notifications (the accounts are now covered by rules)
				// TODO: what hapens if 2 accounts where not covered by rules, but then one of them suddenly is?
				//       we never cleanup the notification in that case, and the message stays the same
				if (existingNotificationsMap.containsKey(person.getUuid())) {
					Notification notification = existingNotificationsMap.get(person.getUuid());

					notificationDao.delete(notification);
				}
			}
		}
		
		if (notifications.size() > 0) {
			notificationDao.save(notifications);
		}
		
		return notifications.size();
	}

	@Transactional
	public long generateDeletedOrgUnitNotifications() {
		Map<String, Notification> existingNotificationsMap = findAllByType(NotificationType.ORGUNIT_DELETED).stream().collect(Collectors.toMap(Notification::getAffectedEntityUuid, Function.identity()));
		
		List<Notification> notifications = new ArrayList<Notification>();
		for (OrgUnit orgUnit : orgUnitService.getAll()) {
			if (orgUnit.isDeleted()) {
				if (!existingNotificationsMap.containsKey(orgUnit.getUuid())) {
					addNotification(notifications, null, orgUnit.getName(), orgUnit.getUuid(), EntityType.ORGUNIT, NotificationType.ORGUNIT_DELETED);
				}
			}
			else {
				if (existingNotificationsMap.containsKey(orgUnit.getUuid())) {
					Notification notification = existingNotificationsMap.get(orgUnit.getUuid());

					notificationDao.delete(notification);
				}
			}
		}
		
		if (notifications.size() > 0) {
			notificationDao.save(notifications);
		}
		
		return notifications.size();
	}

	@Transactional
	public long generateMissingRulesNotifications() {
		List<String> supportedUserTypes = userTypeService.findAll().stream()
				.filter(u -> u.isCanOrder())
				.map(u -> u.getKey())
				.collect(Collectors.toList());
		
		// if nothing can be ordered, no events can be generated
		if (supportedUserTypes.size() == 0) {
			return 0;
		}
		
		Map<String, Notification> existingNotificationsMap = findAllByType(NotificationType.ORGUNIT_WITH_MISSING_RULES).stream().collect(Collectors.toMap(Notification::getAffectedEntityUuid, Function.identity()));
		List<Notification> notifications = new ArrayList<Notification>();
		StringBuilder builder = null;		
		
		for (OrgUnit orgUnit : orgUnitService.getAllActiveWithAffiliations()) {
			OrgUnitAccountOrder accountOrder = accountOrderService.getAccountOrderSettings(orgUnit, false);
			boolean shouldNotify = false;

			for (OrgUnitAccountOrderType type : accountOrder.getTypes()) {
				if (!supportedUserTypes.contains(type.getUserType())) {
					continue;
				}

				boolean missingRule = false;

				switch (type.getRule()) {
					case DISABLED:
					case EVERYONE:
					case EVERYONE_EXCEPT_HOURLY_PAID:
						// ok, no notification needed
						break;
					case BY_POSITION_NAME:
						for (OrgUnitAccountOrderTypePosition position : type.getPositions()) {
							switch (position.getRule()) {
								case BY_POSITION_NAME:
								case DISABLED:
								case EVERYONE:
								case EVERYONE_EXCEPT_HOURLY_PAID:
									// ok, no notification needed
									break;
								case UNDECIDED:
									if (!shouldNotify) {
										builder = new StringBuilder();
										shouldNotify = true;
									}
									missingRule = true;									
									break;
							}
						}
						break;
					case UNDECIDED:
						if (!shouldNotify) {
							builder = new StringBuilder();
							shouldNotify = true;
						}
						missingRule = true;
						break;
				}
				
				if (missingRule) {
					builder.append("Mangler regler for '" + supportedUserTypeService.getPrettyName(type.getUserType()) + "'\n");
				}
			}

			if (shouldNotify && !existingNotificationsMap.containsKey(orgUnit.getUuid())) {
				addNotification(notifications, builder.toString(), orgUnit.getName(), orgUnit.getUuid(), EntityType.ORGUNIT, NotificationType.ORGUNIT_WITH_MISSING_RULES);
			}
			else if (!shouldNotify && existingNotificationsMap.containsKey(orgUnit.getUuid())) {
				Notification notification = existingNotificationsMap.get(orgUnit.getUuid());
				
				notificationDao.delete(notification);
			}
		}
		
		if (notifications.size() > 0) {
			notificationDao.save(notifications);
		}
		
		return notifications.size();
	}
	
	private void addNotification(List<Notification> notifications, String message, String name, String uuid, EntityType entityType, NotificationType notificationType) {
		Notification notification = new Notification();
		notification.setActive(true);
		notification.setAffectedEntityName(name);
		notification.setAffectedEntityType(entityType);
		notification.setAffectedEntityUuid(uuid);
		notification.setMessage(message);
		notification.setCreated(new Date());
		notification.setNotificationType(notificationType);

		notifications.add(notification);
	}
}
