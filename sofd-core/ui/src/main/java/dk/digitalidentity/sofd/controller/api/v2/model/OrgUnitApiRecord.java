package dk.digitalidentity.sofd.controller.api.v2.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

import dk.digitalidentity.sofd.dao.model.*;
import dk.digitalidentity.sofd.service.OrgUnitService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OrgUnitApiRecord extends BaseRecord {

	// primary key
	@Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})", message = "Invalid uuid")
	private String uuid;

	// read/write fields

	@NotNull
	private String master;
	
	@NotNull
	private String masterId;
	
	@NotNull
	private String shortname;
	
	@NotNull
	private String name;
	
	private Long cvr;
	private Long ean;
	private Long senr;
	private Long pnr;
	private String costBearer;
	private String orgType;
	private Long orgTypeId;
	private String parentUuid;
	private Map<String, Object> localExtensions;
	private Boolean deleted;

	@Valid
	private Set<PostApiRecord> postAddresses;
	
	@Valid
	private Set<PhoneApiRecord> phones;
	
	@Valid
	private Set<EmailApiRecord> emails;	
	
	// readonly

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime created;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastChanged;

	private ManagerApiRecord manager;

	private Set<OrgUnitTagApiRecord> tags;
	
	public OrgUnitApiRecord(OrgUnit orgUnit) {
		this.uuid = orgUnit.getUuid();
		this.master = orgUnit.getMaster();
		this.masterId = orgUnit.getMasterId();
		this.created = (orgUnit.getCreated() != null) ? toLocalDateTime(orgUnit.getCreated()) : null;
		this.lastChanged = (orgUnit.getLastChanged() != null) ? toLocalDateTime(orgUnit.getLastChanged()) : null;
		this.deleted = orgUnit.isDeleted();
		this.shortname = orgUnit.getShortname();
		this.name = orgUnit.getName();
		this.cvr = orgUnit.getCvr();
		this.ean = orgUnit.getEan();
		this.senr = orgUnit.getSenr();
		this.pnr = orgUnit.getPnr();
		this.costBearer = orgUnit.getCostBearer();
		this.orgType = orgUnit.getOrgType();
		this.orgTypeId = orgUnit.getOrgTypeId();
		this.localExtensions = stringToMap(orgUnit.getLocalExtensions());
		this.parentUuid = (orgUnit.getParent() != null) ? orgUnit.getParent().getUuid() : null;
		
		if (orgUnit.getManager() != null) {
			this.manager = new ManagerApiRecord();
			this.manager.setName(orgUnit.getManager().getName());
			
			// TODO: might want to extract the uuid to the manager table, so we do not need a full lookup on the person here...
			// TODO: den er faktisk allerede EAGER, så den loades altid, men det kunne man så optimere ud...
			this.manager.setUuid(orgUnit.getManager().getManager().getUuid());
			this.manager.setInherited(orgUnit.getManager().isInherited());
		}
		
		if (orgUnit.getPhones() != null) {
			this.phones = new HashSet<PhoneApiRecord>();

			for (Phone phone : orgUnit.getPhones()) {
				this.phones.add(new PhoneApiRecord(phone));
			}
		}
		
		if (orgUnit.getPostAddresses() != null) {
			this.postAddresses = new HashSet<PostApiRecord>();

			for (Post post : orgUnit.getPostAddresses()) {
				this.postAddresses.add(new PostApiRecord(post));
			}
		}
		
		if (orgUnit.getEmails() != null) {
			this.emails = new HashSet<EmailApiRecord>();

			for (Email email : orgUnit.getEmails()) {
				this.emails.add(new EmailApiRecord(email));
			}
		}

		if( orgUnit.getTags() != null) {
			this.tags = new HashSet<OrgUnitTagApiRecord>();

			for (OrgUnitTag tag : orgUnit.getTags()) {

				this.tags.add(new OrgUnitTagApiRecord(tag));
			}
		}
	}

	public OrgUnit toOrgUnit() {
		OrgUnit orgUnit = new OrgUnit();

		// TODO: the current constructor has some work-arounds for SDR that we have to counter here
		//       but we can remove these null's once SDR is dead, and the constructor no longer has these default values
		orgUnit.clearCollections();

		orgUnit.setLocalExtensions(mapToString(localExtensions));
		orgUnit.setMaster(master);
		orgUnit.setMasterId(masterId);
		orgUnit.setCostBearer(costBearer);
		orgUnit.setCvr(cvr);
		orgUnit.setEan(ean);
		orgUnit.setName(name);
		orgUnit.setOrgType(orgType);
		orgUnit.setOrgTypeId(orgTypeId);
		orgUnit.setPnr(pnr);
		orgUnit.setSenr(senr);
		orgUnit.setShortname(shortname);		
		orgUnit.setUuid(uuid);
		orgUnit.setDeleted((deleted != null) ? deleted : false);

		if (parentUuid != null) {
			OrgUnit ou = OrgUnitService.getInstance().getByUuid(parentUuid);
			if (ou != null) {
				orgUnit.setParent(ou);
			}
		}

		if (postAddresses != null) {
			orgUnit.setPostAddresses(new ArrayList<>());

			for (PostApiRecord postRecord : postAddresses) {
				orgUnit.getPostAddresses().add(postRecord.toPost());
			}
		}
		
		if (phones != null) {
			orgUnit.setPhones(new ArrayList<>());

			for (PhoneApiRecord phoneRecord : phones) {
				orgUnit.getPhones().add(phoneRecord.toPhone());
			}
		}

		if (emails != null) {
			orgUnit.setEmails(new ArrayList<>());

			for (EmailApiRecord emailRecord : emails) {
				orgUnit.getEmails().add(emailRecord.toEmail());
			}
		}

		if( manager != null) {
			orgUnit.setManager(manager.toOrgUnitManager());
		}

		return orgUnit;
	}
}
