package dk.digitalidentity.sofd.log;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.dao.AuditLogDao;
import dk.digitalidentity.sofd.dao.model.AuditLog;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.EventType;
import dk.digitalidentity.sofd.dao.model.enums.ReportType;
import dk.digitalidentity.sofd.security.SecurityUtil;

@Component
public class AuditLogger {

	@Autowired
	private AuditLogDao auditLogDao;

	public void log(Loggable entity, EventType eventType) {
		log(entity.getEntityId(), entity.getEntityType(), eventType);
	}

	public void log(ReportType report, EventType eventType) {
		log(report.toString(), EntityType.REPORT, eventType);
	}
	
	public void log(String entityId, EntityType entityType, EventType eventType) {
		AuditLog entry = new AuditLog();
		entry.setTimestamp(new Date());
		entry.setEntityId(entityId);
		entry.setEntityType(entityType);
		entry.setEventType(eventType);
		
		if (SecurityUtil.getUser() == null) {
			entry.setUserId("system");
		}
		else {
			entry.setUserId(SecurityUtil.getUser());
		}

		if (SecurityUtil.getUsername() == null) {
			entry.setUsername("system");
		}
		else {
			entry.setUsername(SecurityUtil.getUsername());
		}

		auditLogDao.save(entry);		
	}
}