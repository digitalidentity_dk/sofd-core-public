package dk.digitalidentity.sofd.controller.api.v2.model;

import java.time.LocalDate;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

import dk.digitalidentity.sofd.dao.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserApiRecord extends BaseRecord {
	
	// primary key
	
	@NotNull
	private String master;
	
	@NotNull
	private String masterId;

	// read/write fields

	@Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})", message = "Invalid uuid")
	private String uuid;
	
	@NotNull
	private String userId;
	
	@NotNull
	private String userType;

	private String employeeId;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate passwordExpireDate;

	private Map<String, Object> localExtensions;

	private Boolean disabled;

	// read-only
	private boolean prime;

	public UserApiRecord(User user) {
		this.master = user.getMaster();
		this.masterId = user.getMasterId();
		this.uuid = user.getUuid();
		this.userId = user.getUserId();
		this.employeeId = user.getEmployeeId();
		this.userType = user.getUserType();
		this.localExtensions = stringToMap(user.getLocalExtensions());
		this.passwordExpireDate = user.getPasswordExpireDate();
		this.prime = user.isPrime();
		this.disabled = user.getDisabled();
	}

	public User toUser() {
		User user = new User();
		user.setEmployeeId(employeeId);
		user.setLocalExtensions(mapToString(localExtensions));
		user.setMaster(master);
		user.setMasterId(masterId);
		user.setPasswordExpireDate((passwordExpireDate != null) ? passwordExpireDate : LocalDate.of(2099, 12, 31));
		user.setUserId(userId);
		user.setUserType(userType);
		user.setUuid(uuid);
		user.setDisabled(disabled);
		
		return user;
	}
}
