package dk.digitalidentity.sofd.dao.model.projection;

import java.util.Date;
import java.util.List;

import dk.digitalidentity.sofd.dao.model.Organisation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Email;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitManager;
import dk.digitalidentity.sofd.dao.model.OrgUnitType;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.serializer.LocalExtensionsDeserializer;
import dk.digitalidentity.sofd.serializer.LocalExtensionsSerializer;

@Projection(name = "withParentUuid", types = { OrgUnit.class })
public interface OrgUnitWithParentUuid {

	// all existing fields

	String getUuid();

	String getMaster();

	String getMasterId();

	boolean isDeleted();

	Date getCreated();

	Date getLastChanged();

	String getShortname();

	String getName();

	Long getCvr();

	Long getEan();

	Long getSenr();

	Long getPnr();

	String getCostBearer();

	OrgUnitType getType();

	String getOrgType();

	Long getOrgTypeId();

	List<Post> getPostAddresses();

	List<Phone> getPhones();

	List<Email> getEmails();

	OrgUnitManager getManager();

	List<String> getKlePrimary();

	List<String> getKleSecondary();

	String getKeyWords();

	String getOpeningHours();

	List<Affiliation> getAffiliations();

	@JsonSerialize(using = LocalExtensionsSerializer.class)
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	String getLocalExtensions();

	// just documentation to make our extension-projection test happy
	@JsonIgnore
	List<OrgUnit> getChildren();

	// just documentation to make our extension-projection test happy
	@JsonIgnore
	OrgUnit getParent();

	@JsonIgnore
	Organisation getBelongsTo();

	@Value("#{target.getParent() != null ? target.getParent().getUuid() : null}")
	String getParentUuid();

	// quick and dirty fix for mdm-integration (we need to move it to api v2 anyway
	@Value("#{target.getManager() != null ? target.getManager().getManager().getUuid() : null}")
	String getManagerUuid();
}