package dk.digitalidentity.sofd.interceptor;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.OrgUnitDao;
import dk.digitalidentity.sofd.dao.OrgUnitTypeDao;
import dk.digitalidentity.sofd.dao.PersonDao;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.OrgUnitType;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.listener.EntityListenerService;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.OrgUnitService;
import dk.digitalidentity.sofd.service.OrganisationService;
import dk.digitalidentity.sofd.service.PrimeService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;

@Component
public class AbstractBeforeSaveInterceptor {

	@Autowired
	protected PrimeService primeService;

	@Autowired
	private SupportedUserTypeService userTypeService;
	
	@Autowired
	private AccountOrderService accountOrderService;
	
	@Autowired
	private OrgUnitService orgUnitService;
	
	@Autowired
	private OrgUnitDao orgUnitDao;
	
	@Autowired
	private OrganisationService organisationService;
	
	@Autowired
	private SofdConfiguration configuration;
	
	@Autowired
	private OrgUnitTypeDao orgUnitTypeDao;
	
	@Autowired
	private PersonDao personDao;
	
	@Autowired
	private EntityListenerService entityListenerService;
	
	@Autowired
	private AbstractBeforeSaveInterceptor self;

	@Transactional
	public void handleSavePerson(Person person) {

		// always update last changed
		person.setLastChanged(new Date());

		if (person.getUuid() == null) {
			person.setUuid(UUID.randomUUID().toString());
		}

		if (person.getAffiliations() != null) {
			primeService.setPrimeAffilation(person);

			for (Affiliation affiliation : person.getAffiliations()) {
				if (affiliation.getUuid() == null || affiliation.getUuid().isEmpty()) {
					affiliation.setUuid(UUID.randomUUID().toString());
				}

				if (affiliation.getPerson() == null) {
					affiliation.setPerson(person);
				}
			}
		}

		if (person.getUsers() != null) {
			for (String userType : userTypeService.getAllUserTypes()) {
				List<User> users = person.getUsers().stream().filter(u -> u.getUserType().equals(userType)).collect(Collectors.toList());
				if (users != null && users.size() > 0) {
					primeService.setPrimeUser(users);
				}
			}
		}
		
		// empty chosenNames are NULL'ed to ensure consistency with PersonService.getName(person)
		if ("".equals(person.getChosenName())) {
			person.setChosenName(null);
		}
		
		primeService.setPrimePhone(person);
		primeService.setPrimePost(person);
		
		// emit update notifications		
		Person oldPerson = self.loadOldPerson(person.getUuid());
		if (oldPerson != null) {
			entityListenerService.emitUpdateEvent(oldPerson, person);
		}
		else {
			entityListenerService.emitCreateEvent(person);
		}
	}

	public void handleAccountOrders(Person person) {
		// we only do this when saving persons, not OrgUnits, because the order-account-settings are not part
		// of the actual OrgUnit data-structure, and editing those will not trigger this event. Instead we
		// also call this method from the actual service that sets these values on the OrgUnit
		if (person.getAffiliations() != null && person.isDisableAccountOrders() == false) {
			List<AccountOrder> orderAccounts = accountOrderService.getAccountsToCreate(person.getAffiliations(), true);

			if (orderAccounts != null && orderAccounts.size() > 0) {

				// if there are any completed OPUS orders on the table (14 days retention), we remove it from
				// the orders to avoid creating new accounts over and over and over until we get the update from the OPUS file
				List<AccountOrder> completedOpusCreateOrders = accountOrderService.findAllCompletedOpusCreateOrders(person);
				
				if (completedOpusCreateOrders != null && completedOpusCreateOrders.size() > 0) {
					List<String> employeeIdsWithOrders = completedOpusCreateOrders.stream().map(a -> a.getEmployeeId()).collect(Collectors.toList());

					for (Iterator<AccountOrder> iterator = orderAccounts.iterator(); iterator.hasNext();) {
						AccountOrder accountOrder = iterator.next();
						
						if (!SupportedUserTypeService.isOpus(accountOrder.getUserType())) {
							continue;
						}

						if (employeeIdsWithOrders.contains(accountOrder.getEmployeeId())) {
							iterator.remove();
						}
					}
				}
				
				accountOrderService.save(orderAccounts);
			}
		}
	}
	
	@Transactional
	public void handleSaveOrgUnit(OrgUnit orgUnit) {
		
		// always update last changed
		orgUnit.setLastChanged(new Date());

		if (orgUnit.getUuid() == null) {
			orgUnit.setUuid(UUID.randomUUID().toString());
		}
		
		// if the municipality has enabled LOS match, we allow them to match LOS IDs
		// to types inside SOFD Core
		if (configuration.getIntegrations().getOpus().isEnableLosIdMatch()) {
			if (orgUnit.getOrgTypeId() != null) {
				String extId = Long.toString(orgUnit.getOrgTypeId());
				
				OrgUnitType orgUnitType = orgUnitTypeDao.getByExtId(extId);
				if (orgUnitType != null) {
					orgUnit.setType(orgUnitType);
				}
			}
		}

		// default to AFDELING
		if (orgUnit.getType() == null) {
			orgUnit.setType(orgUnitService.getDepartmentType());
		}
		
		if (orgUnit.getBelongsTo() == null) {
			orgUnit.setBelongsTo(organisationService.getAdmOrg());
		}
		
		primeService.setPrimePhone(orgUnit);
		primeService.setPrimeEmail(orgUnit);
		primeService.setPrimePost(orgUnit);
		
		// emit update notifications
		OrgUnit oldOrgUnit = self.loadOldOrgUnit(orgUnit.getUuid());
		if (oldOrgUnit != null) {
			entityListenerService.emitUpdateEvent(oldOrgUnit, orgUnit);
		}
		else {
			entityListenerService.emitCreateEvent(orgUnit);
		}
	}

	// we ensure a fresh copy is loaded by setting the propagation, but we also need
	// to ensure that any data that EntityListenerService needs is loaded, so do some
	// force-loading as needed. No reason to load fields that EntityListenerService
	// does not need though
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public OrgUnit loadOldOrgUnit(String uuid) {
		OrgUnit orgUnit = orgUnitDao.findByUuid(uuid);
		if (orgUnit != null) {
			if (orgUnit.getParent() != null) {
				orgUnit.getParent().getUuid();
			}
		}

		return orgUnit;
	}
	
	// we ensure a fresh copy is loaded by setting the propagation, but we also need
	// to ensure that any data that EntityListenerService needs is loaded, so do some
	// force-loading as needed. No reason to load fields that EntityListenerService
	// does not need though
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Person loadOldPerson(String uuid) {
		Person person = personDao.findByUuid(uuid);
		if (person != null) {
			for (Affiliation affiliation : person.getAffiliations()) {
				affiliation.getOrgUnit().getUuid();
			}

			person.getChildren().size();
			person.getPhones().size();
			person.getUsers().size();
		}

		return person;
	}
}
