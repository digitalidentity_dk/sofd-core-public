package dk.digitalidentity.sofd.dao;

import java.util.List;

import dk.digitalidentity.sofd.dao.model.Kle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface KleDao extends CrudRepository<Kle, Long> {
	
	List<Kle> findAll();

	List<Kle> findAllByParent(String parent);

	Kle getByCode(String code);

	long countByActiveTrue();
}
