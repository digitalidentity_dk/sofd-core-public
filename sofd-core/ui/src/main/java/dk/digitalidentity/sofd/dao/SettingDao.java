package dk.digitalidentity.sofd.dao;

import dk.digitalidentity.sofd.dao.model.Setting;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface SettingDao extends CrudRepository<Setting, Long> {
	Setting findById(long id);
	Setting findByKey(String key);
	
	List<Setting> findAll();
}
