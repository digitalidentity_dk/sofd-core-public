package dk.digitalidentity.sofd.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import dk.digitalidentity.sofd.config.properties.Actuator;
import dk.digitalidentity.sofd.config.properties.Customer;
import dk.digitalidentity.sofd.config.properties.Integrations;
import dk.digitalidentity.sofd.config.properties.Modules;
import dk.digitalidentity.sofd.config.properties.Scheduled;
import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "sofd")
public class SofdConfiguration {
	private Modules modules = new Modules();
	private Customer customer = new Customer();
	private Integrations integrations = new Integrations();
	private Scheduled scheduled = new Scheduled();
	
	@JsonIgnore
	private Actuator actuator = new Actuator();
}
