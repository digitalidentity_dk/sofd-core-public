package dk.digitalidentity.sofd.dao.model.projection.processer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.projection.ExtendedAffiliation;

@Component
public class ExtendedAffiliationResourceProcessor implements ResourceProcessor<Resource<ExtendedAffiliation>> {

	@Autowired
	private EntityLinks entityLinks;

	@Override
	public Resource<ExtendedAffiliation> process(Resource<ExtendedAffiliation> resource) {
		ExtendedAffiliation content = resource.getContent();
		Person person = content.getPerson();
		OrgUnit orgUnit = content.getOrgUnit();

		resource.add(entityLinks.linkToSingleResource(Person.class, person.getUuid()));
		resource.add(entityLinks.linkToSingleResource(OrgUnit.class, orgUnit.getUuid()));

		for (OrgUnit managerFor : content.getManagerFor()) {
			resource.add(entityLinks.linkToSingleResource(OrgUnit.class, managerFor.getUuid()).withRel("managerFor"));
		}

		return resource;
	}
}
