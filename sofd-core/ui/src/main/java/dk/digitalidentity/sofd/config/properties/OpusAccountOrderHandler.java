package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpusAccountOrderHandler {
	private boolean enabled = false;
	private String defaultEmail = null;
	private String defaultEmailDomain = null;
	private String municipalityNumber;
	
	@JsonIgnore
	private String keystore;
	
	@JsonIgnore
	private String keystorePwd;

	// url contains a parameter that needs to be merged with the municipalityNumber
	private String wsUrl = "https://kmdpiprd.kmd.dk/XISOAPAdapter/MessageServlet?senderParty=&senderService=LPE_Employee_KOM_{CODE}&receiverParty=&receiverService=&interface=Employee_Provide_Out&interfaceNamespace=urn:kmd.dk:LPE:EMPLOYEE:external";

	@JsonIgnore
	private String convertedUrl = null;
	
	@JsonIgnore
	public String getConvertedUrl() {
		if (convertedUrl != null) {
			return convertedUrl;
		}
		
		convertedUrl = wsUrl.replace("{CODE}", municipalityNumber);
		
		return convertedUrl;
	}
}
