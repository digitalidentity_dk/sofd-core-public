package dk.digitalidentity.sofd.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.Email;
import dk.digitalidentity.sofd.dao.model.Kle;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.Setting;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.CustomerSetting;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.service.SettingService;
import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.KleService;
import dk.digitalidentity.sofd.service.OrgUnitService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import dk.digitalidentity.sofd.service.SyncService;
import dk.digitalidentity.sofd.service.model.SyncResult;
import lombok.extern.log4j.Log4j;

@Component
@EnableScheduling
@Log4j
public class OS2syncTask {
	// new OS2sync integration
	private static final String updateUserSQL = "INSERT INTO queue_users (uuid, user_id, phone_number, email, racfid, name, cpr, cvr, operation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'UPDATE');";
	private static final String deleteUserSQL = "INSERT INTO queue_users (uuid, cvr, operation) VALUES (?, ?, 'DELETE');";
	private static final String updatePositionSQL = "INSERT INTO queue_user_positions (user_id, name, orgunit_uuid) VALUES (?, ?, ?);";
	private static final String updateOrgUnitSQL = "INSERT INTO queue_orgunits (uuid, name, parent_ou_uuid, los_shortname, phone_number, email, ean, post_address, orgunit_type, cvr, operation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'UPDATE');";
	private static final String deleteOrgUnitSQL = "INSERT INTO queue_orgunits (uuid, cvr, operation) VALUES (?, ?, 'DELETE');";
	private static final String updateOrgUnitTaskSQL = "INSERT INTO queue_orgunits_tasks (unit_id, task) VALUES (?, ?);";
	
	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	@Qualifier("OS2syncTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private SyncService syncService;

	@Autowired
	private PersonService personService;

	@Autowired
	private OrgUnitService orgUnitService;

	@Autowired
	private SettingService settingService;
	
	@Autowired
	private KleService kleService;

	// run once every minute
	@Scheduled(cron = "0 0/1 * * * ?")
	@Transactional(rollbackFor = Exception.class)
	public void processChanges() {
		if (configuration.getScheduled().isEnabled() && configuration.getIntegrations().getOs2sync().isEnabled()) {
			Long lastRun = settingService.getLongValueByKey(CustomerSetting.LAST_STSSYNC_RUN);

			if (lastRun > 0L) {
				deltaUpdate(lastRun);
			}
			else {
				fullUpdate();
			}
		}
	}

	private void fullUpdate() {
		Long head = syncService.getMaxOffset();
		if (head == null) {
			head = 0L;
		}

		List<OrgUnit> orgUnits = orgUnitService.getAll();
		for (OrgUnit orgUnit : orgUnits) {
			if (orgUnit.isDeleted()) {
				deleteOrgUnit(orgUnit.getUuid());
			}
			else {
				updateOrgUnit(orgUnit);
			}
		}

		List<Person> persons = personService.getAll();
		for (Person person : persons) {
			if (person.isDeleted()) {
				deleteUser(person.getUuid());
			}
			else {
				updateUser(person);
			}
		}

		updateLatestSTSSyncRun(head);
	}

	private void deltaUpdate(Long lastRun) {
		Long updatedLastRun = lastRun;

		SyncResult syncResult = syncService.getModificationHistory(lastRun, EntityType.ORGUNIT.toString());
		Set<String> uuids = syncResult.getUuids().stream().map(w -> w.getUuid()).collect(Collectors.toSet());

		for (String uuid : uuids) {
			OrgUnit orgUnit = orgUnitService.getByUuid(uuid);

			if (orgUnit.isDeleted()) {
				deleteOrgUnit(uuid);
			} else {
				updateOrgUnit(orgUnit);
				
				// if the orgunit has inherit KLE flagged, we ALSO need to sync all children (and childrens children)
				if (orgUnit.isInheritKle()) {
					updateChildren(orgUnit, uuids);
				}
			}
		}

		if (syncResult.getOffset() > updatedLastRun) {
			updatedLastRun = syncResult.getOffset();
		}

		syncResult = syncService.getModificationHistory(lastRun, EntityType.PERSON.toString());
		uuids = syncResult.getUuids().stream().map(w -> w.getUuid()).collect(Collectors.toSet());

		for (String uuid : uuids) {
			Person person = personService.getByUuid(uuid);

			if (person.isDeleted()) {
				deleteUser(uuid);
			} else {
				updateUser(person);
			}
		}

		if (syncResult.getOffset() > updatedLastRun) {
			updatedLastRun = syncResult.getOffset();
		}

		if (updatedLastRun > lastRun) {
			updateLatestSTSSyncRun(updatedLastRun);
		}
	}

	private void updateChildren(OrgUnit orgUnit, Set<String> uuids) {
		if (orgUnit.getChildren() == null) {
			return;
		}
		
		for (OrgUnit child : orgUnit.getChildren()) {
			// no reason to update the child if already have this scheduled
			if (!uuids.contains(child.getUuid())) {
				updateOrgUnit(child);
			}

			updateChildren(child, uuids);
		}
	}

	private void updateLatestSTSSyncRun(Long lastRun) {
		Setting setting = settingService.getByKey(CustomerSetting.LAST_STSSYNC_RUN);
		setting.setValue(Long.toString(lastRun));
		settingService.save(setting);
	}

	private void updateOrgUnit(OrgUnit orgUnit) {

		// find name
		final String nameValue = orgUnit.getName();

		// find parent
		final String parentUuid = (orgUnit.getParent() != null) ? orgUnit.getParent().getUuid() : null;

		// find LOS shortkey
		final String losValue = orgUnit.getShortname();

		// find prime phone number
		Optional<Phone> phone = orgUnit.getPhones().stream().filter(p -> p.isPrime()).findFirst();
		final String phoneValue = phone.isPresent() ? phone.get().getPhoneNumber() : null;

		// find prime email
		Optional<Email> email = orgUnit.getEmails().stream().filter(m -> m.isPrime()).findFirst();
		final String emailValue = email.isPresent() ? email.get().getEmail() : null;

		// find EAN
		final String eanValue = (orgUnit.getEan() != null) ? Long.toString(orgUnit.getEan()) : null;

		// find KLE
		List<String> tasks = new ArrayList<>();
		for (String code : orgUnitService.getKleCodesIncludingInherited(orgUnit)) {
			Kle kle = kleService.getByCode(code);
			if (kle != null && !StringUtils.isEmpty(kle.getUuid())) {
				tasks.add(kle.getUuid());
			}
		}

		// find prime post address
		Optional<Post> post = orgUnit.getPostAddresses().stream().filter(p -> p.isPrime()).findFirst();
		String tmpPostValue = null;
		if (post.isPresent()) {
			Post p = post.get();

			// TODO: how does KOMBIT want this?
			tmpPostValue = p.getStreet() + ", " + p.getPostalCode() + " " + p.getCity() + ", " + p.getCountry();
		}
		final String postValue = tmpPostValue;

		final String orgUnitType;
		if ("TEAM".equals(orgUnit.getType().getKey())) {
			orgUnitType = "TEAM";
		}
		else {
			orgUnitType = "DEPARTMENT";
		}
		
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement statement = con.prepareStatement(updateOrgUnitSQL, Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, orgUnit.getUuid());
				statement.setString(2, nameValue);
				statement.setString(3, parentUuid);
				statement.setString(4, losValue);
				statement.setString(5, phoneValue);
				statement.setString(6, emailValue);
				statement.setString(7, eanValue);
				statement.setString(8, postValue);
				statement.setString(9, orgUnitType);
				statement.setString(10, configuration.getCustomer().getCvr());

				return statement;
			}
		}, holder);

		final Long primaryKey = holder.getKey().longValue();
		
		// insert Tasks
		if (tasks.size() > 0) {
			tasks.stream().forEach(t -> jdbcTemplate.update(updateOrgUnitTaskSQL, new Object[] { primaryKey, t }));
		}
	}

	private void deleteOrgUnit(String uuid) {
		jdbcTemplate.update(deleteOrgUnitSQL, new Object[] { uuid, configuration.getCustomer().getCvr() });
	}

	private void updateUser(Person person) {

		// find prime phone number
		Optional<Phone> phone = person.getPhones().stream().filter(p -> p.isPrime()).findFirst();
		final String phoneValue = phone.isPresent() ? phone.get().getPhoneNumber() : null;

		// find prime email
		Optional<User> emailUser = person.getUsers().stream().filter(u -> SupportedUserTypeService.isExchange(u.getUserType()) && u.isPrime()).findFirst();
		final String emailValue = (emailUser.isPresent()) ? emailUser.get().getUserId() : null;

		// find prime kspCics
		Optional<User> kspCicsUser = person.getUsers().stream().filter(u -> SupportedUserTypeService.isKspCics(u.getUserType()) && u.isPrime()).findFirst();
		final String kspCicsValue = (kspCicsUser.isPresent()) ? kspCicsUser.get().getUserId() : null;

		// find name
		final String nameValue = (person.getChosenName() != null) ? person.getChosenName() : (person.getFirstname() + " " + person.getSurname());

		// find cpr number
		final String cprValue = (configuration.getIntegrations().getOs2sync().isCprEnabled()) ? person.getCpr() : null;

		// find user_id
		Optional<User> adUser = person.getUsers().stream().filter(u -> SupportedUserTypeService.isActiveDirectory(u.getUserType()) && u.isPrime()).findFirst();
		final String userId = (adUser.isPresent()) ? adUser.get().getUserId() : null;

		if (userId == null) {
			log.debug("Could not synchronize person without Active Directory account: " + person.getUuid());
			return;
		}

		if (person.getAffiliations().size() == 0) {
			log.debug("Could not synchronize person without Affiliation: " + person.getUuid());
			return;
		}

		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement statement = con.prepareStatement(updateUserSQL, Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, person.getUuid());
				statement.setString(2, userId);
				statement.setString(3, phoneValue);
				statement.setString(4, emailValue);
				statement.setString(5, kspCicsValue);
				statement.setString(6, nameValue);
				statement.setString(7, cprValue);
				statement.setString(8, configuration.getCustomer().getCvr());

				return statement;
			}
		}, holder);

		final Long primaryKey = holder.getKey().longValue();

		// TODO: the 60 is a dirty hack, which ensures that persons created before they are employeed (affiliation with start-date in the future)
		//       are synchronized to STS Organisation, because we do not get an event when the affiliation becomes active, and thus we never
		//       synchronize that affiliation (at least not until we get an unrelated update on the Person)
		AffiliationService.onlyActiveAffiliations(person.getAffiliations(), 60).stream()
				.forEach(a -> jdbcTemplate.update(updatePositionSQL, new Object[] { primaryKey, a.getPositionName(), a.getOrgUnit().getUuid() }));
	}

	private void deleteUser(String uuid) {
		jdbcTemplate.update(deleteUserSQL, new Object[] { uuid, configuration.getCustomer().getCvr() });
	}
}
