package dk.digitalidentity.sofd.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import dk.digitalidentity.saml.extension.SamlLoginPostProcessor;
import dk.digitalidentity.saml.model.TokenUser;
import dk.digitalidentity.sofd.config.RoleConstants;
import dk.digitalidentity.sofd.config.SessionConstants;
import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.service.NotificationService;

@Component
public class RolePostProcesser implements SamlLoginPostProcessor {

	@Autowired
	private SofdConfiguration configuration;
	
	@Autowired
	private NotificationService adminTaskService;
	
	@Override
	public void process(TokenUser tokenUser) {
		List<GrantedAuthority> newAuthorities = new ArrayList<>();

		if (configuration.getModules().getAccountCreation().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_ACCOUNT_CREATION));
		}
		
		if (configuration.getModules().getProfile().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_PROFILE));
		}
		
		if (configuration.getModules().getSmsGateway().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_SMS_GATEWAY));
		}
		
		if (configuration.getModules().getPersonComments().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_PERSON_COMMENT));
		}
		
		if (configuration.getModules().getLos().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_LOS));
		}
		
		if (configuration.getModules().getTelephony().isEnabled()) {
			newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.MODULE_ROLE_TELEPHONY));
		}

		boolean canSeeNotifications = false;
		for (Iterator<? extends GrantedAuthority> iterator = tokenUser.getAuthorities().iterator(); iterator.hasNext();) {
			GrantedAuthority grantedAuthority = iterator.next();
			
			if ("ROLE_admin".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_ADMIN));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_SMS));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT_CONTACT_INFO));
				
				if (configuration.getModules().getLos().isEnabled()) {
					newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_LOS_ADMIN));
				}

				if (configuration.getModules().getTelephony().isEnabled()) {
					newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_TELEPHONY));
				}

				canSeeNotifications = true;
			}
			else if ("ROLE_losadmin".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT_CONTACT_INFO));

				if (configuration.getModules().getLos().isEnabled()) {
					newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_LOS_ADMIN));
				}

				canSeeNotifications = true;
			}
			else if ("ROLE_edit".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT_CONTACT_INFO));
				
				canSeeNotifications = true;
			}
			else if ("ROLE_edit_contact_info".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_EDIT_CONTACT_INFO));
			}
			else if ("ROLE_telephonyadmin".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));

				if (configuration.getModules().getTelephony().isEnabled()) {
					newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_TELEPHONY));
				}
			}
			else if ("ROLE_sms".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));

				if (configuration.getModules().getSmsGateway().isEnabled()) {
					newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_SMS));
				}
			}
			else if ("ROLE_read".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_READ));
			}
			else if ("ROLE_cpr_access".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.USER_ROLE_CPR_ACCESS));
			}
		}

		tokenUser.setAuthorities(newAuthorities);
		
		if (canSeeNotifications) {
			setNotifications();
		}
	}

	private void setNotifications() {
		HttpServletRequest request = getRequest();
		
		if (request != null) {
			long count = adminTaskService.countActive();
			request.getSession().setAttribute(SessionConstants.SESSION_NOTIFICATION_COUNT, count);
		}
	}
	
	private static HttpServletRequest getRequest() {
		try {
			return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		}
		catch (IllegalStateException ex) {
			return null;
		}
	}
}
