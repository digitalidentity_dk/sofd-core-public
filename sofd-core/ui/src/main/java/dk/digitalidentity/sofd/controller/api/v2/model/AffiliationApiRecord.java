package dk.digitalidentity.sofd.controller.api.v2.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationFunction;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationType;
import dk.digitalidentity.sofd.service.OrgUnitService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AffiliationApiRecord extends BaseRecord {

	// primary key
	
	@NotNull
	private String master;

	@NotNull
	private String masterId;

	// read/write fields
	
	@Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})", message = "Invalid uuid")
	private String uuid;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate startDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate stopDate;
	
	private String employeeId;
	private String employmentTerms;
	private String employmentTermsText;
	private Map<String, Object> localExtensions;
	private String payGrade;
	private Double workingHoursDenominator;
	private Double workingHoursNumerator;
	private String affiliationType;
	private String positionId;
	private String positionName;
	private String positionTypeId;
	private String positionTypeName;
	private Set<String> functions;
	private Set<String> managerForUuids;
	
	// TODO: remove at some point once we no longer manage deleted from our AD integration (which we really should stop doing)
	private Boolean deleted;
	
	@Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})", message = "Invalid uuid")
	private String orgUnitUuid;

	// read-only
	private boolean prime;
	private Boolean inheritPrivileges;
	private String personUuid;
	
	public AffiliationApiRecord(Affiliation affiliation) {
		this.master = affiliation.getMaster();
		this.masterId = affiliation.getMasterId();
		this.personUuid = affiliation.getPerson().getUuid();
		this.uuid = affiliation.getUuid();
		this.startDate = toLocalDate(affiliation.getStartDate());
		this.stopDate = toLocalDate(affiliation.getStopDate());
		this.inheritPrivileges = affiliation.isInheritPrivileges();
		this.deleted = affiliation.isDeleted();
		this.employeeId = affiliation.getEmployeeId();
		this.employmentTerms = affiliation.getEmploymentTerms();
		this.employmentTermsText = affiliation.getEmploymentTermsText();
		this.payGrade = affiliation.getPayGrade();
		this.workingHoursDenominator = affiliation.getWorkingHoursDenominator();
		this.workingHoursNumerator = affiliation.getWorkingHoursNumerator();
		this.affiliationType = (affiliation.getAffiliationType() != null) ? affiliation.getAffiliationType().toString() : null;
		this.positionId = affiliation.getPositionId();
		this.positionName = affiliation.getPositionName();
		this.positionTypeId = affiliation.getPositionTypeId();
		this.positionTypeName = affiliation.getPositionTypeName();
		this.localExtensions = stringToMap(affiliation.getLocalExtensions());
		this.prime = affiliation.isPrime();

		if (affiliation.getFunctions() != null) {
			this.functions = new HashSet<String>();
			
			for (AffiliationFunction affiliationFunction : affiliation.getFunctions()) {
				this.functions.add(affiliationFunction.toString());
			}
		}

		if (affiliation.getOrgUnit() != null) {
			this.orgUnitUuid = affiliation.getOrgUnit().getUuid();
		}

		if (affiliation.getManagerFor() != null) {
			this.managerForUuids = new HashSet<String>();

			for (OrgUnit orgUnit : affiliation.getManagerFor()) {
				this.managerForUuids.add(orgUnit.getUuid());
			}
		}	
	}

	public Affiliation toAffiliation(Person person) {
		Affiliation affiliation = new Affiliation();
		affiliation.setAffiliationType((affiliationType != null) ? AffiliationType.valueOf(affiliationType) : AffiliationType.EMPLOYEE);
		affiliation.setEmployeeId(employeeId);
		affiliation.setEmploymentTerms(employmentTerms);
		affiliation.setEmploymentTermsText(employmentTermsText);
		affiliation.setDeleted(deleted != null ? deleted : false);
		affiliation.setLocalExtensions(mapToString(localExtensions));
		affiliation.setMaster(master);
		affiliation.setMasterId(masterId);
		affiliation.setOrgUnit(OrgUnitService.getInstance().getByUuid(orgUnitUuid));
		affiliation.setPayGrade(payGrade);
		affiliation.setPerson(person);
		affiliation.setPositionId(positionId);
		affiliation.setPositionName(positionName);
		affiliation.setPositionTypeId(positionTypeId);
		affiliation.setPositionTypeName(positionTypeName);
		affiliation.setStartDate((startDate != null) ? toDate(startDate) : null);
		affiliation.setStopDate((stopDate != null) ? toDate(stopDate) : null);
		affiliation.setUuid(uuid);
		affiliation.setWorkingHoursDenominator(workingHoursDenominator);
		affiliation.setWorkingHoursNumerator(workingHoursNumerator);

		if (managerForUuids != null) {
			affiliation.setManagerFor(new HashSet<OrgUnit>());

			for (String uuid : managerForUuids) {
				OrgUnit ou = OrgUnitService.getInstance().getByUuid(uuid);
				if (ou != null) {
					affiliation.getManagerFor().add(ou);
				}
			}
		}
		
		if (functions != null) {
			affiliation.setFunctions(new ArrayList<>());

			for (String function : functions) {
				affiliation.getFunctions().add(AffiliationFunction.valueOf(function));
			}
		}
		
		return affiliation;
	}
}
