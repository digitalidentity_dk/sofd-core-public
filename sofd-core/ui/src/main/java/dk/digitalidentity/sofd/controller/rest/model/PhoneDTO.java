package dk.digitalidentity.sofd.controller.rest.model;

import dk.digitalidentity.sofd.dao.model.enums.PhoneType;
import dk.digitalidentity.sofd.dao.model.enums.Visibility;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneDTO {
	private long id;
	private String phoneNumber;
	private PhoneType phoneType;
	private String notes;
	private Visibility visibility;
	private long functionType;
	private boolean prime;
	private boolean typePrime;
}
