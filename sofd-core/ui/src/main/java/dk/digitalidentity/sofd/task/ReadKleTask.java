package dk.digitalidentity.sofd.task;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.Kle;
import dk.digitalidentity.sofd.service.KleService;
import lombok.extern.log4j.Log4j;

@Component
@EnableScheduling
@EnableAsync
@Log4j
public class ReadKleTask {
	private static Map<String, String> kleCacheMap = new HashMap<>();

	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	private KleService kleService;

	@Async
	public void init() {
		if (configuration.getScheduled().isEnabled() && kleService.countByActiveTrue() == 0) {
			parse();
		}
		else {
			// even non scheduled instances should populate the cache
			loadCache();
		}
	}

	private void loadCache() {
		Map<String, String> newKleCacheMap = new HashMap<>();

		kleService.resetKleCache();
		List<Kle> kleList = kleService.findAll();
		for (Kle kle : kleList) {
			newKleCacheMap.put(kle.getCode(), kle.getName());
		}

		kleCacheMap = newKleCacheMap;
	}

	// Run every Saturday at 22:00
	@Scheduled(cron = "${kle.cron.refresh:0 0 22 * * SAT}")
	@Transactional(rollbackFor = Exception.class)
	public synchronized void reloadCache() {
		if (configuration.getScheduled().isEnabled()) {
			return; // do not reload cache on the instance that is running the scheduled task
		}

		log.info("Refreshing KLE cache");

		loadCache();
	}

	// TODO: add fuzz
	// Run every Saturday at 21:00
	@Scheduled(cron = "${kle.cron:0 0 21 * * SAT}")
	@Transactional(rollbackFor = Exception.class)
	public synchronized void parse() {
		if (!configuration.getScheduled().isEnabled()) {
			return;
		}

		if (StringUtils.isEmpty(configuration.getIntegrations().getKle().getUrl())) {
			log.warn("KLE url empty, not fetching updated KLE data");
			return;
		}
		
		log.info("Fetching KLE from KOMBIT and refreshing cache");

		Map<String, String> newCacheMap = new HashMap<>();
		Collection<Kle> updatedKleList = kleService.loadKleFromKOMBIT(configuration.getCustomer().getCvr());
		List<Kle> kleList = kleService.findAll();

		for (Kle updatedKle : updatedKleList) {
			boolean found = false;
			boolean changes = false;

			for (Iterator<Kle> iterator = kleList.iterator(); iterator.hasNext(); ) {
				Kle kle = iterator.next();

				if (kle.getCode().equals(updatedKle.getCode())) {
					found = true;

					if (!Objects.equals(kle.getName(), updatedKle.getName())) {
						changes = true;
					}

					if (kle.isActive() != updatedKle.isActive()) {
						changes = true;
					}
					
					if (!Objects.equals(kle.getUuid(), updatedKle.getUuid())) {
						changes = true;
					}

					iterator.remove();
					break;
				}
			}

			if (found && changes) {
				// fetch from DB to bypass cache
				Kle kle = kleService.getByCode(updatedKle.getCode());
				kle.setName(updatedKle.getName());
				kle.setActive(updatedKle.isActive());
				kle.setUuid(updatedKle.getUuid());

				kleService.save(kle);
			}
			else if (!found) {
				kleService.save(updatedKle);
			}

			newCacheMap.put(updatedKle.getCode(), updatedKle.getName());
		}

		// Deactivate whatever is left in the list
		for (Kle inactiveKle : kleList) {
			Kle kle = kleService.getByCode(inactiveKle.getCode());
			kle.setActive(false);

			kleService.save(kle);
		}

		kleCacheMap = newCacheMap;
	}

	public static String getName(String code) {
		return kleCacheMap.get(code);
	}
}
