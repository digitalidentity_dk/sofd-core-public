package dk.digitalidentity.sofd.dao.model.projection;

import org.springframework.data.rest.core.config.Projection;

import dk.digitalidentity.sofd.dao.model.User;

@Projection(name = "grid", types = { User.class })
public interface GridUser {
	String getUserId();
	String getUserType();
}