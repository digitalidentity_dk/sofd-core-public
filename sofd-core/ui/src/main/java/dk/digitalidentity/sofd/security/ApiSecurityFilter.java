package dk.digitalidentity.sofd.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import dk.digitalidentity.sofd.dao.model.Client;
import dk.digitalidentity.sofd.dao.model.enums.AccessRole;
import dk.digitalidentity.sofd.service.ClientService;

public class ApiSecurityFilter implements Filter {
	private static final Logger logger = Logger.getLogger(ApiSecurityFilter.class);
	private ClientService clientService;

	public ApiSecurityFilter(ClientService clientService) {
		this.clientService = clientService;
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		// we are using a custom header instead of Authorization because the Authorization header plays very badly with the SAML filter
		String authHeader = request.getHeader("ApiKey");
		String versionHeader = request.getHeader("ClientVersion");
		if (authHeader != null) {
			Client client = clientService.getClientByApiKey(authHeader);
			if (client == null) {
				unauthorized(response, "Invalid ApiKey header", authHeader);
				return;
			}

			ArrayList<GrantedAuthority> authorities = new ArrayList<>();
			switch (client.getAccessRole()) {
				case WRITE_ACCESS:
					authorities.add(new SimpleGrantedAuthority("ROLE_" + AccessRole.WRITE_ACCESS.toString()));
					authorities.add(new SimpleGrantedAuthority("ROLE_" + AccessRole.READ_ACCESS.toString()));
					break;
				case LIMITED_READ_ACCESS:
				case READ_ACCESS:
					authorities.add(new SimpleGrantedAuthority("ROLE_" + AccessRole.READ_ACCESS.toString()));
					break;
			}

			if (versionHeader != null) {
				if (!Objects.equals(client.getVersion(), versionHeader)) {
					// update cached version
					client.setVersion(versionHeader);

					// update DB version
					Client clientFromDb = clientService.getClientById(client.getId());
					clientFromDb.setVersion(versionHeader);
					clientService.save(clientFromDb);
				}
			}
			
			ClientToken token = new ClientToken(client.getName(), client.getApiKey(), authorities);
			token.setClient(client);

			SecurityContextHolder.getContext().setAuthentication(token);
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			unauthorized(response, "Missing ApiKey header", authHeader);
		}
	}

	private static void unauthorized(HttpServletResponse response, String message, String authHeader) throws IOException {
		logger.warn(message + " (authHeader = " + authHeader + ")");
		response.sendError(401, message);
	}

	@Override
	public void destroy() {
		;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		;
	}
}