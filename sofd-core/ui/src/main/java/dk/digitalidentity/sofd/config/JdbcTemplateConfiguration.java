package dk.digitalidentity.sofd.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class JdbcTemplateConfiguration {
	
	@Autowired
	private SofdConfiguration configuration;

	@Bean(name = "defaultTemplate")
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "OS2syncTemplate")
	public JdbcTemplate os2syncTemplate() {
		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();

		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setPassword(configuration.getIntegrations().getOs2sync().getDatasourcePassword());
		dataSource.setUsername(configuration.getIntegrations().getOs2sync().getDatasourceUsername());
		dataSource.setUrl(configuration.getIntegrations().getOs2sync().getDatasourceUrl());
		dataSource.setMaxActive(5);
		dataSource.setMinIdle(1);
		dataSource.setMaxIdle(2);
		dataSource.setInitialSize(1);
		dataSource.setTestWhileIdle(true);
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setValidationInterval(30000);
		dataSource.setTestOnBorrow(true);
		dataSource.setMaxAge(360000);

		return new JdbcTemplate(dataSource);
	}
}
