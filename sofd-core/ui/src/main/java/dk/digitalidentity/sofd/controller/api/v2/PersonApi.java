package dk.digitalidentity.sofd.controller.api.v2;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sofd.controller.api.v2.model.PersonApiRecord;
import dk.digitalidentity.sofd.controller.api.v2.model.PersonResult;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.MasteredEntity;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationFunction;
import dk.digitalidentity.sofd.service.PersonService;

@RestController
public class PersonApi {

	@Autowired
	private PersonService personService;

	@GetMapping("/api/v2/persons")
	public PersonResult getPersons(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "size", defaultValue = "100") int size) {
		Page<Person> persons = personService.getAll(new PageRequest(page, size));
		
		PersonResult result = new PersonResult();
		result.setPersons(new HashSet<PersonApiRecord>());
		result.setPage(page);

		for (Person person : persons.getContent()) {
			result.getPersons().add(new PersonApiRecord(person));
		}
		
		return result;
	}

	@GetMapping("/api/v2/persons/byCpr/{cpr}")
	public ResponseEntity<?> getPersonByCpr(@PathVariable("cpr") String cpr) {
		Person person = personService.findByCpr(cpr);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new PersonApiRecord(person), HttpStatus.OK);		
	}

	@GetMapping("/api/v2/persons/{uuid}")
	public ResponseEntity<?> getPerson(@PathVariable("uuid") String uuid) {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new PersonApiRecord(person), HttpStatus.OK);
	}
	
	@PostMapping("/api/v2/persons")
	public ResponseEntity<?> createPerson(@Valid @RequestBody PersonApiRecord record, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
		}

		if (personService.getByUuid(record.getUuid()) != null) {
			return new ResponseEntity<>("Already exists", HttpStatus.CONFLICT);
		}

		Person result = personService.save(record.toPerson(null));
		
		return new ResponseEntity<>(new PersonApiRecord(result), HttpStatus.CREATED);
	}

	@PatchMapping("/api/v2/persons/{uuid}")
	public ResponseEntity<?> patchPerson(@PathVariable("uuid") String uuid, @RequestBody PersonApiRecord record, BindingResult bindingResult) throws Exception {
		Person person = personService.getByUuid(uuid);
		if (person == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		boolean changes = patch(person, record);
		if (!changes) {
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}

		person = personService.save(person);

		return new ResponseEntity<>(new PersonApiRecord(person), HttpStatus.OK);
	}


	private boolean patch(Person person, PersonApiRecord personRecord) throws Exception {
		Person record = personRecord.toPerson(person);
		boolean changes = false;
		
		// in patch() fields are only updated if the supplied record is non-null, meaning PATCH cannot
		// null a field - a PUT operation must be implemented for null'ing to be possible.

		// comparison should be in date, not full object
		if (record.getAnniversaryDate() != null && !Objects.equals(toLocalDate(record.getAnniversaryDate()), toLocalDate(person.getAnniversaryDate()))) {
			person.setAnniversaryDate(record.getAnniversaryDate());
			changes = true;
		}

		if (record.getChosenName() != null && !Objects.equals(record.getChosenName(), person.getChosenName())) {
			person.setChosenName(record.getChosenName());
			changes = true;
		}
		
		if (record.getFirstEmploymentDate() != null && !Objects.equals(toLocalDate(record.getFirstEmploymentDate()), toLocalDate(person.getFirstEmploymentDate()))) {
			person.setFirstEmploymentDate(record.getFirstEmploymentDate());
			changes = true;
		}
		
		if (record.getFirstname() != null && !Objects.equals(record.getFirstname(), person.getFirstname())) {
			person.setFirstname(record.getFirstname());
			changes = true;
		}
		
		if (record.getLocalExtensions() != null && !Objects.equals(record.getLocalExtensions(), person.getLocalExtensions())) {
			person.setLocalExtensions(record.getLocalExtensions());
			changes = true;
		}
		
		if (record.getMaster() != null && !Objects.equals(record.getMaster(), person.getMaster())) {
			person.setMaster(record.getMaster());
			changes = true;
		}
	
		if (record.getSurname() != null && !Objects.equals(record.getSurname(), person.getSurname())) {
			person.setSurname(record.getSurname());
			changes = true;
		}
		
		// due to the way patching works, it is not possible "null" a collection using the PATCH operation,
		// an empty collection must be supplied to "empty" it.
		
		if (record.getUsers() != null) {
			if (this.<User>patchCollection(person, record, Person.class.getMethod("getUsers"), Person.class.getMethod("setUsers", List.class))) {
				changes = true;
			}
		}
		
		if (record.getAffiliations() != null) {
			if (this.<Affiliation>patchCollection(person, record, Person.class.getMethod("getAffiliations"), Person.class.getMethod("setAffiliations", List.class))) {
				changes = true;
			}
		}
		
		if (record.getPhones() != null) {
			if (this.<Phone>patchCollection(person, record, Person.class.getMethod("getPhones"), Person.class.getMethod("setPhones", List.class))) {
				changes = true;
			}
		}
		
		if (record.getResidencePostAddress() != null) {
			if (patchResidencePostAddress(person, record)) {
				changes = true;
			}
		}

		if (record.getRegisteredPostAddress() != null) {
			if (patchRegisteredPostAddress(person, record)) {
				changes = true;
			}
		}
		
		if (changes) {
			// if there are changes, flip any delete flag
			person.setDeleted(false);
		}

		return changes;
	}
	
	private boolean patchRegisteredPostAddress(Person person, Person record) {
		boolean changes = false;

		if (record.getRegisteredPostAddress() != null) {
			if (person.getRegisteredPostAddress() == null) {
				person.setRegisteredPostAddress(record.getRegisteredPostAddress());
				changes = true;
			}
			else {
				Post personPost = person.getRegisteredPostAddress();
				Post recordPost = record.getRegisteredPostAddress();

				if (patchPost(personPost, recordPost)) {
					changes = true;
				}
			}
		}

		return changes;
	}

	private boolean patchResidencePostAddress(Person person, Person record) {
		boolean changes = false;

		if (record.getResidencePostAddress() != null) {
			if (person.getResidencePostAddress() == null) {
				person.setResidencePostAddress(record.getResidencePostAddress());
				changes = true;
			}
			else {
				Post personPost = person.getResidencePostAddress();
				Post recordPost = record.getResidencePostAddress();

				if (patchPost(personPost, recordPost)) {
					changes = true;
				}
			}
		}

		return changes;

	}

	private boolean patchPost(Post personPost, Post recordPost) {
		boolean changes = false;

		// TODO: should we allow external APIs to take control of a POST object? If is likely controlled by the CPR integration,
		//       so the current master should be SOFD, right?
		if (recordPost.getMaster() != null && !Objects.equals(personPost.getMaster(), recordPost.getMaster())) {
			personPost.setMaster(recordPost.getMaster());
			changes = true;
		}

		if (recordPost.getMasterId() != null && !Objects.equals(personPost.getMasterId(), recordPost.getMasterId())) {
			personPost.setMasterId(recordPost.getMasterId());
			changes = true;
		}
		
		if (recordPost.getCity() != null && !Objects.equals(personPost.getCity(), recordPost.getCity())) {
			personPost.setCity(recordPost.getCity());
			changes = true;
		}
		
		if (recordPost.getCountry() != null && !Objects.equals(personPost.getCountry(), recordPost.getCountry())) {
			personPost.setCountry(recordPost.getCountry());
			changes = true;
		}
		
		if (recordPost.getLocalname() != null && !Objects.equals(personPost.getLocalname(), recordPost.getLocalname())) {
			personPost.setLocalname(recordPost.getLocalname());
			changes = true;
		}
		
		if (recordPost.getPostalCode() != null && !Objects.equals(personPost.getPostalCode(), recordPost.getPostalCode())) {
			personPost.setPostalCode(recordPost.getPostalCode());
			changes = true;
		}
		
		if (recordPost.getStreet() != null && !Objects.equals(personPost.getStreet(), recordPost.getStreet())) {
			personPost.setStreet(recordPost.getStreet());
			changes = true;
		}
		
		if (recordPost.isAddressProtected() != personPost.isAddressProtected()) {
			personPost.setAddressProtected(recordPost.isAddressProtected());
			changes = true;
		}

		return changes;
	}

	@SuppressWarnings("unchecked")
	private <T extends MasteredEntity> boolean patchCollection(Person person, Person record, Method getCollectionMethod, Method setCollectionMethod) throws Exception {
		boolean changes = false;
		
		// record has no entries, person does
		Collection<T> recordCollection = (Collection<T>) getCollectionMethod.invoke(record);
		Collection<T> personCollection = (Collection<T>) getCollectionMethod.invoke(person);
		
		if (recordCollection == null || recordCollection.size() == 0) {
			if (personCollection != null && personCollection.size() > 0) {
				for (Iterator<T> iterator = personCollection.iterator(); iterator.hasNext();) {
					iterator.next();
					iterator.remove();
				}
				
				changes = true;
			}
		}
		else { // record has entries in all of the below cases
			
			// existing person does not have any entries
			if (personCollection == null || personCollection.size() == 0) {
				if (personCollection == null) {
					setCollectionMethod.invoke(person, new ArrayList<T>());
				}

				for (T recordEntry : recordCollection) {
					personCollection.add(recordEntry);
				}
				
				changes = true;
			}
			else {
				// both have entries, the big comparison case

				// to add or update
				for (T recordEntry : recordCollection) {
					boolean found = false;
					
					for (T personEntry : personCollection) {
						if (Objects.equals(personEntry.getMaster(), recordEntry.getMaster()) &&
							Objects.equals(personEntry.getMasterId(), recordEntry.getMasterId())) {

							if (personEntry instanceof Phone) {
								if (patchPhoneEntityFields((Phone) personEntry, (Phone) recordEntry)) {
									changes = true;
								}
							}
							else if (personEntry instanceof User) {
								if (patchUserEntityFields((User) personEntry, (User) recordEntry)) {
									changes = true;
								}
							}
							else if (personEntry instanceof Affiliation) {
								if (patchAffiliationEntityFields((Affiliation) personEntry, (Affiliation) recordEntry)) {
									changes = true;
								}
							}

							found = true;
							break;
						}
					}
					
					// add if it does not exist
					if (!found) {
						personCollection.add(recordEntry);
						changes = true;
					}
				}
				
				// to remove
				for (Iterator<T> iterator = personCollection.iterator(); iterator.hasNext();) {
					T personEntry = iterator.next();
					boolean found = false;

					for (T recordEntry : recordCollection) {
						if (Objects.equals(personEntry.getMaster(), recordEntry.getMaster()) &&
							Objects.equals(personEntry.getMasterId(), recordEntry.getMasterId())) {
							
							found = true;
							break;
						}
					}
					
					// add if it does not exist
					if (!found) {
						iterator.remove();
						changes = true;
					}
				}
			}
		}

		return changes;
	}

	private boolean patchAffiliationEntityFields(Affiliation personEntry, Affiliation recordEntry) {
		boolean changes = false;

		if (recordEntry.getAffiliationType() != null && !Objects.equals(personEntry.getAffiliationType(), recordEntry.getAffiliationType())) {
			personEntry.setAffiliationType(recordEntry.getAffiliationType());
			changes = true;
		}

		if (recordEntry.getEmployeeId() != null && !Objects.equals(personEntry.getEmployeeId(), recordEntry.getEmployeeId())) {
			personEntry.setEmployeeId(recordEntry.getEmployeeId());
			changes = true;
		}

		if (recordEntry.getEmploymentTerms() != null && !Objects.equals(personEntry.getEmploymentTerms(), recordEntry.getEmploymentTerms())) {
			personEntry.setEmploymentTerms(recordEntry.getEmploymentTerms());
			changes = true;
		}
		
		if (recordEntry.getEmploymentTermsText() != null && !Objects.equals(personEntry.getEmploymentTermsText(), recordEntry.getEmploymentTermsText())) {
			personEntry.setEmploymentTermsText(recordEntry.getEmploymentTermsText());
			changes = true;
		}

		if (recordEntry.getLocalExtensions() != null && !Objects.equals(personEntry.getLocalExtensions(), recordEntry.getLocalExtensions())) {
			personEntry.setLocalExtensions(recordEntry.getLocalExtensions());
			changes = true;
		}
		
		if (recordEntry.getPayGrade() != null && !Objects.equals(personEntry.getPayGrade(), recordEntry.getPayGrade())) {
			personEntry.setPayGrade(recordEntry.getPayGrade());
			changes = true;
		}

		if (recordEntry.getPositionId() != null && !Objects.equals(personEntry.getPositionId(), recordEntry.getPositionId())) {
			personEntry.setPositionId(recordEntry.getPositionId());
			changes = true;
		}
		
		if (recordEntry.getPositionName() != null && !Objects.equals(personEntry.getPositionName(), recordEntry.getPositionName())) {
			personEntry.setPositionName(recordEntry.getPositionName());
			changes = true;
		}
		
		if (recordEntry.getPositionTypeId() != null && !Objects.equals(personEntry.getPositionTypeId(), recordEntry.getPositionTypeId())) {
			personEntry.setPositionTypeId(recordEntry.getPositionTypeId());
			changes = true;
		}
		
		if (recordEntry.getPositionTypeName() != null && !Objects.equals(personEntry.getPositionTypeName(), recordEntry.getPositionTypeName())) {
			personEntry.setPositionTypeName(recordEntry.getPositionTypeName());
			changes = true;
		}
		
		if (recordEntry.getStartDate() != null && !Objects.equals(toLocalDate(personEntry.getStartDate()), toLocalDate(recordEntry.getStartDate()))) {
			personEntry.setStartDate(recordEntry.getStartDate());
			changes = true;
		}

		if (recordEntry.getStopDate() != null && !Objects.equals(toLocalDate(personEntry.getStopDate()), toLocalDate(recordEntry.getStopDate()))) {
			personEntry.setStopDate(recordEntry.getStopDate());
			changes = true;
		}
		else if (recordEntry.getStopDate() == null && personEntry.getStopDate() != null) {
			personEntry.setStopDate(null);
			changes = true;
		}
		
		if (recordEntry.getWorkingHoursDenominator() != null && !Objects.equals(personEntry.getWorkingHoursDenominator(), recordEntry.getWorkingHoursDenominator())) {
			personEntry.setWorkingHoursDenominator(recordEntry.getWorkingHoursDenominator());
			changes = true;
		}
		
		if (recordEntry.getWorkingHoursNumerator() != null && !Objects.equals(personEntry.getWorkingHoursNumerator(), recordEntry.getWorkingHoursNumerator())) {
			personEntry.setWorkingHoursNumerator(recordEntry.getWorkingHoursNumerator());
			changes = true;
		}

		if (personEntry.isDeleted() != recordEntry.isDeleted()) {
			personEntry.setDeleted(recordEntry.isDeleted());
			changes = true;
		}
		
		if (recordEntry.isInheritPrivileges() != personEntry.isInheritPrivileges()) {
			personEntry.setInheritPrivileges(recordEntry.isInheritPrivileges());
			changes = true;
		}

		if (recordEntry.getFunctions() != null) {
			if (recordEntry.getFunctions().size() > 0) {
				if (personEntry.getFunctions() == null || personEntry.getFunctions().size() == 0) {
					if (personEntry.getFunctions() == null) {
						personEntry.setFunctions(new ArrayList<>());
					}
					
					for (AffiliationFunction function : recordEntry.getFunctions()) {
						personEntry.getFunctions().add(function);
					}
					
					changes = true;
				}
				else {
					// complex update case
					
					// add
					for (AffiliationFunction recordFunction : recordEntry.getFunctions()) {
						boolean found = false;

						for (AffiliationFunction personFunction : personEntry.getFunctions()) {
							if (personFunction.equals(recordFunction)) {
								found = true;
								break;
							}
						}
						
						if (!found) {
							personEntry.getFunctions().add(recordFunction);
							changes = true;
						}
					}
					
					// remove
					for (Iterator<AffiliationFunction> iterator = personEntry.getFunctions().iterator(); iterator.hasNext();) {
						AffiliationFunction personFunction = iterator.next();
						boolean found = false;

						for (AffiliationFunction recordFunction : recordEntry.getFunctions()) {
							if (personFunction.equals(recordFunction)) {
								found = true;
								break;
							}
						}
						
						if (!found) {
							iterator.remove();
							changes = true;
						}
					}
				}
			}
			else {
				// supplied empty set, so remove all
				if (personEntry.getFunctions() != null && personEntry.getFunctions().size() > 0) {
					for (Iterator<AffiliationFunction> iterator = personEntry.getFunctions().iterator(); iterator.hasNext();) {
						iterator.next();
						iterator.remove();
					}

					changes = true;
				}
			}
		}

		if (recordEntry.getManagerFor() != null) {
			if (recordEntry.getManagerFor().size() > 0) {
				if (personEntry.getManagerFor() == null || personEntry.getManagerFor().size() == 0) {
					if (personEntry.getManagerFor() == null) {
						personEntry.setManagerFor(new HashSet<>());
					}
					
					for (OrgUnit orgUnit : recordEntry.getManagerFor()) {
						personEntry.getManagerFor().add(orgUnit);
					}

					changes = true;
				}
				else {
					// complex update case

					// add
					for (OrgUnit recordOrgUnit : recordEntry.getManagerFor()) {
						boolean found = false;

						for (OrgUnit personOrgUnit : personEntry.getManagerFor()) {
							if (personOrgUnit.getUuid().equals(recordOrgUnit.getUuid())) {
								found = true;
								break;
							}
						}
						
						if (!found) {
							personEntry.getManagerFor().add(recordOrgUnit);
							changes = true;
						}
					}
					
					// remove
					for (Iterator<OrgUnit> iterator = personEntry.getManagerFor().iterator(); iterator.hasNext();) {
						OrgUnit personOrgUnit = iterator.next();
						boolean found = false;

						for (OrgUnit recordOrgUnit : recordEntry.getManagerFor()) {
							if (personOrgUnit.getUuid().equals(recordOrgUnit.getUuid())) {
								found = true;
								break;
							}
						}
						
						if (!found) {
							iterator.remove();
							changes = true;
						}
					}
				}
			}
			else {
				// supplied empty set, so remove all
				if (personEntry.getManagerFor() != null && personEntry.getManagerFor().size() > 0) {
					for (Iterator<OrgUnit> iterator = personEntry.getManagerFor().iterator(); iterator.hasNext();) {
						iterator.next();
						iterator.remove();
					}

					changes = true;
				}
			}
		}

		if (recordEntry.getOrgUnit() != null) {
			if (!recordEntry.getOrgUnit().getUuid().equals(personEntry.getOrgUnit().getUuid())) {
				personEntry.setOrgUnit(recordEntry.getOrgUnit());
				changes = true;				
			}
		}

		// TODO: should probably not support changing this
		if (recordEntry.getUuid() != null && !Objects.equals(personEntry.getUuid(), recordEntry.getUuid())) {
			personEntry.setUuid(recordEntry.getUuid());
			changes = true;
		}

		return changes;
	}
	
	private boolean patchPhoneEntityFields(Phone personEntry, Phone recordEntry) {
		boolean changes = false;

		// note that patching cannot be used for null'ing fields, only setting or updating them

		if (recordEntry.getFunctionType() != null && !Objects.equals(personEntry.getFunctionType(), recordEntry.getFunctionType())) {
			personEntry.setFunctionType(recordEntry.getFunctionType());
			changes = true;
		}

		if (recordEntry.getPhoneNumber() != null && !Objects.equals(personEntry.getPhoneNumber(), recordEntry.getPhoneNumber())) {
			personEntry.setPhoneNumber(recordEntry.getPhoneNumber());
			changes = true;
		}

		if (recordEntry.getPhoneType() != null && !Objects.equals(personEntry.getPhoneType(), recordEntry.getPhoneType())) {
			personEntry.setPhoneType(recordEntry.getPhoneType());
			changes = true;
		}

		if (recordEntry.getVisibility() != null && !Objects.equals(personEntry.getVisibility(), recordEntry.getVisibility())) {
			personEntry.setVisibility(recordEntry.getVisibility());
			changes = true;
		}
		
		return changes;
	}

	private boolean patchUserEntityFields(User personUser, User recordUser) {
		boolean changes = false;

		// note that patching cannot be used for null'ing fields, only setting or updating them
		
		if (recordUser.getEmployeeId() != null && !Objects.equals(personUser.getEmployeeId(), recordUser.getEmployeeId())) {
			personUser.setEmployeeId(recordUser.getEmployeeId());
			changes = true;
		}

		if (recordUser.getLocalExtensions() != null && !Objects.equals(personUser.getLocalExtensions(), recordUser.getLocalExtensions())) {
			personUser.setLocalExtensions(recordUser.getLocalExtensions());
			changes = true;
		}

		if (recordUser.getPasswordExpireDate() != null && !Objects.equals(personUser.getPasswordExpireDate(), recordUser.getPasswordExpireDate())) {
			personUser.setPasswordExpireDate(recordUser.getPasswordExpireDate());
			changes = true;
		}

		// TODO: should we even allow patching of these three values?

		if (recordUser.getUuid() != null && !Objects.equals(personUser.getUuid(), recordUser.getUuid())) {
			personUser.setEmployeeId(recordUser.getUuid());
			changes = true;
		}

		if (recordUser.getUserId() != null && !Objects.equals(personUser.getUserId(), recordUser.getUserId())) {
			personUser.setUserId(recordUser.getUserId());
			changes = true;
		}

		if (recordUser.getUserType() != null && !Objects.equals(personUser.getUserType(), recordUser.getUserType())) {
			personUser.setUserType(recordUser.getUserType());
			changes = true;
		}

		if (recordUser.getDisabled() != null && !Objects.equals(personUser.getDisabled(), recordUser.getDisabled())) {
			personUser.setDisabled(recordUser.getDisabled());
			changes = true;
		}

		return changes;
	}
	
	private LocalDate toLocalDate(Date date) {
		if (date == null) {
			return null;
		}

		// TODO: hack, remove once we no longer need it (look in LocalDateAttributeConverter for reason)
		if (date instanceof java.sql.Date) {
			return ((java.sql.Date) date).toLocalDate();
		}
		
	    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
