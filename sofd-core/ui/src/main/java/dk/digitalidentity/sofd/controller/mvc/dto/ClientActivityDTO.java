package dk.digitalidentity.sofd.controller.mvc.dto;

import java.util.Calendar;
import java.util.Date;

import dk.digitalidentity.sofd.dao.model.Client;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientActivityDTO {
	private String name;
	private Date lastActive;
	private String color;
	private String colorDark;
	
	public ClientActivityDTO(Client client) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -2);
		Date twoDaysAgo = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, -5);
		Date sevenDaysAgo = calendar.getTime();
		
		if (client.getLastActive() == null || client.getLastActive().before(sevenDaysAgo)) {
			color = "bg-red";
			colorDark = "bg-red-dark";
		}
		else if (client.getLastActive().before(twoDaysAgo)) {
			color = "bg-yellow";
			colorDark = "bg-yellow-dark";
		}
		else {
			color = "bg-primary";
			colorDark = "bg-primary-dark";
		}
		
		name = client.getName();
		lastActive = client.getLastActive();
	}
}
