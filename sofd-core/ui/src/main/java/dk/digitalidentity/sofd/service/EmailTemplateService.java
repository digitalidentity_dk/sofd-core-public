package dk.digitalidentity.sofd.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.dao.EmailTemplateDao;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import dk.digitalidentity.sofd.dao.model.enums.EmailTemplateType;

@Service
public class EmailTemplateService {
	public static final String EMPLOYEE_PLACEHOLDER = "{medarbejder}";
	public static final String AFFILIATIONUUID_PLACEHOLDER = "{tilhørsforholduuid}";
	public static final String ORGUNIT_PLACEHOLDER = "{orgenhed}";
	public static final String ACCOUNT_PLACEHOLDER = "{kontonavn}";
	public static final String RECEIVER_PLACEHOLDER = "{modtager}";
	public static final String TIMESTAMP_PLACEHOLDER = "{tidspunkt}";
	public static final String CHANGES_PLACEHOLDER = "{ændringer}";
	
	@Autowired
	private EmailTemplateDao emailTemplateDao;

	public List<EmailTemplate> findAll() {
		List<EmailTemplate> result = new ArrayList<>();
		
		for (EmailTemplateType type : EmailTemplateType.values()) {
			result.add(findByTemplateType(type));
		}
		
		return result;
	}

	public EmailTemplate findByTemplateType(EmailTemplateType type) {
		EmailTemplate template = emailTemplateDao.findByTemplateType(type);
		if (template == null) {
			template = new EmailTemplate();
			String title = "Overskrift";
			String message = "Besked";
			
			switch (type) {
				case AD_CREATE_EMPLOYEE:
					title = "Brugerkonto oprettet";
					message = "Kære {modtager}\n<br/>\n<br/>\nDer er blevet oprettet en brugerkonto til dig med brugernavn\n<br/>\n<br/>\n{kontonavn}";
					break;
				case AD_CREATE_FAILED:
					title = "Oprettelse af brugerkonto fejlet";
					message = "Kære {modtager}\n<br/>\n<br/>\nBestillingen af en brugerkonto til {medarbejder} er fejlet, og kontoen er ikke blevet oprettet.";
					break;
				case AD_CREATE_MANAGER:
					title = "Brugerkonto oprettet";
					message = "Kære {modtager}\n<br/>\n<br/>\nDer er blevet oprettet en brugerkonto til {medarbejder} med brugernavn\n<br/>\n<br/>\n{kontonavn}";
					break;
				case AD_DISABLE_MANAGER:
					title = "Brugerkonto lukket";
					message = "Kære {modtager}\n<br/>\n<br/>\nBrugerkontoen for {medarbejder} er blevet lukket. Der er tale om brugerkontoen med brugernavnet\n<br/>\n<br/>\n{kontonavn}";
					break;
				case EXCHANGE_CREATE_EMPLOYEE:
					title = "Mailkonto oprettet";
					message = "Kære {modtager}\n<br/>\n<br/>Der er blevet oprettet en mailkonto til dig.";
					break;
				case NEW_AFFILIATION:
					title = "Ny medarbejder";
					// følgende er også muligt, men ønskes ikke vist i std skabelonen
					// Uuid for tilhørsforhold: {tilhørsforholduuid}
					message = "Kære {modtager}\n<br/>\n<br/>{medarbejder} starter i din afdeling {orgenhed} d. {tidspunkt}.";
					break;
				case NEW_EMPLOYEE_WELCOME:
					title = "Velkommen, ny medarbejder";
					message = "Kære {modtager}\n<br/>\n<br/>Der starter en ny medarbejder: {medarbejder}.";
					break;
				case NEW_EMPLOYEE_REMINDER:
					title = "Påmindelse om ny medarbejder";
					message = "Kære {modtager}\n<br/>\n<br/>Husk, der er startet en ny medarbejder: {medarbejder}.";
					break;
				case ORGUNIT_CHANGES:
					title = "Organisationsændringer";
					message = "Kære {modtager}\n<br/>\n<br/>Der er sket ændringer i organisationen: {ændringer}";
					break;
			}
			
			template.setTitle(title);
			template.setMessage(message);
			template.setTemplateType(type);
			
			template = emailTemplateDao.save(template);
		}
		
		return template;
	}

	public EmailTemplate save(EmailTemplate template) {
		return emailTemplateDao.save(template);
	}

	public EmailTemplate findById(long id) {
		return emailTemplateDao.findOne(id);
	}
	
	public List<String> getRecipientsList(EmailTemplate emailTemplate) {
		List<String> recipients = new ArrayList<String>();
		
		for (String recipient : emailTemplate.getRecipients().split(";")) {
			recipients.add(recipient.trim());
		}
		
		return recipients;
	}
}
