package dk.digitalidentity.sofd.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.dao.AffiliationDao;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;

@Service
@EnableScheduling
public class AffiliationService {

	@Autowired
	private AffiliationDao affiliationDao;

	public List<Affiliation> findAll() {
		return affiliationDao.findAll();
	}

	public Affiliation findByUuid(String uuid) {
		return affiliationDao.findByUuid(uuid);
	}

	public Affiliation save(Affiliation affiliation) {
		return affiliationDao.save(affiliation);
	}

	public Long countByOrgUnitAndActive(OrgUnit orgUnit) {
		return affiliationDao.countByOrgUnitAndActive(orgUnit.getUuid());
	}

	public List<Affiliation> findByOrgUnitAndActive(OrgUnit orgUnit) {
		return affiliationDao.findByOrgUnitAndActive(orgUnit.getUuid());
	}
	
	public void delete(Affiliation affiliation) {
		affiliationDao.delete(affiliation);
	}
	
	// helper methods to decide if an affiliation is active

	public static List<Affiliation> onlyActiveAffiliations(List<Affiliation> affiliations) {
		return onlyActiveAffiliations(affiliations, 0);
	}
	
	public static List<Affiliation> notStoppedAffiliations(List<Affiliation> affiliations) {
		return CollectionUtils.emptyIfNull(affiliations).stream()
				.filter(a ->
						(!a.isDeleted()) && // TODO: goes away at some point
						(!notActiveAnymore(a)))
				.collect(Collectors.toList());
	}
	
	public static List<Affiliation> onlyActiveAffiliations(List<Affiliation> affiliations, int offsetDays) {
		return CollectionUtils.emptyIfNull(affiliations).stream()
				.filter(a ->
						(!a.isDeleted()) && // TODO: goes away at some point
						(!notActiveYet(a, offsetDays)) &&
						(!notActiveAnymore(a)))
				.collect(Collectors.toList());
	}

	public static List<Affiliation> allAffiliationsActiveSinceMonths(List<Affiliation> affiliations, int months) {
		return affiliations.stream()
				.filter(a ->
						(!a.isDeleted()) && // TODO: goes away at some point
						(!notActiveYet(a, 30 * months)) && // yeah yeah, 30 days per month is not exact, but close enough
						(!notActiveAnymoreSinceMonthsAgo(a, months)))
				.collect(Collectors.toList());
	}

	// TODO: we removed the "stopped" marker, so we can cleanup this stuff at some point

	public static boolean inactiveAndNotStopped(Affiliation affiliation) {
		return inactiveAndNotStopped(affiliation, 0);
	}

	public static boolean inactiveAndNotStopped(Affiliation affiliation, int offsetDays) {
		if (notActiveYet(affiliation, offsetDays) || notActiveAnymore(affiliation)) {
			return true;
		}
		
		return false;
	}

	public static boolean activeAndNotStopped(Affiliation affiliation) {
		return activeAndNotStopped(affiliation, 0);
	}

	public static boolean activeAndNotStopped(Affiliation affiliation, int offsetDays) {
		if (!notActiveYet(affiliation, offsetDays) && !notActiveAnymore(affiliation)) {
			return true;
		}
		
		return false;
	}

	/**
	 * The offsetDays should be >= 0, and is subtracted from "today", when deciding if a given
	 * affiliation is still active, allowing for affiliations that becomes active within the
	 * next "offsetDays" to be treated as active
	 */
	public static boolean notActiveYet(Affiliation affiliation, int offsetDays) {
		Date today = getToday();
		
		if (offsetDays > 0) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, (offsetDays));
			today = cal.getTime();
		}

		return (affiliation.getStartDate() != null && affiliation.getStartDate().after(today)); 
	}

	public static boolean notActiveAnymore(Affiliation affiliation) {
		return (affiliation.getStopDate() != null && affiliation.getStopDate().before(getYesterday()));
	}

	public static boolean notActiveAnymoreSinceMonthsAgo(Affiliation affiliation, int months) {
		return (affiliation.getStopDate() != null && affiliation.getStopDate().before(getMonthsAgo(months)));
	}
	
	// ensure we know when today and yesterday are ;)
	
	// runs one minute past midnight
	@Scheduled(cron = "1 0 0 * * ?")
	public void generateTodayAndYesterday() {
		_today = null;
		_yesterday = null;
	}

	private static Date _today = null;
	private static Date getToday() {
		if (_today == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.HOUR_OF_DAY, 8);   // all stop_dates are set to 00:00, so by picking 08:00, we do not have a 00:00:00 vs 00:00:00 issue
			_today = cal.getTime();      // today at 08:00
		}
		
		return _today;
	}
	
	private static Date _yesterday = null;
	private static Date getYesterday() {
		if (_yesterday == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.HOUR_OF_DAY, 8);      // all stop_dates are set to 00:00, so by picking 08:00, we do not have a 00:00:00 vs 00:00:00 issue
			cal.add(Calendar.DATE, -1);
			
			_yesterday = cal.getTime();     // yesterday at 08:00			
		}
		
		return _yesterday;
	}
	
	private static Date getMonthsAgo(int months) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 8);      // all stop_dates are set to 00:00, so by picking 08:00, we do not have a 00:00:00 vs 00:00:00 issue
		cal.add(Calendar.MONTH, (-1 * months));

		return cal.getTime();     // x months ago at 08:00
	}
}
