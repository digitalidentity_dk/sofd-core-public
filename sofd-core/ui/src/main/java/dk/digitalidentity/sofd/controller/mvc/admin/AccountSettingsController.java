package dk.digitalidentity.sofd.controller.mvc.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.controller.rest.admin.model.AttachmentDTO;
import dk.digitalidentity.sofd.controller.rest.admin.model.EmailTemplateDTO;
import dk.digitalidentity.sofd.dao.model.Attachment;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import dk.digitalidentity.sofd.security.RequireAdminAccess;
import dk.digitalidentity.sofd.service.EmailTemplateService;

@RequireAdminAccess
@Controller
public class AccountSettingsController {

	@Autowired
	private EmailTemplateService emailTemplateService;
	
	@Autowired
	private SofdConfiguration configuration;

	@GetMapping("/ui/admin/mailtemplates")
	public String editTemplate(Model model) {
		List<EmailTemplate> templates = emailTemplateService.findAll();
		
		if (!configuration.getModules().getAccountCreation().isEnabled()) {
			templates = templates.stream()
					.filter(t -> !t.getTemplateType().isRequireAccountCreation())
					.collect(Collectors.toList());
		}
		
		List<EmailTemplateDTO> templateDTOs = templates.stream()
				.map(t -> EmailTemplateDTO.builder()
						.id(t.getId())
						.message(t.getMessage())
						.title(t.getTitle())
						.enabled(t.isEnabled())
						.minutesDelay(t.getMinutesDelay())
						.templateTypeText(t.getTemplateType().getMessage())
						.emailTemplateType(t.getTemplateType())
						.recipients(t.getRecipients())
						.attachments(new ArrayList<>())
						.build())
				.collect(Collectors.toList());

		// map attachments
		for (EmailTemplate template : templates) {
			if (template.getAttachments() != null && template.getAttachments().size() > 0) {
				for (EmailTemplateDTO dto : templateDTOs) {
					if (dto.getId() == template.getId()) {
						for (Attachment attachment : template.getAttachments()) {
							AttachmentDTO attachmentDTO = new AttachmentDTO();
							attachmentDTO.setFilename(attachment.getFilename());
							attachmentDTO.setId(attachment.getId());
							
							dto.getAttachments().add(attachmentDTO);
						}
						break;
					}
				}
			}
		}
		
		model.addAttribute("templates", templateDTOs);

		return "admin/emailtemplate/edit";
	}
}
