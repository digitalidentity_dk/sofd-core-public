package dk.digitalidentity.sofd.service;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.xml.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.Attachment;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.EntityType;
import dk.digitalidentity.sofd.dao.model.enums.NotificationType;
import dk.digitalidentity.sofd.service.eboks.dto.EboksAttachment;
import dk.digitalidentity.sofd.service.eboks.dto.EboksMessage;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class EboksService {

	@Autowired
	private SofdConfiguration configuration;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private NotificationService notificationService;

	@Autowired
	private PersonService personService;
	
	public boolean sendMessage(String cpr, String subject, String message) {
		return sendMessageWithAttachments(cpr, subject, message, null);
	}
	
	public boolean sendMessageWithAttachments(String cpr, String subject, String message, List<Attachment> attachments) {
		if (!configuration.getIntegrations().getEboks().isEnabled()) {
			log.warn("e-boks server is not configured - not sending digital post!");
			return false;
		}

		RestTemplate restTemplate = new RestTemplate();

		log.info("Sending e-boks message: '" + subject + "' to " + PersonService.maskCpr(cpr));

		String resourceUrl = configuration.getIntegrations().getEboks().getUrl();
		if (!resourceUrl.endsWith("/")) {
			resourceUrl += "/";
		}
		resourceUrl += "api/remotePrint/SendLetterToCpr";

		try {
			EboksMessage eBoks = new EboksMessage();
			eBoks.setContentTypeId(configuration.getIntegrations().getEboks().getMaterialeId());
			eBoks.setCpr(cpr);
			eBoks.setCvr(configuration.getCustomer().getCvr());
			eBoks.setSenderId(configuration.getIntegrations().getEboks().getSenderId());
			eBoks.setSubject(subject);
			eBoks.setPdfFileBase64(Base64.encode(generatePDF(subject, message)));
			eBoks.setAttachments(new ArrayList<>());
			
			if (attachments != null && attachments.size() > 0) {
				for (Attachment attachment : attachments) {
					EboksAttachment eboksAttachment = new EboksAttachment();
					eboksAttachment.setFilename(attachment.getFilename());
					eboksAttachment.setContent(Base64.encode(attachment.getFile().getContent()));

					eBoks.getAttachments().add(eboksAttachment);
				}
			}
			
	    	HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Type", "application/json");
			HttpEntity<EboksMessage> request = new HttpEntity<EboksMessage>(eBoks, headers);

			ResponseEntity<String> response = restTemplate.postForEntity(resourceUrl, request, String.class);
			
			if (response.getStatusCodeValue() != 200) {
				log.error("Failed to send e-boks message to: " + PersonService.maskCpr(cpr) + ". HTTP: " + response.getStatusCodeValue());
				return false;
			}
			else {
				// TODO: we should REALLY use some sort of error checking instead
				if (response.getBody().equalsIgnoreCase("Ikke tilmeldt e-boks!")) {
					Person person = personService.findByCpr(cpr);
					if (person != null) {
						Notification notification = new Notification();
						notification.setActive(true);
						notification.setAffectedEntityName(PersonService.getName(person));
						notification.setAffectedEntityType(EntityType.PERSON);
						notification.setAffectedEntityUuid(person.getUuid());
						notification.setMessage("Personen er ikke tilmeldt e-boks, og beskeden omkring oprettet AD konto er derfor ikke sendt til medarbejderen");
						notification.setNotificationType(NotificationType.EBOKS_REJECTED);
						notification.setCreated(new Date());
	
						notificationService.save(notification);
					}
				}
			}
		}
		catch (RestClientException ex) {
			log.error("Failed to send e-boks message to: " + PersonService.maskCpr(cpr), ex);
			return false;
		}
		
		return true;
	}
	
	private byte[] generatePDF(String subject, String message) {
		Context ctx = new Context();
		ctx.setVariable("subject", subject);
		ctx.setVariable("message", message);

		String htmlContent = templateEngine.process("pdf/template", ctx);

		// Create PDF document and return as byte[]
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(htmlContent);
			renderer.layout();
			renderer.createPDF(outputStream);

			return outputStream.toByteArray();
		}
		catch (Exception ex) {
			log.error("Failed to generate pdf", ex);
			return null;
		}
	}
}
