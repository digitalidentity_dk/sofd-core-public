package dk.digitalidentity.sofd.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

	// we keep these as non-sofd settings, as they are part of Spring settings in Spring Boot 2+

	@Value("${dataSource.url}")
	private String url;

	@Value("${dataSource.username}")
	private String username;

	@Value("${dataSource.password}")
	private String password;

	// TODO: all of this goes away with Spring Boot 2+
	private String driverClassName = "com.mysql.jdbc.Driver";
	private int maxActive = 40;
	private int maxIdle = 5;
	private int minIdle = 2;
	private int initialSize = 2;
	private boolean testWhileIdle = true;
	private String validationQuery = "SELECT 1";
	private int validationInterval = 30000;
	private boolean testOnBorrow = true;
	private int maxAge = 360000;

	@Bean
	public DataSource dataSource() {
		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();

		dataSource.setDriverClassName(driverClassName);
		dataSource.setPassword(password);
		dataSource.setUsername(username);
		dataSource.setUrl(url);
		dataSource.setMaxActive(maxActive);
		dataSource.setMinIdle(minIdle);
		dataSource.setMaxIdle(maxIdle);
		dataSource.setInitialSize(initialSize);
		dataSource.setTestWhileIdle(testWhileIdle);
		dataSource.setValidationQuery(validationQuery);
		dataSource.setValidationInterval(validationInterval);
		dataSource.setTestOnBorrow(testOnBorrow);
		dataSource.setMaxAge(maxAge);

		return dataSource;
	}
}
