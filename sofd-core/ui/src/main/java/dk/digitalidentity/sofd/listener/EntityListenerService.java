package dk.digitalidentity.sofd.listener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sofd.dao.EntityChangeQueueDao;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.EntityChangeQueue;
import dk.digitalidentity.sofd.dao.model.EntityChangeQueueDetail;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.PersonService;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class EntityListenerService {
	private static final String CHANGE_TYPE_UPDATE = "UPDATE";
	private static final String CHANGE_TYPE_CREATE = "CREATE";
	private static final String CHANGE_TYPE_DELETE = "DELETE"; // TODO: not implemented yet
	private static final String ENTITY_TYPE_ORGUNIT = "ORGUNIT";
	private static final String ENTITY_TYPE_PERSON = "PERSON";

	public enum ChangeType {
		CHANGED_NAME,                     // the name of the entity has changed
		ADDED_AFFILIATION,                // the person has a new employment
		CHANGED_AFFILIATION_LOCATION,     // the person has changed "location" (i.e. extra employment or moved orgUnit on existing employment)
		CHANGED_EMAIL,                    // the person has a changed or new email
		CHANGED_PARENT_UUID				  // the orgunit has changed parent uuid
	};

	@Autowired
	private EntityChangeQueueDao entityChangeQueueDao;

	@Autowired
	private List<ListenerAdapter> adapters;

	public void emitCreateEvent(OrgUnit orgUnit) {
		EntityChangeQueue change = new EntityChangeQueue();
		change.setEntityUuid(orgUnit.getUuid());
		change.setChangeType(CHANGE_TYPE_CREATE);
		change.setEntityType(ENTITY_TYPE_ORGUNIT);
		change.setTts(new Date());

		entityChangeQueueDao.save(change);
	}

	public void emitUpdateEvent(OrgUnit oldOrgUnit, OrgUnit updatedOrgUnit) {
		EntityChangeQueue change = new EntityChangeQueue();
		change.setEntityUuid(oldOrgUnit.getUuid());
		change.setChangeType(CHANGE_TYPE_UPDATE);
		change.setEntityType(ENTITY_TYPE_ORGUNIT);
		change.setEntityChangeQueueDetails(new ArrayList<>());
		change.setTts(new Date());

		checkForOrgUnitNameChange(oldOrgUnit, updatedOrgUnit, change);
		checkForParentUuidChange(oldOrgUnit, updatedOrgUnit, change);
		
		if (change.getEntityChangeQueueDetails().size() > 0) {
			entityChangeQueueDao.save(change);
		}
	}

	public void emitCreateEvent(Person person) {
		EntityChangeQueue change = new EntityChangeQueue();
		change.setEntityUuid(person.getUuid());
		change.setChangeType(CHANGE_TYPE_CREATE);
		change.setEntityType(ENTITY_TYPE_PERSON);
		change.setTts(new Date());

		entityChangeQueueDao.save(change);
	}

	public void emitUpdateEvent(Person oldPerson, Person updatedPerson) {
		EntityChangeQueue change = new EntityChangeQueue();
		change.setEntityUuid(oldPerson.getUuid());
		change.setChangeType(CHANGE_TYPE_UPDATE);
		change.setEntityType(ENTITY_TYPE_PERSON);
		change.setEntityChangeQueueDetails(new ArrayList<>());
		change.setTts(new Date());
		
		checkForPersonNameChange(oldPerson, updatedPerson, change);
		
		checkForPersonEmailChange(oldPerson, updatedPerson, change);

		checkForPersonAddedAffiliation(oldPerson, updatedPerson, change);
		
		checkForPersonAffiliationLocationChange(oldPerson, updatedPerson, change);
		
		if (change.getEntityChangeQueueDetails().size() > 0) {
			entityChangeQueueDao.save(change);
		}
	}

	@Transactional
	public void emit() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -1);
		Date oneMinuteAgo = cal.getTime();

		List<EntityChangeQueue> changes = entityChangeQueueDao.findByTtsBefore(oneMinuteAgo);
		if (changes == null || changes.size() == 0) {
			return;
		}
		
		for (EntityChangeQueue change : changes) {
			switch (change.getEntityType()) {
				case ENTITY_TYPE_ORGUNIT:
					switch (change.getChangeType()) {
						case CHANGE_TYPE_CREATE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.orgUnitCreated(change.getEntityUuid());
								}
								catch (Exception ex) {
									log.error("Event handling failed for OrgUnit " + change.getEntityUuid(), ex);
								}
							}
							break;
						case CHANGE_TYPE_DELETE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.orgUnitDeleted(change.getEntityUuid());
								}
								catch (Exception ex) {
									log.error("Event handling failed for OrgUnit " + change.getEntityUuid(), ex);
								}
							}
							break;
						case CHANGE_TYPE_UPDATE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.orgUnitUpdated(change.getEntityUuid(), change.getEntityChangeQueueDetails());
								}
								catch (Exception ex) {
									log.error("Event handling failed for OrgUnit " + change.getEntityUuid(), ex);
								}
							}
							break;
					}
					break;
				case ENTITY_TYPE_PERSON:
					switch (change.getChangeType()) {
						case CHANGE_TYPE_CREATE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.personCreated(change.getEntityUuid());
								}
								catch (Exception ex) {
									log.error("Event handling failed for Person " + change.getEntityUuid(), ex);
								}
							}
							break;
						case CHANGE_TYPE_DELETE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.personDeleted(change.getEntityUuid());
								}
								catch (Exception ex) {
									log.error("Event handling failed for Person " + change.getEntityUuid(), ex);
								}
							}
							break;
						case CHANGE_TYPE_UPDATE:
							for (ListenerAdapter adapter : adapters) {
								try {
									adapter.personUpdated(change.getEntityUuid(), change.getEntityChangeQueueDetails());
								}
								catch (Exception ex) {
									log.error("Event handling failed for Person " + change.getEntityUuid(), ex);
								}
							}
							break;
					}
					break;
			}
			
			entityChangeQueueDao.delete(change);;
		}
	}
	
	private void checkForPersonAddedAffiliation(Person oldPerson, Person updatedPerson, EntityChangeQueue change) {
		for (Affiliation affiliation : updatedPerson.getAffiliations()) {
			// if ID is 0, then it is not persisted yet, and hence a new affiliation
			if (affiliation.getId() == 0) {
				EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
				entityChangeQueueDetail.setChangeType(ChangeType.ADDED_AFFILIATION);
				entityChangeQueueDetail.setChangeTypeDetails(affiliation.getUuid());
				entityChangeQueueDetail.setEntityChangeQueue(change);

				change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
			}
		}
	}
	
	private void checkForPersonAffiliationLocationChange(Person oldPerson, Person updatedPerson, EntityChangeQueue change) {
		if (AffiliationService.notStoppedAffiliations(oldPerson.getAffiliations()).size() > 0) {
			List<String> oldAffiliationUuids = oldPerson.getAffiliations().stream().map(oa -> oa.getUuid()).collect(Collectors.toList());
			
			for (Affiliation updatedAffiliation : updatedPerson.getAffiliations()) {
				boolean changedAffiliation = false;

				if (!oldAffiliationUuids.contains(updatedAffiliation.getUuid())) {
					changedAffiliation = true;
				}
				else {
					// check if the affiliation orgUnit has changed
					for (Affiliation oldAffiliation : oldPerson.getAffiliations()) {
						if (Objects.equals(oldAffiliation.getUuid(), updatedAffiliation.getUuid())) {
							if (!Objects.equals(oldAffiliation.getOrgUnit().getUuid(), updatedAffiliation.getOrgUnit().getUuid())) {
								changedAffiliation = true;
							}
							
							break;
						}
					}
				}
				
				if (changedAffiliation) {
					EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
					entityChangeQueueDetail.setChangeType(ChangeType.CHANGED_AFFILIATION_LOCATION);
					entityChangeQueueDetail.setChangeTypeDetails(updatedAffiliation.getUuid());
					entityChangeQueueDetail.setEntityChangeQueue(change);
					
					change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
				}
			}
		}
	}

	private void checkForPersonNameChange(Person oldPerson, Person updatedPerson, EntityChangeQueue change) {
		if (!Objects.equals(PersonService.getName(oldPerson), PersonService.getName(updatedPerson))) {
			EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
			entityChangeQueueDetail.setChangeType(ChangeType.CHANGED_NAME);
			entityChangeQueueDetail.setEntityChangeQueue(change);
			
			change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
		}
	}
	
	private void checkForPersonEmailChange(Person oldPerson, Person updatedPerson, EntityChangeQueue change) {
		String oldEmail = PersonService.getEmail(oldPerson);
		String newEmail = PersonService.getEmail(updatedPerson);

		if (!Objects.equals(oldEmail, newEmail)) {
			EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
			entityChangeQueueDetail.setChangeType(ChangeType.CHANGED_EMAIL);
			entityChangeQueueDetail.setOldValue(oldEmail);
			entityChangeQueueDetail.setNewValue(newEmail);
			entityChangeQueueDetail.setEntityChangeQueue(change);

			change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
		}
	}
	
	private void checkForOrgUnitNameChange(OrgUnit oldOrgUnit, OrgUnit updatedOrgUnit, EntityChangeQueue change) {
		if (!Objects.equals(oldOrgUnit.getName(), updatedOrgUnit.getName())) {
			EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
			entityChangeQueueDetail.setChangeType(ChangeType.CHANGED_NAME);
			entityChangeQueueDetail.setOldValue(oldOrgUnit.getName());
			entityChangeQueueDetail.setNewValue(updatedOrgUnit.getName());
			entityChangeQueueDetail.setEntityChangeQueue(change);

			change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
		}
	}
	
	private void checkForParentUuidChange(OrgUnit oldOrgUnit, OrgUnit updatedOrgUnit, EntityChangeQueue change) {
		if (updatedOrgUnit.getParent() != null) {
			if (oldOrgUnit.getParent() != null) {
				if (!Objects.equals(oldOrgUnit.getParent().getUuid(), updatedOrgUnit.getParent().getUuid())) {
					createAndAdd(oldOrgUnit.getParent().getName(), updatedOrgUnit.getParent().getName(), change);
				}
			}
			else {
				createAndAdd(null, updatedOrgUnit.getParent().getName(), change);
			}
			
		}else {
			if (oldOrgUnit.getParent() != null) {
				createAndAdd(oldOrgUnit.getParent().getName(), null, change);
			}
		}
	}
	
	private void createAndAdd(String oldValue, String newValue, EntityChangeQueue change) {
		EntityChangeQueueDetail entityChangeQueueDetail = new EntityChangeQueueDetail();
		entityChangeQueueDetail.setChangeType(ChangeType.CHANGED_PARENT_UUID);
		entityChangeQueueDetail.setOldValue(oldValue);
		entityChangeQueueDetail.setNewValue(newValue);
		entityChangeQueueDetail.setEntityChangeQueue(change);

		change.getEntityChangeQueueDetails().add(entityChangeQueueDetail);
	}
}
