package dk.digitalidentity.sofd.config.properties;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Children {
	private String url = "http://childrenservice.digital-identity.dk";
	// default set to 8 to support "omsorgsdage"
	private int maxAge = 8;
	private List<String> affiliationMasters = new ArrayList<>();
}
