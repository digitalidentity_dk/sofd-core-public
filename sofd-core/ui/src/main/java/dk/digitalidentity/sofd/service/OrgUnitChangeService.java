package dk.digitalidentity.sofd.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.dao.model.EmailTemplate;
import dk.digitalidentity.sofd.dao.model.enums.EmailTemplateType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.dao.OrgUnitChangeDao;
import dk.digitalidentity.sofd.dao.model.OrgUnitChange;
import dk.digitalidentity.sofd.dao.model.enums.OrgUnitChangeStatus;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@Slf4j
public class OrgUnitChangeService {
	@Autowired
	private OrgUnitChangeDao orgUnitChangeDao;

	@Autowired
	private SofdConfiguration configuration;

	@Autowired
	private OrgUnitChangeService orgUnitChangeService;

	@Autowired
	private EmailTemplateService emailTemplateService;

	@Autowired
	private EmailQueueService emailQueueService;

	@Autowired
	private TemplateEngine templateEngine;

	// using already existing configuration although it breaks segregation
	@Value("${saml.baseUrl}")
	private String linkBaseUrl;

	public OrgUnitChange save(OrgUnitChange orgUnitChange) {
		return orgUnitChangeDao.save(orgUnitChange);
	}
	
	public List<OrgUnitChange> getByStatus(OrgUnitChangeStatus status) {
		return orgUnitChangeDao.findByStatus(status);
	}
	
	public void deleteAll(List<OrgUnitChange> toDelete) {
		orgUnitChangeDao.delete(toDelete);
	}

	@Transactional
	public void processOrgUnitChanges() {
		if (!configuration.getScheduled().isEnabled()) {
			log.debug("Scheduled jobs are disabled on this instance");
			return;
		}

		EmailTemplate template = emailTemplateService.findByTemplateType(EmailTemplateType.ORGUNIT_CHANGES);
		if (!template.isEnabled()) {
			return;
		}


		List<OrgUnitChange> pendingChanges = orgUnitChangeService.getByStatus(OrgUnitChangeStatus.PENDING);
		if (pendingChanges.isEmpty()) {
			return;
		}

		List<String> recipients = emailTemplateService.getRecipientsList(template);
		if (recipients.isEmpty()) {
			return;
		}

		pendingChanges.sort((a,b) -> a.getChangedTimestamp().compareTo(b.getChangedTimestamp()));
		String html = getHtmlString(pendingChanges);

		for (String recipient : recipients) {
			String message = template.getMessage();
			message = message.replace(EmailTemplateService.RECEIVER_PLACEHOLDER, recipient);
			message = message.replace(EmailTemplateService.CHANGES_PLACEHOLDER, html);
			emailQueueService.queueEmail(recipient, template.getTitle(), message, 0, template);
		}

		for (OrgUnitChange change : pendingChanges) {
			change.setStatus(OrgUnitChangeStatus.SENT);
			change.setSentTimestamp(new Date());
			orgUnitChangeService.save(change);
		}

		deleteOldSent();
	}

	private String getHtmlString(List<OrgUnitChange> pendingChanges) {
		Context context = new Context();
		context.setVariable("pendingChanges", pendingChanges);
		context.setVariable("linkBaseUrl", linkBaseUrl);
		String html = templateEngine.process("orgunit/changes/pendingChangesEmailTable", context);
		return html;
	}

	private void deleteOldSent() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date oneMonthAgo = cal.getTime();
		List<OrgUnitChange> toBeDeleted = orgUnitChangeService.getByStatus(OrgUnitChangeStatus.SENT).stream().filter(c -> c.getSentTimestamp().before(oneMonthAgo)).collect(Collectors.toList());
		orgUnitChangeService.deleteAll(toBeDeleted);
	}

}
