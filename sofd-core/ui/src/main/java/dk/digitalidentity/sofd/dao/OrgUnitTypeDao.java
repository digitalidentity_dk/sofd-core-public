package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.OrgUnitType;

@RepositoryRestResource(exported = false)
public interface OrgUnitTypeDao extends CrudRepository<OrgUnitType, Long> {
	List<OrgUnitType> findAll();
	List<OrgUnitType> findByActiveTrue();
	OrgUnitType findByKey(String key);
	OrgUnitType getByExtId(String extId);
}