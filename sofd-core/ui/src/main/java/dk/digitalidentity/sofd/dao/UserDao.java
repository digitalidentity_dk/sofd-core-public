package dk.digitalidentity.sofd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dk.digitalidentity.sofd.dao.model.User;

@RepositoryRestResource(exported = false)
public interface UserDao extends CrudRepository<User, Long> {
	User findByUserIdAndUserType(String userId, String userType);
	User findByUuid(String uuid);
	List<User> findAll();
	List<User> findByUserIdLikeAndUserType(String word, String userType);
}