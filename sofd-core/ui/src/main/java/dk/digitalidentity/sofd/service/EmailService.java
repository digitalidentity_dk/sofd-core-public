package dk.digitalidentity.sofd.service;

import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.config.SofdConfiguration;
import dk.digitalidentity.sofd.controller.mvc.dto.InlineImageDTO;
import dk.digitalidentity.sofd.dao.model.Attachment;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class EmailService {

	@Autowired
	private SofdConfiguration configuration;

	public boolean sendMessage(String email, String subject, String message) {
		if (!configuration.getIntegrations().getEmail().isEnabled()) {
			log.warn("email server is not configured - not sending email to " + email);
			return false;
		}

		Transport transport = null;

		log.info("Sending email: '" + subject + "' to " + email);

		try {
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", 25);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			Session session = Session.getDefaultInstance(props);

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(configuration.getIntegrations().getEmail().getFrom(), configuration.getIntegrations().getEmail().getFromName()));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			msg.setSubject(subject, "UTF-8");
			msg.setText(message, "UTF-8");
			msg.setHeader("Content-Type", "text/html; charset=UTF-8");

			transport = session.getTransport();
			transport.connect(configuration.getIntegrations().getEmail().getHost(), configuration.getIntegrations().getEmail().getUsername(), configuration.getIntegrations().getEmail().getPassword());
			transport.addTransportListener(new TransportErrorHandler());
			transport.sendMessage(msg, msg.getAllRecipients());
		}
		catch (Exception ex) {
			log.error("Failed to send email to '" + email + "'", ex);
			
			return false;
		}
		finally {
			try {
				if (transport != null) {
					transport.close();
				}
			}
			catch (Exception ex) {
				log.warn("Error occured while trying to terminate connection", ex);
			}
		}
		
		return true;
	}
	
	public boolean sendMessage(String email, String subject, String message, List<InlineImageDTO> inlineImages) {
		if (!configuration.getIntegrations().getEmail().isEnabled()) {
			log.warn("email server is not configured - not sending email to " + email);
			return false;
		}

		Transport transport = null;

		log.info("Sending email: '" + subject + "' to " + email);

		try {
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", 25);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			Session session = Session.getDefaultInstance(props);

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(configuration.getIntegrations().getEmail().getFrom(), configuration.getIntegrations().getEmail().getFromName()));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			msg.setSubject(subject, "UTF-8");
			msg.setHeader("Content-Type", "text/html; charset=UTF-8");
			
			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message, "text/html; charset=UTF-8");

			// creates multi-part
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
			
			// adds inline image attachments
	        if (inlineImages != null && inlineImages.size() > 0) {
	             
	            for (InlineImageDTO inlineImageDTO : inlineImages) {
	                
	                if (inlineImageDTO.isBase64()) {
	                	MimeBodyPart imagePart = new PreencodedMimeBodyPart("base64");
	                	String src = inlineImageDTO.getSrc();
	                	String dataType = StringUtils.substringBetween(src, "data:", ";base64,"); // extract data type ( fx dataType = "image/png") 
	                	String base64EncodedFileContent = src.replaceFirst("data:.*;base64,", ""); // remove prefix from fileContent String ( fx base64EncodedFileContent = "iVBORw0KGg......etc"
	                	imagePart.setContent(base64EncodedFileContent, dataType);
	                	imagePart.setFileName(inlineImageDTO.getCid());
	                	imagePart.setHeader("Content-ID", "<" + inlineImageDTO.getCid() + ">");
	                	imagePart.setDisposition(MimeBodyPart.INLINE);
	                	imagePart.setDisposition(Part.ATTACHMENT);
	                	multipart.addBodyPart(imagePart);
	                }
	                else if (inlineImageDTO.isUrl()) {
	                	MimeBodyPart imagePart = new MimeBodyPart();
	                	URL url = new URL(inlineImageDTO.getSrc());
		                URLDataSource uds = new URLDataSource(url);
		                imagePart.setDataHandler(new DataHandler(uds));
		                imagePart.setDisposition(Part.ATTACHMENT);
		                imagePart.setHeader("Content-ID", "<" + inlineImageDTO.getCid() + ">");
		                imagePart.setDisposition(MimeBodyPart.INLINE);
		                imagePart.setFileName(url.getFile());
		                multipart.addBodyPart(imagePart);
	                }
	            }
	        }
			
			msg.setContent(multipart);
			//STOP code for handling inline images

			transport = session.getTransport();
			transport.connect(configuration.getIntegrations().getEmail().getHost(), configuration.getIntegrations().getEmail().getUsername(), configuration.getIntegrations().getEmail().getPassword());
			transport.addTransportListener(new TransportErrorHandler());
			transport.sendMessage(msg, msg.getAllRecipients());
		}
		catch (Exception ex) {
			log.error("Failed to send email to '" + email + "'", ex);
			
			return false;
		}
		finally {
			try {
				if (transport != null) {
					transport.close();
				}
			}
			catch (Exception ex) {
				log.warn("Error occured while trying to terminate connection", ex);
			}
		}
		
		return true;
	}
	
	public boolean sendMessageWithAttachments(String email, String subject, String message, List<Attachment> attachments) {
		if (!configuration.getIntegrations().getEmail().isEnabled()) {
			log.warn("email server is not configured - not sending email to " + email);
			return false;
		}

		Transport transport = null;

		log.info("Sending email: '" + subject + "' to " + email);

		try {
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", 25);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			Session session = Session.getDefaultInstance(props);

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(configuration.getIntegrations().getEmail().getFrom(), "SOFD Core"));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			msg.setSubject(subject, "UTF-8");

			MimeBodyPart htmlBodyPart = new MimeBodyPart();
			htmlBodyPart.setContent(message, "text/html; charset=UTF-8");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(htmlBodyPart);

			if (attachments != null) {
				for (Attachment attachment : attachments) {
					MimeBodyPart attachmentBodyPart = new MimeBodyPart();
					attachmentBodyPart = new MimeBodyPart();
					DataSource source = new ByteArrayDataSource(attachment.getFile().getContent(), "application/octet-stream");
					attachmentBodyPart.setDataHandler(new DataHandler(source));
					attachmentBodyPart.setFileName(attachment.getFilename());
					
					multipart.addBodyPart(attachmentBodyPart);
				}
			}

			msg.setContent(multipart);

			transport = session.getTransport();
			transport.connect(configuration.getIntegrations().getEmail().getHost(), configuration.getIntegrations().getEmail().getUsername(), configuration.getIntegrations().getEmail().getPassword());
			transport.addTransportListener(new TransportErrorHandler());
			transport.sendMessage(msg, msg.getAllRecipients());
		}
		catch (Exception ex) {
			log.error("Failed to send email to '" + email + "'", ex);
			
			return false;
		}
		finally {
			try {
				if (transport != null) {
					transport.close();
				}
			}
			catch (Exception ex) {
				log.warn("Error occured while trying to terminate connection", ex);
			}
		}
		
		return true;
	}
	
	
	public boolean sendMessageWithAttachments(String email, String subject, String message, List<Attachment> attachments, List<InlineImageDTO> inlineImages) {
		if (!configuration.getIntegrations().getEmail().isEnabled()) {
			log.warn("email server is not configured - not sending email to " + email);
			return false;
		}

		Transport transport = null;

		log.info("Sending email: '" + subject + "' to " + email);

		try {
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", 25);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			Session session = Session.getDefaultInstance(props);

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(configuration.getIntegrations().getEmail().getFrom(), "SOFD Core"));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			msg.setSubject(subject, "UTF-8");

			MimeBodyPart htmlBodyPart = new MimeBodyPart();
			htmlBodyPart.setContent(message, "text/html; charset=UTF-8");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(htmlBodyPart);

			if (attachments != null) {
				for (Attachment attachment : attachments) {
					MimeBodyPart attachmentBodyPart = new MimeBodyPart();
					attachmentBodyPart = new MimeBodyPart();
					DataSource source = new ByteArrayDataSource(attachment.getFile().getContent(), "application/octet-stream");
					attachmentBodyPart.setDataHandler(new DataHandler(source));
					attachmentBodyPart.setFileName(attachment.getFilename());
					
					multipart.addBodyPart(attachmentBodyPart);
				}
			}
			
			// adds inline image attachments
	        if (inlineImages != null && inlineImages.size() > 0) {
	             
	            for (InlineImageDTO inlineImageDTO : inlineImages) {
	                
	                if (inlineImageDTO.isBase64()) {
	                	MimeBodyPart imagePart = new PreencodedMimeBodyPart("base64");
	                	String src = inlineImageDTO.getSrc();
	                	String dataType = StringUtils.substringBetween(src, "data:", ";base64,"); // extract data type ( fx dataType = "image/png") 
	                	String base64EncodedFileContent = src.replaceFirst("data:.*;base64,", ""); // remove prefix from fileContent String ( fx base64EncodedFileContent = "iVBORw0KGg......etc"
	                	imagePart.setContent(base64EncodedFileContent, dataType);
	                	imagePart.setFileName(inlineImageDTO.getCid());
	                	imagePart.setHeader("Content-ID", "<" + inlineImageDTO.getCid() + ">");
	                	imagePart.setDisposition(MimeBodyPart.INLINE);
	                	imagePart.setDisposition(Part.ATTACHMENT);
	                	multipart.addBodyPart(imagePart);
	                }
	                else if (inlineImageDTO.isUrl()) {
	                	MimeBodyPart imagePart = new MimeBodyPart();
	                	URL url = new URL(inlineImageDTO.getSrc());
		                URLDataSource uds = new URLDataSource(url);
		                imagePart.setDataHandler(new DataHandler(uds));
		                imagePart.setDisposition(Part.ATTACHMENT);
		                imagePart.setHeader("Content-ID", "<" + inlineImageDTO.getCid() + ">");
		                imagePart.setDisposition(MimeBodyPart.INLINE);
		                imagePart.setFileName(url.getFile());
		                multipart.addBodyPart(imagePart);
	                }
	            }
	        }

			msg.setContent(multipart);

			transport = session.getTransport();
			transport.connect(configuration.getIntegrations().getEmail().getHost(), configuration.getIntegrations().getEmail().getUsername(), configuration.getIntegrations().getEmail().getPassword());
			transport.addTransportListener(new TransportErrorHandler());
			transport.sendMessage(msg, msg.getAllRecipients());
		}
		catch (Exception ex) {
			log.error("Failed to send email to '" + email + "'", ex);
			
			return false;
		}
		finally {
			try {
				if (transport != null) {
					transport.close();
				}
			}
			catch (Exception ex) {
				log.warn("Error occured while trying to terminate connection", ex);
			}
		}
		
		return true;
	}
}
