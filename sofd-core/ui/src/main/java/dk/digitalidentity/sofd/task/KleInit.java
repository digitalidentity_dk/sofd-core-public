package dk.digitalidentity.sofd.task;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KleInit {

	@Autowired
	private ReadKleTask kleReader;
	
	@PostConstruct
	public void init() {
		kleReader.init();
	}
}
