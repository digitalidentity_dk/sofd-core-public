package dk.digitalidentity.sofd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sofd.dao.KnownUsernamesDao;
import dk.digitalidentity.sofd.dao.model.KnownUsername;

@Service
@EnableCaching
public class KnownUsernamesService {

	@Autowired
	private KnownUsernamesDao knownUsernamesDao;
	
	@Autowired
	private KnownUsernamesService self;

	public List<KnownUsername> findAll() {
		return knownUsernamesDao.findAll();
	}

	@Cacheable(value = "knownUsernamesByType")
	public List<KnownUsername> findByUserType(String userType) {
		return knownUsernamesDao.findByUserType(userType);
	}

	public KnownUsername findByUsernameAndUserType(String username, String userType) {
		return knownUsernamesDao.findByUsernameAndUserType(username, userType);
	}

	public void save(List<KnownUsername> entities) {
		knownUsernamesDao.save(entities);
	}
	
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void resetKnownUsernamesByTypeCacheTask() {
    	self.resetKnownUsernamesByTypeCache();
    }

    @CacheEvict(value = "knownUsernamesByType", allEntries = true)
    public void resetKnownUsernamesByTypeCache() {
    	; // clears cache every hour, the result is generated nightly, so no reason to hammer the database
    }
}
