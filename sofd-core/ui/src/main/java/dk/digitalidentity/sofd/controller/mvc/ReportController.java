package dk.digitalidentity.sofd.controller.mvc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import dk.digitalidentity.sofd.config.SessionConstants;
import dk.digitalidentity.sofd.controller.mvc.dto.AccountOrderDTO;
import dk.digitalidentity.sofd.controller.mvc.xls.GenericReportXlsView;
import dk.digitalidentity.sofd.dao.model.AccountOrder;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Notification;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.enums.AccountOrderStatus;
import dk.digitalidentity.sofd.dao.model.enums.ReportType;
import dk.digitalidentity.sofd.security.RequireControllerWriteAccess;
import dk.digitalidentity.sofd.security.RequireReadAccess;
import dk.digitalidentity.sofd.service.AccountOrderService;
import dk.digitalidentity.sofd.service.AffiliationService;
import dk.digitalidentity.sofd.service.NotificationService;
import dk.digitalidentity.sofd.service.PersonService;
import dk.digitalidentity.sofd.service.SupportedUserTypeService;
import lombok.extern.log4j.Log4j;

@Log4j
@RequireReadAccess
@Controller
public class ReportController {

	@Autowired
	private AccountOrderService accountOrderService;

	@Autowired
	private PersonService personService;

	@Autowired
	private NotificationService adminTasksService;

	@Autowired
	private MessageSource messageSource;

	@GetMapping("/ui/report/accountorders")
	public String accountOrders(Model model) {
		List<AccountOrder> adAccountOrders = accountOrderService.findAll();
		adAccountOrders.sort(Comparator.comparing(AccountOrder::getStatus));

		List<AccountOrderDTO> dtos = new ArrayList<>();
		for (AccountOrder order : adAccountOrders) {
			Person person = personService.getByUuid(order.getPersonUuid());
			if (person == null) {
				log.warn("Could not find person with uuid: " + order.getPersonUuid());
				continue;
			}
			
			dtos.add(new AccountOrderDTO(order, person));
		}
		
		model.addAttribute("orders", dtos);
		
		return "report/accountorders";
	}
	
	@GetMapping("/ui/report/accountorders/{id}")
	public String accountOrder(Model model, @PathVariable("id") long id) {
		AccountOrder order = accountOrderService.findById(id);
		if (order == null) {
			return "redirect:/ui/report/accountorders";
		}

		Person person = personService.getByUuid(order.getPersonUuid());
		if (person == null) {
			log.warn("Could not find person with uuid: " + order.getPersonUuid());
			
			return "redirect:/ui/report/accountorders";
		}

		AccountOrderDTO orderDTO = new AccountOrderDTO(order, person);
		
		model.addAttribute("order", orderDTO);
		
		return "report/accountorderView";
	}
	
	@RequireControllerWriteAccess
	@GetMapping("/ui/report/accountorders/delete/{id}")
	public String deleteAccountOrder(Model model, @PathVariable("id") long id) {
		AccountOrder order = accountOrderService.findById(id);
		if (order != null && order.getStatus().equals(AccountOrderStatus.FAILED)) {
			accountOrderService.delete(order);
		}

		return "redirect:/ui/report/accountorders";
	}
	
	@RequireControllerWriteAccess
	@GetMapping("/ui/report/accountorders/retry/{id}/{userId:.+}")
	public String retryAccountOrder(Model model, @PathVariable("id") long id, @PathVariable("userId") String userId) {
		AccountOrder order = accountOrderService.findById(id);
		if (order != null && order.getStatus().equals(AccountOrderStatus.FAILED)) {
			order.setRequestedUserId(userId);
			order.setMessage(null);
			order.setStatus(AccountOrderStatus.PENDING);
			accountOrderService.save(order);
		}

		return "redirect:/ui/report/accountorders";
	}

	@GetMapping("/ui/report/reports")
	public String reports(Model model) {
		ReportType[] reports = ReportType.values();

		model.addAttribute("reports", reports);

		return "report/reports";
	}

	@GetMapping("/ui/report/reports/{reportType}")
	public String getReport(Model model, @PathVariable("reportType") ReportType report) {
		model.addAttribute("reportType", report);

		switch (report) {
			case AD_ACCOUNT_BUT_NO_AFFILIATION:
				model.addAttribute("persons", generateADWithoutAffiliationReport());
				return "report/ad_without_affiliation";
			case DUPLICATE_AFFILIATION:
				model.addAttribute("persons", generateDuplicateAffiliationReport());
				return "report/duplicate_affiliation";
			case OPUS_ACCOUNT_BUT_NO_AD_ACCOUNT:
				model.addAttribute("persons", generateOpusButNoADReport());
				return "report/opus_without_ad";
			case PERSONS_DISABLE_ACCOUNT_ORDERS:
				model.addAttribute("persons", generateAccountOrdersDisabledReport());
				return "report/account_orders_disabled";
			case PERSONS_ON_LEAVE:
				model.addAttribute("persons", generatePersonsOnLeaveReport());
				return "report/persons_on_leave";
			case PERSONS_STOPPED:
				model.addAttribute("persons", generatePersonsForceStopReport());
				return "report/persons_force_stopped";
		}

		return "redirect:/ui/report/reports";
	}
	
	@GetMapping("/ui/report/reports/{reportType}/download")
	public ModelAndView downloadReport(@PathVariable("reportType") ReportType report, HttpServletResponse response, Locale loc) {
		Map<String, Object> model = new HashMap<>();
		model.put("report", report);
		model.put("locale", loc);
		model.put("messagesBundle", messageSource);

		switch (report) {
			case AD_ACCOUNT_BUT_NO_AFFILIATION:
				model.put("persons", generateADWithoutAffiliationReport());
				break;
			case DUPLICATE_AFFILIATION:
				model.put("persons", generateDuplicateAffiliationReport());
				break;
			case OPUS_ACCOUNT_BUT_NO_AD_ACCOUNT:
				model.put("persons", generateOpusButNoADReport());
				break;
			case PERSONS_ON_LEAVE:
				model.put("persons", generatePersonsOnLeaveReport());
				break;
			case PERSONS_DISABLE_ACCOUNT_ORDERS:
				model.put("persons", generateAccountOrdersDisabledReport());
				break;
			case PERSONS_STOPPED:
				model.put("persons", generatePersonsForceStopReport());
				break;
		}

		response.setContentType("application/ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"rapport.xls\"");

		return new ModelAndView(new GenericReportXlsView(), model);
	}
	
	@GetMapping(path = "/ui/report/notifications")
	public String listNotifications(Model model, HttpServletRequest request) {
		List<Notification> adminTasks = adminTasksService.findAll();
		
		Map<String, String> map = new HashMap<>();
		for (Notification adminTask : adminTasks) {
			map.put(adminTask.getNotificationType().toString(), messageSource.getMessage(adminTask.getNotificationType().getMessage(), null, new Locale("da-DK")));
		}

		Person person = personService.getLoggedInPerson();
		model.addAttribute("personUuid", (person != null) ? person.getUuid() : "");
		
		// update badge in UI
		long count = adminTasks.stream().filter(t -> t.isActive()).count();
		request.getSession().setAttribute(SessionConstants.SESSION_NOTIFICATION_COUNT, count);
		
		model.addAttribute("adminTasks", adminTasks);
		model.addAttribute("typesMap", map);

		return "report/notifications";
	}
	
	@GetMapping(path = "/ui/report/notifications/{id}")
	public String viewNotification(Model model, @PathVariable("id") long id) {
		Notification adminTask = adminTasksService.findById(id);
		if (adminTask == null) {
			return "redirect:/ui/report/notifications";
		}

		model.addAttribute("notification", adminTask);

		return "report/notificationView";
	}

	private List<Person> generateOpusButNoADReport() {
		List<Person> persons = personService.getActiveCached().stream()
				// Persons that have OPUS account
				.filter(p -> p.getUsers().stream().anyMatch(u -> SupportedUserTypeService.isOpus(u.getUserType()) && u.isDisabled() == false))
				// Persons without AD account
				.filter(p -> p.getUsers().stream().noneMatch(u -> SupportedUserTypeService.isActiveDirectory(u.getUserType()) && u.isDisabled() == false))
				.collect(Collectors.toList());

		return persons;
	}

	private List<Person> generateADWithoutAffiliationReport() {
		List<Person> persons = personService.getActiveCached().stream()
				// Persons that have AD account
				.filter(p -> p.getUsers().stream().anyMatch(u -> SupportedUserTypeService.isActiveDirectory(u.getUserType()) && u.isDisabled() == false))
				// Persons without affiliations or persons that have all deleted or stopped affiliations
				.filter(p -> p.getAffiliations().isEmpty() ||
						p.getAffiliations().stream().allMatch(af -> (af.isDeleted()) || (AffiliationService.notActiveAnymore(af)))
				)
				.collect(Collectors.toList());

		return persons;
	}
	
	private List<Person> generateAccountOrdersDisabledReport() {
		List<Person> persons = personService.getActiveCached().stream()
				.filter(p -> p.isDisableAccountOrders())
				.collect(Collectors.toList());

		return persons;
	}
	
	private List<Person> generatePersonsForceStopReport() {
		List<Person> persons = personService.getActiveCached().stream()
				.filter(p -> p.isForceStop())
				.collect(Collectors.toList());

		return persons;
	}

	private List<Person> generatePersonsOnLeaveReport() {
		List<Person> persons = personService.getActiveCached().stream()
				.filter(p -> p.getLeave() != null)
				.collect(Collectors.toList());

		return persons;
	}

	private List<Person> generateDuplicateAffiliationReport() {
		List<Person> persons = new ArrayList<Person>();

		for (Person person : personService.getActiveCached()) {
			Set<String> orgUnits = new HashSet<>();

			// find all SOFD affiliations
			for (Affiliation  affiliation : person.getAffiliations()) {
				if (affiliation.getMaster().equals("SOFD")) {
					orgUnits.add(affiliation.getOrgUnit().getUuid());
				}
			}

			// find non-SOFD affiliations that maps to same OrgUnit as a SOFD-owned affiliation
			for (Affiliation  affiliation : person.getAffiliations()) {
				if (!affiliation.getMaster().equals("SOFD")) {
					if (orgUnits.contains(affiliation.getOrgUnit().getUuid())) {
						persons.add(person);
						break;
					}
				}
			}
		}

		return persons;
	}
}
