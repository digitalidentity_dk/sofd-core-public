package dk.digitalidentity.sofd.config.properties;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EBoks {
	private boolean enabled = false;
	private String materialeId;
	private String senderId;

	@JsonIgnore
	private String url = "http://remoteprint.digital-identity.dk/";	
}
