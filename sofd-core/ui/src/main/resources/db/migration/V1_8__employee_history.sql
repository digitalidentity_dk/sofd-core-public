CREATE TABLE users_aud (
  id                        BIGINT NOT NULL,
  uuid                      VARCHAR(36),
  master                    VARCHAR(64),
  master_id                 VARCHAR(255),
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  user_id                   VARCHAR(64),
  local_extensions          TEXT NULL,
  email_id                  BIGINT,
  user_type					VARCHAR(36),

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE persons_aud (
  uuid                            VARCHAR(36) NOT NULL,
  master                          VARCHAR(64),
  rev                             BIGINT NOT NULL,
  revtype                         TINYINT,
  
  deleted                         BOOLEAN NULL,
  created                         TIMESTAMP NULL,
  last_changed                    TIMESTAMP NULL,

  firstname                       VARCHAR(255),
  surname                         VARCHAR(255),
  cpr                             VARCHAR(10),
  chosen_name                     VARCHAR(255),
  first_employment_date           DATE NULL,
  anniversary_date                DATE NULL,
  local_extensions                TEXT NULL,
  registered_post_address_id      BIGINT,
  residence_post_address_id       BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (uuid, rev)
);

CREATE TABLE persons_phones_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  person_uuid               VARCHAR(36),
  phone_id                  BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE persons_users_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  person_uuid               VARCHAR(36),
  user_id                   BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);