ALTER TABLE entity_change_queue DROP COLUMN fields;

CREATE TABLE entity_change_queue_details (
  id                            BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  entity_change_queue_id        BIGINT NOT NULL,
  change_type                   VARCHAR(255) NOT NULL,
  change_type_details           VARCHAR(255),

  FOREIGN KEY (entity_change_queue_id) REFERENCES entity_change_queue(id) ON DELETE CASCADE
);