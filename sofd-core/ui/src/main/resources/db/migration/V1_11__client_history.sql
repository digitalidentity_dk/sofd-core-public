CREATE TABLE client_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  name                      VARCHAR(64),
  api_key                   VARCHAR(36),
  access_role               VARCHAR(36),
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE access_field_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  client_id                 BIGINT,
  entity                    VARCHAR(36),
  field                     VARCHAR(64),

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);