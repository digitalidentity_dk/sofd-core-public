CREATE VIEW subview_callcenter_orgunit_prime_post AS
(
	SELECT
		op.orgunit_uuid
        ,CONCAT(p.street, ' ', p.postal_code ,' ', p.city) AS 'address'
		,p.*
	FROM orgunits_posts op
	INNER JOIN posts p ON p.id = op.post_id and p.prime=1
);

CREATE VIEW subview_callcenter_prime_ad_user AS
(
	SELECT
		pu.person_uuid
        ,u.*
        FROM persons_users pu
        INNER JOIN users u ON u.id = pu.user_id AND u.master = 'ActiveDirectory' AND u.prime = 1
);

CREATE VIEW subview_callcenter_prime_person_phone AS
(
	SELECT
		pp.person_uuid
        ,p.*
	FROM persons_phones pp
	INNER JOIN phones p ON p.id = pp.phone_id AND p.prime = 1
);

CREATE VIEW subview_callcenter_prime_orgunit_phone AS
(
	SELECT
		oup.orgunit_uuid
        ,p.*
	FROM orgunits_phones oup
	INNER JOIN phones p ON p.id = oup.phone_id AND p.prime = 1
);

CREATE VIEW subview_callcenter_person_prime_email AS
(
	SELECT
		pu.person_uuid
        ,e.*
	FROM persons_users pu
	INNER JOIN users u ON u.id = pu.user_id
	INNER JOIN emails e ON e.id = u.email_id and e.prime = 1
);

CREATE VIEW subview_callcenter_orgunit_prime_email AS
(
	SELECT
		oue.orgunit_uuid
        ,e.*
	FROM orgunits_emails oue
	INNER JOIN emails e ON e.id = oue.email_id and e.prime = 1
);


CREATE VIEW subview_callcenter_orgunits AS
(
	SELECT DISTINCT
		'OrgUnit'			AS 'type',
		ou.uuid             AS 'uuid',
		ou.name 			AS 'name',
		ou.name 			AS 'org_unit',
		NULL 				AS 'user_id',
		ou.key_words		AS 'keywords',
        ou.opening_hours	AS 'opening_hours',
		pop.phone_number	AS 'phone',
		oupe.email			AS 'email',
		oupp.address        AS 'address',
		NULL				AS 'position_name'
	FROM
		orgunits ou
        LEFT JOIN subview_callcenter_prime_orgunit_phone pop ON pop.orgunit_uuid = ou.uuid
        LEFT JOIN subview_callcenter_orgunit_prime_email oupe ON oupe.orgunit_uuid = ou.uuid
        LEFT JOIN subview_callcenter_orgunit_prime_post oupp ON oupp.orgunit_uuid = ou.uuid
	WHERE ou.deleted = 0
);

CREATE VIEW subview_callcenter_employees AS
(
	SELECT DISTINCT
		'Employee'				AS 'type',
		af.uuid                  AS 'uuid',
		IFNULL(p.chosen_name,CONCAT(p.firstname,' ',p.surname)) AS 'name',
		ou.name 				AS 'org_unit',
		padu.user_id			AS 'user_id',
		ou.key_words			AS 'keywords',
        ou.opening_hours		AS 'opening_hours',
		ppp.phone_number		AS 'phone',
		ppe.email				AS 'email',
		oupp.address			AS 'address',
		af.position_name		AS 'position_name'
	FROM
		persons p
		LEFT JOIN affiliations af ON
			af.person_uuid = p.uuid
			AND af.deleted = 0
			AND (af.start_date IS NULL OR af.start_date <= NOW())
			AND (af.stop_date IS NULL OR af.stop_date >= NOW())
		LEFT JOIN orgunits ou ON ou.uuid = af.orgunit_uuid and ou.deleted = 0
		LEFT JOIN subview_callcenter_orgunit_prime_post oupp ON oupp.orgunit_uuid = ou.uuid
		LEFT JOIN subview_callcenter_prime_ad_user padu ON padu.person_uuid = p.uuid
		LEFT JOIN subview_callcenter_prime_person_phone ppp ON ppp.person_uuid = p.uuid
		LEFT JOIN subview_callcenter_person_prime_email ppe ON ppe.person_uuid = p.uuid
);

CREATE VIEW view_callcenter AS
	SELECT * FROM subview_callcenter_orgunits
    UNION ALL
    SELECT * FROM subview_callcenter_employees;