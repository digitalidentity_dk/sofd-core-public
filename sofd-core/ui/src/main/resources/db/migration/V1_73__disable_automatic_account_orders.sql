ALTER TABLE persons     ADD COLUMN disable_account_orders BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE persons_aud ADD COLUMN disable_account_orders BOOLEAN;