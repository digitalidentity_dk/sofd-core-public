CREATE TABLE client_config (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  client_id                 BIGINT NOT NULL,
  client_name               VARCHAR(64) NOT NULL,
  configuration             MEDIUMBLOB NOT NULL,
  last_changed              TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);