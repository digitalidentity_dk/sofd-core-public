CREATE TABLE client (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name                      VARCHAR(64) NOT NULL,
  api_key                   VARCHAR(36) NOT NULL,
  access_role               VARCHAR(36) NOT NULL
);

CREATE TABLE access_field (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  client_id                 BIGINT NOT NULL,
  entity                    VARCHAR(36) NOT NULL,
  field                     VARCHAR(64) NOT NULL,

  FOREIGN KEY (client_id) REFERENCES client(id)
);