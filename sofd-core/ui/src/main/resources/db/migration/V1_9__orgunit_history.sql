CREATE TABLE orgunits_aud (
  uuid                      VARCHAR(36) NOT NULL,
  master                    VARCHAR(64),
  master_id                 VARCHAR(255),
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  deleted                   BOOLEAN NULL,
  created                   TIMESTAMP NULL,
  last_changed              TIMESTAMP NULL,

  parent_uuid               VARCHAR(36),
  shortname                 VARCHAR(64),
  name                      VARCHAR(255),
  cvr                       BIGINT,
  ean                       BIGINT,
  senr                      BIGINT,
  pnr                       BIGINT,
  cost_bearer               VARCHAR(255),
  org_type                  VARCHAR(255),
  org_type_id               BIGINT,
  local_extensions          TEXT NULL,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (uuid, rev)
);

CREATE TABLE orgunits_kle_primary_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  orgunit_uuid              VARCHAR(36),
  kle_value                 VARCHAR(8),
  
  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE orgunits_kle_secondary_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,
  
  orgunit_uuid              VARCHAR(36),
  kle_value                 VARCHAR(8),
  
  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE orgunits_manager_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,
  
  orgunit_uuid              VARCHAR(36),
  manager_uuid              VARCHAR(36),
  inherited                 BOOLEAN NULL,
  name                      VARCHAR(255),
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE orgunits_phones_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,
  
  orgunit_uuid              VARCHAR(36),
  phone_id                  BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE orgunits_emails_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  orgunit_uuid              VARCHAR(36),
  email_id                  BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE orgunits_posts_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  orgunit_uuid              VARCHAR(36),
  post_id                   BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);