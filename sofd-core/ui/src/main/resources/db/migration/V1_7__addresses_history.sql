CREATE TABLE phones_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  prime                     BOOLEAN NULL,
  phone_number              VARCHAR(32),
  phone_type                VARCHAR(32),
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE posts_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  prime                     BOOLEAN NULL,
  street                    VARCHAR(255),
  localname                 VARCHAR(255),
  postal_code               VARCHAR(8),
  city                      VARCHAR(255),
  country                   VARCHAR(255),
  address_protected         BOOLEAN NULL,
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE emails_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  prime                     BOOLEAN NULL,
  email                     VARCHAR(255),
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);