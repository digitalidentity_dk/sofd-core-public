-- magic to get rid of foreign key constraint
SELECT CONCAT(
        'ALTER TABLE `telephony_phones` DROP FOREIGN KEY `',
        constraint_name,
        '`'
) INTO @sqlst
        FROM information_schema.KEY_COLUMN_USAGE
        WHERE table_name = 'telephony_phones'
                AND referenced_table_name='orgunits'
                AND referenced_column_name='uuid' LIMIT 1;

SELECT @sqlst;

PREPARE stmt FROM @sqlst;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @sqlst = NULL;

ALTER TABLE telephony_phones DROP COLUMN orgunit_uuid;
ALTER TABLE telephony_phones DROP COLUMN orgunit_name;
ALTER TABLE telephony_phones_aud DROP COLUMN orgunit_uuid;
ALTER TABLE telephony_phones_aud DROP COLUMN orgunit_name;

CREATE TABLE telephony_phones_orgunits (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  telephony_phones_id       BIGINT NOT NULL,
  orgunit_uuid              VARCHAR(36) NOT NULL,
  orgunit_name              VARCHAR(255) NOT NULL,
  
  FOREIGN KEY (telephony_phones_id) REFERENCES telephony_phones(id)
);

CREATE TABLE telephony_phones_orgunits_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  telephony_phones_id       BIGINT,
  orgunit_uuid              VARCHAR(36),
  orgunit_name              VARCHAR(255),

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);
