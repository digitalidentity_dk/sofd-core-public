CREATE TABLE orgunits (
  uuid                      VARCHAR(36) NOT NULL PRIMARY KEY,
  master                    VARCHAR(64) NOT NULL,
  master_id                 VARCHAR(255) NOT NULL,
  deleted                   BOOLEAN DEFAULT FALSE,
  created                   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_changed              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

  parent_uuid               VARCHAR(36),
  shortname                 VARCHAR(64) NOT NULL,
  name                      VARCHAR(255) NOT NULL,
  cvr                       BIGINT,
  ean                       BIGINT,
  senr                      BIGINT,
  pnr                       BIGINT,
  cost_bearer               VARCHAR(255), -- TODO: type og nullable? gml Omkostningssted
  org_type                  VARCHAR(255), -- TODO: type og nullable
  org_type_id               BIGINT,       -- TODO: type og nullable
  local_extensions          TEXT NULL,

  FOREIGN KEY (parent_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunits_kle_primary (
  orgunit_uuid              VARCHAR(36) NOT NULL,
  kle_value                 VARCHAR(8) NOT NULL,
  
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunits_kle_secondary (
  orgunit_uuid              VARCHAR(36) NOT NULL,
  kle_value                 VARCHAR(8) NOT NULL,
  
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunits_manager (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  orgunit_uuid              VARCHAR(36) NOT NULL,
  manager_uuid              VARCHAR(36) NOT NULL,
  inherited                 BOOLEAN NOT NULL DEFAULT FALSE,
  name                      VARCHAR(255) NOT NULL,
  
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (manager_uuid) REFERENCES persons(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunits_phones (
  orgunit_uuid              VARCHAR(36) NOT NULL,
  phone_id                  BIGINT NOT NULL,

  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (phone_id) REFERENCES phones(id) ON DELETE CASCADE
);

CREATE TABLE orgunits_emails (
  orgunit_uuid              VARCHAR(36) NOT NULL,
  email_id                  BIGINT NOT NULL,

  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (email_id) REFERENCES emails(id) ON DELETE CASCADE
);

CREATE TABLE orgunits_posts (
  orgunit_uuid              VARCHAR(36) NOT NULL,
  post_id                   BIGINT NOT NULL,

  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE CASCADE
);