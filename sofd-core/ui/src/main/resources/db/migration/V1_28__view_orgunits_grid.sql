CREATE VIEW view_orgunits_kle_primary AS
  SELECT kp.orgunit_uuid,  GROUP_CONCAT(kp.kle_value SEPARATOR ',') AS kle_values 
    FROM orgunits_kle_primary kp 
    GROUP BY kp.orgunit_uuid;

CREATE VIEW view_orgunits_kle_secondary AS
  SELECT ks.orgunit_uuid, GROUP_CONCAT(ks.kle_value SEPARATOR ',') AS kle_values 
    FROM orgunits_kle_secondary ks 
    GROUP BY ks.orgunit_uuid;
    
CREATE VIEW view_orgunits AS
  SELECT o.uuid, 
    o.name, 
    o.parent_uuid, 
    m.manager_uuid,
    kpa.kle_values AS kle_primary_values, 
    ksa.kle_values AS kle_secondary_values 
  FROM orgunits o
  LEFT JOIN orgunits_manager m ON m.orgunit_uuid = o.uuid
  LEFT JOIN view_orgunits_kle_primary kpa ON kpa.orgunit_uuid = o.uuid 
  LEFT JOIN view_orgunits_kle_secondary ksa ON ksa.orgunit_uuid = o.uuid
  WHERE o.deleted = 0;