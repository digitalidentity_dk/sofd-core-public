CREATE TABLE email_templates (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title                  VARCHAR(255) NOT NULL,
  message                TEXT NOT NULL,
  enabled                BOOLEAN NOT NULL DEFAULT 0,
  template_type          VARCHAR(36) NOT NULL
);