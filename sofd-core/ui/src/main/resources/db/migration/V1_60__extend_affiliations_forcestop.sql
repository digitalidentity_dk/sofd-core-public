ALTER TABLE affiliations     DROP COLUMN stopped;
ALTER TABLE affiliations_aud DROP COLUMN stopped;
ALTER TABLE affiliations     ADD COLUMN force_stop_date DATETIME NULL;
ALTER TABLE affiliations_aud ADD COLUMN force_stop_date DATETIME NULL;
