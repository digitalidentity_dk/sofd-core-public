ALTER TABLE persons     ADD COLUMN taxed_phone BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE persons_aud ADD COLUMN taxed_phone BOOLEAN;

CREATE TABLE function_types (
    id      BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name    VARCHAR(255) NOT NULL
);

CREATE TABLE function_types_aud (
    id                        BIGINT NOT NULL,
    rev                       BIGINT NOT NULL,
    revtype                   TINYINT,

    name                      VARCHAR(255),

    FOREIGN KEY (rev) REFERENCES revisions(id),
    PRIMARY KEY (id, rev)
);

CREATE TABLE function_type_constraints (
    function_type_id    BIGINT NOT NULL,
    phone_type          VARCHAR(255) NOT NULL,

    FOREIGN KEY         (function_type_id) REFERENCES function_types (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE function_type_constraints_aud (
    rev                 BIGINT NOT NULL,
    revtype             TINYINT,

    function_type_id    BIGINT,
    phone_type          VARCHAR(255),

    FOREIGN KEY (rev) REFERENCES revisions(id)
);

ALTER TABLE phones
DROP COLUMN         function_type,
ADD  COLUMN         notes TEXT,
ADD  COLUMN         visibility VARCHAR(255),
ADD  COLUMN         function_type_id BIGINT,
ADD  COLUMN         type_prime BOOLEAN AFTER `prime`,
ADD  FOREIGN KEY    (function_type_id) REFERENCES function_types(id) ON DELETE SET NULL;

UPDATE phones SET visibility = 'VISIBLE';
UPDATE phones SET type_prime = 0;

ALTER TABLE phones_aud
DROP COLUMN         function_type,
ADD  COLUMN         notes TEXT,
ADD  COLUMN         visibility VARCHAR(255),
ADD  COLUMN         function_type_id BIGINT,
ADD  COLUMN         type_prime BOOLEAN AFTER `prime`;
