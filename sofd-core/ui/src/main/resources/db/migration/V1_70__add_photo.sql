CREATE TABLE photos (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  person_uuid               VARCHAR(36) NOT NULL,
  last_changed              TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data                      MEDIUMBLOB NOT NULL,
  checksum                  BIGINT NOT NULL,
  format                    VARCHAR(50) NOT NULL,

  CONSTRAINT fk_photos_persons FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE
);