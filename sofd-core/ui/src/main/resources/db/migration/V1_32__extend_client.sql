ALTER TABLE client ADD COLUMN monitor_for_activity BOOLEAN NOT NULL DEFAULT 0;
ALTER TABLE client ADD COLUMN show_on_frontpage BOOLEAN NOT NULL DEFAULT 0;
ALTER TABLE client ADD COLUMN last_active TIMESTAMP NULL;

ALTER TABLE client_aud ADD COLUMN monitor_for_activity BOOLEAN;
ALTER TABLE client_aud ADD COLUMN show_on_frontpage BOOLEAN;
ALTER TABLE client_aud ADD COLUMN last_active TIMESTAMP NULL;