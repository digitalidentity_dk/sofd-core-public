CREATE TABLE orgunit_change_queue (
  id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  orgunit_uuid        VARCHAR(36)     NOT NULL,
  orgunit_name        VARCHAR(64)     NOT NULL,
  change_date         DATE            NOT NULL,
  change_type         VARCHAR(255)    NOT NULL,
  attribute_field     VARCHAR(255)    NULL,
  attribute_value     VARCHAR(255)    NULL,
  create_payload      TEXT            NULL,
  parent_uuid         VARCHAR(36)     NULL,
  parent_name         VARCHAR(64)     NULL,
  applied_status      VARCHAR(255)    NOT NULL,
  applied_date        TIMESTAMP       NULL
);