CREATE TABLE message_template (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name                   VARCHAR(255) NOT NULL,
  message                TEXT NOT NULL
);
