ALTER TABLE telephony_phones ADD COLUMN subscription_type VARCHAR(255);
ALTER TABLE telephony_phones ADD COLUMN notes TEXT;

ALTER TABLE telephony_phones_aud ADD COLUMN subscription_type VARCHAR(255);
ALTER TABLE telephony_phones_aud ADD COLUMN notes TEXT;