CREATE TABLE organisations (
  id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  short_name          VARCHAR(64) NOT NULL,
  name                VARCHAR(255) NOT NULL,
  description         TEXT,
  
  UNIQUE(short_name)
);

CREATE TABLE organisations_aud (
  id                  BIGINT NOT NULL,
  rev                 BIGINT NOT NULL,
  revtype             TINYINT,

  short_name          VARCHAR(64) NULL,
  name                VARCHAR(255) NULL,
  description         TEXT,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

INSERT INTO organisations (short_name, name, description) VALUES ('ADMORG', 'Administrativ organisation', 'Den administrative organisation');


ALTER TABLE orgunits ADD COLUMN belongs_to BIGINT;
ALTER TABLE orgunits ADD CONSTRAINT fk_belongs_to FOREIGN KEY (belongs_to) REFERENCES organisations(id) ON DELETE CASCADE;

ALTER TABLE orgunits_aud ADD COLUMN belongs_to BIGINT;

UPDATE orgunits SET belongs_to = (SELECT id FROM organisations WHERE short_name = 'ADMORG');

ALTER TABLE orgunits MODIFY belongs_to BIGINT NOT NULL;

CREATE VIEW view_adm_organisation AS SELECT id FROM organisations WHERE short_name = 'ADMORG';
