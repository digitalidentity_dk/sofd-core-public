CREATE TABLE sofd_account (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  person_uuid               VARCHAR(36) NOT NULL,
  user_id                   VARCHAR(64) NOT NULL,
  password                  VARCHAR(60) NOT NULL,
  
  FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE
);