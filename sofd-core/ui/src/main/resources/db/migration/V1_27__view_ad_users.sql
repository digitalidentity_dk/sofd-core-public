CREATE VIEW view_affiliations_primary_kle AS
  SELECT kp.affiliation_id, GROUP_CONCAT(kp.kle_value SEPARATOR ',') AS kle_values
    FROM affiliations_kle_primary kp
    GROUP BY kp.affiliation_id;

CREATE VIEW view_affiliations_secondary_kle AS
  SELECT ks.affiliation_id, GROUP_CONCAT(ks.kle_value SEPARATOR ',') AS kle_values
    FROM affiliations_kle_secondary ks
    GROUP BY ks.affiliation_id;

CREATE VIEW view_ad_users AS
  SELECT p.uuid,
    COALESCE(p.chosen_name, CONCAT(p.firstname, ' ', p.surname)) AS name,
    u.user_id,
    e.email,
    ph.phone_number,
    a.position_name,
    a.orgunit_uuid,
    kpa.kle_values AS kle_primary_values,
    ksa.kle_values AS kle_secondary_values
  FROM persons p
  JOIN persons_users pu ON pu.person_uuid = p.uuid
  JOIN users u ON u.id = pu.user_id
  LEFT JOIN affiliations a ON a.person_uuid = p.uuid AND (u.employee_id = a.employee_id OR u.employee_id IS NULL)
  LEFT JOIN persons_phones pp ON pp.person_uuid = p.uuid
  LEFT JOIN phones ph ON ph.id = pp.phone_id
  LEFT JOIN emails e ON e.id = u.email_id
  LEFT JOIN view_affiliations_primary_kle kpa ON kpa.affiliation_id = a.id
  LEFT JOIN view_affiliations_secondary_kle ksa ON ksa.affiliation_id = a.id
  WHERE p.deleted = 0                    
    AND (e.prime IS NULL OR e.prime = 1)   
    AND (ph.prime IS NULL OR ph.prime = 1) 
    AND u.user_type = 'ACTIVE_DIRECTORY'   
    AND a.deleted = 0 AND (a.stop_date IS NULL OR a.stop_date > CURRENT_TIMESTAMP);