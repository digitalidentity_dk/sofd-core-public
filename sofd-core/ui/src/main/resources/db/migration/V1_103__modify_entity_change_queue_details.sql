ALTER TABLE entity_change_queue_details ADD COLUMN old_value VARCHAR(255);
ALTER TABLE entity_change_queue_details ADD COLUMN new_value VARCHAR(255);