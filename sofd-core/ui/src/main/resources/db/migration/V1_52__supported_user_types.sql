CREATE TABLE supported_user_types (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  can_order              BOOLEAN DEFAULT FALSE,
  days_to_deactivate     BIGINT NOT NULL DEFAULT 0,
  days_to_delete         BIGINT NOT NULL DEFAULT 0,
  unique_key             VARCHAR(255) NOT NULL,
  name                   VARCHAR(255) NOT NULL
);

INSERT INTO supported_user_types (can_order, unique_key, name) VALUES (0, 'ACTIVE_DIRECTORY', 'Active Directory');
INSERT INTO supported_user_types (can_order, unique_key, name) VALUES (0, 'OPUS', 'Lønsystem');
INSERT INTO supported_user_types (can_order, unique_key, name) VALUES (0, 'UNILOGIN', 'UNI-Login');
INSERT INTO supported_user_types (can_order, unique_key, name) VALUES (0, 'EXCHANGE', 'Exchange');