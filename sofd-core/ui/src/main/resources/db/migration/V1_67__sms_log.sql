CREATE TABLE sms_log (
  id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  message             TEXT NOT NULL,
  timestamp           TIMESTAMP NOT NULL,
  user_id             VARCHAR(64) NOT NULL
);

CREATE TABLE sms_log_recipients (
  id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  sms_log_id          BIGINT NOT NULL,

  name                VARCHAR(255) NOT NULL,
  phone               VARCHAR(255) NOT NULL,
  type                VARCHAR(255) NOT NULL,

  FOREIGN KEY (sms_log_id) REFERENCES sms_log(id)
);
