-- add a prime field to the users table
ALTER TABLE users ADD COLUMN prime BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE users_aud ADD COLUMN prime BOOLEAN NULL;

-- add master/masterId to address fields
ALTER TABLE posts ADD COLUMN master VARCHAR(64) NOT NULL DEFAULT '';
ALTER TABLE posts ADD COLUMN master_id VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE posts_aud ADD COLUMN master VARCHAR(64);
ALTER TABLE posts_aud ADD COLUMN master_id VARCHAR(255);
ALTER TABLE emails ADD COLUMN master VARCHAR(64) NOT NULL DEFAULT '';
ALTER TABLE emails ADD COLUMN master_id VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE emails_aud ADD COLUMN master VARCHAR(64);
ALTER TABLE emails_aud ADD COLUMN master_id VARCHAR(255);
ALTER TABLE phones ADD COLUMN master VARCHAR(64) NOT NULL DEFAULT '';
ALTER TABLE phones ADD COLUMN master_id VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE phones_aud ADD COLUMN master VARCHAR(64);
ALTER TABLE phones_aud ADD COLUMN master_id VARCHAR(255);

CREATE TABLE ad_account_orders (
  id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  person_uuid                  VARCHAR(36) NOT NULL,
  requester_uuid               VARCHAR(36),
  requested_user_id            VARCHAR(128),
  requested_initial_password   VARCHAR(128),
  ordered_timestamp            DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status                       VARCHAR(64) NOT NULL,
  person_notified              BOOLEAN DEFAULT FALSE,
  requester_notified           BOOLEAN DEFAULT FALSE
);
