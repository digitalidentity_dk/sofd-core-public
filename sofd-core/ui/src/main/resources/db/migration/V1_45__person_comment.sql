CREATE TABLE comment (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  person_uuid            VARCHAR(36) NOT NULL,
  user_id                BIGINT NOT NULL,
  user_name              VARCHAR(255) NOT NULL,
  `timestamp`            TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  comment                TEXT NOT NULL
);