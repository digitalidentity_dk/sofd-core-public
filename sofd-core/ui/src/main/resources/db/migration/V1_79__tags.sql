CREATE TABLE tags (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  value                     VARCHAR(255) NOT NULL,
  description               TEXT NOT NULL,
  UNIQUE KEY unique_value (value)
);

CREATE TABLE tags_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  value                     VARCHAR(255),
  description               TEXT,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE orgunits_tags (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  orgunit_uuid              VARCHAR(36) NOT NULL,
  tag_id                    BIGINT NOT NULL,
 
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (tag_id) REFERENCES tags(id) ON DELETE CASCADE
);

CREATE TABLE orgunits_tags_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  orgunit_uuid              VARCHAR(36),
  tag_id                    BIGINT,
 
  FOREIGN KEY (rev) REFERENCES revisions(id)
);