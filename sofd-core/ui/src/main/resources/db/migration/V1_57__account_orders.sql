DROP TABLE ad_account_orders;

CREATE TABLE account_orders (
  id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  -- which person/affiliation is the account linked to
  person_uuid                  VARCHAR(36) NOT NULL,
  employee_id                  VARCHAR(255),

  -- ordered by whom and when
  requester_uuid               VARCHAR(36),
  requester_api_user_id        VARCHAR(255),
  ordered_timestamp            DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 
  -- details on the actual order
  user_type                    VARCHAR(64) NOT NULL,
  order_type                   VARCHAR(64) NOT NULL,
  activation_timestamp         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  end_date                     DATETIME,
  
  -- status on order
  status                       VARCHAR(64) NOT NULL,
  affected_user_id             VARCHAR(255),
  message                      TEXT,
  modified_timestamp           DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  person_notified              BOOLEAN DEFAULT FALSE,
  requester_notified           BOOLEAN DEFAULT FALSE
);
