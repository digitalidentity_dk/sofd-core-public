CREATE TABLE persons_children (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  cpr                       VARCHAR(10) NOT NULL,
  name                      VARCHAR(255) NOT NULL,
  person_uuid               VARCHAR(36) NOT NULL,

  FOREIGN KEY (person_uuid) REFERENCES persons(uuid)
);

CREATE TABLE persons_children_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  cpr                       VARCHAR(10) NULL,
  name                      VARCHAR(255) NULL,
  person_uuid               VARCHAR(36) NULL,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);