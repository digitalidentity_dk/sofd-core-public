ALTER TABLE tags ADD COLUMN custom_value_enabled bit NOT NULL DEFAULT 0;
ALTER TABLE tags ADD COLUMN custom_value_unique bit NOT NULL DEFAULT 0;
ALTER TABLE tags ADD COLUMN custom_value_name varchar(255) NULL;
ALTER TABLE tags ADD COLUMN custom_value_regex varchar(255) NULL;

ALTER TABLE tags_aud ADD COLUMN custom_value_enabled bit NULL;
ALTER TABLE tags_aud ADD COLUMN custom_value_unique bit NULL;
ALTER TABLE tags_aud ADD COLUMN custom_value_name varchar(255) NULL;
ALTER TABLE tags_aud ADD COLUMN custom_value_regex varchar(255) NULL;

ALTER TABLE orgunits_tags ADD COLUMN custom_value varchar(255) NULL;

ALTER TABLE orgunits_tags_aud ADD COLUMN custom_value varchar(255) NULL;
ALTER TABLE orgunits_tags_aud ADD COLUMN id bigint NULL;