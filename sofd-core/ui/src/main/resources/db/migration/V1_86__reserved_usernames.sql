CREATE TABLE reserved_usernames (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  person_uuid               VARCHAR(36) NOT NULL,
  employee_id               VARCHAR(255),
  user_type                 VARCHAR(255) NOT NULL,
  user_id                   VARCHAR(255) NOT NULL,
  tts                       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  
  FOREIGN KEY (person_uuid) REFERENCES persons(uuid)
);