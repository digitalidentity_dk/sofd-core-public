ALTER TABLE phones ADD COLUMN function_type VARCHAR(32) NOT NULL DEFAULT 'NONE' AFTER phone_type;
ALTER TABLE phones MODIFY phone_number VARCHAR(128);

ALTER TABLE phones_aud MODIFY phone_number VARCHAR(128);
ALTER TABLE phones_aud ADD COLUMN function_type VARCHAR(32) AFTER phone_type;

ALTER TABLE orgunits ADD COLUMN key_words TEXT;
ALTER TABLE orgunits ADD COLUMN opening_hours TEXT;

ALTER TABLE orgunits_aud ADD COLUMN key_words TEXT;
ALTER TABLE orgunits_aud ADD COLUMN opening_hours TEXT;