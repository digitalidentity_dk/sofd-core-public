SELECT CONCAT(
        'ALTER TABLE `users` DROP FOREIGN KEY `',
        constraint_name,
        '`'
) INTO @sqlst
        FROM information_schema.KEY_COLUMN_USAGE
        WHERE table_name = 'users'
                AND referenced_table_name = 'emails'
                AND referenced_column_name = 'id' LIMIT 1;

SELECT @sqlst;

PREPARE stmt FROM @sqlst;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @sqlst = NULL;

ALTER TABLE users DROP COLUMN email_id;
ALTER TABLE users_aud DROP COLUMN email_id;
