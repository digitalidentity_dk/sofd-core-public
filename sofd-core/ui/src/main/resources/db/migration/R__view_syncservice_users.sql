-- one-shots, to cleanup old views
DROP VIEW IF EXISTS view_affiliations_primary_kle;
DROP VIEW IF EXISTS view_affiliations_secondary_kle;
DROP VIEW IF EXISTS view_ad_users;

CREATE OR REPLACE VIEW subview_syncservice_person_prime_email AS
(
	SELECT p.uuid AS person_uuid, u.id, u.prime, u.user_id AS email, u.master, u.master_id
	  FROM persons p
	  JOIN persons_users pu ON pu.person_uuid = p.uuid
	  JOIN users u ON u.id = pu.user_id
	  WHERE u.user_type = 'EXCHANGE' AND u.prime = 1
);

CREATE OR REPLACE VIEW subview_syncservice_affiliations_primary_kle AS
  SELECT kp.affiliation_id, GROUP_CONCAT(kp.kle_value SEPARATOR ',') AS kle_values
    FROM affiliations_kle_primary kp
    GROUP BY kp.affiliation_id;

CREATE OR REPLACE VIEW subview_syncservice_affiliations_secondary_kle AS
  SELECT ks.affiliation_id, GROUP_CONCAT(ks.kle_value SEPARATOR ',') AS kle_values
    FROM affiliations_kle_secondary ks
    GROUP BY ks.affiliation_id;

CREATE OR REPLACE VIEW view_syncservice_users AS
  SELECT p.uuid,
    p.cpr,
    COALESCE(NULLIF(p.chosen_name, ''), CONCAT(p.firstname, ' ', p.surname)) AS name,
    u.user_id,
    u.prime,
    e.email,
    ph.phone_number,
    a.position_name,
    a.orgunit_uuid,
    a.inherit_privileges,
    kpa.kle_values AS kle_primary_values,
    ksa.kle_values AS kle_secondary_values
  FROM persons p
  JOIN persons_users pu ON pu.person_uuid = p.uuid
  JOIN users u ON u.id = pu.user_id
  LEFT JOIN affiliations a ON a.person_uuid = p.uuid AND (u.employee_id = a.employee_id OR u.employee_id IS NULL)
  LEFT JOIN orgunits o ON o.uuid = a.orgunit_uuid
  LEFT JOIN persons_phones pp ON pp.person_uuid = p.uuid
  LEFT JOIN phones ph ON ph.id = pp.phone_id
  LEFT JOIN subview_syncservice_person_prime_email e ON e.person_uuid = p.uuid
  LEFT JOIN subview_syncservice_affiliations_primary_kle kpa ON kpa.affiliation_id = a.id
  LEFT JOIN subview_syncservice_affiliations_secondary_kle ksa ON ksa.affiliation_id = a.id
  INNER JOIN view_adm_organisation vao ON vao.id = o.belongs_to
  WHERE p.deleted = 0
    AND p.force_stop = 0
    AND (ph.prime IS NULL OR ph.prime = 1)
    AND u.user_type = 'ACTIVE_DIRECTORY'
    AND a.deleted = 0
    AND o.deleted = 0
    AND u.disabled = 0
    AND (a.stop_date IS NULL OR CAST(a.stop_date AS DATE) >= CAST(CURRENT_TIMESTAMP AS DATE));
