ALTER TABLE account_orders ADD COLUMN requested_user_id VARCHAR(255);
ALTER TABLE account_orders ADD COLUMN linked_user_id VARCHAR(255);
ALTER TABLE account_orders ADD COLUMN actual_user_id VARCHAR(255);

-- migrate existing orders
UPDATE account_orders SET actual_user_id = affected_user_id;

ALTER TABLE account_orders DROP COLUMN affected_user_id;
