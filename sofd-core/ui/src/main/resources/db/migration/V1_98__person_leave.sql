-- remove old force-stop on affiliations
ALTER TABLE affiliations DROP COLUMN force_stop_date;
ALTER TABLE affiliations_aud DROP COLUMN force_stop_date;

-- create new force-stop on persons
ALTER TABLE persons ADD COLUMN force_stop BOOLEAN NOT NULL DEFAULT 0;
ALTER TABLE persons_aud ADD COLUMN force_stop BOOLEAN NULL;

CREATE TABLE persons_leave (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  start_date                DATE NOT NULL,
  stop_date                 DATE NULL,
  reason                    VARCHAR(64) NOT NULL,
  reason_text               TEXT NULL,
  disable_account_orders    BOOLEAN NOT NULL DEFAULT 0,
  deactivate_accounts       BOOLEAN NOT NULL DEFAULT 0,
  accounts_to_deactivate    TEXT
);

CREATE TABLE persons_leave_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  start_date                DATE NULL,
  stop_date                 DATE NULL,
  reason                    VARCHAR(64) NULL,
  reason_text               TEXT NULL,
  disable_account_orders    BOOLEAN NULL,
  deactivate_accounts       BOOLEAN NULL,
  accounts_to_deactivate    TEXT,
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

ALTER TABLE persons
  ADD COLUMN leave_id BIGINT NULL,
  ADD CONSTRAINT fk_persons_leave FOREIGN KEY (leave_id) REFERENCES persons_leave(id);

ALTER TABLE persons_aud ADD COLUMN leave_id BIGINT;
