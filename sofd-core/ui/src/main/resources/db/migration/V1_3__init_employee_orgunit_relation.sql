CREATE TABLE affiliations (
  id                             BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  uuid                           VARCHAR(36) NOT NULL,
  master                         VARCHAR(64) NOT NULL,
  master_id                      VARCHAR(255) NOT NULL,
  start_date                     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  stop_date					     TIMESTAMP NULL,
  deleted                        BOOLEAN DEFAULT FALSE,

  orgunit_uuid                   VARCHAR(36) NOT NULL,
  person_uuid                    VARCHAR(36) NOT NULL,

  employee_id                    VARCHAR(255),
  employment_terms               VARCHAR(255),
  employment_terms_text          VARCHAR(255),
  pay_grade                      VARCHAR(255),
  
  working_hours_denominator      DOUBLE(5,3),
  working_hours_numerator        DOUBLE(5,3),
  affiliation_type               VARCHAR(64) NOT NULL,

  local_extensions               TEXT NULL,

  position_id                    VARCHAR(255),
  position_name                  VARCHAR(255) NOT NULL,

  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE
);

CREATE TABLE affiliations_function (
  affiliation_id                 BIGINT NOT NULL,
  function                       VARCHAR(255) NOT NULL,

  FOREIGN KEY (affiliation_id) REFERENCES affiliations(id) ON DELETE CASCADE
);

CREATE TABLE affiliations_manager (
  orgunit_uuid                   VARCHAR(36) NOT NULL,
  affiliation_id                 BIGINT NOT NULL,

  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE,
  FOREIGN KEY (affiliation_id) REFERENCES affiliations(id) ON DELETE CASCADE
);

CREATE TABLE affiliations_kle_primary (
  affiliation_id            BIGINT NOT NULL,
  kle_value                 VARCHAR(8) NOT NULL,
  
  FOREIGN KEY (affiliation_id) REFERENCES affiliations(id) ON DELETE CASCADE
);

CREATE TABLE affiliations_kle_secondary (
  affiliation_id            BIGINT NOT NULL,
  kle_value                 VARCHAR(8) NOT NULL,
  
  FOREIGN KEY (affiliation_id) REFERENCES affiliations(id) ON DELETE CASCADE
);
