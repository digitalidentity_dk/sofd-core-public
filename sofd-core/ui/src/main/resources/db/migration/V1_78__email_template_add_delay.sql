ALTER TABLE email_templates ADD COLUMN minutes_delay BIGINT NOT NULL DEFAULT 0;
ALTER TABLE email_templates MODIFY COLUMN template_type VARCHAR(64) NOT NULL;

CREATE TABLE email_queue (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title                     VARCHAR(255) NOT NULL,
  message                   TEXT NOT NULL,
  cpr                       VARCHAR(10),
  email                     VARCHAR(255),
  delivery_tts              TIMESTAMP NOT NULL
);