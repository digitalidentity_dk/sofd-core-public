ALTER TABLE affiliations_aud MODIFY COLUMN stop_date DATETIME NULL;
ALTER TABLE affiliations_aud MODIFY COLUMN start_date DATETIME NULL;
