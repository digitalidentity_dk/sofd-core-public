ALTER TABLE affiliations_manager ADD COLUMN affiliation_uuid VARCHAR(36);
UPDATE affiliations_manager am SET affiliation_uuid = (SELECT uuid FROM affiliations WHERE id = am.affiliation_id);

SELECT CONSTRAINT_NAME INTO @fkc
  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
  WHERE TABLE_NAME = 'affiliations_manager' AND REFERENCED_TABLE_NAME = 'affiliations';

SELECT CONCAT(
	'ALTER TABLE `affiliations_manager` DROP FOREIGN KEY `',
	constraint_name,
	'`'
) INTO @sqlst
	FROM information_schema.KEY_COLUMN_USAGE
	WHERE table_name = 'affiliations_manager'
	AND referenced_table_name='affiliations' LIMIT 1;

PREPARE stmt FROM @sqlst;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @sqlst = NULL;
  
ALTER TABLE affiliations_manager DROP COLUMN affiliation_id;
ALTER TABLE affiliations_manager MODIFY COLUMN affiliation_uuid VARCHAR(36) NOT NULL;
