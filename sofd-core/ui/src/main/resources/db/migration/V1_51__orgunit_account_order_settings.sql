CREATE TABLE orgunit_account_order (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  orgunit_uuid              VARCHAR(36) NOT NULL,
  
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunit_account_order_type (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  account_order_id          BIGINT NOT NULL,
  user_type                 VARCHAR(255) NOT NULL,
  rule                      VARCHAR(64) NOT NULL,
  
  FOREIGN KEY (account_order_id) REFERENCES orgunit_account_order(id) ON DELETE CASCADE
);

CREATE TABLE orgunit_account_order_type_position (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  account_order_type_id     BIGINT NOT NULL,
  position_name             VARCHAR(255) NOT NULL,
  rule                      VARCHAR(64) NOT NULL,
  
  FOREIGN KEY (account_order_type_id) REFERENCES orgunit_account_order_type(id) ON DELETE CASCADE
);
