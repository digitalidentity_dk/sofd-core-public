CREATE VIEW persons_grid AS SELECT p.uuid, CONCAT(p.firstname, ' ', p.surname) AS name, CONCAT(a.position_name, ' i ', o.name) AS affiliation, ph.phone_number
  FROM persons p
  LEFT JOIN persons_phones pp ON p.uuid = pp.person_uuid
  LEFT JOIN phones ph ON pp.phone_id = ph.id
  LEFT JOIN affiliations a ON p.uuid = a.person_uuid
  LEFT JOIN orgunits o ON o.uuid = a.orgunit_uuid
  WHERE p.deleted = 0
  AND (ph.prime IS NULL OR ph.prime = 1)
  AND (a.prime IS NULL OR (a.prime = 1 AND a.deleted = 0 AND (a.stop_date IS NULL OR a.stop_date > CURRENT_TIMESTAMP)));
