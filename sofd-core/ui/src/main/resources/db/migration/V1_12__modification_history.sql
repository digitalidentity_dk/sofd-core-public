CREATE TABLE modification_history (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  changed                   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  uuid                      VARCHAR(36) NOT NULL,
  entity                    VARCHAR(64) NOT NULL,
  change_type               VARCHAR(36) NOT NULL
);

CREATE TABLE settings (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  setting_key               VARCHAR(64) NOT NULL,
  setting_value             TEXT NOT NULL
);
