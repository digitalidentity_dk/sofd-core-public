CREATE TABLE future_emails (
	id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	all_or_none				  BOOLEAN NOT NULL,
	delivery_tts		      TIMESTAMP NOT NULL,
	eboks					  BOOLEAN NOT NULL,
	title 					  VARCHAR(255) NOT NULL,
	message                   MEDIUMTEXT NOT NULL
);

CREATE TABLE future_emails_persons (
	id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	person_uuid 			  VARCHAR(36) NOT NULL,
	future_email_id			  BIGINT NOT NULL,
	
	FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE,
    FOREIGN KEY (future_email_id) REFERENCES future_emails(id) ON DELETE CASCADE	
);