ALTER TABLE affiliations ADD COLUMN selected_prime BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE affiliations_aud ADD COLUMN selected_prime BOOLEAN NULL;