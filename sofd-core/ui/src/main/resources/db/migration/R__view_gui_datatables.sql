DROP VIEW IF EXISTS persons_grid;
DROP VIEW IF EXISTS persons_deleted_grid;

CREATE OR REPLACE VIEW subview_datatables_affiliations AS SELECT
     position_name,
     person_uuid,
     orgunit_uuid
   FROM affiliations
   WHERE prime = 1;

CREATE OR REPLACE VIEW view_datatables_persons AS SELECT
    p.uuid,
    COALESCE(p.chosen_name, CONCAT(p.firstname, ' ', p.surname)) AS name,
    CONCAT(a.position_name, ' i ', o.name) AS affiliation,
    phs.phone_number,
    IF (leave_id IS NOT NULL, 1, 0) AS `leave`,
    p.force_stop,
    p.disable_account_orders,
	usrs.user_id
  FROM persons p
  LEFT JOIN (
    SELECT pu.person_uuid, u.user_id
      FROM persons_users pu
      INNER JOIN users u ON u.id = pu.user_id AND u.prime = 1 AND u.user_type = 'ACTIVE_DIRECTORY'
    ) usrs ON usrs.person_uuid = p.uuid
  LEFT JOIN (
    SELECT pp.person_uuid, p.phone_number
      FROM persons_phones pp
      INNER JOIN phones p ON p.id = pp.phone_id AND p.prime = 1
    ) phs ON phs.person_uuid = p.uuid
  LEFT JOIN subview_datatables_affiliations a ON p.uuid = a.person_uuid
  LEFT JOIN orgunits o ON o.uuid = a.orgunit_uuid
  WHERE p.deleted = 0;

CREATE OR REPLACE VIEW view_datatables_persons_deleted AS SELECT
    p.uuid,
    CONCAT(p.firstname, ' ', p.surname) AS name,
    NULL AS affiliation,
    NULL AS phone_number,
    0 AS `leave`,
    0 AS force_stop,
    0 AS disable_account_orders,
    NULL AS user_id
  FROM persons p
  WHERE p.deleted = 1;
