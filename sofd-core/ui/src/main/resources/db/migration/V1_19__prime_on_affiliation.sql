-- add a prime field to the affilations table
ALTER TABLE affiliations ADD COLUMN prime BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE affiliations_aud ADD COLUMN prime BOOLEAN NULL;
