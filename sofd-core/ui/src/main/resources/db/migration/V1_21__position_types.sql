ALTER TABLE affiliations ADD COLUMN position_type_id VARCHAR(255);
ALTER TABLE affiliations ADD COLUMN position_type_name VARCHAR(255);

ALTER TABLE affiliations_aud ADD COLUMN position_type_id VARCHAR(255);
ALTER TABLE affiliations_aud ADD COLUMN position_type_name VARCHAR(255);
