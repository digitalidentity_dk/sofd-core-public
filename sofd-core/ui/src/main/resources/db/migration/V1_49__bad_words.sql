CREATE TABLE bad_words (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  value                  VARCHAR(255) NOT NULL
);