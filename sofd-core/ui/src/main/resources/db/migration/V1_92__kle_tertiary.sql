CREATE TABLE orgunits_kle_tertiary (
  orgunit_uuid                        VARCHAR(36) NOT NULL,
  kle_value                           VARCHAR(8) NOT NULL,
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);

CREATE TABLE orgunits_kle_tertiary_aud (
  rev                                 BIGINT NOT NULL,
  revtype                             TINYINT,

  orgunit_uuid                        VARCHAR(36),
  kle_value                           VARCHAR(8),
  FOREIGN KEY (rev) REFERENCES revisions(id)
);