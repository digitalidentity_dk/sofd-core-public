CREATE TABLE revisions (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  timestamp                 BIGINT NOT NULL,
  auditor_id                VARCHAR(128),
  auditor_name              VARCHAR(128)
);
