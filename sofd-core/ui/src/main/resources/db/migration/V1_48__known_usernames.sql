CREATE TABLE known_usernames (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  user_type              VARCHAR(255) NOT NULL,
  username               VARCHAR(255) NOT NULL,
  INDEX known_username_idx (user_type),
  UNIQUE KEY known_usernames_UNIQUE (user_type, username)
);