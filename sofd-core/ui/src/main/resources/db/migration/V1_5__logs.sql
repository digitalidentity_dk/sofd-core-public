CREATE TABLE audit_log (
  id                      BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  timestamp               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id                 VARCHAR(128) NOT NULL,
  username                VARCHAR(128) NOT NULL,
  entity_type             VARCHAR(64) NOT NULL,   -- Client, User, OrgUnit
  entity_id               VARCHAR(64) NOT NULL,   -- UUID/ID
  event_type              VARCHAR(64) NOT NULL    -- CREATE, DELETE, UPDATE, etc...
);

CREATE TABLE security_log (
  id                      BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  timestamp               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  client_id               VARCHAR(128) NOT NULL,
  clientname              VARCHAR(128) NOT NULL,
  method                  VARCHAR(32) NOT NULL,
  request                 TEXT NOT NULL,
  ip_address              VARCHAR(32) NOT NULL
);