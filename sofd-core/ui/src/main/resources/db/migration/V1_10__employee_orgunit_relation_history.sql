CREATE TABLE affiliations_aud (
  id                             BIGINT NOT NULL,
  uuid                           VARCHAR(36),
  master                         VARCHAR(64),
  master_id                      VARCHAR(255),
  rev                            BIGINT NOT NULL,
  revtype                        TINYINT,

  start_date                     TIMESTAMP NULL,
  stop_date					     TIMESTAMP NULL,
  deleted                        BOOLEAN NULL,

  orgunit_uuid                   VARCHAR(36),
  person_uuid                    VARCHAR(36),

  employee_id                    VARCHAR(255),
  employment_terms               VARCHAR(255),
  employment_terms_text          VARCHAR(255),
  pay_grade                      VARCHAR(255),
  
  working_hours_denominator      DOUBLE(5,3),
  working_hours_numerator        DOUBLE(5,3),
  affiliation_type               VARCHAR(64),

  local_extensions               TEXT NULL,
  
  position_id                    VARCHAR(255),
  position_name                  VARCHAR(255),

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

CREATE TABLE affiliations_function_aud (
  rev                            BIGINT NOT NULL,
  revtype                        TINYINT,
  
  affiliation_id                 BIGINT,
  function                       VARCHAR(255),

  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE affiliations_manager_aud (
  rev                            BIGINT NOT NULL,
  revtype                        TINYINT,
  
  orgunit_uuid                   VARCHAR(36),
  affiliation_id                 BIGINT,

  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE affiliations_kle_primary_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  affiliation_id            BIGINT,
  kle_value                 VARCHAR(8),
  
  FOREIGN KEY (rev) REFERENCES revisions(id)
);

CREATE TABLE affiliations_kle_secondary_aud (
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,
  
  affiliation_id            BIGINT,
  kle_value                 VARCHAR(8),
  
  FOREIGN KEY (rev) REFERENCES revisions(id)
);
