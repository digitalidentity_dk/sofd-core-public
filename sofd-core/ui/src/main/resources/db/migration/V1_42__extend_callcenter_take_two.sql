ALTER TABLE persons      ADD COLUMN notes TEXT;
ALTER TABLE persons_aud  ADD COLUMN notes TEXT;

ALTER TABLE orgunits      ADD COLUMN notes TEXT;
ALTER TABLE orgunits_aud  ADD COLUMN notes TEXT;

DROP VIEW view_callcenter;
DROP VIEW subview_callcenter_orgunits;
DROP VIEW subview_callcenter_employees;

CREATE VIEW subview_callcenter_orgunits AS
(
	SELECT DISTINCT
		'OrgUnit'			AS 'type',
		ou.uuid             AS 'uuid',
		ou.name 			AS 'name',
		ou.name 			AS 'org_unit',
		NULL 				AS 'user_id',
		ou.key_words		AS 'keywords',
        ou.opening_hours	AS 'opening_hours',
		pop.phone_number	AS 'phone',
		oupn.phone_numbers  AS 'phone_numbers',
		oupe.email			AS 'email',
		oupp.address        AS 'address',
		NULL				AS 'position_name',
		ou.notes            AS 'notes',
		IFNULL(mp.chosen_name,CONCAT(mp.firstname,' ',mp.surname)) AS 'managerName',
		mppp.phone_number   AS 'managerPhone',
		pop.phone_number	AS 'orgUnitPhone'
	FROM
		orgunits ou
        LEFT JOIN subview_callcenter_prime_orgunit_phone pop ON pop.orgunit_uuid = ou.uuid
        LEFT JOIN subview_callcenter_orgunit_prime_email oupe ON oupe.orgunit_uuid = ou.uuid
        LEFT JOIN subview_callcenter_orgunit_prime_post oupp ON oupp.orgunit_uuid = ou.uuid
        LEFT JOIN subview_callcenter_orgunit_phone_numbers oupn ON oupn.orgunit_uuid = ou.uuid
        LEFT JOIN orgunits_manager ouman ON ouman.orgunit_uuid = ou.uuid
        LEFT JOIN persons mp ON mp.uuid = ouman.manager_uuid
        LEFT JOIN subview_callcenter_prime_person_phone mppp ON mppp.person_uuid = mp.uuid
	WHERE ou.deleted = 0
);

CREATE VIEW subview_callcenter_employees AS
(
	SELECT DISTINCT
		'Employee'				AS 'type',
		af.uuid                 AS 'uuid',
		IFNULL(p.chosen_name,CONCAT(p.firstname,' ',p.surname)) AS 'name',
		ou.name 				AS 'org_unit',
		padu.user_id			AS 'user_id',
		p.key_words				AS 'keywords',
        ou.opening_hours		AS 'opening_hours',
		ppp.phone_number		AS 'phone',
		ppn.phone_numbers       AS 'phone_numbers',
		ppe.email				AS 'email',
		oupp.address			AS 'address',
		af.position_name		AS 'position_name',
		p.notes                 AS 'notes',
		IFNULL(mp.chosen_name,CONCAT(mp.firstname,' ',mp.surname)) AS 'managerName',
		mppp.phone_number		AS 'managerPhone',
		pop.phone_number		AS 'orgUnitPhone'
	FROM
		persons p
		INNER JOIN affiliations af ON
			af.person_uuid = p.uuid
			AND af.deleted = 0
			AND (af.start_date IS NULL OR af.start_date <= NOW())
			AND (af.stop_date IS NULL OR af.stop_date >= NOW())
		INNER JOIN orgunits ou ON ou.uuid = af.orgunit_uuid and ou.deleted = 0
        LEFT JOIN subview_callcenter_prime_orgunit_phone pop ON pop.orgunit_uuid = ou.uuid
		LEFT JOIN subview_callcenter_orgunit_prime_post oupp ON oupp.orgunit_uuid = ou.uuid
		LEFT JOIN subview_callcenter_prime_ad_user padu ON padu.person_uuid = p.uuid
		LEFT JOIN subview_callcenter_prime_person_phone ppp ON ppp.person_uuid = p.uuid
		LEFT JOIN subview_callcenter_person_prime_email ppe ON ppe.person_uuid = p.uuid
		LEFT JOIN subview_callcenter_person_phone_numbers ppn ON ppn.person_uuid = p.uuid
		LEFT JOIN orgunits_manager ouman ON ouman.orgunit_uuid = ou.uuid
        LEFT JOIN persons mp ON mp.uuid = ouman.manager_uuid
        LEFT JOIN subview_callcenter_prime_person_phone mppp ON mppp.person_uuid = mp.uuid
);

CREATE VIEW view_callcenter AS
	SELECT * FROM subview_callcenter_orgunits
    UNION ALL
    SELECT * FROM subview_callcenter_employees;
    