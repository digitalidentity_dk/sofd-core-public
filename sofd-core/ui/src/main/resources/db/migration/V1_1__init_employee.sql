CREATE TABLE users (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  uuid                      VARCHAR(36) NOT NULL,
  master                    VARCHAR(64) NOT NULL,
  master_id                 VARCHAR(255) NOT NULL,
  
  user_id                   VARCHAR(64) NOT NULL,
  local_extensions          TEXT NULL,
  email_id                  BIGINT,
  user_type					VARCHAR(36) NOT NULL,

  FOREIGN KEY (email_id) REFERENCES emails(id)
);

CREATE TABLE persons (
  uuid                            VARCHAR(36) NOT NULL PRIMARY KEY,
  master                          VARCHAR(64) NOT NULL,
  deleted                         BOOLEAN DEFAULT FALSE,
  created                         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_changed                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

  firstname                       VARCHAR(255) NOT NULL,
  surname                         VARCHAR(255) NOT NULL,
  cpr                             VARCHAR(10) NOT NULL,
  chosen_name                     VARCHAR(255),
  first_employment_date           DATE NULL,
  anniversary_date                DATE NULL,
  local_extensions                TEXT NULL,
  registered_post_address_id      BIGINT,
  residence_post_address_id       BIGINT,

  UNIQUE(cpr),
  FOREIGN KEY (registered_post_address_id) REFERENCES posts(id),
  FOREIGN KEY (residence_post_address_id) REFERENCES posts(id)
);

CREATE TABLE persons_phones (
  person_uuid               VARCHAR(36) NOT NULL,
  phone_id                  BIGINT NOT NULL,

  FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE,
  FOREIGN KEY (phone_id) REFERENCES phones(id) ON DELETE CASCADE
);

CREATE TABLE persons_users (
  person_uuid               VARCHAR(36) NOT NULL,
  user_id                   BIGINT NOT NULL,

  FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);