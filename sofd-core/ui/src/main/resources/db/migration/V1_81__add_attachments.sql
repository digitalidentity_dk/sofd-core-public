CREATE TABLE email_templates_attachment_file (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  content                   MEDIUMBLOB NOT NULL
);

CREATE TABLE email_templates_attachment (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  filename                  VARCHAR(255) NOT NULL,
  email_template_id         BIGINT NOT NULL,
  file_id                   BIGINT NOT NULL,
  
  FOREIGN KEY (email_template_id) REFERENCES email_templates(id),
  FOREIGN KEY (file_id) REFERENCES email_templates_attachment_file(id)
);

ALTER TABLE email_queue ADD COLUMN email_template_id BIGINT REFERENCES email_templates(id);