CREATE TABLE phones (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  prime                     BOOLEAN NOT NULL DEFAULT FALSE,
  phone_number              VARCHAR(32) NOT NULL,
  phone_type                VARCHAR(32) NOT NULL
);

CREATE TABLE posts (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  prime                     BOOLEAN NOT NULL DEFAULT FALSE,
  street                    VARCHAR(255) NOT NULL,
  localname                 VARCHAR(255),
  postal_code               VARCHAR(8) NOT NULL,
  city                      VARCHAR(255) NOT NULL,
  country                   VARCHAR(255) NOT NULL,
  address_protected         BOOLEAN NOT NULL
);

CREATE TABLE emails (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  prime                     BOOLEAN NOT NULL DEFAULT FALSE,
  email                     VARCHAR(255) NOT NULL
);