ALTER TABLE affiliations     ADD COLUMN inherit_privileges BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE affiliations     ADD COLUMN stopped BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE affiliations_aud ADD COLUMN inherit_privileges BOOLEAN;
ALTER TABLE affiliations_aud ADD COLUMN stopped BOOLEAN;
