UPDATE supported_user_types SET username_prefix = 'NONE' WHERE username_prefix IS NULL;
UPDATE supported_user_types SET username_infix = 'RANDOM' WHERE username_infix IS NULL;
UPDATE supported_user_types SET username_suffix = 'NONE' WHERE username_suffix IS NULL;

ALTER TABLE supported_user_types
    MODIFY username_prefix varchar(64) NOT NULL DEFAULT 'NONE',
    MODIFY username_infix varchar(64) NOT NULL DEFAULT 'RANDOM',
    MODIFY username_suffix varchar(64) NOT NULL DEFAULT 'NONE';