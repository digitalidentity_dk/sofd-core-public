ALTER TABLE persons ADD COLUMN time_track BIGINT;
ALTER TABLE orgunits ADD COLUMN time_track BIGINT;
UPDATE persons SET time_track = 0;
UPDATE orgunits SET time_track = 0;
ALTER TABLE persons_aud ADD COLUMN time_track BIGINT;
ALTER TABLE orgunits_aud ADD COLUMN time_track BIGINT;
