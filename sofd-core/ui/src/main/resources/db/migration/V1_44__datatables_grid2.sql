CREATE VIEW persons_deleted_grid AS SELECT p.uuid, CONCAT(p.firstname, ' ', p.surname) AS name, '' AS affiliation, '' AS phone_number
  FROM persons p
  WHERE p.deleted = 1;
