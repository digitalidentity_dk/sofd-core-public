UPDATE supported_user_types SET can_order = 0;

ALTER TABLE supported_user_types ADD COLUMN username_prefix VARCHAR(64) DEFAULT 'NONE';
ALTER TABLE supported_user_types ADD COLUMN username_prefix_value VARCHAR(64) DEFAULT '';
ALTER TABLE supported_user_types ADD COLUMN username_infix VARCHAR(64) DEFAULT 'RANDOM';
ALTER TABLE supported_user_types ADD COLUMN username_infix_value VARCHAR(64) DEFAULT '5';
ALTER TABLE supported_user_types ADD COLUMN username_suffix VARCHAR(64) DEFAULT 'NONE';
ALTER TABLE supported_user_types ADD COLUMN username_suffix_value VARCHAR(64) DEFAULT '';
