CREATE TABLE orgunit_types (
  id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  type_key               VARCHAR(64) NOT NULL,
  type_value             VARCHAR(255) NOT NULL,  
  active                 BOOLEAN DEFAULT TRUE
);

CREATE TABLE orgunit_types_aud (
  id                     BIGINT NOT NULL,
  rev                    BIGINT NOT NULL,
  revtype                TINYINT,

  type_key               VARCHAR(64) NULL,
  type_value             VARCHAR(255) NULL,  
  active                 BOOLEAN NULL,
  
  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);

INSERT INTO orgunit_types (id, type_key, type_value, active) VALUES (1, 'AFDELING', 'Afdeling', 1);
INSERT INTO orgunit_types (id, type_key, type_value, active) VALUES (2, 'TEAM', 'Team', 1);

ALTER TABLE orgunits     ADD COLUMN orgunit_type_id BIGINT NULL REFERENCES orgunit_types(id);
ALTER TABLE orgunits_aud ADD COLUMN orgunit_type_id BIGINT NULL;

UPDATE orgunits SET orgunit_type_id = 1;