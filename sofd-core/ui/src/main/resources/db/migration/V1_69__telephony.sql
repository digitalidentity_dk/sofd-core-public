CREATE TABLE telephony_phones (
  id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,

  master                    VARCHAR(64) NOT NULL,
  master_id                 VARCHAR(255) NOT NULL,
  phone_number              VARCHAR(128),
  vendor                    VARCHAR(128),
  account_number            VARCHAR(128),
  ean                       BIGINT,
  phone_type                VARCHAR(32) NOT NULL,
  visibility                VARCHAR(255) NOT NULL,
  function_type_id          BIGINT,
  person_uuid               VARCHAR(36),
  person_name               VARCHAR(255),
  orgunit_uuid              VARCHAR(36),
  orgunit_name              VARCHAR(255),
  last_changed              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  
  FOREIGN KEY (function_type_id) REFERENCES function_types(id) ON DELETE SET NULL,
  FOREIGN KEY (person_uuid) REFERENCES persons(uuid) ON DELETE SET NULL,
  FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE SET NULL
);

CREATE TABLE telephony_phones_aud (
  id                        BIGINT NOT NULL,
  rev                       BIGINT NOT NULL,
  revtype                   TINYINT,

  master                    VARCHAR(64),
  master_id                 VARCHAR(255),
  phone_number              VARCHAR(128),
  vendor                    VARCHAR(128),
  account_number            VARCHAR(128),
  ean                       BIGINT,
  phone_type                VARCHAR(32),
  visibility                VARCHAR(255),
  function_type_id          BIGINT,
  person_uuid               VARCHAR(36),
  person_name               VARCHAR(255),
  orgunit_uuid              VARCHAR(36),
  orgunit_name              VARCHAR(255),
  last_changed              TIMESTAMP NULL,

  FOREIGN KEY (rev) REFERENCES revisions(id),
  PRIMARY KEY (id, rev)
);