CREATE TABLE entity_change_queue (
	id                        BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	entity_type				  VARCHAR(64) NOT NULL,
	entity_uuid       		  VARCHAR(36) NOT NULL,
	change_type	              VARCHAR(64) NOT NULL,
	fields					  VARCHAR(255) NOT NULL,	
	tts						  TIMESTAMP NOT NULL
);