CREATE TABLE orgunit_changes (
	id                     BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	orgunit_uuid 		   VARCHAR(36) NOT NULL,
	changed_timestamp      DATETIME,
	sent_timestamp         DATETIME,
	status 				   VARCHAR(64) NOT NULL,
	change_type            VARCHAR(64) NOT NULL,
	old_value              VARCHAR(255),
	new_value              VARCHAR(255),

    FOREIGN KEY (orgunit_uuid) REFERENCES orgunits(uuid) ON DELETE CASCADE
);