ALTER TABLE users     ADD COLUMN password_expire_date DATE NOT NULL DEFAULT '9999-12-31';
ALTER TABLE users_aud ADD COLUMN password_expire_date DATE NULL;