package dk.digitalidentity.sofd.api.projectionTests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.projection.ExtendedAffiliation;
import dk.digitalidentity.sofd.dao.model.projection.ExtendedOrgUnit;
import dk.digitalidentity.sofd.dao.model.projection.ExtendedPerson;
import dk.digitalidentity.sofd.util.ProjectionAnalyzer;

@RunWith(SpringRunner.class)
public class ProjectionTest {

	@Test
	public void testOrgUnit() {
		assertTrue(ProjectionAnalyzer.verifyProjectionExtendsEntity(OrgUnit.class, ExtendedOrgUnit.class));
	}

	@Test
	public void testPerson() {
		assertTrue(ProjectionAnalyzer.verifyProjectionExtendsEntity(Person.class, ExtendedPerson.class));
	}

	@Test
	public void testAffiliation() {
		assertTrue(ProjectionAnalyzer.verifyProjectionExtendsEntity(Affiliation.class, ExtendedAffiliation.class));
	}
}
