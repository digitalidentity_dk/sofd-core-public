package dk.digitalidentity.sofd;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sofd.dao.ClientDao;
import dk.digitalidentity.sofd.dao.OrgUnitDao;
import dk.digitalidentity.sofd.dao.PersonDao;
import dk.digitalidentity.sofd.dao.model.Affiliation;
import dk.digitalidentity.sofd.dao.model.Client;
import dk.digitalidentity.sofd.dao.model.Email;
import dk.digitalidentity.sofd.dao.model.OrgUnit;
import dk.digitalidentity.sofd.dao.model.Person;
import dk.digitalidentity.sofd.dao.model.Phone;
import dk.digitalidentity.sofd.dao.model.Post;
import dk.digitalidentity.sofd.dao.model.User;
import dk.digitalidentity.sofd.dao.model.enums.AccessRole;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationFunction;
import dk.digitalidentity.sofd.dao.model.enums.AffiliationType;
import dk.digitalidentity.sofd.dao.model.enums.PhoneType;
import dk.digitalidentity.sofd.security.SecurityUtil;
import lombok.Getter;

@Component
@Getter
public class DataGenerator {

	@Autowired
	private DataGenerator generator;

	@Autowired
	private ClientDao clientDao;

	@Autowired
	private PersonDao personDao;

	@Autowired
	private OrgUnitDao orgUnitDao;

	private String apiKey = "TestApiKey";
	private String ouKommuneUuid = UUID.randomUUID().toString();
	private String ouHRUuid = UUID.randomUUID().toString();
	private String ouSundhedOgOmsorgUuid = UUID.randomUUID().toString();
	private String ouOmsorgUuid = UUID.randomUUID().toString();
	private String ouSundhedUuid = UUID.randomUUID().toString();
	private String user1Uuid = UUID.randomUUID().toString();
	private String user2Uuid = UUID.randomUUID().toString();
	private String user3Uuid = UUID.randomUUID().toString();
	private String user4Uuid = UUID.randomUUID().toString();
	private String user5Uuid = UUID.randomUUID().toString();
	private String user6Uuid = UUID.randomUUID().toString();
	private String user7Uuid = UUID.randomUUID().toString();
	private String user8Uuid = UUID.randomUUID().toString();
	private String user9Uuid = UUID.randomUUID().toString();
	private String user10Uuid = UUID.randomUUID().toString();

	// TODO: update with master/masterId where needed

	public void initData() {
		
		// before each test, we wipe all the data, and recreate, so we know the exact state between each test
		clientDao.deleteAll();
		personDao.deleteAll();
		orgUnitDao.deleteAll();

		// create client
		Client client = new Client();
		client.setApiKey(apiKey);
		client.setName("testclient");
		client.setAccessRole(AccessRole.WRITE_ACCESS);
		clientDao.save(client);

		SecurityUtil.fakeLoginSession();

		// Create 5 orgUnits with this structure
		//
		//  kommune
		//    |
		//    |-- HR
		//    |
		//    |-- Sundhed og Omsorg
		//          |
		//          |-- Sundhed
		//          |
		//          |-- Omsorg
		//
		List<OrgUnit> units = new ArrayList<>();
		OrgUnit kommune = new OrgUnit();
		kommune.setCvr(12345678L);
		kommune.setEan(12345678900L);
		kommune.getEmails().add(Email.builder().email("email@email.dk").master("TEST").masterId("TEST").prime(true).build());
		kommune.getEmails().add(Email.builder().email("email2@email.dk").master("TEST").masterId("TEST").prime(false).build());
		kommune.getPhones().add(Phone.builder().phoneNumber("12341234").master("TEST").masterId("TEST").prime(true).phoneType(PhoneType.LANDLINE).build());
		kommune.getPhones().add(Phone.builder().phoneNumber("87878787").master("TEST").masterId("TEST").prime(false).phoneType(PhoneType.MOBILE).build());
		kommune.getKlePrimary().add("27.00.00");
		kommune.getKlePrimary().add("24");
		kommune.getKleSecondary().add("01.00");
		kommune.setName("Kommune");
		kommune.setShortname("KOMMUNE");
		kommune.setUuid(ouKommuneUuid);
		kommune.setOrgType("OrgTypeString");
		kommune.setPnr(78787878L);
		kommune.setSenr(87654321L);
		kommune.setOrgTypeId(50L);
		kommune.setLocalExtensions("{\"key\":\"value\"}");
		kommune.setMaster("TEST");
		kommune.setMasterId(kommune.getUuid());
		units.add(kommune);

		OrgUnit hr = new OrgUnit();
		hr.setCvr(12345678L);
		hr.setEan(12345678900L);
		hr.getPostAddresses().add(Post.builder().addressProtected(false).city("Viby J").country("DK").localname("localName").postalCode("8260").prime(true).street("Hasselager Centervej 17").master("TEST").masterId("TEST").build());
		hr.setName("HR");
		hr.setShortname("HR");
		hr.setUuid(ouHRUuid);
		hr.setOrgType("OrgTypeString");
		hr.setParent(kommune);
		hr.setPnr(78787878L);
		hr.setSenr(87654321L);
		hr.setOrgTypeId(50L);
		hr.setMaster("TEST");
		hr.setMasterId(hr.getUuid());
		hr.getEmails().add(Email.builder().email("email_hr@email.dk").master("TEST").masterId("TEST").prime(true).build());
		units.add(hr);

		OrgUnit sundhedOgOmsorg = new OrgUnit();
		sundhedOgOmsorg.setParent(kommune);
		sundhedOgOmsorg.setCvr(12345678L);
		sundhedOgOmsorg.setEan(12345678900L);
		sundhedOgOmsorg.getPostAddresses().add(Post.builder().addressProtected(false).city("Viby J").country("DK").localname("localName").postalCode("8260").prime(true).street("Hasselager Centervej 17").master("TEST").masterId("TEST").build());
		sundhedOgOmsorg.setName("Sundhed og omsorg");
		sundhedOgOmsorg.setShortname("SUNDOMSORG");
		sundhedOgOmsorg.setUuid(ouSundhedOgOmsorgUuid);
		sundhedOgOmsorg.setOrgType("OrgTypeString");
		sundhedOgOmsorg.setPnr(78787878L);
		sundhedOgOmsorg.setSenr(87654321L);
		sundhedOgOmsorg.setOrgTypeId(50L);
		sundhedOgOmsorg.setMaster("TEST");
		sundhedOgOmsorg.setMasterId(sundhedOgOmsorg.getUuid());
		sundhedOgOmsorg.getEmails().add(Email.builder().email("email_soo@email.dk").master("TEST").masterId("TEST").prime(true).build());
		units.add(sundhedOgOmsorg);

		OrgUnit sundhed = new OrgUnit();
		sundhed.setParent(sundhedOgOmsorg);
		sundhed.setCvr(12345678L);
		sundhed.setEan(12345678900L);
		sundhed.setName("Sundhed");
		sundhed.setShortname("SUND");
		sundhed.setUuid(ouSundhedUuid);
		sundhed.setOrgType("OrgTypeString");
		sundhed.setPnr(78787878L);
		sundhed.setSenr(87654321L);
		sundhed.setOrgTypeId(50L);
		sundhed.setMaster("TEST");
		sundhed.setMasterId(sundhed.getUuid());
		sundhed.getEmails().add(Email.builder().email("email_s@email.dk").master("TEST").masterId("TEST").prime(true).build());
		units.add(sundhed);

		OrgUnit omsorg = new OrgUnit();
		omsorg.setUuid(ouOmsorgUuid);
		omsorg.setParent(sundhedOgOmsorg);
		omsorg.setCvr(12345678L);
		omsorg.setEan(12345678900L);
		omsorg.setName("Omsorg");
		omsorg.setShortname("OMSORG");
		omsorg.setOrgType("OrgTypeString");
		omsorg.setPnr(78787878L);
		omsorg.setSenr(87654321L);
		omsorg.setOrgTypeId(50L);
		omsorg.setMaster("TEST");
		omsorg.setMasterId(omsorg.getUuid());
		omsorg.getEmails().add(Email.builder().email("email_om@email.dk").master("TEST").masterId("TEST").prime(true).build());
		units.add(omsorg);

		// save all created OUs
		orgUnitDao.save(units);

		// Create our 10 employees
		List<Person> persons = new ArrayList<>();

		for (int i = 0; i < 10 ; i ++) {
			String displayName = null;
			String cpr = null;
			String firstName = null;
			String surName = null;
			String uuid = null;
			String userId = null;
			OrgUnit positionOU1 = null;
			OrgUnit positionOU2 = null;
			OrgUnit managerOU = null;

			switch (i) {
				case 0:
					displayName = "Hr. Hansen";
					cpr = "0112550980";
					firstName = "Hans";
					surName = "Hansen";
					uuid = user1Uuid;
					userId = "user1";
					positionOU1 = omsorg;
					break;
				case 1:
					cpr = "0404550990";
					firstName = "Grethe";
					surName = "Hansen";
					uuid = user2Uuid;
					userId = "user2";
					positionOU1 = omsorg;
					break;
				case 2:
					cpr = "0101201133";
					firstName = "Gert";
					surName = "Gunnerson";
					uuid = user3Uuid;
					userId = "user3";
					positionOU1 = sundhed;
					break;
				case 3:
					cpr = "0101551008";
					firstName = "Birthe";
					surName = "Biversen";
					uuid = user4Uuid;
					userId = "user4";
					positionOU1 = sundhed;
					break;
				case 4:
					cpr = "1234512345";
					firstName = "Hans";
					surName = "Jensen";
					uuid = user5Uuid;
					userId = "user5";
					positionOU1 = omsorg;
					break;
				case 5:
					cpr = "1234512346";
					firstName = "Jens";
					surName = "Hansen";
					uuid = user6Uuid;
					userId = "user6";
					positionOU1 = omsorg;
					break;
				case 6:
					cpr = "1234512347";
					firstName = "Morten";
					surName = "Sørensen";
					uuid = user7Uuid;
					userId = "user7";
					managerOU = kommune;
					positionOU1 = omsorg;
					break;
				case 7:
					cpr = "1234512348";
					firstName = "Gurli";
					surName = "Gris";
					uuid = user8Uuid;
					userId = "user8";
					positionOU1 = omsorg;
					break;
				case 8:
					cpr = "1234512349";
					firstName = "Bente";
					surName = "Bomstærk";
					uuid = user9Uuid;
					userId = "user9";
					positionOU1 = hr;
					break;
				case 9:
					cpr = "1094778861";
					firstName = "Søren";
					surName = "Sørensen";
					uuid = user10Uuid;
					userId = "user10";
					positionOU1 = kommune;
					positionOU2 = hr;
					break;
			}

			Person person = new Person();
			person.setAnniversaryDate(new Date());
			person.setChosenName(displayName);
			person.setCpr(cpr);
			person.setMaster("TEST");
			person.setFirstEmploymentDate(new Date());
			person.setFirstname(firstName);
			person.setLocalExtensions("{\"key\":\"value\"}");
			
			person.getPhones().add(Phone.builder().phoneNumber("12341234").master("TEST").masterId("TEST").prime(true).phoneType(PhoneType.LANDLINE).build());
			person.getPhones().add(Phone.builder().phoneNumber("89898989").master("TEST").masterId("TEST").prime(false).phoneType(PhoneType.MOBILE).build());

			if (positionOU1 != null) {
				Affiliation position = new Affiliation();
				position.setEmployeeId("1");
				position.setEmploymentTerms("102");
				position.setEmploymentTermsText("102");
				position.setAffiliationType(AffiliationType.EMPLOYEE);
				position.setLocalExtensions("{\"key\":\"value\"}");
				List<AffiliationFunction> flags = new ArrayList<>();
				flags.add(AffiliationFunction.MED_UDVALG);
				position.setFunctions(flags);
				position.setOrgUnit(positionOU1);
				position.setPayGrade("33");
				position.setPerson(person);
				position.setPositionId("41");
				position.setPositionName("Ansat");
				position.setStartDate(new Date());
				position.setWorkingHoursDenominator(37.0);
				position.setWorkingHoursNumerator(37.0);
				position.setUuid(UUID.randomUUID().toString());
				position.setMaster("TEST");
				position.setMasterId(position.getUuid());
				person.getAffiliations().add(position);
			}

			if (managerOU != null) {
				Set<OrgUnit> managerFor = new HashSet<>();
				managerFor.add(omsorg);
				person.getAffiliations().iterator().next().setManagerFor(managerFor);
			}

			if (positionOU2 != null) {
				Affiliation position = new Affiliation();
				position.setEmployeeId("1");
				position.setEmploymentTerms("101");
				position.setEmploymentTermsText("101");
				position.setAffiliationType(AffiliationType.EMPLOYEE);
				position.setLocalExtensions("{\"key\":\"value\"}");
				List<AffiliationFunction> flags = new ArrayList<>();
				flags.add(AffiliationFunction.MED_UDVALG);
				position.setFunctions(flags);
				position.setOrgUnit(positionOU2);
				position.setPayGrade("33");
				position.setPerson(person);
				position.setPositionId("41");
				position.setPositionName("Ansat");
				position.setStartDate(new Date());
				position.setWorkingHoursDenominator(37.0);
				position.setWorkingHoursNumerator(37.0);
				position.setUuid(UUID.randomUUID().toString());
				position.setMaster("TEST");
				position.setMasterId(position.getUuid());
				person.getAffiliations().add(position);
			}

			person.setResidencePostAddress(Post.builder().addressProtected(true).city("Viby J").country("DK").localname("localName").postalCode("8260").prime(false).street("Hasselager Centervej 17").master("TEST").masterId("TEST").build());
			person.setRegisteredPostAddress(Post.builder().addressProtected(true).city("Viby J").country("DK").localname("localName").postalCode("8260").prime(true).street("Hasselager Centervej 17").master("TEST").masterId("TEST").build());

			person.setSurname(surName);
			User user = new User();
			user.setUserType("ACTIVE_DIRECTORY");
			user.setUuid(UUID.randomUUID().toString());
			user.setUserId(userId);
			user.setLocalExtensions("{\"key\":\"value\"}");
			user.setMaster("TEST");
			user.setMasterId(userId);
			person.getUsers().add(user);
			
			// Bente get's an extra user
			if (i == 8) {
				user = new User();
				user.setUserType("UNILOGIN");
				user.setUuid("f78336af-6ef6-400b-bc6d-6a001787ab1f");
				user.setUserId("uniloginid");
				user.setMaster("TEST");
				user.setMasterId("uniloginid");
				person.getUsers().add(user);				
			}
			
			person.setUuid(uuid);
			persons.add(person);
		}

		// save all users
		personDao.save(persons);
	}
}
