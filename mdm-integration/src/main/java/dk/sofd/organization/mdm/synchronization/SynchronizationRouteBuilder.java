package dk.sofd.organization.mdm.synchronization;

import dk.sofd.organization.mdm.sofdorganization.model.Person;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component
public class SynchronizationRouteBuilder extends RouteBuilder {


	public static final String ROUTE_ID = "SynchronizationRoute";
	private static final String OFFSET_HEADER = "OffsetHeader";

	@Override
	public void configure() throws Exception {

		// Main sync route
		from("timer:SynchronizationTimer?period={{synchronization.intervalmilliseconds}}")
		.routeId(ROUTE_ID)
		.autoStartup("{{SynchronizationRoute.autoStartup}}")
		.choice().when().method("SOFDOrganizationService","isFullSync")
			.to("direct:fullsync")
		.otherwise()
			.to("direct:deltasync")
		.endChoice()
		.end();

		// Full sync route
		from("direct:fullsync")
		.routeId("FullSyncRoute")
		.log("Full synchronization started")
		// get the current header offset from SOFD Core
		.to("bean:SOFDOrganizationService?method=getHead")
		.setHeader(OFFSET_HEADER,simple("${body}"))
		// get all orgunits from SOFD Core and perform merge
		.to("bean:SOFDOrganizationService?method=getOrgUnits")
		.split().body()
			.to("bean:MDMService?method=save")
		.end()
		.to("bean:SOFDOrganizationService?method=setOrgUnitOffset(${header." + OFFSET_HEADER + "})")
		// get all persons from SOFD Core and perform merge
		.to("bean:SOFDOrganizationService?method=getPersons")
		.split().body()
			.to("bean:MDMService?method=save")
		.end()
		.to("bean:SOFDOrganizationService?method=setPersonOffset(${header." + OFFSET_HEADER + "})")
		// disable full sync so that next iteration will be delta only
		.to("bean:SOFDOrganizationService?method=setFullSync(false)")
		.log("Full synchronization completed");

		// Delta sync Route
		from("direct:deltasync")
		.routeId("DeltaSyncRoute")
		// get delta orgunits from SOFD Core and perform merge
		.to("bean:SOFDOrganizationService?method=getDeltaSyncOrgUnits")
		.setHeader(OFFSET_HEADER,simple("${body.offset}"))
		.split(simple("${body.uuids}"))
			.log("Delta sync updating orgUnit ${body.uuid}")
			.to("bean:SOFDOrganizationService?method=getOrgUnit(${body.uuid})")
			.to("bean:MDMService?method=save")
		.end()
		.to("bean:SOFDOrganizationService?method=setOrgUnitOffset(${header." + OFFSET_HEADER + "})")
		// get delta persons from SOFD Core and perform merge
		.to("bean:SOFDOrganizationService?method=getDeltaSyncPersons")
		.setHeader(OFFSET_HEADER,simple("${body.offset}"))
		.split(simple("${body.uuids}"))
			.log("Delta sync updating person ${body.uuid}")
			.to("bean:SOFDOrganizationService?method=getPerson(${body.uuid})")
			.to("bean:MDMService?method=save")
		.end()
		.to("bean:SOFDOrganizationService?method=setPersonOffset(${header." + OFFSET_HEADER + "})");

	}
}