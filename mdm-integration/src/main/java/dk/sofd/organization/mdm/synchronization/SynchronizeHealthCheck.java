package dk.sofd.organization.mdm.synchronization;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.xml.XPathBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import javax.management.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component("synchronize")
public class SynchronizeHealthCheck extends AbstractHealthIndicator
{
    public static final String FAILED_TIMESTAMP = "lastExchangeFailureTimestamp";
    public static final String COMPLETED_TIMESTAMP = "lastExchangeCompletedTimestamp";
    @Autowired
    CamelContext camelContext;

    private String routesStatsXML;
    private DateTimeFormatter xmlFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXX");
    private DateTimeFormatter displayFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception
    {
        updateRoutesStats();

        ZonedDateTime syncCompleted = ZonedDateTime.parse( getRouteStat(SynchronizationRouteBuilder.ROUTE_ID, COMPLETED_TIMESTAMP), xmlFormat);
        ZonedDateTime syncFailed =ZonedDateTime.parse( getRouteStat(SynchronizationRouteBuilder.ROUTE_ID, FAILED_TIMESTAMP), xmlFormat);
        ZonedDateTime resetSyncCookieCompleted = ZonedDateTime.parse( getRouteStat(ResetSyncOffsetRouteBuilder.ROUTE_ID, COMPLETED_TIMESTAMP), xmlFormat);


        // if the most recent sync job failed, this health check reports down
        if( syncFailed.isAfter(syncCompleted) )
        {
            builder.down();
            builder.withDetail("Error", "Synchronization failure since last successful sync");
        }
        else if( syncFailed.isAfter(resetSyncCookieCompleted) )
        {
            builder.down();
            builder.withDetail("Error", "Synchronization failure since last reset ");
        }
        else
        {
            builder.up();
        }

        builder
            .withDetail("Last completed", syncCompleted.format(displayFormat))
            .withDetail("Last failed",syncFailed.format(displayFormat))
            .withDetail("Last reset", resetSyncCookieCompleted.format(displayFormat));
    }

    private void updateRoutesStats() throws MBeanException, InstanceNotFoundException, ReflectionException, MalformedObjectNameException
    {
        // The data we are looking for is not directly available from the management bean, but it is available from the dumpRoutesStatsAsXml operation as xml
        ObjectName routeObjectName = camelContext.getManagementStrategy().getManagementNamingStrategy().getObjectNameForCamelContext(camelContext);
        MBeanServer server = camelContext.getManagementStrategy().getManagementAgent().getMBeanServer();
        String operation = "dumpRoutesStatsAsXml";
        routesStatsXML = (String) server.invoke(routeObjectName, operation, new Object[]{true, true}, new String[]{"boolean", "boolean"});
    }

    private String getRouteStat(String routeId, String stat )
    {
        return XPathBuilder.xpath("//routeStat[@id='" + routeId+ "']/@" + stat).evaluate(camelContext, routesStatsXML);
    }
}
