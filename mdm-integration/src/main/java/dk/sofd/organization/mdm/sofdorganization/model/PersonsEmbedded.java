package dk.sofd.organization.mdm.sofdorganization.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class PersonsEmbedded implements EntityEmbedded
{
	private Collection<Person> persons;

	@Override
	public Collection<? extends SOFDEntity> get()
	{
		return persons;
	}
}
