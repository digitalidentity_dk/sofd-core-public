package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import dk.sofd.organization.mdm.sofdorganization.serializer.LocalExtensionsDeserializer;
import dk.sofd.organization.mdm.sofdorganization.model.enums.UserType;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity(name = "users")
@Getter
@Setter
@EqualsAndHashCode
public class User {
	@Id
	@Column(columnDefinition = "uniqueidentifier")
	@Type(type = "uuid-char")
	private UUID uuid;

	@NotNull
	private String master;

	@NotNull
	private String masterId;

	@NotNull
	@Size(max = 64)
	private String userId;

	@Enumerated(EnumType.STRING)
	private UserType userType;

	@Column(columnDefinition = "xml")
	@JsonDeserialize(using = LocalExtensionsDeserializer.class)
	private String localExtensions;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "email_id")
	private Email email;

	@ManyToOne
	@JoinColumn(name = "person_uuid", nullable = false, updatable = false, insertable = true)
	@JsonBackReference
	private Person person;
}