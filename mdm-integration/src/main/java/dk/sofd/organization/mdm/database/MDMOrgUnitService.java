package dk.sofd.organization.mdm.database;

import dk.sofd.organization.mdm.sofdorganization.model.OrgUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

public interface MDMOrgUnitService extends JpaRepository<OrgUnit, Long>
{
}
