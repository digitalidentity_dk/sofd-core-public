package dk.sofd.organization.mdm.database;

import dk.sofd.organization.mdm.sofdorganization.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

public interface MDMPersonService extends JpaRepository<Person, Long>
{
}
