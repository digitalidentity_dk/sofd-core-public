package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "orgunits_manager")
@Getter
@Setter
public class OrgUnitManager {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private String name;

    @ManyToOne
    private Person manager;

    @Column
    private boolean inherited;

    @OneToOne
    @JoinColumn(name = "orgunit_uuid")
    @JsonBackReference
    private OrgUnit orgUnit;


}
