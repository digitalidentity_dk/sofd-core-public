package dk.sofd.organization.mdm.sofdorganization.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class OrgUnitsEmbedded implements EntityEmbedded
{
	private Collection<OrgUnit> orgUnits;

	@Override
	public Collection<? extends SOFDEntity> get()
	{
		return orgUnits;
	}
}
