package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import dk.sofd.organization.mdm.sofdorganization.serializer.LocalExtensionsDeserializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Parent;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity(name = "orgunits")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrgUnit implements SOFDEntity
{
    @Id
    @Column(columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    private UUID uuid;

    @Column(columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    private UUID parentUuid;

    @NotNull
    private String master;

    @NotNull
    private String masterId;

    private boolean deleted;

    private Date created;

    private Date lastChanged;


    @Size(max = 64)
    @NotNull
    private String shortname;

    @NotNull
    private String name;

    private Long cvr;

    private Long ean;

    private Long senr;

    private Long pnr;

    private String costBearer;

    private String orgType;

    private Long orgTypeId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "orgunits_posts", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "post_id"))
    private Set<Post> postAddresses;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "orgunits_phones", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
    private Set<Phone> phones;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "orgunits_emails", joinColumns = @JoinColumn(name = "orgunit_uuid"), inverseJoinColumns = @JoinColumn(name = "email_id"))
    private Set<Email> emails;

/*
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "orgUnit", orphanRemoval = true)
    private OrgUnitManager manager;
*/

    @ElementCollection
    @CollectionTable(name = "orgunits_kle_primary", joinColumns = @JoinColumn(name = "orgunit_uuid"))
    @Column(name = "kle_value")
    private Set<String> klePrimary;

    @ElementCollection
    @CollectionTable(name = "orgunits_kle_secondary", joinColumns = @JoinColumn(name = "orgunit_uuid"))
    @Column(name = "kle_value")
    private Set<String> kleSecondary;

    @Column
    @JsonDeserialize(using = LocalExtensionsDeserializer.class)
    private String localExtensions;
}
