package dk.sofd.organization.mdm.sofdorganization.model.enums;

public enum ChangeType
{
	CREATE, UPDATE
}
