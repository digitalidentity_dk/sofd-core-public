package dk.sofd.organization.mdm.sofdorganization.model.enums;

public enum UserType {
	ACTIVE_DIRECTORY, OPUS, UNILOGIN
}
