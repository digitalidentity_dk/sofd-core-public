package dk.sofd.organization.mdm.config;

import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Servlet;

@Configuration
public class CamelConfiguration {

    // Register a bean that binds the synchronization rest servlet to the existing spring boot web servlet
    @Bean
    public ServletRegistrationBean<Servlet> camelServlet() {
        ServletRegistrationBean<Servlet> mapping = new ServletRegistrationBean<Servlet>();
        mapping.setName("CamelServlet");
        mapping.setLoadOnStartup(1);
        mapping.setServlet(new CamelHttpTransportServlet());
        mapping.addUrlMappings("/api/*");

        return mapping;
    }
}
