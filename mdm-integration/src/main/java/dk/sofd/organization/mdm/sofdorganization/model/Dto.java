package dk.sofd.organization.mdm.sofdorganization.model;

public interface Dto
{
    EntityEmbedded getEmbedded();
    Page getPage();
}
