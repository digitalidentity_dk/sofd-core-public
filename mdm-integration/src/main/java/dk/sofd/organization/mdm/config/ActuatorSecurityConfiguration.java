package dk.sofd.organization.mdm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(99) // the default security configuration has order 100
public class ActuatorSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Value("${management.user.name}")
	private String manageUsername;

	@Value("${management.user.password}")
	private String managePassword;

    @Value("${api.user.name}")
    private String apiUsername;

    @Value("${api.user.password}")
    private String apiPassword;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        var encoder = new BCryptPasswordEncoder();

        auth.inMemoryAuthentication()
            .withUser(apiUsername)
            .password(encoder.encode(apiPassword))
            .roles("USER")
        .and()
            .withUser(manageUsername)
            .password(encoder.encode(managePassword))
            .roles("ACTUATOR");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/manage/health").permitAll()
            .antMatchers("/manage/**").hasRole("ACTUATOR")
            .antMatchers("/api/**").hasRole("USER")
        .and()
            .httpBasic();
    }
    
    @Bean
    public PasswordEncoder defaultPasswordEncoder() {
    	return new BCryptPasswordEncoder();
    }
}