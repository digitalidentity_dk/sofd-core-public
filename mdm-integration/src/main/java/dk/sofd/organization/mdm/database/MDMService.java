package dk.sofd.organization.mdm.database;

import dk.sofd.organization.mdm.sofdorganization.model.OrgUnit;
import dk.sofd.organization.mdm.sofdorganization.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MDMService
{
    @Autowired
    MDMOrgUnitService mdmOrgUnitService;

    @Autowired
    MDMPersonService mdmPersonService;

    public void save(OrgUnit orgUnit)
    {
        mdmOrgUnitService.save(orgUnit);
    }

    public void save(Person person)
    {
        mdmPersonService.save(person);
    }
}
