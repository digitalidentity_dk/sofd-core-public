package dk.sofd.organization.mdm.sofdorganization.model;

import dk.sofd.organization.mdm.sofdorganization.model.enums.PhoneType;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "phones")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Phone
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(max = 32)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @NotNull
    private PhoneType phoneType;

    @NotNull
    private boolean prime;

}

