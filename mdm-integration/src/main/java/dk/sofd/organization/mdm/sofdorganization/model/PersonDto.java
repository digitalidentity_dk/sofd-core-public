package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonDto implements Dto
{
	@JsonProperty("_embedded")
	private PersonsEmbedded embedded;
	private Page page;
}