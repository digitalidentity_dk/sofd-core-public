package dk.sofd.organization.mdm.sofdorganization.model;

import java.util.Collection;

public interface EntityEmbedded
{
    Collection<? extends SOFDEntity> get();
}
