package dk.sofd.organization.mdm.sofdorganization.model.enums;

public enum AffiliationFunction {
	MED_UDVALG, SR, TR, TR_SUPPLEANT
}