package dk.sofd.organization.mdm.sofdorganization.model.enums;

public enum AffiliationType {
	EMPLOYEE, EXTERNAL
}
