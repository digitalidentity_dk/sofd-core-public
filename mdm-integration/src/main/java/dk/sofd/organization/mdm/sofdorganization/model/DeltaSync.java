package dk.sofd.organization.mdm.sofdorganization.model;

import dk.sofd.organization.mdm.sofdorganization.model.Change;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class DeltaSync
{
	private int offset;
	private Collection<Change> uuids;
}
