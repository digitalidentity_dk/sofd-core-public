package dk.sofd.organization.mdm.sofdorganization;

import dk.sofd.organization.mdm.sofdorganization.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.HashSet;

@Service
@Slf4j
public class SOFDOrganizationService
{
    private static final String PROJECTION = "withParentUuid";
    private static final String ORGUNITS_REST_NAME = "orgUnits";
    private static final String PERSONS_REST_NAME = "persons";

    @Value("${sofd.core.url:https://demo.sofd.io/api}")
    private String url;

    @Value("${sofd.core.apikey}")
    private String apiKey;

    @Value("${sofd.core.pagesize:100}")
    private int pageSize;

    @Autowired
    private RestTemplate restTemplate;

    @Getter
    @Setter
    private boolean fullSync = true;
    @Getter
    @Setter
    private int orgUnitOffset;
    @Getter
    @Setter
    private int personOffset;

    public int getHead() throws Exception
    {
        var request = new HttpEntity(getHeaders());
        String query = "/sync/head";
        var response = restTemplate.exchange(url + query, HttpMethod.GET, request, Integer.class);
        if (!response.getStatusCode().equals(HttpStatus.OK))
        {
            throw new Exception("Failed to get sync head. " + response.getStatusCodeValue() + ", response=" + response.getBody());
        }
        var result = response.getBody();
        return result;
    }

    public DeltaSync getDeltaSyncPersons() throws Exception
    {
        return getDeltaSync(PERSONS_REST_NAME, personOffset);
    }

    public DeltaSync getDeltaSyncOrgUnits() throws Exception
    {
        return getDeltaSync(ORGUNITS_REST_NAME, orgUnitOffset);
    }

    public SOFDEntity getPerson(String uuid) throws Exception
    {
        return getEntity(Person.class, PERSONS_REST_NAME, uuid);
    }

    public SOFDEntity getOrgUnit(String uuid) throws Exception
    {
        return getEntity(OrgUnit.class, ORGUNITS_REST_NAME, uuid);
    }

    public Collection<SOFDEntity> getPersons() throws Exception
    {
        return getEntities(Person.class, PERSONS_REST_NAME, PersonDto.class);
    }

    public Collection<SOFDEntity> getOrgUnits() throws Exception
    {
        return getEntities(OrgUnit.class, ORGUNITS_REST_NAME, OrgUnitDto.class);
    }


    private SOFDEntity getEntity(Class<? extends SOFDEntity> entity, String entityName, String uuid) throws Exception
    {
        var request = new HttpEntity(getHeaders());
        String query = "/" + entityName + "/" + uuid;
        query += "?projection=" + PROJECTION;
        var response = restTemplate.exchange(url + query, HttpMethod.GET, request, entity);
        if (!response.getStatusCode().equals(HttpStatus.OK))
        {
            throw new Exception("Failed to get " + entityName + " with uuid " + uuid + ". " + response.getStatusCodeValue() + ", response=" + response.getBody());
        }
        var result = response.getBody();
        return result;
    }

    private Collection<SOFDEntity> getEntities(Class<? extends SOFDEntity> entity, String entityName, Class<? extends Dto> dtoClass) throws Exception
    {
        var request = new HttpEntity(getHeaders());
        String query = "/" + entityName;
        query += "?size=" + pageSize;
        query += "&projection=" + PROJECTION;
        Collection<SOFDEntity> result = new HashSet<>();
        Page page = new Page();
        page.setNumber(0);
        do
        {
            var response = restTemplate.exchange(url + query + "&page=" + page.getNumber(), HttpMethod.GET, request, dtoClass);
            if (!response.getStatusCode().equals(HttpStatus.OK))
            {
                throw new Exception("Failed to fetch a list of " + entityName + ". " + response.getStatusCodeValue() + ", response=" + response.getBody());
            }
            result.addAll(response.getBody().getEmbedded().get());
            page = response.getBody().getPage();
            page.setNumber(page.getNumber() + 1);
        }
        while (page.getNumber() < page.getTotalPages());
        return result;
    }

    private DeltaSync getDeltaSync(String entityName, int offset) throws Exception
    {
        var request = new HttpEntity(getHeaders());
        String query = "/sync/" + entityName.toLowerCase() + "?offset=" + offset;
        var response = restTemplate.exchange(url + query, HttpMethod.GET, request, DeltaSync.class);
        if (!response.getStatusCode().equals(HttpStatus.OK))
        {
            throw new Exception("Failed to get delta sync for " + entityName.toLowerCase() + " with offset " + offset + ". " + response.getStatusCodeValue() + ", response=" + response.getBody());
        }
        var result = response.getBody();
        return result;
    }

    private HttpHeaders getHeaders()
    {
        var headers = new HttpHeaders();
        headers.add("apiKey", apiKey);
        headers.add("Content-Type", "application/json");
        return headers;
    }
}
