package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import dk.sofd.organization.mdm.sofdorganization.serializer.LocalExtensionsDeserializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;
import java.util.UUID;


@Entity(name = "persons")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person implements SOFDEntity
{
    @Id
    @Column(columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    private UUID uuid;

    @NotNull
    private String master;

    private Date created;

    private Date lastChanged;

    private boolean deleted;

    @Size(min = 10, max = 10)
    @NotNull
    private String cpr;

    @NotNull
    @Size(max = 255)
    private String firstname;

    @NotNull
    @Size(max = 255)
    private String surname;

    @Size(max = 255)
    private String chosenName;

    @Column
    private Date firstEmploymentDate;

    @Column
    private Date anniversaryDate;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "registered_post_address_id")
    private Post registeredPostAddress;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "residence_post_address_id")
    private Post residencePostAddress;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "persons_phones", joinColumns = @JoinColumn(name = "person_uuid"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
    private Set<Phone> phones;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "person")
    @JsonManagedReference
    private Set<Affiliation> affiliations;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "person")
    @JsonManagedReference
    private Set<User> users;

    @Column(columnDefinition = "xml")
    @JsonDeserialize(using = LocalExtensionsDeserializer.class)
    private String localExtensions;

}
