package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import dk.sofd.organization.mdm.sofdorganization.model.enums.AffiliationFunction;
import dk.sofd.organization.mdm.sofdorganization.model.enums.AffiliationType;
import dk.sofd.organization.mdm.sofdorganization.serializer.LocalExtensionsDeserializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity(name = "affiliations")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Affiliation
{
    @Id
    @Column(columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    private UUID uuid;

    @NotNull
    private String master;

    @NotNull
    private String masterId;

    private Date startDate;

    private Date stopDate;

    private boolean deleted;

    @Column(name = "orgunit_uuid", columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    @NotNull
    private UUID orgUnitUuid;

    @ManyToOne
    @JoinColumn(name = "person_uuid", nullable = false, updatable = false, insertable = true)
    @JsonBackReference
    private Person person;

    private String employeeId;

    private String employmentTerms;

    private String employmentTermsText;

    private String payGrade;

    private Double workingHoursDenominator;

    private Double workingHoursNumerator;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AffiliationType affiliationType;

    private String positionId;

    @NotNull
    private String positionName;

    @ElementCollection
    @CollectionTable(name = "affiliations_kle_primary", joinColumns = @JoinColumn(name = "affiliation_uuid"))
    @Column(name = "kle_value")
    private Set<String> klePrimary;

    @ElementCollection
    @CollectionTable(name = "affiliations_kle_secondary", joinColumns = @JoinColumn(name = "affiliation_uuid"))
    @Column(name = "kle_value")
    private Set<String> kleSecondary;

    @Column(columnDefinition = "xml")
    @JsonDeserialize(using = LocalExtensionsDeserializer.class)
    private String localExtensions;

    @ElementCollection(targetClass = AffiliationFunction.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "affiliations_function", joinColumns = @JoinColumn(name = "affiliation_uuid"))
    @Column(name = "affiliation_function")
    private Set<AffiliationFunction> functions;

    @ElementCollection
    @CollectionTable(name = "affiliations_manager", joinColumns = @JoinColumn(name = "affiliation_uuid"))
    @Column(name = "orgunit_uuid",columnDefinition = "uniqueidentifier")
    @Type(type = "uuid-char")
    private Set<UUID> managerForUuids;


}