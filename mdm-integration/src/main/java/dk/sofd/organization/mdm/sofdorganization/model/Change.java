package dk.sofd.organization.mdm.sofdorganization.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import dk.sofd.organization.mdm.sofdorganization.model.enums.ChangeType;
import lombok.*;

@Getter
@Setter
public class Change
{
	private String uuid;
	private ChangeType changeType;
}
