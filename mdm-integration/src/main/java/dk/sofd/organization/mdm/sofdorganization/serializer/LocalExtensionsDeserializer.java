package dk.sofd.organization.mdm.sofdorganization.serializer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.BaseJsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;

public class LocalExtensionsDeserializer extends JsonDeserializer<String>
{
    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException
    {
        BaseJsonNode node = jp.readValueAsTree();

        if (!node.isObject())
        {
            throw new JsonParseException(jp, "Only JSON objects are allowed as localExtensions!");
        } else
        {
            XmlMapper xmlMapper = new XmlMapper();
            return xmlMapper.writer().withRootName("LocalExtensions").writeValueAsString(node);
        }
    }
}
