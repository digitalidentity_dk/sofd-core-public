package dk.sofd.organization.mdm.synchronization;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component
public class ResetSyncOffsetRouteBuilder extends RouteBuilder {

	private static final String RESET_ENDPOINT_URI = "direct:resetSyncOffset";
	public static final String ROUTE_ID = "resetSyncOffsetRoute";

	@Override
	public void configure() throws Exception {
		from(RESET_ENDPOINT_URI)
		.routeId(ROUTE_ID)
		.log("Resetting sync offset")
		.to("bean:SOFDOrganizationService?method=setFullSync(true)");

		// invoke reset route on schedule
		from("quartz2:fullSyncScheduler?cron={{synchronization.fullsynccron}}")
		.to(ResetSyncOffsetRouteBuilder.RESET_ENDPOINT_URI);

		// invoke reset route from api call
		rest("/")
		.get("/reset")
		.produces("text/html")
		.route()
		.routeId("RestFullsync")
		.to(ResetSyncOffsetRouteBuilder.RESET_ENDPOINT_URI)
		.transform()
		.constant("Resetting sync offset")
		.endRest();
	}


}
