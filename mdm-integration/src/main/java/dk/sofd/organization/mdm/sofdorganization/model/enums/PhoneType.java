package dk.sofd.organization.mdm.sofdorganization.model.enums;

public enum PhoneType {
	MOBILE
	,BROADBAND
	,IP
	,DATA_SIM
	,LANDLINE
	,FUNCTION
}