package dk.sofd.organization.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.sofd.organization.mdm.sofdorganization.model.OrgUnit;
import dk.sofd.organization.mdm.sofdorganization.model.Person;
import dk.sofd.organization.mdm.sofdorganization.model.SOFDEntity;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class TestData
{
    public static Collection<SOFDEntity> GetOrgUnits() throws IOException
    {
        var file = new File(TestData.class.getClassLoader().getResource("TestData/OrgUnits.json").getFile());
        Collection<SOFDEntity> entities = new ObjectMapper().readValue(file, new TypeReference<Collection<OrgUnit>>() {});
        return entities;
    }

    public static Collection<SOFDEntity> GetPersons() throws IOException
    {
        var file = new File(TestData.class.getClassLoader().getResource("TestData/Persons.json").getFile());
        Collection<SOFDEntity> entities = new ObjectMapper().readValue(file, new TypeReference<Collection<Person>>() {});
        return entities;
    }

}