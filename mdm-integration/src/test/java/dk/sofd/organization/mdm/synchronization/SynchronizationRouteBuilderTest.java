package dk.sofd.organization.mdm.synchronization;

import dk.sofd.organization.mdm.TestData;
import dk.sofd.organization.mdm.sofdorganization.SOFDOrganizationService;
import dk.sofd.organization.mdm.sofdorganization.model.Change;
import dk.sofd.organization.mdm.sofdorganization.model.DeltaSync;
import dk.sofd.organization.mdm.sofdorganization.model.OrgUnit;
import dk.sofd.organization.mdm.sofdorganization.model.Person;
import dk.sofd.organization.mdm.sofdorganization.model.enums.ChangeType;
import org.apache.camel.CamelContext;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest
@MockEndpoints
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SynchronizationRouteBuilderTest
{
    @Autowired
    protected CamelContext camelContext;

    @Produce(uri = "direct:fullsync")
    protected ProducerTemplate fullSync;

    @Produce(uri = "direct:deltasync")
    protected ProducerTemplate deltaSync;

    @MockBean
    private SOFDOrganizationService sofdOrganizationService;

    @Test
    public void t1_testFullSync() throws Exception
    {
        Mockito.when(sofdOrganizationService.getOrgUnits()).thenReturn(TestData.GetOrgUnits());
        Mockito.when(sofdOrganizationService.getPersons()).thenReturn(TestData.GetPersons());

        fullSync.sendBody(null);
    }

    @Test
    public void t2_testDeltaSync() throws Exception
    {
        DeltaSync deltaSync = new DeltaSync();
        deltaSync.setOffset(100);
        var changes = new HashSet<Change>();
        Change change = new Change();
        change.setChangeType(ChangeType.UPDATE);
        change.setUuid("uuid-to-be-changed-1234");
        changes.add(change);
        deltaSync.setUuids(changes);
        Mockito.when(sofdOrganizationService.getDeltaSyncOrgUnits()).thenReturn(deltaSync);
        Mockito.when(sofdOrganizationService.getDeltaSyncPersons()).thenReturn(deltaSync);

        var orgUnit = (OrgUnit) TestData.GetOrgUnits().iterator().next();
        orgUnit.setName("Changed by deltasync");
        Mockito.when(sofdOrganizationService.getOrgUnit(any(String.class))).thenReturn(orgUnit);

        var person = (Person) TestData.GetPersons().iterator().next();
        person.setChosenName("Changed by deltasync");
        Mockito.when(sofdOrganizationService.getPerson(any(String.class))).thenReturn(person);

        this.deltaSync.sendBody(null);
    }


}