package dk.sofd.core.stil.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import dk.sofd.core.stil.service.model.enums.AffiliationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(exclude = { "uuid", "orgUnit" })
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
public class Affiliation {
	private String uuid;
	private String master;
	private String masterId;
	private boolean deleted;
	private String orgUnit;
	private String orgUnitUuid; // ouput-only (comes from projection)
	private AffiliationType affiliationType;
	private String positionName;
}