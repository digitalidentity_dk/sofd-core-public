package dk.sofd.core.stil.service.model.enums;

public enum AffiliationType {
	EMPLOYEE, EXTERNAL
}
