package dk.sofd.core.stil.service.model.enums;

public enum UserType {
	ACTIVE_DIRECTORY, OPUS, UNILOGIN
}
