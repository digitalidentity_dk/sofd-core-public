package dk.sofd.core.stil.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Municipality {
	private String name;
	private String sofdApiKey;
	private String sofdUrl;
	private String stilUsername;
	private String stilPassword;
	private int[] stilInstitutions; 
	private String emailSuffix;

}
