package dk.sofd.core.stil.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dk.sofd.core.stil.config.Municipality;
import dk.stil.ws17.request.EksporterXmlFuldMyndighed;
import dk.stil.ws17.request.Envelope;
import dk.stil.ws17.request.Envelope.Body;
import dk.stil.ws17.response.UNILoginExport.Institution;

@Service
public class StilService {

	@Autowired
	private RestTemplate template;

	@Value("${stil.url:https://wsieksport.uni-login.dk/wsieksport-v3/ws}")
	private String url;

	public Institution getInstitution(long institutionCode, Municipality municipality) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/soap+xml; charset=UTF-8");

		Envelope envelope = new Envelope();
		envelope.setBody(new Body());
		envelope.getBody().setEksporterXmlFuldMyndighed(new EksporterXmlFuldMyndighed());
		envelope.getBody().getEksporterXmlFuldMyndighed().setInstnr(institutionCode);
		envelope.getBody().getEksporterXmlFuldMyndighed().setWsBrugerid(municipality.getStilUsername());
		envelope.getBody().getEksporterXmlFuldMyndighed().setWsPassword(municipality.getStilPassword());
		
		HttpEntity<Envelope> request = new HttpEntity<Envelope>(envelope, headers);

		ResponseEntity<dk.stil.ws17.response.Envelope> response = template.postForEntity(url, request, dk.stil.ws17.response.Envelope.class);
		
		return response.getBody().getBody().getEksporterXmlFuldMyndighedResponse().getXml().getUNILoginExport().getInstitution();
	}
}
