package dk.sofd.core.stil.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import dk.sofd.core.stil.config.Municipality;
import dk.sofd.core.stil.service.model.Email;
import dk.sofd.core.stil.service.model.Person;
import dk.sofd.core.stil.service.model.Post;
import dk.sofd.core.stil.service.model.StilPerson;
import dk.sofd.core.stil.service.model.User;
import dk.sofd.core.stil.service.model.enums.UserType;
import dk.stil.ws17.response.UNILoginExport.Institution;
import dk.stil.ws17.response.UNILoginExport.Institution.InstitutionPerson;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SyncService {

	@Autowired
	private SOFDOrganizationService sofdOrganizationService;
	
	@Autowired
	private StilService stilService;

	@Value("${stil.master.identifier:STIL}")
	private String stilMasterIdentifier;

	@Async
	public void sync(Municipality municipality) {
		try {
			Collection<Person> sofdPersons = sofdOrganizationService.getPersons(municipality);
			List<StilPerson> stilPersons = new ArrayList<>();
	
			for (long code : municipality.getStilInstitutions()) {
				log.info("Reading users from institution " + code + " for " + municipality.getName());
	
				Institution institution = stilService.getInstitution(code, municipality);
	
				for (InstitutionPerson person : institution.getInstitutionPerson()) {
					if (person.getEmployee() != null) {
	
						// ignore users that we already got from other institutions
						if (stilPersons.stream().filter(p -> p.getUniLogin().equals(person.getUNILogin().getUserId())).count() == 0) {
							stilPersons.add(map(person, institution.getInstitutionName(), institution.getInstitutionNumber(), municipality.getEmailSuffix()));						
						}
						else {
							log.debug("Skipping duplicate user: " + person.getUNILogin().getUserId() + " for " + municipality.getName());
						}
					}
				}
			}
	
			log.info("Synchronizing " + stilPersons.size() + " users from STIL for " + municipality.getName());
	
			fullSync(stilPersons, sofdPersons, municipality);
		}
		catch (Exception ex) {
			log.error("Synchronization failed for " + municipality.getName(), ex);
		}
	}
	
	private StilPerson map(InstitutionPerson person, String institutionName, long institutionNumber, String emailSuffix) {
		try {
			StilPerson stilPerson = new StilPerson();
			
			stilPerson.setCpr(person.getUNILogin().getCivilRegistrationNumber());
			stilPerson.setUniLogin(person.getUNILogin().getUserId());
			stilPerson.setEmail(person.getUNILogin().getUserId() + emailSuffix);		
			stilPerson.setInstitutionName(institutionName);
			stilPerson.setInstitutionNumber(institutionNumber);

			if (person.getPerson() != null) {
				stilPerson.setFirstname(person.getPerson().getFirstName());
				stilPerson.setSurname(person.getPerson().getFamilyName());
				stilPerson.setStreetAddress(person.getPerson().getAddress().getStreetAddress());
				stilPerson.setCity(person.getPerson().getAddress().getPostalDistrict());
				stilPerson.setPostalCode(person.getPerson().getAddress().getPostalCode());
			}
			else {
				String name = person.getUNILogin().getName();
				int idx = name.lastIndexOf(' ');
				if (idx > 0 && idx < (name.length() - 1)) {
					stilPerson.setFirstname(name.substring(0, idx));
					stilPerson.setSurname(name.substring(idx + 1));
				}
				else {
					stilPerson.setFirstname(name);
					stilPerson.setSurname("");
				}
			}

			return stilPerson;
		}
		catch (Exception ex) {
			log.warn("Failed on : " + person.getUNILogin().getUserId());
			throw ex;
		}
	}

	private void fullSync(Collection<StilPerson> stilPersons, Collection<Person> sofdPersons, Municipality municipality) throws Exception {
		merge(buildStilPersonsHashMap(stilPersons), buildPersonHashMap(sofdPersons), municipality);
	}

	private void merge(HashMap<String, StilPerson> stilPersons, HashMap<String, Person> persons, Municipality municipality) throws Exception {

		// find person elements that need to be created. ie. elements that are not in sofd organization
		var toBeCreated = new HashSet<>(stilPersons.keySet());
		toBeCreated.removeAll(persons.keySet());
		for (String cpr : toBeCreated) {
			handleCreate(stilPersons.get(cpr), municipality);
		}

		// find person elements that need to be updated. ie. the intersection of both sets
		var toBeUpdated = new HashSet<>(stilPersons.keySet());
		toBeUpdated.retainAll(persons.keySet());
		for (String cpr : toBeUpdated) {
			handleUpdate(stilPersons.get(cpr), persons.get(cpr), municipality);
		}

		// find person elements that need to be deleted. ie. elements that are not in STIL
		var toBeDeleted = new HashSet<>(persons.keySet());
		toBeDeleted.removeAll(stilPersons.keySet());
		for (String cpr : toBeDeleted) {
			handleDelete(persons.get(cpr), municipality);
		}
	}

	private void handleCreate(StilPerson stilPerson, Municipality municipality) throws Exception {
		var person = new Person();
		person.setMaster(stilMasterIdentifier);
		person.setMasterId(stilPerson.getUniLogin());
		person.setCpr(stilPerson.getCpr());
		person.setFirstname(stilPerson.getFirstname());
		person.setSurname(stilPerson.getSurname());
		person.setRegisteredPostAddress(getPostFromStilPerson(stilPerson));

		var user = new User();
		user.setUuid(UUID.randomUUID().toString());
		inflateUserFromStilPerson(user, stilPerson);

		person.setUsers(new HashSet<>());
		person.getUsers().add(user);

		sofdOrganizationService.create(person, municipality);
	}

	private Post getPostFromStilPerson(StilPerson stilPerson) {
		if (stilPerson.getStreetAddress() != null && stilPerson.getStreetAddress().length() > 0) {
			Post post = new Post();
			post.setAddressProtected(false);
			post.setMaster(stilMasterIdentifier);
			post.setMasterId(stilPerson.getUniLogin());
			post.setStreet(stilPerson.getStreetAddress());
			post.setPostalCode(Long.toString(stilPerson.getPostalCode()));			
			post.setCity(stilPerson.getCity());
			post.setCountry("DK");

			return post;
		}
		
		return null;
	}

	private void handleUpdate(StilPerson stilPerson, Person person, Municipality municipality) throws Exception {
		var patchedPerson = new Person();
		boolean shouldUpdate = false;

		// only perform person element updates if STIL is master or if it has been deleted
		if (person.getMaster().equalsIgnoreCase(stilMasterIdentifier) || person.isDeleted()) {

            if (person.isDeleted()) {
            	shouldUpdate = true;
            	patchedPerson.setMaster(stilMasterIdentifier);
            	patchedPerson.setDeleted(false);
            }
		}
		
		var users = person.getUsers();
		
		var foundUser = false;

		for (User user : users) {
			if (user.getMaster().equalsIgnoreCase(stilMasterIdentifier) && user.getMasterId().equalsIgnoreCase(stilPerson.getUniLogin())) {

				// found a matching user. We should update it
				foundUser = true;
				
				// create a copy of the user object
				var originalUser = user.toBuilder().build();
				inflateUserFromStilPerson(user, stilPerson);

				// patch if user differs from original
				if (!user.equals(originalUser)) {
					shouldUpdate = true;
					patchedPerson.setUsers(users);
				}
			}
			else if (user.getMaster().equalsIgnoreCase(stilMasterIdentifier)) {
				// found another user created by this master. We don't allow
				// multiple STIL users per person so we should delete it
				users.remove(user);
				patchedPerson.setUsers(users);
				
				shouldUpdate = true;
			}
		}

		if (!foundUser) {
			// no corresponding user was found. We should add one
			// this users email is only prime if there is not another users
			// email maintained by another master that is prime
			var user = new User();
			user.setUuid(UUID.randomUUID().toString());
			inflateUserFromStilPerson(user, stilPerson);
			users.add(user);
			patchedPerson.setUsers(users);
			
			shouldUpdate = true;
		}

		if (shouldUpdate) {
			patchedPerson.setUuid(person.getUuid());
			sofdOrganizationService.update(patchedPerson, municipality);
		}
	}

	private User inflateUserFromStilPerson(User user, StilPerson stilPerson) {
		user.setUserType(UserType.UNILOGIN);
		user.setMaster(stilMasterIdentifier);
		user.setMasterId(stilPerson.getUniLogin());
		if (stilPerson.getEmail() != null && stilPerson.getEmail().length() > 0) {
			var email = new Email();
			email.setEmail(stilPerson.getEmail());
			email.setMaster(stilMasterIdentifier);
			email.setMasterId(stilPerson.getUniLogin());
			user.setEmail(email);
		}

		user.setUserId(stilPerson.getUniLogin());
		Map<String, String> extensions = new HashMap<>();
		extensions.put("institution", stilPerson.getInstitutionName());
		extensions.put("institutionCode", Long.toString(stilPerson.getInstitutionNumber()));
		user.setLocalExtensions(extensions);

		return user;
	}

	private void handleDelete(Person person, Municipality municipality) throws Exception {
		var patchedPerson = new Person();
		patchedPerson.setUuid(person.getUuid());
		boolean shouldUpdate = false;
		
		// delete person if STIL is master and it is not already deleted
		if (person.getMaster().equalsIgnoreCase(stilMasterIdentifier) && !person.isDeleted()) {
			patchedPerson.setDeleted(true);
			shouldUpdate = true;
		}

		// remove all STIL users from this person
		var nonSTILUsers = new HashSet<User>();
		for (User user : person.getUsers()) {
			if (user.getMaster().equalsIgnoreCase(stilMasterIdentifier)) {
				shouldUpdate = true;
			}
			else {
				nonSTILUsers.add(user);
			}
		}
		
		patchedPerson.setUsers(nonSTILUsers);
		
		if (shouldUpdate) {
			sofdOrganizationService.update(patchedPerson, municipality);
		}
	}

	private HashMap<String, StilPerson> buildStilPersonsHashMap(Collection<StilPerson> stilPersons) {
		var result = new HashMap<String, StilPerson>();
		for (var stilUser : stilPersons) {
			result.put(stilUser.getCpr(), stilUser);
		}

		return result;
	}

	private HashMap<String, Person> buildPersonHashMap(Collection<Person> persons) {
		var result = new HashMap<String, Person>();
		for (Person person : persons) {
			result.put(person.getCpr(), person);
		}
		return result;
	}
}