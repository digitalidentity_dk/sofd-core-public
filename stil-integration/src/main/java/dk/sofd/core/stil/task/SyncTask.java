package dk.sofd.core.stil.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import dk.sofd.core.stil.config.Municipality;
import dk.sofd.core.stil.config.MunicipalityConfiguration;
import dk.sofd.core.stil.service.SyncService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@EnableScheduling
public class SyncTask {

	@Autowired
	private SyncService syncService;

	@Autowired
	private MunicipalityConfiguration configuration;

	@Scheduled(cron = "${sync.cron}")
	public void sync() throws Exception {
		log.info("Running STIL synchronization");

		for (Municipality municipality : configuration.getMunicipalities()) {
			syncService.sync(municipality);
		}

		log.info("Done...");
	}
}
