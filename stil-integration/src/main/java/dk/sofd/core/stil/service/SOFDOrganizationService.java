package dk.sofd.core.stil.service;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dk.sofd.core.stil.config.Municipality;
import dk.sofd.core.stil.service.model.Page;
import dk.sofd.core.stil.service.model.Person;
import dk.sofd.core.stil.service.model.PersonsDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SOFDOrganizationService {

	@Value("${sofd.core.pagesize:1000}")
	private int pageSize;

	@Autowired
	private RestTemplate restTemplate;

	public Collection<Person> getPersons(Municipality municipality) throws Exception {
		return getPersons(null, municipality);
	}

	public Collection<Person> getPersons(String cpr, Municipality municipality) throws Exception {
		var request = new HttpEntity<>(getHeaders(municipality.getSofdApiKey()));
		String query = "/persons?projection=withParentUuid&size=" + pageSize;
		if (cpr != null) {
			query += "&cpr=" + cpr;
		}
		Collection<Person> result = new HashSet<>();
		Page page = new Page();
		page.setNumber(0);
		do {
			var response = restTemplate.exchange(municipality.getSofdUrl() + query + "&page=" + page.getNumber(), HttpMethod.GET, request, PersonsDto.class);
			if (!response.getStatusCode().equals(HttpStatus.OK)) {
				throw new Exception("Failed to fetch a list of Persons. " + response.getStatusCodeValue() + ", response=" + response.getBody());
			}
			result.addAll(response.getBody().getEmbedded().getPersons());
			page = response.getBody().getPage();
			page.setNumber(page.getNumber() + 1);
		}
		while (page.getNumber() < page.getTotalPages());

		return result;
	}

	public void create(Person person, Municipality municipality) throws Exception {
		log.info("Creating person. uuid={}", person.getUuid());

		var request = new HttpEntity<>(person, getHeaders(municipality.getSofdApiKey()));

		var response = restTemplate.exchange(municipality.getSofdUrl() + "/persons", HttpMethod.POST, request, String.class);
		if (response.getStatusCodeValue() < 200 || response.getStatusCodeValue() > 299) {
			throw new Exception("Failed to create Person " + person.getMasterId() + ". " + response.getStatusCodeValue() + ", response=" + response.getBody());
		}
	}

	public void update(Person person, Municipality municipality) throws Exception {
		log.info("Patching person. uuid={}", person.getUuid());

		var request = new HttpEntity<>(person, getHeaders(municipality.getSofdApiKey()));

		var response = restTemplate.exchange(municipality.getSofdUrl() + "/persons/" + person.getUuid(), HttpMethod.PATCH, request, String.class);
		if (response.getStatusCodeValue() < 200 || response.getStatusCodeValue() > 299) {
			throw new Exception("Failed to update Person " + person.getMasterId() + ". " + response.getStatusCodeValue() + ", response=" + response.getBody());
		}
	}

	private HttpHeaders getHeaders(String apiKey) {
		var headers = new HttpHeaders();
		headers.add("apiKey", apiKey);
		headers.add("Content-Type", "application/json");

		return headers;
	}
}
