package dk.sofd.core.stil.service.model;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class Person {
	private String uuid;
	private String master;
	private String masterId;
	private Date created;
	private Date lastChanged;
	private boolean deleted;
	private String cpr;
	private String firstname;
	private String surname;
	private String chosenName;
	private Date firstEmploymentDate;
	private Date anniversaryDate;
	private Set<User> users;
	private Set<Affiliation> affiliations;
	private Post registeredPostAddress;
}
