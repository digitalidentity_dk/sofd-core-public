package dk.sofd.core.stil.service.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import dk.sofd.core.stil.service.model.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.ALWAYS)
public class User {
	private String uuid;
	private String master;
	private String masterId;
	private String userId;
	private UserType userType;
	private Email email;
	private Map<String, String> localExtensions;
}