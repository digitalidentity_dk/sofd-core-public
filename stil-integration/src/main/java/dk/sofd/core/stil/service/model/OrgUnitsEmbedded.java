package dk.sofd.core.stil.service.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrgUnitsEmbedded {
	private List<OrgUnit> orgUnits;
}
