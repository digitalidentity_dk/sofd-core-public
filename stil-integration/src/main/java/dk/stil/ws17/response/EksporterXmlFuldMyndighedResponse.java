//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.04.24 at 08:53:55 AM CEST 
//


package dk.stil.ws17.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xml">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{https://wsieksport.uni-login.dk/eksport/fullmyndighed}UNILoginExport"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xml"
})
@XmlRootElement(name = "eksporterXmlFuldMyndighedResponse", namespace = "https://wsieksport.uni-login.dk/ws")
public class EksporterXmlFuldMyndighedResponse {

    @XmlElement(namespace = "https://wsieksport.uni-login.dk/ws", required = true)
    protected EksporterXmlFuldMyndighedResponse.Xml xml;

    /**
     * Gets the value of the xml property.
     * 
     * @return
     *     possible object is
     *     {@link EksporterXmlFuldMyndighedResponse.Xml }
     *     
     */
    public EksporterXmlFuldMyndighedResponse.Xml getXml() {
        return xml;
    }

    /**
     * Sets the value of the xml property.
     * 
     * @param value
     *     allowed object is
     *     {@link EksporterXmlFuldMyndighedResponse.Xml }
     *     
     */
    public void setXml(EksporterXmlFuldMyndighedResponse.Xml value) {
        this.xml = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{https://wsieksport.uni-login.dk/eksport/fullmyndighed}UNILoginExport"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uniLoginExport"
    })
    public static class Xml {

        @XmlElement(name = "UNILoginExport", namespace = "https://wsieksport.uni-login.dk/eksport/fullmyndighed", required = true)
        protected UNILoginExport uniLoginExport;

        /**
         * Gets the value of the uniLoginExport property.
         * 
         * @return
         *     possible object is
         *     {@link UNILoginExport }
         *     
         */
        public UNILoginExport getUNILoginExport() {
            return uniLoginExport;
        }

        /**
         * Sets the value of the uniLoginExport property.
         * 
         * @param value
         *     allowed object is
         *     {@link UNILoginExport }
         *     
         */
        public void setUNILoginExport(UNILoginExport value) {
            this.uniLoginExport = value;
        }

    }

}
