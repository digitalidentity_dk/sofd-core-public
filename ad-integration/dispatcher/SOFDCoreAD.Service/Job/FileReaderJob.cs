﻿using Quartz;
using Serilog;
using SOFDCoreAD.Service.Backend;
using SOFDCoreAD.Service.File;
using System;
using System.Threading.Tasks;

namespace SOFDCoreAD.Service.Job
{
    [DisallowConcurrentExecution]
    class FileReaderJob : IJob
    {
        public ILogger Logger { get; set; }
        public FileService FileService { get; set; }
        public BackendService BackendService { get; set; }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                var users = FileService.ReadFile();

                if (users != null)
                {
                    Logger.Information("Performing full file-sync");
                    BackendService.FullSync(users);
                    Logger.Information("Full sync complete");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Exception caught in FileReaderJob");
            }

            return Task.CompletedTask;
        }
    }
}
