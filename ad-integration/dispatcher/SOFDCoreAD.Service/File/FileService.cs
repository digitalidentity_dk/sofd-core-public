﻿using Serilog;
using SOFDCoreAD.Service.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace SOFDCoreAD.Service.File
{
    class FileService
    {
        private static string LastProcessedFile = "";

        public ILogger Logger { get; set; }

        // FileName: SOFD_yyyyMMdd.csv
        //
        // Encoding: "ISO-8859-1"
        //
        // Content:
        // cpr,ad-kontonavn,mobilnummer,telefonnummer,kaldenavn,medarbejderid,fornavn,efternavn,uuid
        // 030179xxxx,xxss,xx575757,xxxx7589,,00000005,Brian Storm,Graversen,dc193726-8115-4e63-8793-5ba3f488516f

        public List<Model.ADUser> ReadFile()
        {
            string location = Settings.GetStringValue("File.Location");
            bool headerRow = Settings.GetBooleanValue("File.HeaderRow");

            DateTime dateTime;
            string latestFile = GetLatestFile(location, out dateTime);
            if (latestFile == null)
            {
                return null;
            }

            if (LastProcessedFile.Equals(latestFile))
            {
                return null;
            }

            DateTime creationTime = System.IO.File.GetCreationTime(latestFile);
            DateTime currentTime = DateTime.Now;

            // wait for the file to be stable
            if (currentTime.Subtract(creationTime).TotalSeconds > 120)
            {
                List<ADUser> result = ProcessFile(latestFile, headerRow);

                LastProcessedFile = latestFile;

                return result;
            }

            // file not ready to be processed yet
            return null;
        }

        private List<ADUser> ProcessFile(string latestFile, bool headerRow)
        {
            List<ADUser> result = new List<ADUser>();

            using (StreamReader file = new StreamReader(latestFile, Encoding.GetEncoding("ISO-8859-1")))
            {
                bool firstRow = true;
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    if (headerRow && firstRow)
                    {
                        firstRow = false;
                    }
                    else
                    {
                        string[] columns = line.Split(',');
                        if (columns.Length != 9)
                        {
                            Logger.Warning("Bad Line: " + line);
                            continue;
                        }

                        string cpr = columns[0];
                        string userId = columns[1];
                        string mobile = columns[2];
                        string phone = columns[3];
                        string displayName = columns[4];
                        string employeeId = columns[5];
                        string givenName = columns[6];
                        string surName = columns[7];
                        string uuid = columns[8];

                        // trim CPR
                        string trimmedCpr = cpr.Replace("-", "");
                        trimmedCpr = trimmedCpr.Replace(" ", "");

                        // trim and lower-case uuid
                        string trimmedUuid = (uuid != null) ? uuid.Replace("{", "") : "";
                        trimmedUuid = trimmedUuid.Replace("}", "");
                        trimmedUuid = trimmedUuid.ToLower();

                        if (trimmedCpr.Length != 10)
                        {
                            Logger.Warning("Bad cpr: " + cpr);
                            continue;
                        }

                        if (string.IsNullOrEmpty(userId))
                        {
                            Logger.Warning("Empty userId for: " + cpr);
                            continue;
                        }

                        if (string.IsNullOrEmpty(givenName) || string.IsNullOrEmpty(surName))
                        {
                            Logger.Warning("Missing name for: " + cpr);
                            continue;
                        }

                        if (string.IsNullOrEmpty(trimmedUuid) || trimmedUuid.Length != 36)
                        {
                            Logger.Warning("Bad UUID: " + uuid);
                            continue;
                        }

                        ADUser user = new ADUser();
                        user.Cpr = cpr;
                        user.ObjectGuid = trimmedUuid;
                        user.UserId = userId;
                        user.Surname = surName;
                        user.Firstname = givenName;
                        user.ChosenName = (!string.IsNullOrEmpty(displayName)) ? displayName : null;
                        user.EmployeeId = (!string.IsNullOrEmpty(employeeId)) ? employeeId : null;
                        user.Mobile = (!string.IsNullOrEmpty(mobile)) ? mobile : null;
                        user.Phone = (!string.IsNullOrEmpty(phone)) ? phone : null;

                        result.Add(user);
                    }
                }
            }

            return result;
        }

        private string GetLatestFile(string location, out DateTime timestamp)
        {
            var files = new List<string>();

            IEnumerator<string> enumerator = Directory.EnumerateFiles(location).GetEnumerator();
            while (enumerator.MoveNext())
            {
                files.Add(enumerator.Current);
            }

            if (files.Count == 0)
            {
                Logger.Information("No files in folder - skipping run");
                timestamp = DateTime.MinValue;
                return null;
            }

            string latestFile = null;
            DateTime latestFileTimestamp = DateTime.MinValue;
            foreach (string file in files)
            {
                DateTime dateTime = GetDateFromFileName(file);
                if (dateTime.CompareTo(latestFileTimestamp) > 0)
                {
                    latestFile = file;
                    latestFileTimestamp = dateTime;
                }
            }

            timestamp = latestFileTimestamp;

            return latestFile;
        }

        private DateTime GetDateFromFileName(string fileName)
        {
            string dateString = null;

            // only look at the actual filename
            int idx = fileName.LastIndexOf("\\");
            if (idx >= 0)
            {
                fileName = fileName.Substring(idx + 1);
            }

            try
            {
                // SOFD_yyyyMMdd.csv
                if (fileName == null || fileName.Length < 17)
                {
                    return DateTime.MinValue;
                }

                dateString = fileName.Substring(5, 8);

                return DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Logger.Warning("Could not parse " + dateString);
                throw ex;
            }
        }
    }
}
