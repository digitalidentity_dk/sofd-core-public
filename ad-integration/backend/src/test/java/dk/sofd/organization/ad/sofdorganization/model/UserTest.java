package dk.sofd.organization.ad.sofdorganization.model;

import org.junit.Test;

import dk.sofd.organization.ad.service.model.User;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class UserTest
{

    @Test
    public void equalsReturnsTrueWhenEqual()
    {
        User user1 = getTestUser();
        User user2 = getTestUser();

        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void equalsReturnsTrueWhenEqualSet()
    {
    	User user1 = new User();
        Map<String, String> localExtensions = new HashMap<String,String>();
        localExtensions.put("testkey2","testvalue");
        localExtensions.put("testkey","testvalue");
        user1.setLocalExtensions(localExtensions);

        User user2 = new User();
        Map<String, String> localExtensions2 = new HashMap<String,String>();
        // this set is equal to the above, put added in reverse order
        localExtensions2.put("testkey","testvalue");
        localExtensions2.put("testkey2","testvalue");
        user2.setLocalExtensions(localExtensions2);

        assertEquals(true, user1.equals(user2));
    }


    @Test
    public void equalsReturnsFalseWhenNotEqual()
    {
    	User user1 = getTestUser();
    	User user2 = getTestUser();
        user2.getLocalExtensions().replace("testkey","someothervalue");

        assertEquals(false, user1.equals(user2));
    }

    private User getTestUser(){
    	User testUser = new User();
        testUser.setUserType("ACTIVE_DIRECTORY");
        testUser.setMaster("testMaster");
        testUser.setMasterId("testGuid");
        testUser.setUserId("testUserId");
        Map<String, String> localExtensions = new HashMap<String,String>();
        localExtensions.put("testkey","testvalue");
        localExtensions.put("testkey2","testvalue");
        testUser.setLocalExtensions(localExtensions);
        testUser.setUuid("TestUuid");
        return testUser;
    }
}