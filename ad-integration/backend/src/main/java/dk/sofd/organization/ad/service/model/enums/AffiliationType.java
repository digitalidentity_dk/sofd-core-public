package dk.sofd.organization.ad.service.model.enums;

public enum AffiliationType {
    EMPLOYEE, EXTERNAL
}
