package dk.sofd.organization.ad.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Municipality {
	private String name;
	private String password;
	private String sofdUrl;
	private String sofdApiKey;
	private boolean singleUser = true;
	private String userType = "ACTIVE_DIRECTORY";
	private String masterIdPrefix = "";
	private boolean supportInactiveUsers = false;
}
