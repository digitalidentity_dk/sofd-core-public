package dk.sofd.organization.ad.service.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dk.sofd.organization.ad.service.model.enums.AffiliationType;
import dk.sofd.organization.ad.service.model.serializer.LocalExtensionsDeserializer;
import dk.sofd.organization.ad.service.model.serializer.LocalExtensionsSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@EqualsAndHashCode(exclude = {"uuid", "orgUnit"})
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
public class Affiliation {
    private String uuid;
    private String master;
    private String masterId;
    private String startDate;
    private String stopDate;
    private boolean deleted;
    private String orgUnitUuid;
    private String employeeId; // TODO: vi skal nok bruge denne til Slagelse integrationen, men vi bruger den ikke pt...
    private AffiliationType affiliationType;
    private String positionName;

    @JsonSerialize(using = LocalExtensionsSerializer.class)
    @JsonDeserialize(using = LocalExtensionsDeserializer.class)
    private String localExtensions;

    // TODO: only used for the old API, can be removed once we no longer call that
    private String orgUnit;
    
    // also OLD code... to make the SDR output work with new string-based dates
    public void fixDates() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

    	if (startDate != null && startDate.length() != 10) {
        	try {
        		Date date = sdf.parse(startDate);
        		startDate = sdf2.format(date);
        	}
        	catch (Exception ex) {
        		log.error("Bad timestamp: " + startDate, ex);
        	}    		
    	}
    	
    	if (stopDate != null && stopDate.length() != 10) {
        	try {
        		Date date = sdf.parse(stopDate);
        		stopDate = sdf2.format(date);
        	}
        	catch (Exception ex) {
        		log.error("Bad timestamp: " + stopDate, ex);
        	}    		    		
    	}
    }
}
