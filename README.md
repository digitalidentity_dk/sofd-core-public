# SOFD Core

## Modules

* *sofd-core*. main module
* *ad-integration*. agent and middleware for AD integration
* *opus-integration*. middleware for OPUS integration
* *stil-integration*. middleware for STIL integration
* *os2rollekatalog-integration*. middleware for OS2rollekatalog integration
* *cpr-integration*. middleware for SP/CPR integration
* *remoteprint-integration*. middleware for remote print (fjernprint) integration
* *sms-integration*. middleware for sms integration
* *writeback-agent*. agent for updating AD with data from SOFD Core
* *mdm-integration*. agent for updating SOFD Local with data from SOFD Core
