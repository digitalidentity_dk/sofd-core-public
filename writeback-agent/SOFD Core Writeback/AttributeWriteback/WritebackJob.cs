﻿using Active_Directory;
using Quartz;
using Serilog;
using SOFD_Core;
using SOFD_Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace SOFD
{
    [DisallowConcurrentExecution]
    public class WritebackJob : IJob
    {
        private static ILogger adLogger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger().ForContext(typeof(ActiveDirectoryAttributeService));
        private static ILogger log = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger().ForContext(typeof(WritebackJob));
        private static string adServer = Properties.Settings.Default.ActiveDirectoryDomain;
        private static string adAttributeCpr = Properties.Settings.Default.ActiveDirectoryAttributeCpr;
        private static string adAttributeEmployeeId = Properties.Settings.Default.ActiveDirectoryAttributeEmployeeId;
        private static string updateUserType = Properties.Settings.Default.ActiveDirectoryUserType;
        private static string activeDirectoryWritebackExcludeOUs = Properties.Settings.Default.ActiveDirectoryWritebackExcludeOUs;

        private SOFDOrganizationService organizationService;

        public WritebackJob()
        {
            if (Properties.Settings.Default.ActiveDirectoryEnableAttributeUpdate)
            {
                this.organizationService = new SOFDOrganizationService(Properties.Settings.Default.SofdUrl, Properties.Settings.Default.SofdApiKey);
            }
            else
            {
                log.Debug("Active Directory Writeback job not enabled");
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            if (Properties.Settings.Default.ActiveDirectoryEnableAttributeUpdate)
            {
                try
                {
                    List<Person> persons = new List<Person>();
                    List<OrgUnit> orgUnits = new List<OrgUnit>();
                    var updateManagersEnabled = false; // we only update managers on full sync
                    if (SOFDOrganizationService.fullSync)
                    {
                        log.Information("Running full synchronization");
                        updateManagersEnabled = Properties.Settings.Default.ActiveDirectoryEnableManagerUpdate;
                        organizationService.MoveToHead();
                        orgUnits = organizationService.GetOrgUnits();
                        persons = organizationService.GetPersons();
                        if (updateManagersEnabled)
                        {
                            SetPersonManager(persons, orgUnits);
                        }
                        SOFDOrganizationService.fullSync = false;
                    }
                    else
                    {
                        var deltaSyncPerson = organizationService.GetDeltaSyncPersons();

                        if (deltaSyncPerson.uuids.Count > 0)
                        {
                            List<string> uuids = deltaSyncPerson.uuids.Select(c => c.uuid).Distinct().ToList();

                            foreach (string uuid in uuids)
                            {
                                Person person = organizationService.GetPerson(uuid);

                                if (person != null)
                                {
                                    persons.Add(person);
                                }
                                else
                                {
                                    log.Warning("Could not find a person in SOFD with uuid = " + uuid);
                                }
                            }
                        }
                    }

                    UpdateAttributes(persons, orgUnits, updateManagersEnabled);
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Failed to update attributes");
                }
            }
        }

        private void SetPersonManager(List<Person> persons, List<OrgUnit> orgUnits)
        {
            // Set each persons manager to the primary ad user account of the manager of the primary affiliation
            foreach (var person in persons)
            {
                try
                {
                    var primeAffiliation = person.affiliations.FirstOrDefault(a => a.prime);
                    if (primeAffiliation == null)
                    {
                        continue;
                    }
                    var orgUnit = orgUnits.First(o => o.uuid == primeAffiliation.orgUnitUuid);
                    var managerPersonUuid = orgUnit.manager.uuid;
                    // if the manager is the same as the person (it is for all managers), then set the manager to the manager of parent org
                    if (person.uuid.ToString() == managerPersonUuid)
                    {
                        managerPersonUuid = orgUnits.First(o => o.uuid.ToString() == orgUnit.parentUuid)?.manager?.uuid;
                    }
                    var manager = persons.First(p => p.uuid.ToString() == managerPersonUuid);
                    var managerADUser = manager.users.FirstOrDefault(u => u.prime && u.userType == "ACTIVE_DIRECTORY");
                    if (managerADUser != null)
                    {
                        person.managerADAccountName = managerADUser.userId;
                    }
                }
                catch (Exception e)
                {
                    log.Warning($"Failed to find manager for person {person.name} ({person.uuid})");
                }
            }
        }

        private void UpdateAttributes(List<Person> persons, List<OrgUnit> orgUnits, bool updateManagersEnabled)
        {
            var adMappingList = LoadMappingList("AttributeWriteback/ad-mapping.xml");

            var activeDirectoryWritebackExcludeOUsList = new List<string>();
            if (!String.IsNullOrEmpty(activeDirectoryWritebackExcludeOUs))
            {
                activeDirectoryWritebackExcludeOUsList.AddRange(activeDirectoryWritebackExcludeOUs.Split(';'));
            }

            ActiveDirectoryAttributeService activeDirectoryService = new ActiveDirectoryAttributeService(new ActiveDirectoryConfig()
            {
                map = adMappingList,
                adServer = adServer,
                attributeCpr = adAttributeCpr,
                attributeEmployeeId = adAttributeEmployeeId,
                ActiveDirectoryWritebackExcludeOUs = activeDirectoryWritebackExcludeOUsList
            }, adLogger);
            bool orgUnitRequired = OrgUnitRequired(adMappingList.Values);
            bool orgUnitParentRequired = OrgUnitParentRequired(adMappingList.Values);

            if (persons.Count > 0)
            {
                log.Information("Found " + persons.Count + " modified persons");

                foreach (var person in persons)
                {
                    bool foundADAccount = false;
                    foreach (User user in person.users)
                    {
                        if (user.userType.Equals(updateUserType))
                        {
                            foundADAccount = true;
                            break;
                        }
                    }

                    if (!foundADAccount)
                    {
                        continue;
                    }

                    OrgUnit orgUnit = null;
                    if (orgUnitRequired && person.affiliations != null && person.affiliations.Count > 0)
                    {
                        foreach (Affiliation a in person.affiliations)
                        {
                            if (!a.prime)
                            {
                                continue;
                            }

                            // OrgUnit without parent
                            orgUnit = GetOrgUnit(orgUnits, a.orgUnitUuid.ToString());

                            // check if we need ou with parent
                            if (orgUnitParentRequired)
                            {
                                // declare pointers
                                string parentUUID = orgUnit.parentUuid;
                                OrgUnit ou = orgUnit;

                                while (parentUUID != null)
                                {
                                    ou.parent = GetOrgUnit(orgUnits, parentUUID);

                                    ou = ou.parent;
                                    parentUUID = ou.parentUuid;
                                }
                            }
                            break;
                        }
                    }

                    try
                    {
                        activeDirectoryService.UpdatePerson(person, orgUnit, updateUserType, updateManagersEnabled);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex, "Failed to update attributes on " + person.uuid);
                    }
                }

                log.Information("Done updating AD");
            }
        }

        private OrgUnit GetOrgUnit(List<OrgUnit> orgUnits, string uuid)
        {
            foreach (var orgUnit in orgUnits)
            {
                if (orgUnit.uuid.ToString().Equals(uuid))
                {
                    return orgUnit;
                }
            }

            // fallback to calling the service
            return organizationService.GetOrgUnit(uuid);
        }

        private bool OrgUnitRequired(Dictionary<string, string>.ValueCollection values)
        {
            foreach (string key in values)
            {
                string[] tokens = key.Split('.');

                if (tokens.Length >= 2 && tokens[1].Equals("orgUnit"))
                {
                    return true;
                }
            }

            return false;
        }

        private bool OrgUnitParentRequired(Dictionary<string, string>.ValueCollection values)
        {
            foreach (string key in values)
            {
                string[] tokens = key.Split('.');

                if (tokens.Length >= 2 && tokens[1].Equals("orgUnit") && tokens[2].Contains("^"))
                {
                    return true;
                }
                if (tokens.Length >= 2 && tokens[1].Equals("orgUnit") && tokens[2].StartsWith("tag[") && tokens[2].Contains("true"))
                {
                    return true;
                }
            }

            return false;
        }

        private Dictionary<string, string> LoadMappingList(string fileName)
        {
            Dictionary<string, string> mappingList = new Dictionary<string, string>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(fileName);
            XmlNodeList mappings = xDoc.GetElementsByTagName("mapping");

            foreach (XmlNode mapping in mappings)
            {
                string property = mapping.Attributes["sofd"].Value;
                log.Debug("SOFD field: " + property + " is mapped to AD field: " + mapping.Attributes["ad"].Value);

                try
                {
                    mappingList.Add(mapping.Attributes["ad"].Value, mapping.Attributes["sofd"].Value);
                }
                catch (Exception innerException)
                {
                    throw new SystemException("Property mapping \"" + property + "\" is invalid.", innerException);
                }
            }

            return mappingList;
        }
    }
}