﻿using Quartz;
using Serilog;
using SOFD_Core;
using SOFD_Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SOFD
{
    public class OpusJob : IJob
    {
        private static ILogger log = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger().ForContext(typeof(OpusJob));
        private static bool createEnabled = Properties.Settings.Default.OpusCreateEnabled;
        private static bool deleteEnabled = Properties.Settings.Default.OpusDeleteEnabled;
        private SOFDOrganizationService organizationService;

        public OpusJob()
        {
            if (createEnabled || deleteEnabled)
            {
                if (!EmailService.Validate())
                {
                    createEnabled = false;
                    deleteEnabled = false;
                }
                else
                {
                    this.organizationService = new SOFDOrganizationService(Properties.Settings.Default.SofdUrl, Properties.Settings.Default.SofdApiKey);
                }
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            if (createEnabled)
            {
                try
                {
                    var response = organizationService.GetPendingOrders("OPUS", "CREATE");

                    if (response.pendingOrders != null && response.pendingOrders.Count > 0)
                    {
                        var result = RequestCreateOPUSAccounts(response);

                        organizationService.SetOrderStatus("OPUS", result);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Failed to process OPUS create orders");
                }
            }

            if (deleteEnabled)
            {
                try
                {
                    var response = organizationService.GetPendingOrders("OPUS", new List<string>() { "DEACTIVATE", "DELETE" });

                    if (response.pendingOrders != null && response.pendingOrders.Count > 0)
                    {
                        var result = RequestDeleteOPUSAccounts(response);

                        organizationService.SetOrderStatus("OPUS", result);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Failed to process OPUS delete orders");
                }
            }
        }

        private List<AccountOrderStatus> RequestCreateOPUSAccounts(AccountOrderResponse response)
        {
            List<AccountOrderStatus> result = new List<AccountOrderStatus>();

            foreach (var order in response.pendingOrders)
            {
                AccountOrderStatus status = new AccountOrderStatus();
                status.id = order.id;

                string name = order.person.firstname + " " + order.person.surname;
                log.Information("Ordering creation of OPUS account for " + name);

                try
                {
                    // fetch person from SOFD
                    Person person = organizationService.GetPerson(order.person.uuid);
                    if (person == null)
                    {
                        status.status = Active_Directory.Constants.FAILED;
                        status.message = "Personen findes ikke i SOFD";
                    }
                    else
                    {
                        string userId = order.userId;
                        if (userId == null)
                        {
                            status.status = Active_Directory.Constants.FAILED;
                            status.message = "Der var ikke angivet et ønsket bruger-id";
                        }
                        else
                        {
                            // get email from primary AD account (if one exists)
                            string email = person.users.Where(u => u.userType.Equals("EXCHANGE") && u.prime).Select(u => u.userId).FirstOrDefault();

                            string emailText = (email == null) ? "ingen email adresse" : email;

                            bool emailResult = EmailService.SendEmail(EmailService.OpusTemplateType.CREATE, emailText, order.person.cpr, userId, name, order.person.employeeId);
                            if (emailResult == true)
                            {
                                status.status = Active_Directory.Constants.CREATED;
                                status.affectedUserId = userId;
                                status.message = "Er sendt til manuel behandling";
                            }
                            else
                            {
                                status.status = Active_Directory.Constants.FAILED;
                                status.message = "Det var ikke muligt at afsende en bestillings email!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Creating account failed");

                    status.status = Active_Directory.Constants.FAILED;
                    status.message = ex.Message;
                }

                result.Add(status);
            }

            return result;
        }

        private List<AccountOrderStatus> RequestDeleteOPUSAccounts(AccountOrderResponse response)
        {
            List<AccountOrderStatus> result = new List<AccountOrderStatus>();

            foreach (var order in response.pendingOrders)
            {
                AccountOrderStatus status = new AccountOrderStatus();
                status.id = order.id;

                string name = order.person.firstname + " " + order.person.surname;
                log.Information("Ordering deletation of OPUS account " + order.userId + " for " + name);

                try
                {
                    bool emailResult = EmailService.SendEmail(EmailService.OpusTemplateType.DELETE, "", order.person.cpr, order.userId, name, order.person.employeeId);
                    if (emailResult == true)
                    {
                        status.status = Active_Directory.Constants.DELETED;
                        status.message = "Er sendt til manuel behandling";
                    }
                    else
                    {
                        status.status = Active_Directory.Constants.FAILED;
                        status.message = "Det var ikke muligt at afsende en bestillings email!";
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Deleting account failed");

                    status.status = Active_Directory.Constants.FAILED;
                    status.message = ex.Message;
                }

                result.Add(status);
            }

            return result;
        }
    }
}
