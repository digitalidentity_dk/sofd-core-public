﻿using Serilog;
using System;
using System.Net;
using System.Net.Mail;

namespace SOFD
{
    public class EmailService
    {
        public enum OpusTemplateType { CREATE, DELETE };

        private static ILogger log = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger().ForContext(typeof(EmailService));
        private static string from = Properties.Settings.Default.EmailFromAddress;
        private static string fromname = Properties.Settings.Default.EmailFromName;
        private static string to = Properties.Settings.Default.EmailToAddress;
        private static string subjectCreate = Properties.Settings.Default.EmailSubjectCreate;
        private static string subjectDelete = Properties.Settings.Default.EmailSubjectDelete;
        private static string username = Properties.Settings.Default.EmailUsername;
        private static string password = Properties.Settings.Default.EmailPassword;
        private static string host = Properties.Settings.Default.EmailHost;
        private static int port = Properties.Settings.Default.EmailPort;
        private static string emailCreateTemplateFile = Properties.Settings.Default.OpusCreateEmail;
        private static string emailDeleteTemplateFile = Properties.Settings.Default.OpusDeleteEmail;
        private static bool ssl = Properties.Settings.Default.EmailSsl;

        public static bool Validate()
        {
            if (string.IsNullOrEmpty(from))
            {
                log.Warning("SMTP from is not set");
                return false;
            }

            if (string.IsNullOrEmpty(fromname))
            {
                log.Warning("SMTP fromname is not set");
                return false;
            }

            if (string.IsNullOrEmpty(to))
            {
                log.Warning("SMTP to is not set");
                return false;
            }

            if (string.IsNullOrEmpty(host))
            {
                log.Warning("SMTP host is not set");
                return false;
            }

            return true;
        }

        public static bool SendEmail(OpusTemplateType templateType, string email, string cpr, string userId, string name, string employeeId)
        {
            string template = null;
            string subject = null;

            switch (templateType)
            {
                case OpusTemplateType.CREATE:
                    template = System.IO.File.ReadAllText(emailCreateTemplateFile);
                    subject = subjectCreate;
                    break;
                case OpusTemplateType.DELETE:
                    template = System.IO.File.ReadAllText(emailDeleteTemplateFile);
                    subject = subjectDelete;
                    break;
            }

            // not always supplied
            if (employeeId == null)
            {
                employeeId = "";
            }

            // not always supplied
            if (userId == null)
            {
                userId = "";
            }

            string body = template.Replace("{CPR}", cpr)
                                  .Replace("{USER_ID}", userId)
                                  .Replace("{NAME}", name)
                                  .Replace("{EMPLOYEE_ID}", employeeId)
                                  .Replace("{EMAIL}", email);

            // Create and build a new MailMessage object
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.From = new MailAddress(from, fromname);
            message.To.Add(new MailAddress(to));
            message.Subject = subject;
            message.Body = body;

            using (var client = new SmtpClient(host, port))
            {
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    client.Credentials = new NetworkCredential(username, password);
                }

                client.EnableSsl = ssl;

                try
                {
                    client.Send(message);

                    log.Information("Successfully send email to " + to + " regarding " + name);

                    return true;
                }
                catch (Exception ex)
                {
                    log.Error(ex, "Failed to send email");

                    return false;
                }
            }
        }
    }
}
