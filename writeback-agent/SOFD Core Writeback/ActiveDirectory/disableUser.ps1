﻿function Invoke-Method {
	param(
        [string] $SAMAccountName = $(throw "Please specify a sAMAccountName."),
        [string] $Name = $(throw "Please specify a name."),
		[string] $Uuid = $(throw "Please specify a uuid.")
	)
	
	$result = "Disabling " + $SAMAccountName + ", " + $Name + ", " + $Uuid;

	$result | Out-File 'c:\logs\log.txt'
}
