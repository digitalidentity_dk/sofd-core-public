﻿using Serilog;
using System;
using System.Management.Automation.Runspaces;
using System.Text;

namespace Active_Directory
{
    public class ExchangeService
    {
        private ILogger logger;
        private string exchangeServer;        private string onlineDomain;        private bool remote;        private bool usePSSnapin;
        public ExchangeService(string exchangeServer, string onlineDomain, bool remote, bool usePSSnapin, ILogger logger)
        {
            this.exchangeServer = exchangeServer;
            this.remote = remote;
            this.onlineDomain = onlineDomain;
            this.logger = logger;
            this.usePSSnapin = usePSSnapin;
        }

        public void EnableMailbox(string accountName, string email)
        {
            logger.Debug("Initializing runspace");
            using (Runspace runspace = InitializeRunspace())
            {
                try
                {
                    logger.Debug("Creating pipeline");
                    using (Pipeline pipeline = runspace.CreatePipeline())
                    {
                        Command command = (remote) ? new Command("Enable-RemoteMailbox") : new Command("Enable-Mailbox");
                        command.Parameters.Add("Identity", accountName);
                        command.Parameters.Add("PrimarySmtpAddress", email);

                        if (remote)
                        {
                            int idx = email.IndexOf("@");
                            if (idx >= 0)
                            {
                                string alias = email.Substring(0, idx);
                                string onlineEmail = alias + onlineDomain;

                                logger.Information($"Setting Alias to: {alias}. Setting RemoteRoutingAddress to: {onlineEmail}");
                                command.Parameters.Add("Alias", alias);
                                command.Parameters.Add("RemoteRoutingAddress", onlineEmail);
                            }
                        }
                        pipeline.Commands.Add(command);
                        logger.Debug("Invoking pipeline for Enable-RemoteMailbox");
                        InvokePipelineFailOnError(pipeline);
                    }

                    if (remote)
                    {
                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {
                            var emailPolicyCommand = new Command("Set-RemoteMailbox");
                            emailPolicyCommand.Parameters.Add("Identity", accountName);
                            emailPolicyCommand.Parameters.Add("EmailAddressPolicyEnabled", true);
                            pipeline.Commands.Add(emailPolicyCommand);
                            logger.Debug("Invoking pipeline for Set-RemoteMailbox");
                            InvokePipelineFailOnError(pipeline);
                        }
                    }
                }
                finally
                {
                    DisconnectRunspace(runspace);
                }                
            }
        }

        public void DisableMailbox(string sAMAccountName)
        {
            // remote mailboxes are automatically closed after 30 days, so we do nothing
            if (remote)
            {
                return;
            }

            try
            {
                using (Runspace runspace = InitializeRunspace())
                {
                    try
                    {
                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {
                            Command command = new Command("Disable-Mailbox");
                            command.Parameters.Add("Identity", sAMAccountName);
                            command.Parameters.Add("Confirm", "$false");

                            pipeline.Commands.Add(command);
                            InvokePipelineFailOnError(pipeline);
                        }
                    }
                    finally
                    {
                        DisconnectRunspace(runspace);
                    }                    
                }
            }
            catch (Exception ex)
            {
                logger.Warning(ex, "Failed to disable mailbox for " + sAMAccountName);
            }
        }        public bool MailboxExists(string accountName)
        {
            using (Runspace runspace = InitializeRunspace())
            {
                try
                {
                    logger.Information("Verifying existance of " + accountName);

                    // alway check local mailbox first
                    try
                    {
                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {
                            Command command = new Command("Get-Mailbox");
                            command.Parameters.Add("Identity", accountName);

                            pipeline.Commands.Add(command);
                            InvokePipelineFailOnError(pipeline);
                        }

                        logger.Information("It existed on-premise");

                        return true;
                    }
                    catch (Exception)
                    {
                        logger.Information("It did not exist on-premise");

                        // if remote, we also need to check the remote mailbox
                        if (!remote)
                        {
                            return false;
                        }
                    }

                    if (remote)
                    {
                        try
                        {
                            using (Pipeline pipeline = runspace.CreatePipeline())
                            {
                                Command command = new Command("Get-RemoteMailbox");
                                command.Parameters.Add("Identity", accountName);

                                pipeline.Commands.Add(command);
                                InvokePipelineFailOnError(pipeline);
                            }

                            logger.Information("It existed in Exchange Online");

                            return true;
                        }
                        catch (Exception)
                        {
                            logger.Information("It did not exist in exchange online");
                            return false;
                        }
                    }
                }
                finally
                {
                    DisconnectRunspace(runspace);
                }                
            }

            // Visual Studio thinks we can reach this point (we cannot), so let's return true ;)
            return true;
        }
        private Runspace InitializeRunspace()
        {
            RunspaceConfiguration runspaceConfig = RunspaceConfiguration.Create();
            Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfig);
            runspace.Open();

            using (Pipeline pipeline = runspace.CreatePipeline())
            {
                if (usePSSnapin)
                {
                    pipeline.Commands.AddScript("add-PSSnapin Microsoft.Exchange.Management.powershell.snapin");
                }
                else
                {
                    pipeline.Commands.AddScript("$session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://" + exchangeServer + "/powershell");
                    pipeline.Commands.AddScript("Import-PSSession $session | Out-Null");
                }

                InvokePipelineFailOnError(pipeline);
            }

            return runspace;
        }        private void DisconnectRunspace(Runspace runspace)
        {
            using (Pipeline pipeline = runspace.CreatePipeline())
            {
                if (!usePSSnapin)
                {
                    pipeline.Commands.AddScript("Remove-PSSession $session");
                    InvokePipelineFailOnError(pipeline);
                }
            }
        }
        private void InvokePipelineFailOnError(Pipeline pipeline)
        {
            pipeline.Invoke();
            StringBuilder sb = new StringBuilder();
            if (pipeline.Error != null && pipeline.Error.Count > 0)
            {
                var errors = pipeline.Error.ReadToEnd();
                foreach (var error in errors)
                {
                    sb.Append(error.ToString());
                }
                throw new Exception(sb.ToString());
            }
        }
    }
}
