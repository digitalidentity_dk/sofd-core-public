﻿using System;
using System.DirectoryServices;
using SOFD_Core.Model;
using SOFD_Core.Model.Enums;
using Serilog;
using System.Linq;

namespace Active_Directory
{
    public class ActiveDirectoryAttributeService : ActiveDirectoryServiceBase
    {
        public ActiveDirectoryAttributeService(ActiveDirectoryConfig config, ILogger log) : base(config, log)
        {

        }

        public void SetUserPrincipleNameAndNickname(string sAMAccountName, string emailAlias)
        {
            string nickname = (emailAlias.IndexOf('@') > 0) ? emailAlias.Substring(0, emailAlias.IndexOf('@')) : sAMAccountName;

            try
            {
                using (var de = GetBySAMAccountName(sAMAccountName))
                {
                    de.Properties["userprincipalname"].Value = emailAlias;
                    de.Properties["mailnickname"].Value = nickname;
                    de.CommitChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex, "Unable to set UserPrincipleName to " + emailAlias + " and mailNickName to " + nickname + " on " + sAMAccountName);
            }
        }

        public void UpdatePerson(Person person, OrgUnit orgUnit, string updateUserType, bool updateManagersEnabled)
        {
            if (person.users == null || person.deleted)
            {
                return;
            }

            foreach (User user in person.users)
            {
                try
                {
                    // only update AD accounts
                    if (!user.userType.Equals(updateUserType))
                    {
                        continue;
                    }
                    var filter = string.Format("(&(objectClass=user)(objectClass=person)(sAMAccountName={0}))", user.userId);

                    using (DirectoryEntry entry = new DirectoryEntry())
                    {
                        using (DirectorySearcher search = new DirectorySearcher(entry))
                        {
                            search.Filter = filter;

                            // Fetch from AD only the properties we have in mapping for
                            foreach (var item in config.map)
                            {
                                search.PropertiesToLoad.Add(item.Key);
                            }
                            if (updateManagersEnabled)
                            {
                                search.PropertiesToLoad.Add("manager");
                            }
                            search.PropertiesToLoad.Add("distinguishedName");

                            SearchResult result = search.FindOne();

                            if (result != null)
                            {
                                using (var de = result.GetDirectoryEntry())
                                {
                                    if (de.Properties == null)
                                    {
                                        log.Warning("de.Properties is null for: " + user.userId);
                                        continue;
                                    }
                                    using (var userOU = de.Parent)
                                    {
                                        var ouDN = (string)userOU.Properties["distinguishedName"].Value;
                                        foreach (var activeDirectoryWritebackExcludeOU in config.ActiveDirectoryWritebackExcludeOUs)
                                        {
                                            if (ouDN.Contains(activeDirectoryWritebackExcludeOU))
                                            {
                                                log.Debug($"Skipping writeback for {user.userId} because user is in excluded OU {activeDirectoryWritebackExcludeOU}");
                                                continue;
                                            }
                                        }
                                    }

                                    string newCn = null;
                                    bool changes = false;

                                    foreach (var item in config.map)
                                    {
                                        try
                                        {
                                            string adValue = de.Properties.Contains(item.Key) ? de.Properties[item.Key].Value.ToString() : null;
                                            string sofdValue = FieldMapper.GetValue(user.userId, item.Value, person, orgUnit);

                                            // Handle special case: accountExpires
                                            // Todo: remove the accountExpires feature when Syddjurs no longer needs it (awaiting SOFD IDM implementation)
                                            if ("accountExpires".Equals(item.Key, StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                adValue = result.Properties.Contains(item.Key) ? ((Int64)result.Properties[item.Key][0]).ToString() : null;
                                                if (!person.affiliations.Any(a => a.prime))
                                                {
                                                    // if we can't read a prime affiliation we should not change the account expires value in AD
                                                    continue;
                                                }
                                                DateTime accountExpires;
                                                if (DateTime.TryParse(sofdValue, out accountExpires))
                                                {
                                                    // we expect the input to be affiliation stop date or similar date
                                                    // but we do not want the account to expire on the last day of work, so we add 1 day
                                                    accountExpires = accountExpires.AddDays(1);
                                                    // accountExpires expects the number of 100-nanosecond intervals since January 1, 1601 (UTC) 
                                                    // we can get this using ToFileTime
                                                    sofdValue = Convert.ToString((Int64)accountExpires.ToFileTime());
                                                }
                                                else
                                                {
                                                    // A value of 0 means that the account never expires
                                                    sofdValue = "0";
                                                }
                                            }

                                            if (!"cn".Equals(item.Key))
                                            {

                                                if (string.IsNullOrEmpty(sofdValue))
                                                {
                                                    if (!string.IsNullOrEmpty(adValue))
                                                    {
                                                        log.Information("Clearing attribute for " + user.userId + ": " + item.Key);
                                                        de.Properties[item.Key].Clear();
                                                        changes = true;
                                                    }
                                                }
                                                else if (!sofdValue.Equals(adValue))
                                                {
                                                    log.Information("Setting attribute for " + user.userId + ": " + item.Key + "=" + sofdValue);
                                                    de.Properties[item.Key].Value = sofdValue;
                                                    changes = true;
                                                }
                                            }
                                            else
                                            {
                                                // if we have a new name for the user, store it here
                                                if (!string.IsNullOrEmpty(sofdValue) && !object.Equals(sofdValue, adValue))
                                                {
                                                    log.Information("Renaming " + user.userId + " to " + sofdValue);
                                                    newCn = sofdValue;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            log.Error(ex, "Property mapping \"" + item.Key + "\" is incorrect for: " + user.userId);

                                            return;
                                        }
                                    }

                                    if (updateManagersEnabled)
                                    {
                                        try
                                        {

                                            string currentManagerDN = de.Properties.Contains("manager") ? de.Properties["manager"].Value.ToString() : null;
                                            string actualManagerDN = null;
                                            if (String.IsNullOrEmpty(person.managerADAccountName))
                                            {
                                                if (!String.IsNullOrEmpty(currentManagerDN))
                                                {
                                                    log.Information($"Clearing attribute for {user.userId}: manager");
                                                    de.Properties["manager"].Clear();
                                                    changes = true;
                                                }
                                            }
                                            else
                                            {
                                                // lookup manager in AD to get DN from the accountname
                                                using (DirectoryEntry managerEntry = new DirectoryEntry())
                                                {
                                                    using (DirectorySearcher managerSearch = new DirectorySearcher(managerEntry))
                                                    {
                                                        managerSearch.Filter = $"(SAMAccountName={person.managerADAccountName})";
                                                        SearchResult managerResult = managerSearch.FindOne();
                                                        if (managerResult == null)
                                                        {
                                                            log.Warning($"Could not lookup manager in AD. Filter={managerSearch.Filter}");
                                                        }
                                                        else
                                                        {
                                                            using (var managerDE = managerResult.GetDirectoryEntry())
                                                            {
                                                                actualManagerDN = managerDE.Properties["distinguishedName"].Value.ToString();
                                                            }
                                                        }
                                                    }
                                                }
                                                if (currentManagerDN != actualManagerDN)
                                                {
                                                    log.Information($"Setting attribute for {user.userId}: manager={actualManagerDN}");
                                                    de.Properties["manager"].Value = actualManagerDN;
                                                    changes = true;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            log.Error(e, $"Failed to set manager for {user.userId}");
                                        }
                                    }

                                    if (changes)
                                    {
                                        de.CommitChanges();
                                    }

                                    // has to happen after CommitChanges, otherwise changes will not be committed
                                    if (newCn != null)
                                    {
                                        de.Rename("cn=" + newCn);
                                    }
                                }
                            }
                            else
                            {
                                log.Warning("Could not find user with sAMAccountName: " + user.userId);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to connect to AD for user: '" + user?.userId + "'");
                    throw ex;
                }
            }
        }
    }
}
