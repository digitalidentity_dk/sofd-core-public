﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using SOFD_Core.Model;
using Serilog;

namespace Active_Directory
{
    public class ActiveDirectoryAccountService : ActiveDirectoryServiceBase
    {
        private bool allowEnablingWithoutEmployeeIdMatch = false;

        public ActiveDirectoryAccountService(ActiveDirectoryConfig config, ILogger log) : base(config, log)
        {
            this.allowEnablingWithoutEmployeeIdMatch = config.allowEnablingWithoutEmployeeIdMatch;
        }

        public ProcessStatus ProcessDisableOrder(AccountOrder order)
        {
            ProcessStatus response = new ProcessStatus();

            if (string.IsNullOrEmpty(order.userId))
            {
                throw new Exception("Ingen brugerkonto angivet");
            }

            AccountStatus status = GetAccountStatusBySAMAccountName(order.userId);
            if (status != null)
            {
                response = DisableAccount(status);
                if (response == null)
                {
                    throw new Exception("Kunne ikke disable brugerkontoen: " + order.userId);
                }
            }
            else
            {
                throw new Exception("Brugerkontoen findes ikke: " + order.userId);
            }

            return response;
        }

        public ProcessStatus ProcessDeleteOrder(AccountOrder order)
        {
            if (string.IsNullOrEmpty(order.userId))
            {
                throw new Exception("Ingen brugerkonto angivet");
            }

            ProcessStatus response = DeleteAccount(order.userId);
            if (response == null)
            {
                throw new Exception("Kunne ikke slette brugerkontoen: " + order.userId);
            }

            return response;
        }

        public ProcessStatus ProcessCreateOrder(AccountOrder order)
        {
            // should we create a new account?
            bool createNewAccount = true;

            // find all existing accounts with the supplied cpr (twice, one with and one without dash)
            List<AccountStatus> existingAccounts = GetAccountStatiByCpr(order.person.cpr);
            List<AccountStatus> extraExistingAccounts = GetAccountStatiByCpr(GetCprWithDash(order.person.cpr));
            existingAccounts.AddRange(extraExistingAccounts);

            foreach (var existingAccount in existingAccounts)
            {
                // if an employeeId was supplied, see if an account exist with this employeeId
                if (!allowEnablingWithoutEmployeeIdMatch && !string.IsNullOrEmpty(order.person.employeeId))
                {
                    if (order.person.employeeId.Equals(existingAccount.employeeId))
                    {
                        if (existingAccount.disabled)
                        {
                            createNewAccount = false;
                        }
                        else
                        {
                            // when matching accounts with employeeIds, only one account is allowed
                            throw new Exception("Der er en AD konto til ansættelsen (" + order.person.employeeId + ") allerede: " + existingAccount.sAMAccountName);
                        }
                    }
                }
                else // ordinary scenario
                {
                    // if an existing account is disabled, reenable that instead
                    if (existingAccount.disabled)
                    {
                        createNewAccount = false;
                    }
                }
            }

            if (createNewAccount)
            {
                return CreateAccount(order);
            }
            
            return EnableAccount(order, existingAccounts);
        }

        private string GetCprWithDash(string cpr)
        {
            if (cpr.Length != 10)
            {
                return cpr;
            }

            return cpr.Substring(0, 6) + "-" + cpr.Substring(6, 4);
        }

        private ProcessStatus EnableAccount(AccountOrder order, List<AccountStatus> existingAccounts)
        {
            ProcessStatus result = new ProcessStatus();
            result.status = Constants.REACTIVATED;

            foreach (var existingAccount in existingAccounts)
            {
                // attempt to re-enable disabled accounts
                if (existingAccount.disabled)
                {
                    // if and employeeId was supplied, only reactivate accounts that matches this employeeId
                    if (!string.IsNullOrEmpty(order.person.employeeId) && !order.person.employeeId.Equals(existingAccount.employeeId))
                    {
                        continue;
                    }

                    EnableAccount(existingAccount, order);
                    result.sAMAccountName = existingAccount.sAMAccountName;

                    break;
                }
            }

            if (string.IsNullOrEmpty(result.sAMAccountName))
            {
                // if the config allows re-activating without employeeId match, and there is at least one
                // disabled account, then we re-activate that one
                if (allowEnablingWithoutEmployeeIdMatch)
                {
                    foreach (var existingAccount in existingAccounts)
                    {
                        if (existingAccount.disabled)
                        {
                            EnableAccount(existingAccount, order);
                            result.sAMAccountName = existingAccount.sAMAccountName;

                            break;
                        }
                    }
                }

                // everything failed
                if (string.IsNullOrEmpty(result.sAMAccountName))
                {
                    throw new Exception("Der var hverken muligt at oprette elle reaktivere en AD konto");
                }
            }

            return result;
        }

        private ProcessStatus DisableAccount(AccountStatus status)
        {
            using (var de = GetBySAMAccountName(status.sAMAccountName))
            {
                if (de != null)
                {
                    int flags = (int)de.Properties["useraccountcontrol"].Value;
                    de.Properties["useraccountcontrol"].Value = (flags | 0x0002);
                    de.CommitChanges();

                    return new ProcessStatus()
                    {
                        status = Constants.DEACTIVATED,
                        sAMAccountName = status.sAMAccountName
                    };
                }

                return null;
            }
        }

        private ProcessStatus DeleteAccount(string userId)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, userId);
            if (user != null)
            {
                user.Delete();

                return new ProcessStatus()
                {
                    status = Constants.DELETED,
                    sAMAccountName = userId
                };
            }

            return null;
        }

        private ProcessStatus CreateAccount(AccountOrder order)
        {
            ProcessStatus result = new ProcessStatus();

            string sAMAccountName = order.userId;
            if (string.IsNullOrEmpty(sAMAccountName))
            {
                result.status = Constants.FAILED;

                return result;
            }

            log.Information("Attempting to create AD account: " + sAMAccountName);

            using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, config.adServer, config.userOU))
            {
                bool failed = true;

                using (var newUser = new UserPrincipal(ctx))
                {
                    newUser.Name = order.person.firstname + " " + order.person.surname + " (" + sAMAccountName + ")";
                    newUser.GivenName = order.person.firstname;
                    newUser.Surname = order.person.surname;
                    newUser.SamAccountName = sAMAccountName;
                    newUser.DisplayName = order.person.firstname + " " + order.person.surname;
                    newUser.SetPassword(Guid.NewGuid().ToString());

                    newUser.AccountExpirationDate = order.endDate;
                    newUser.ExpirePasswordNow();
                    newUser.Enabled = true;
                    newUser.Save();

                    using (DirectoryEntry de = newUser.GetUnderlyingObject() as DirectoryEntry)
                    {
                        if (de != null)
                        {
                            de.Properties[config.attributeCpr].Value = order.person.cpr;

                            if (!string.IsNullOrEmpty(config.attributeEmployeeId) && !string.IsNullOrEmpty(order.person.employeeId))
                            {
                                de.Properties[config.attributeEmployeeId].Value = order.person.employeeId;
                            }

                            de.CommitChanges();

                            failed = false;
                        }
                    }
                }

                if (failed)
                {
                    using (UserPrincipal user = UserPrincipal.FindByIdentity(ctx, sAMAccountName))
                    {
                        if (user != null)
                        {
                            user.Delete();
                        }
                    }

                    throw new Exception("Ikke muligt at registrere CPR på AD konto!");
                }
            }

            result.status = Constants.CREATED;
            result.sAMAccountName = sAMAccountName;

            return result;
        }

        private void EnableAccount(AccountStatus status, AccountOrder order)
        {
            log.Information("Attempting to re-enable AD account: " + status.sAMAccountName);

            using (var de = GetBySAMAccountName(status.sAMAccountName))
            {
                if (de != null)
                {
                    int flags = (int)de.Properties["useraccountcontrol"].Value;
                    de.Properties["useraccountcontrol"].Value = (flags & ~0x0002);
                    if (order.endDate.HasValue)
                    {
                        de.Properties["accountExpires"].Value = order.endDate.Value.ToFileTime().ToString();
                    }
                    else
                    {
                        de.Properties["accountExpires"].Value = 0;
                    }

                    if (!string.IsNullOrEmpty(config.attributeEmployeeId) && !string.IsNullOrEmpty(order.person.employeeId))
                    {
                        de.Properties[config.attributeEmployeeId].Value = order.person.employeeId;
                    }

                    de.CommitChanges();
                }
            }
        }
    }
}
