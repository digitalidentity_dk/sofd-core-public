﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SOFD_Core.Model;

namespace Active_Directory
{
    class FieldMapper
    {
        public static string GetValue(string userId, string sofdField, Person person, OrgUnit orgUnit)
        {
            if (sofdField.StartsWith("concat("))
            {
                return GetValueConcat(userId, sofdField, person, orgUnit);
            }
            if (sofdField.StartsWith("right("))
            {
                return GetValueRight(userId, sofdField, person, orgUnit);
            }
            if (sofdField.StartsWith("left("))
            {
                return GetValueLeft(userId, sofdField, person, orgUnit);
            }
            if (sofdField.StartsWith("static("))
            {
                return GetValueStatic(sofdField);
            }

            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 1)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            switch (tokens[0])
            {
                case "opususer":
                    return MapOpusUser(sofdField, person);
                case "userid":
                    return userId;
                case "unilogin":
                    return MapUniLogin(person);
                case "affiliation":
                    return MapAffiliation(sofdField, person, orgUnit);
                case "phone":
                    return MapPhone(sofdField, person);
                case "post":
                    return MapPost(sofdField, person);
                default:
                    return person.GetType().GetProperty(tokens[0])?.GetValue(person)?.ToString();
            }

            throw new Exception("Should never arrive here!");
        }

        private static string MapUniLogin(Person person)
        {
            foreach (var user in person.users)
            {
                if (user.userType.Equals("UNILOGIN"))
                {
                    return user.userId;
                }
            }

            return null;
        }

        private static string GetValueConcat(string userId, string sofdField, Person person, OrgUnit orgUnit)
        {
            // concat(xxx,xxxx) - strip first 7 chars and last, then trim
            var newSofdField = sofdField.Substring(7, sofdField.Length - 8).Trim();

            string[] fields = newSofdField.Split(',');
            if (fields.Length != 2)
            {
                throw new Exception("concat takes 2 arguments: " + sofdField);
            }

            var field1 = GetValue(userId, fields[0], person, orgUnit);
            var field2 = GetValue(userId, fields[1], person, orgUnit);

            return field1 + " (" + field2 + ")";
        }

        private static string GetValueRight(string userId, string sofdField, Person person, OrgUnit orgUnit)
        {
            // right(xxx,4) - return only the rightmost 4 characters of the value
            var newSofdField = sofdField.Substring(6, sofdField.Length - 7).Trim();
            string[] args = newSofdField.Split(',');
            if (args.Length != 2)
            {
                throw new Exception("right takes 2 arguments: " + sofdField);
            }
            var length = int.Parse(args[1]);
            var field = GetValue(userId, args[0], person, orgUnit);
            
            if (field != null && field.Length > length)
            {
                field = field.Substring(field.Length - length, length);
            }
            return field;
        }

        private static string GetValueLeft(string userId, string sofdField, Person person, OrgUnit orgUnit)
        {
            // left(xxx,4) - return only the leftmost 4 characters of the value
            var newSofdField = sofdField.Substring(5, sofdField.Length - 6).Trim();
            string[] args = newSofdField.Split(',');
            if (args.Length != 2)
            {
                throw new Exception("left takes 2 arguments: " + sofdField);
            }
            var length = int.Parse(args[1]);
            var field = GetValue(userId, args[0], person, orgUnit);

            if (field != null && field.Length > length)
            {
                field = field.Substring(0, length);
            }
            return field;
        }

        private static string GetValueStatic(string sofdField)
        {
            var value = sofdField.Substring(7, sofdField.Length - 8).Trim();
            return value;
        }

        private static string MapPost(string sofdField, Person person)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 2)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            if (person.registeredPostAddress != null)
            {
                return person.registeredPostAddress.GetType().GetProperty(tokens[1])?.GetValue(person.registeredPostAddress)?.ToString();
            }
            else if (person.residencePostAddress != null)
            {
                return person.residencePostAddress.GetType().GetProperty(tokens[1])?.GetValue(person.residencePostAddress)?.ToString();
            }

            return null;
        }

        private static string MapPost(string sofdField, OrgUnit orgUnit)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 4)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            if (orgUnit.postAddresses != null && orgUnit.postAddresses.Count > 0)
            {
                foreach (Post post in orgUnit.postAddresses)
                {
                    if (post.prime)
                    {
                        return post.GetType().GetProperty(tokens[3])?.GetValue(post)?.ToString();
                    }
                }
            }

            return null;
        }

        private static OrgUnit GetOrgUnitWithTag(OrgUnit orgUnit, string tag, bool inherit)
        {
            if (orgUnit == null)
            {
                return null;
            }
            if (orgUnit.tags.Exists(t => t.tag == tag))
            {
                return orgUnit;
            }
            else if (inherit)
            {
                return GetOrgUnitWithTag(orgUnit.parent, tag, inherit);
            }
            return null;
        }

        private static string MapTag(string sofdField, string tagToken, OrgUnit orgUnit)
        {
            var match = Regex.Match(tagToken, @"^tag\[(?<tag>[^,].*?),(?<inherit>[^,].*?),(?<defaultValue>[^,].*?)\]$");
            if (!match.Success)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            var tag = match.Groups["tag"].Value;
            var inherit = Boolean.Parse(match.Groups["inherit"].Value);
            var defaultValue = match.Groups["defaultValue"].Value;

            var orgUnitWithTag = GetOrgUnitWithTag(orgUnit,tag,inherit);            
            if (orgUnitWithTag != null)
            {
                var orgUnitTag = orgUnitWithTag.tags.First(t => t.tag == tag);
                if (!String.IsNullOrEmpty(orgUnitTag.customValue))
                {
                    return orgUnitTag.customValue;
                }                
                if (defaultValue != "null")
                {
                    return orgUnit?.GetType().GetProperty(defaultValue)?.GetValue(orgUnitWithTag)?.ToString();
                }                
            }
            return null;
        }


        private static string MapPhone(string sofdField, Person person)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 3)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }
            var phoneType = tokens[1];
            var phoneProperty = tokens[2];

            if (person.phones != null)
            {
                var phone = person.phones.Where(p => p.phoneType.Equals(phoneType, StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(p => p.prime).FirstOrDefault();
                if (phone != null)
                {
                    return phone.GetType().GetProperty(phoneProperty)?.GetValue(phone)?.ToString();
                }
            }
            return null;
        }

        private static string MapAffiliation(string sofdField, Person person, OrgUnit orgUnit)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 2)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            if (person.affiliations != null && person.affiliations.Count > 0)
            {
                Affiliation affiliation = null;

                foreach (Affiliation a in person.affiliations)
                {
                    if (a.prime)
                    {
                        affiliation = a;
                        break;
                    }
                }
                if (affiliation == null)
                {
                    // user might have affiliations, but no prime affiliation
                    return null;
                }                
                
                switch (tokens[1])
                {
                    case "orgUnit":
                        return MapOrgUnit(sofdField, orgUnit);
                    default:
                        return affiliation.GetType().GetProperty(tokens[1])?.GetValue(affiliation)?.ToString();
                }
            }

            return null;
        }

        private static string MapOrgUnit(string sofdField, OrgUnit orgUnit)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 3)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            if ("post".Equals(tokens[2]))
            {
                return MapPost(sofdField, orgUnit);
            }
            else if ("manager".Equals(tokens[2]))
            {
                return orgUnit.manager?.name;
            }
            else if (tokens[2].StartsWith("tag["))
            {
                return MapTag(sofdField, tokens[2], orgUnit);
            }
            else if (tokens[2].StartsWith("^"))
            {
                if (tokens.Length < 4)
                {
                    throw new Exception("Invalid sofd field: " + sofdField);
                }

                int level = int.Parse(tokens[2].Substring(1));
                Stack<OrgUnit> stack = new Stack<OrgUnit>();

                OrgUnit parent = orgUnit;
                while (parent != null)
                {
                    stack.Push(parent);
                    parent = parent.parent;
                }

                OrgUnit parentOU = null;
                for (int i = 0; i < level && stack.Count > 0; i++)
                {
                    parentOU = stack.Pop();
                }

                return parentOU?.GetType().GetProperty(tokens[3])?.GetValue(parentOU)?.ToString();
            }

            return orgUnit?.GetType().GetProperty(tokens[2])?.GetValue(orgUnit)?.ToString();
        }

        private static string MapOpusUser(string sofdField, Person person)
        {
            string[] tokens = sofdField.Split('.');
            if (tokens.Length < 2)
            {
                throw new Exception("Invalid sofd field: " + sofdField);
            }

            if (person.users != null && person.users.Count > 0)
            {
                User user = null;

                foreach (User u in person.users)
                {
                    if (u.prime && u.userType.Equals("OPUS"))
                    {
                        user = u;
                    }
                }

                return user.GetType().GetProperty(tokens[1])?.GetValue(user)?.ToString();
            }

            return null;
        }
    }
}
