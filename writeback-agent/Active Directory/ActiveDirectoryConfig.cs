﻿using System.Collections.Generic;

namespace Active_Directory
{
    public class ActiveDirectoryConfig
    {
        public Dictionary<string, string> map { get; set; }
        public string attributeCpr { get; set; }
        public string attributeEmployeeId { get; set; }
        public string adServer { get; set; }
        public string userOU { get; set; }
        public bool allowEnablingWithoutEmployeeIdMatch { get; set; }
        public List<string> ActiveDirectoryWritebackExcludeOUs { get; set; }
    }
}
