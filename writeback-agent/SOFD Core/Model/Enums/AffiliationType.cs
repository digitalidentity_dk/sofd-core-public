﻿namespace SOFD_Core.Model.Enums
{
    public enum AffiliationType
    {
        EMPLOYEE, EXTERNAL
    }
}
