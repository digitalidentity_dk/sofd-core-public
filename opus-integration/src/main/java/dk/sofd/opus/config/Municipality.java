package dk.sofd.opus.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Municipality {
	private String name;
	
	// S3
	private String bucket;
	
	// SOFD
	private String apiKey;
	private String url;
	
	// Settings
	private short medudvalg;
	private short sr;
	private short tr;
	private short trSuppleant;
	
	private String managerOUForLevel1;
	private String managerOUForLevel2;
	private String managerOUForLevel3;
	private String managerOUForLevel4;
	
	private boolean filterEfterindtaegt;
}
