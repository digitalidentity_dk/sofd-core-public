package dk.sofd.opus.service.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

import dk.sofd.opus.service.model.enums.AffiliationFunction;
import dk.sofd.opus.service.model.enums.AffiliationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@EqualsAndHashCode(exclude = { "uuid", "orgUnit", "managerFor" })
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
public class Affiliation {
    private String uuid;
    private String personUuid;
    private String master;
    private String masterId;
    private String startDate;
    private String stopDate;
    private boolean deleted;
    private String orgUnitUuid;
    private String employeeId;
    private String employmentTerms;
    private String employmentTermsText;
    private String payGrade;
    private Double workingHoursDenominator;
    private Double workingHoursNumerator;
    private AffiliationType affiliationType;
    private String positionId;
    private String positionName;
    private String positionTypeId;
    private String positionTypeName;
    private Set<AffiliationFunction> functions;
    private Set<String> managerForUuids;
    private Map<String, String> localExtensions;
    
    // OLD - only used by the old Spring Data REST API, and can be removed when it is gone
    private Set<String> managerFor; // input-only (we will format it before sending)
    private String orgUnit; // input-only (we will format it before sending)
    
    // also OLD code... to make the SDR output work with new string-based dates
    public void fixDates() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

    	if (startDate != null && startDate.length() != 10) {
        	try {
        		Date date = sdf.parse(startDate);
        		startDate = sdf2.format(date);
        	}
        	catch (Exception ex) {
        		log.error("Bad timestamp: " + startDate, ex);
        	}    		
    	}
    	
    	if (stopDate != null && stopDate.length() != 10) {
        	try {
        		Date date = sdf.parse(stopDate);
        		stopDate = sdf2.format(date);
        	}
        	catch (Exception ex) {
        		log.error("Bad timestamp: " + stopDate, ex);
        	}    		    		
    	}
    }
}
