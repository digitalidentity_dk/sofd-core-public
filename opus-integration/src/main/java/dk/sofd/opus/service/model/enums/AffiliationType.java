package dk.sofd.opus.service.model.enums;

public enum AffiliationType
{
    EMPLOYEE, EXTERNAL
}
