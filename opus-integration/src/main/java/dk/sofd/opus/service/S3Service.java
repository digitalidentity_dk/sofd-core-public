package dk.sofd.opus.service;

import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

import lombok.SneakyThrows;

@Service
public class S3Service {
    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    @SneakyThrows
    public String getNewestFilename(String bucketName, String filePrefix) {
    	
    	// try the opus subfolder first - if it is empty, try the root
        Resource[] xmlFiles = resourcePatternResolver.getResources("s3://" + bucketName + "/opus/" + filePrefix + "*");
        Resource newestResource = null;
        for (Resource resource : xmlFiles) {
            if (newestResource == null || resource.getFilename().compareTo(newestResource.getFilename()) > 0) {
                newestResource = resource;
            }
        }
        
        if (newestResource != null) {
        	return newestResource.getFilename();
        }
        
        // fallback to root folder
        xmlFiles = resourcePatternResolver.getResources("s3://" + bucketName + "/" + filePrefix + "*");
        newestResource = null;
        for (Resource resource : xmlFiles) {
            if (newestResource == null || resource.getFilename().compareTo(newestResource.getFilename()) > 0) {
                newestResource = resource;
            }
        }

        return newestResource == null ? null : newestResource.getFilename();
    }

    @SneakyThrows
    public InputStreamReader readFile(String bucketName, String fileName) {
		Resource resource = resourcePatternResolver.getResource("s3://" + bucketName + "/" + fileName);

		return new InputStreamReader(resource.getInputStream());
    }
}